<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicantDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'full_name',
        'email',
        'mobile_no',
        'cnic',
        'date_of_birth',
        'emergency_no',
        'address',
        'city',
        
    ];
}
