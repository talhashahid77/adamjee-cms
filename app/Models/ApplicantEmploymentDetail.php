<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicantEmploymentDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'applicant_id',
        'current_employer_name',
        'designation',
        'functional_area',
        'start_date',
        'end_date',
        'in_progress',
        'gross_salary',
        'benefits_details',
        'expected_salary',

    ];
 
}
