<?php

namespace App\Models\Admin\General;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomPost extends Model
{
    use HasFactory;

    protected $table = "custom_fields";
}
