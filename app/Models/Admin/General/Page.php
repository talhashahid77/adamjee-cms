<?php

namespace App\Models\Admin\General;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $table = "pages";

    public function url(){
        return $this->hasOne(Urls::class);
    }

    public function customPost(){
        return $this->belongsTo(CustomPost::class);
    }
}
