<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;
    protected $fillable = [
        'plan_type',
        'plan_Date',
        'premium_mode',
        'amount',
        'name',
        'date_of_birth',
        'gender',
        'mobile_no',
        'city',
        'plan_sub_type',
        'plan_name',
        
       ];
}
