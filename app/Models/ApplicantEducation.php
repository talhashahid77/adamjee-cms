<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicantEducation extends Model
{
    use HasFactory;
    protected $fillable = [
        'applicant_id',
        'current_qualification',
        'institution',
        'additional_certification',
        'start_date',
        'end_date',
        'in_progress',
        
       ];
    
}
