<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FundPricesLog extends Model
{
    use HasFactory;

    protected $table = "fund_prices_log";
}
