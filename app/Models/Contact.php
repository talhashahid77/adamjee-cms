<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
      'name',
      'policy_number',
      'email',
      'cnic',
      'contact_number',
      'city',
      'category',
      'sub_category',
      'summary',
      'file',
      'form_type',
      'claim_type'
      
     ];
}
