<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // die('end');
        return [
            'name' => 'required',
            'email' => 'required|email',
            'policy_number' => 'required',
            'cnic' => 'required',
            'contact_number' => 'required',
            'city' => 'required',
            'category' => 'required',
            'sub_category' => 'required',
            'summary' => 'required',
            // 'file' => 'required|mimes:pdf,doc,docx,jpg',
            
        ];
    }

    public function messages()
    {
        return [
            // 'name.required' => 'name feild is required',
            
        ];
    }
}
