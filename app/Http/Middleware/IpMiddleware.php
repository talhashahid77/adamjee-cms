<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IpMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        $ip = @$_SERVER["HTTP_CF_CONNECTING_IP"];
        if($ip == NULL){
            $ip = request()->ip();
        }
        // if ($_SERVER["HTTP_CF_CONNECTING_IP"] != "103.197.46.230" || $_SERVER["HTTP_REFERER"] != "103.197.46.230") {
        if($ip != "103.197.46.230"){
            return abort(404);
        }
        else {
            return $next($request);
        }
       
    }
}
