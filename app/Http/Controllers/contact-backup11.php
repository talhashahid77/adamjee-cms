public function contact(ContactRequest $request){
          
    try {
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->policy_number = $request->policy_number;
        $contact->email = $request->email;
        $contact->cnic = $request->cnic;
        $contact->contact_number = $request->contact_number;
        $contact->city = $request->city;
        $contact->category = $request->category;
        $contact->sub_category = $request->sub_category;
        $contact->summary = $request->summary;
    
        $files = [];
        if (!empty($request->file)) {

            foreach ($request->file as $key => $fileName) {

                $file = $key . '-' . time().'.'.$fileName->extension();  
                $fileName->move(public_path('uploads/contact'), $file);
                $files[] = $file;
            }
                $contact->file = implode(',', $files);

        }else{
            return back()->with('error', 'Please Select the File!');
        }
        
        $contact->save();
        $data = $contact->toArray();

        Mail::send('email.contact-form',$data,
        function($message) use ($files){
            $message->to('talha.shahid@convexinteractive.com', 'Talha Shahid')
            ->subject('Your Website Contact Form');

            foreach($files as $item){
                $message->attach(public_path('uploads/contact/'.$item));
            }
                
        });
        
        return back()->with('success', 'Thanks for contacting me, I will get back to you soon!');
    } catch (Exception $e) {
        return back()->with('error', 'Something Went Wrong!');
    }
