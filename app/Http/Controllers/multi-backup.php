<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Admin\General\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Hsfo;
use App\Models\Lsfo;
use App\Models\Hsd;
use App\Models\Lpg;
use App\Models\Lsfoi;
use App\Models\Rlng;
use App\Models\Eo;
use App\Models\OilPrice;
use App\Models\InNews;
use App\Models\NewsEvent;
use App\Models\Lubricant;
use Illuminate\Support\Facades\Session;

class MultiController extends Controller
{
    public function enShow($x,Request $request){
        try {
            $device_type = $this->deviceType();
            $url_type = 'en';
            Session::put('url', 'en');
            $url = explode('/', $x);
            $url = implode('/', $url);
            // echo $url;
            $id = DB::table('url')->select('page_id')->where('path','=','/'.$url)->where('language','=','en')->get();
            // Page id retrieve..
            $page_id = $id[0]->page_id;
    
            $pagecon = DB::table('pages')->select('*')->where('id','=',$page_id)->where('status','=',1)->where('visible','=','Y')->get();
            $pagesdata = $pagecon;

            if(count($pagesdata) > 0){

                // Post data retrieve
                $posts = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $postsdata = $posts;

                // $postsDataDesc = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('id', 'DESC')->get();
                $postsDataDesc = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $postsdesc = $postsDataDesc;

                $menus = DB::table('menu_item')->select('*')->where('menu_id','=',11)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $menu_items = $menus;
                
                // $menus_parents = DB::table('menu_item')->select('*')->where('menu_id','=',12)->where('status','=',1)->orderBy('menu_parent_item_id', 'ASC')->get();
                $menus_parents = DB::table('menu_item')->select('*')->where('menu_id','=',12)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $menus_parents = $menus_parents;

                // Fuel Prices
                $fuelPrice = Eo::orderBy('id', 'DESC')->limit(1)->get(); 
                $oilprice = OilPrice::orderBy('id', 'DESC')->where('Status','1')->limit(1)->get();
                $hsfo = Hsfo::orderBy('id', 'DESC')->where('Status','1')->limit(4)->get();
                $lsfo = Lsfo::orderBy('id', 'DESC')->where('Status','1')->limit(3)->get();
                $lsfoi = Lsfoi::orderBy('id', 'DESC')->where('Status','1')->limit(3)->get();
                $hsd = Hsd::orderBy('id', 'DESC')->where('Status','1')->limit(5)->get();
                $rlng = Rlng::orderBy('id', 'DESC')->where('Status','1')->limit(1)->get();
                $lpg = Lpg::orderBy('id', 'DESC')->where('Status','1')->limit(1)->get();
				
				$lubricant1 = Lubricant::orderBy('id', 'DESC')->where('Status','1')->whereIn('CatID', array(1, 2, 3))->get();  
                $lubricant2 = Lubricant::orderBy('id', 'DESC')->where('Status','1')->whereIn('CatID', array(4))->get();
                
                // echo 'Testing'.$request->input('month');
                $year  = $request->input('year') ? $request->input('year'): "";
                $month  = $request->input('month') ? $request->input('month'): "";
                // echo "Testing". $year;
                //talha
                $search  = $request->input('search') ? $request->input('search'): "";
                $NewsEvent = NewsEvent::orderBy('start_date', 'DESC')->where('msg_type','N')->limit(10)->WhereRaw('name like "%'.$search.'%"' )
                                            ->WhereRaw('Year(start_date) like "%'.$year.'%"')
                                            ->WhereRaw('Month(start_date) like "%'.$month.'%"')->where('status','1')->get();

                // $NewsEvent = NewsEvent::orderBy('id', 'DESC')->where('msg_type','N')->limit(10)->orWhere(function ($query) {$query->where('name', 'like', '%'.strtolower($search).'%')->where('name', 'like', '%'.strtoupper($search).'%');})
                //                             ->WhereRaw('Year(insertion_date) like "%'.$year.'%"')
                //                             ->WhereRaw('Month(insertion_date) like "%'.$month.'%"')->where('status','1')->get();
                //yaseen
                $PressRelease = NewsEvent::orderBy('start_date', 'DESC')->where('msg_type','A')->where('status','1')->limit(12)->WhereRaw('name like "%'.$search.'%"' )
                                            ->WhereRaw('Year(start_date) like "%'.$year.'%"')
                                            ->WhereRaw('Month(start_date) like "%'.$month.'%"')->get();

                $NewsEvents = NewsEvent::orderBy('start_date', 'DESC')->where('Status','1')->where('msg_type','N')->paginate(10); 
                $NewsEventFilter = NewsEvent::orderBy('start_date', 'DESC')->where('status','1')->where('id',$request->id)->get(); 
                $NewsEventReg = NewsEvent::orderBy('start_date', 'DESC')->where('status','1')->where('msg_type','A')->paginate(10); 
                $InNewsArcheive = InNews::orderBy('insertion_date', 'DESC')->where('status','1')->paginate(10); 
                
            
                $InNews = InNews::orderBy('insertion_date', 'DESC')->where('Status','1')->limit(20)->WhereRaw('name like "%'.$search.'%"' )
                                            ->WhereRaw('Year(insertion_date) like "%'.$year.'%"')
                                            ->WhereRaw('Month(insertion_date) like "%'.$month.'%"')->get();

                $InNewss = InNews::orderBy('insertion_date', 'DESC')->where('Status','1')->limit(10)->WhereRaw('name like "%'.$search.'%"' )
                ->WhereRaw('Year(insertion_date) like "%'.$year.'%"')
                ->WhereRaw('Month(insertion_date) like "%'.$month.'%"')->get();
                $monthsArr = [
                    "January" => "1",
                    "February" => "2",
                    "March" => "3",
                    "April" => "4",
                    "May" => "5",
                    "June" => "6",
                    "July" => "7",
                    "August" => "8",
                    "September" => "9",
                    "October" => "10",
                    "November" => "11",
                    "December" => "12",
                    
                ];
                $InNew = array();
                foreach ($InNews as $key => $element) {
                
                    $InNew[$element->name][$key][] = $element->url;
                    $InNew[$element->name][$key][] = $element->thumb;
                    $InNew[$element->name][$key][] = $element->insertion_date;
                }

                // View retrive..
                $view = $pagesdata[0]->view_en;

                // Inner Pages
                $InPages = DB::table('pages')->select('*')->where('parent_id','=',$page_id)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $innerPages = $InPages;

                // Inner Pages
                $InPagesDesc = DB::table('pages')->select('*')->where('parent_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $innerPagesDesc = $InPagesDesc;

                // Inner Pages Detail
                $InPagesDetail = DB::table('pages')->select('*')->where('id','=',$request->id)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $innerPagesDetails = $InPagesDetail;

                // Brands Detail
                $brand = DB::table('vehicle_brand')->select('*')->where('status', '=', '1')->orderBy('id', 'ASC')->get();
                $brands = $brand;

                // Model Detail
                $modelDetail = DB::table('vehicle_model')->select('*')->where('status', '=', '1')->orderBy('id', 'ASC')->get();
                $modelDetails = $modelDetail;

                // Vehicle Detail
                $vehicle = DB::table('vehicles')->select('*')->where('status', '=', '1')->orderBy('id', 'ASC')->get();
                $vehicles = $vehicle;

                $BodAndMangment =  Page::all();



                return view(substr($view,1),compact('pagesdata','device_type','hsfo','lsfo','search','NewsEvents','InNewsArcheive','lsfoi','InNewss','hsd','vehicles','NewsEvent','PressRelease','NewsEventFilter','NewsEventReg','modelDetails','brands','rlng','lpg','postsdesc','postsdata','url','url_type','menu_items','menus_parents','fuelPrice','oilprice','innerPages','innerPagesDesc','innerPagesDetails','InNew', 'year', 'month', 'monthsArr','lubricant1','lubricant2','BodAndMangment'));
            } else {
                // return abort(404);
            }
        } catch (\Throwable $th) {
            abort(404);
        }
    }

    public function urShow($x,Request $request){
        try {
            $device_type = $this->deviceType();
            $url_type = 'ur';
            Session::put('url', 'ur');
            $url = explode('/', $x);
            $url = implode('/', $url);
            $id = DB::table('url')->select('page_id')->where('path','=','/'.$url)->where('language','=','ur')->get();
            // Page id retrieve..
            $page_id = $id[0]->page_id;
    
            $pagecon = DB::table('pages')->select('*')->where('id','=',$page_id)->where('status','=',1)->where('visible','=','Y')->get();
            $pagesdata = $pagecon;
           
            if(count($pagesdata) > 0){
                // Post data retrieve
                $posts = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $postsdata = $posts;

                // $postsDataDesc = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('id', 'DESC')->get();
                $postsDataDesc = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $postsdesc = $postsDataDesc;
               
                $menus = DB::table('menu_item')->select('*')->where('menu_id','=',11)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $menu_items = $menus;
                
                $menus_parents = DB::table('menu_item')->select('*')->where('menu_id','=',12)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $menus_parents = $menus_parents;

                // Fuel Prices
                $fuelPrice = Eo::orderBy('id', 'DESC')->limit(1)->get(); 
                $oilprice = OilPrice::orderBy('id', 'DESC')->where('Status','1')->limit(1)->get();
                $hsfo = Hsfo::orderBy('id', 'DESC')->where('Status','1')->limit(4)->get();
                $lsfo = Lsfo::orderBy('id', 'DESC')->where('Status','1')->limit(3)->get();
                $lsfoi = Lsfoi::orderBy('id', 'DESC')->where('Status','1')->limit(3)->get();
                $hsd = Hsd::orderBy('id', 'DESC')->where('Status','1')->limit(5)->get();
                $rlng = Rlng::orderBy('id', 'DESC')->where('Status','1')->limit(1)->get();
                $lpg = Lpg::orderBy('id', 'DESC')->where('Status','1')->limit(1)->get();
                
				$lubricant1 = Lubricant::orderBy('id', 'DESC')->where('Status','1')->whereIn('CatID', array(1, 2, 3))->get();  
                $lubricant2 = Lubricant::orderBy('id', 'DESC')->where('Status','1')->whereIn('CatID', array(4))->get();
                $NewsEvent = NewsEvent::orderBy('id', 'DESC')->where('Status','1')->paginate(10); 
                // $NewsEvent = NewsEvent::orderBy('id', 'DESC')->where('msg_type','N')->where('Status','1')->limit(10)->Where('name', 'like', '%'.$search.'%' )
                // ->WhereRaw('Year(insertion_date) like "%'.$year.'%"')
                // ->WhereRaw('Month(insertion_date) like "%'.$month.'%"')->get();
                // $PressRelease = NewsEvent::orderBy('id', 'DESC')->where('msg_type','A')->where('Status','1')->limit(10)->Where('name', 'like', '%'.$search.'%' )
                // ->WhereRaw('Year(insertion_date) like "%'.$year.'%"')
                // ->WhereRaw('Month(insertion_date) like "%'.$month.'%"')->get();
                $year  = $request->input('year') ? $request->input('year'): "";
                $month  = $request->input('month') ? $request->input('month'): "";
                $search  = $request->input('search') ? $request->input('search'): "";
                $NewsEventFilter = NewsEvent::orderBy('id', 'DESC')->where('status','1')->where('id',$request->id)->get(); 
                $NewsEventReg = NewsEvent::orderBy('id', 'DESC')->where('status','1')->where('msg_type','A')->paginate(10);
                $NewsEvents = NewsEvent::orderBy('id', 'DESC')->where('Status','1')->where('msg_type','N')->paginate(10);  
                
                $InNews = InNews::orderBy('id', 'DESC')->where('Status','1')->Where('name','like','%'.$search.'%')
                ->WhereRaw('Year(insertion_date) like "%'.$year.'%"')
                ->WhereRaw('Month(insertion_date) like "%'.$month.'%"')->get();
                
                $monthsArr = [
                    "January" => "01",
                    "February" => "02",
                    "March" => "03",
                    "April" => "04",
                    "May" => "05",
                    "June" => "06",
                    "July" => "07",
                    "August" => "08",
                    "September" => "09",
                    "October" => "10",
                    "November" => "11",
                    "December" => "12",
                    
                ];
                $InNew = array();
                foreach ($InNews as $key => $element) {
                
                    $InNew[$element->name][$key][] = $element->url;
                    $InNew[$element->name][$key][] = $element->thumb;
                    $InNew[$element->name][$key][] = $element->insertion_date;
                }
                // View retrive..
                
                if($pagesdata[0]->view_ur == ""){
                    $view = $pagesdata[0]->view_en;
                    $url_type = 'en';
                    Session::put('url', 'en');
                } else {
                    $view = $pagesdata[0]->view_ur;
                }

                // Inner Pages
                $InPages = DB::table('pages')->select('*')->where('parent_id','=',$page_id)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $innerPages = $InPages;

                // Inner Pages
                $InPagesDesc = DB::table('pages')->select('*')->where('parent_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $innerPagesDesc = $InPagesDesc;

                // Inner Pages Detail
                $InPagesDetail = DB::table('pages')->select('*')->where('id','=',$request->id)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $innerPagesDetails = $InPagesDetail;

                // Brands Detail
                $brand = DB::table('vehicle_brand')->select('*')->where('status', '=', '1')->orderBy('id', 'ASC')->get();
                $brands = $brand;

                // Model Detail
                $modelDetail = DB::table('vehicle_model')->select('*')->where('status', '=', '1')->orderBy('id', 'ASC')->get();
                $modelDetails = $modelDetail;

                // Vehicle Detail
                $vehicle = DB::table('vehicles')->select('*')->where('status', '=', '1')->orderBy('id', 'ASC')->get();
                $vehicles = $vehicle;

           
                return view(substr($view,1),compact('pagesdata','device_type','hsfo','lsfo','lsfoi','hsd','vehicles','NewsEvents','NewsEvent','NewsEventFilter','modelDetails','brands','rlng','lpg','NewsEventReg','postsdesc','postsdata','url','url_type','menu_items','menus_parents','fuelPrice','oilprice','innerPages','innerPagesDesc','innerPagesDetails','InNew', 'year', 'month', 'monthsArr','lubricant1','lubricant2'));
            } else {
                return abort(404);
            }
        } catch (\Throwable $th) {
            abort(404);
        }
    }

    public function showLubricant(){
        return 0;
    }
}
