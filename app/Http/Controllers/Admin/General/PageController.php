<?php

namespace App\Http\Controllers\Admin\General;

use App\Http\Controllers\Controller;
use App\Models\Admin\General\Page;
use App\Models\Admin\General\Urls;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DataTables;

class PageController extends Controller
{

    public function index(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            if ($request->ajax()) {
                
                if($request->id){
                    $mId = array();
                    $mCnt = $request->id;
                    $k = 0;
                    while (1){
                        $k += 1;
                        $mCh2 = DB::table('pages')
                        ->select('id')
                        ->where('parent_id', '=', $mCnt)
                        ->get();
                        $mPrt = $mCh2;
                        if(count($mPrt) > 0){
                            for ($i=0; $i < count($mPrt); $i++) { 
                                array_push($mId,$mPrt[$i]->id);
                            }
                        }
                        break;
                    }
                    $reqId = $request->id;
                    $data =  Page::with('url')->latest()->whereIn('id', $mId)->where("status", "=", 1)->get();
                    // $data =  Page::join("url", "url.page_id", "=", "pages.id")->whereIn('pages.id', $mId)->where("status", "=", 1)->get();
                    return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="page?id='.$row->id.'" class="edit btn btn-blue btn-sm">Pages</a><a href="article-list/'.$row->id.'" class="edit btn btn-warning btn-sm">Posts</a><a href="page/'.$row->id.'/edit?id='.$row->parent_id.'" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
                            return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                    
                } else {
                    
                    $data =  Page::with('url')->latest()->where("parent_id", "=", 0)->where("status", "=", 1)->get();
                    return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="page?id='.$row->id.'" class="edit btn btn-blue btn-sm">Pages</a><a href="article-list/'.$row->id.'" class="edit btn btn-warning btn-sm">Posts</a><a href="page/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
                            return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                }    
            }
        
            return view('admin.general.page.index');
        } else {
            
            return view('admin.auth.login');
        }
    }

    public function create()
    {
        $id = Auth::id();
        if($id)
        {
            $views_en = DB::table('view')->orderBy('title','ASC')->get();
            $parents = DB::table('pages')->get();
            return view('admin.general.page.create',compact('views_en','parents'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != "" && $request->view_en != "" && $request->slug != "" && $request->sorting != ""){
                    $count = DB::table('pages')->where('slug','=','/'.$request->slug)->where('view_en','=',$request->view_en)->where("status", "=", 1)->count();
                    if($count == 0){
                        $Page = new Page;
                        $Page->title_en = $request->title_en;
                        $Page->sub_title_en = $request->sub_title_en;
                        $Page->description_en = $request->descen;
                        $Page->short_desc_en = $request->short_descen;
                        $Page->meta_title_en = $request->meta_title_en;
                        $Page->meta_desc_en = $request->meta_desc_en;
                        $Page->meta_keywords_en = $request->meta_keywords_en;
                        $Page->view_en = $request->view_en;
                        $Page->position_en = $request->position_en;
                        $Page->grid_class_en = $request->grid_class_en;
                        $Page->title_ur = $request->title_ur;
                        $Page->sub_title_ur = $request->sub_title_ur;
                        $Page->description_ur = $request->descur;
                        $Page->short_desc_ur = $request->short_descur;
                        $Page->meta_title_ur = $request->meta_title_ur;
                        $Page->meta_desc_ur = $request->meta_desc_ur;
                        $Page->meta_keywords_ur = $request->meta_keywords_ur;
                        $Page->view_ur = $request->view_ur;
                        $Page->position_ur = $request->position_ur;
                        $Page->grid_class_ur = $request->grid_class_ur;
                        $Page->parent_id = $request->parent;
                        $Page->author = $request->author;
                        $Page->update_by = $id;
                        $Page->visible = $request->status;
                        $Page->show_in_menu = $request->show_in_menu;
                        $Page->sorting = $request->sorting;
                        $Page->slug = "/".$request->slug;
                        $mCnt = DB::table('pages')->where('parent_id','=',$request->parent)->orWhere('id','=',$request->parent)->where("status", "=", 1)->count();
                        if($mCnt > 0){
                            $mParent = DB::table('pages')->select('mlevel')->where('parent_id','=',$request->parent)->orWhere('id','=',$request->parent)->where("status", "=", 1)->get();
                            $mParLevel = $mParent[0]->mlevel;
                            $Page->mlevel = $mParLevel + 1;
                        } else {
                            $mParLevel = 0;
                            $Page->mlevel = $mParLevel;
                        }
                        if($request->image_ur != ""){
                            if( strpos($request->image_ur, ',') !== false ) {
                                $Page->image_ur = $request->image_ur; 
                            }else{
                                $Page->image_ur = '["'.$request->image_ur.'"]';
                            }
                        } else {
                            $Page->image_ur = "[]";
                        }
                        if($request->image_en != ""){
                            if( strpos($request->image_en, ',') !== false ) {
                                $Page->image_en = $request->image_en; 
                            }else{
                                $Page->image_en = '["'.$request->image_en.'"]';
                            }
                        } else {
                            $Page->image_en = "[]";
                        }
                        if($request->image_mobile_en != ""){
                            if( strpos($request->image_mobile_en, ',') !== false ) {
                                $Page->image_mobile_en = $request->image_mobile_en; 
                            }else{
                                $Page->image_mobile_en = '["'.$request->image_mobile_en.'"]';
                            }
                        } else {
                            $Page->image_mobile_en = "[]";
                        }
                        if($request->image_mobile_ur != ""){
                            if( strpos($request->image_mobile_ur, ',') !== false ) {
                                $Page->image_mobile_ur = $request->image_mobile_ur; 
                            }else{
                                $Page->image_mobile_ur = '["'.$request->image_mobile_ur.'"]';
                            }
                        } else {
                            $Page->image_mobile_ur = "[]";
                        }
                        if($request->file_en != ""){
                            if( strpos($request->file_en, ',') !== false ) {
                                $Page->file_en = $request->file_en; 
                            }else{
                                $Page->file_en = '["'.$request->file_en.'"]';
                            }
                        } else {
                            $Page->file_en = "[]";
                        }
                        if($request->file_ur != ""){
                            if( strpos($request->file_ur, ',') !== false ) {
                                $Page->file_ur = $request->file_ur; 
                            }else{
                                $Page->file_ur = '["'.$request->file_ur.'"]';
                            }
                        } else {
                            $Page->file_ur = "[]";
                        }
                        // Page data saving
                        $Page->save();

                        //echo $request->slug; die;
    
                        $mid = DB::table('pages')->select('id')->where('slug', '=', "/".$request->slug)->where('view_en','=',$request->view_en)->where("status", "=", 1)->get();
                        $mPid = $mid[0]->id;
                        $mPrnF = "";
                        $mCnt = $request->parent;
                        if($mCnt == 0){
                            $Url_en = new Urls;
                            $Url_en->page_id = $mPid;
                            $Url_en->language = 'en';
                            $Url_en->path = "/".$request->slug;
                            $Url_en->save();

                            $Url_ur = new Urls;
                            $Url_ur->page_id = $mPid;
                            $Url_ur->language = 'ur';
                            $Url_ur->path = "/".$request->slug;
                            $Url_ur->save();
                        } else {
                            $mC3 = 0;
                            while (1){
                                $mCh2 = DB::table('pages')->select('slug')->where('id', '=', $mCnt)->where("status", "=", 1)->get();
                                $mPrt = $mCh2[0]->slug;
                                $mPrs = DB::table('pages')->select('parent_id')->where('slug', '=', $mPrt)->where("status", "=", 1)->get();
                                $mCh1 = $mPrs[0]->parent_id;
                                $Temp = $mPrnF;
                                $mPrnF = "";
                                $mPrnF = $mPrt . $Temp;
                                $mCnt = $mCh1;
                                if($mCnt == 0){
                                    break;
                                } else {
                                    continue;
                                }
                            }
                            //echo $mPrnF; die;
                            $Url_en = new Urls;
                            $Url_en->page_id = $mPid;
                            $Url_en->language = 'en';
                            $Url_en->path = $mPrnF."/".$request->slug;
                            $Url_en->save();

                            $Url_ur = new Urls;
                            $Url_ur->page_id = $mPid;
                            $Url_ur->language = 'ur';
                            $Url_ur->path = $mPrnF."/".$request->slug;
                            $Url_ur->save();
                        }
                        return response()->json('success');
                    } else{
                        return response()->json('data found');
                    }
                } else {
                    return response()->json('error');
                    // return "error";
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function show(Page $Page)
    {
        //
    }

    public function edit(Page $page)
    {
        $id = Auth::id();
        if($id)
        {
            $views_en = DB::table('view')->get();
            $parents = DB::table('pages')->where('mlevel','<',$page->mlevel)->where("status", "=", 1)->get();
            $custom_posts = DB::table('custom_fields')->select("*")->where('parent_type','=','page')->where("status", "=", 1)->where("parent_id", "=", $page->id)->get();
            return view('admin.general.page.edit', compact('page','views_en','parents','custom_posts'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function update(Request $request, Page $Page)
    {
        
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != "" && $request->view_en != "" && $request->slug != "" && $request->sorting != ""){
                    $mPage = DB::table('pages')->select('*')->where('id', '=', $Page->id)->where("status", "=", 1)->get();
                    $mSlug = $mPage[0]->slug;
                    if($mSlug == '/'.$request->slug){
                        $Page = Page::firstOrNew(['id' => $Page->id]);
                        $Page->title_en = $request->title_en;
                        $Page->sub_title_en = $request->sub_title_en;
                        $Page->description_en = $request->descen;
                        $Page->short_desc_en = $request->short_descen;
                        $Page->meta_title_en = $request->meta_title_en;
                        $Page->meta_desc_en = $request->meta_desc_en;
                        $Page->meta_keywords_en = $request->meta_keywords_en;
                        $Page->view_en = $request->view_en;
                        $Page->position_en = $request->position_en;
                        $Page->grid_class_en = $request->grid_class_en;
                        $Page->title_ur = $request->title_ur;
                        $Page->sub_title_ur = $request->sub_title_ur;
                        $Page->description_ur = $request->descur;
                        $Page->short_desc_ur = $request->short_descur;
                        $Page->meta_title_ur = $request->meta_title_ur;
                        $Page->meta_desc_ur = $request->meta_desc_ur;
                        $Page->meta_keywords_ur = $request->meta_keywords_ur;
                        $Page->view_ur = $request->view_ur;
                        $Page->position_ur = $request->position_ur;
                        $Page->grid_class_ur = $request->grid_class_ur;
                        $Page->parent_id = $request->parent;
                        $Page->author = $request->author;
                        $Page->update_by = $id;
                        $Page->visible = $request->status;
                        $Page->show_in_menu = $request->show_in_menu;
                        $Page->sorting = $request->sorting;
                        $Page->slug = "/".$request->slug;
                        if($request->imgur != ""){
                            $Page->image_ur =  explode(",",$request->imgur);
                        } else {
                            $Page->image_ur = [];
                        }
                        if($request->imgen != ""){
                            $Page->image_en =  explode(",",$request->imgen);
                        } else {
                            $Page->image_en = [];
                        }
                        if($request->imgmoben != ""){
                            $Page->image_mobile_en =  explode(",",$request->imgmoben);
                        } else {
                            $Page->image_mobile_en = [];
                        }
                        if($request->imgmobur != ""){
                            $Page->image_mobile_ur =  explode(",",$request->imgmobur);
                        } else {
                            $Page->image_mobile_ur = [];
                        }
                        if($request->fileen != ""){
                            $Page->file_en =  explode(",",$request->fileen);
                        } else {
                            $Page->file_en = [];
                        }
                        if($request->fileur != ""){
                            $Page->file_ur =  explode(",",$request->fileur);
                        } else {
                            $Page->file_ur = [];
                        }
                        // Page data saving
                        $Page->save();
    
                        $mid = DB::table('pages')->select('id')->where('slug', '=', "/".$request->slug)->where('id', '=', $Page->id)->where("status", "=", 1)->get();
                        $mPid = $mid[0]->id;
                        $mPrnF = "";
                        $mCnt = $request->parent;
                        if($mCnt == 0){
                            Urls::where('page_id', $mPid)->delete();
                            $Url_en = new Urls;
                            $Url_en->page_id = $mPid;
                            $Url_en->language = 'en';
                            $Url_en->path = "/".$request->slug;
                            $Url_en->save();

                            $Url_ur = new Urls;
                            $Url_ur->page_id = $mPid;
                            $Url_ur->language = 'ur';
                            $Url_ur->path = "/".$request->slug;
                            $Url_ur->save();
                        } else {
                            $mC3 = 0;
                            while (1){
                                    $mCh2 = DB::table('pages')->select('slug')->where('id', '=', $mCnt)->where("status", "=", 1)->get();
                                    $mPrt = $mCh2[0]->slug;
                                    $mPrs = DB::table('pages')->select('parent_id')->where('slug', '=', $mPrt)->where("status", "=", 1)->get();
                                    $mCh1 = $mPrs[0]->parent_id;
                                    $Temp = $mPrnF;
                                    $mPrnF = "";
                                    $mPrnF = $mPrt . $Temp;
                                    $mCnt = $mCh1;
                                    if($mCnt == 0){
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            Urls::where('page_id', $mPid)->delete();
                            $Url_en = new Urls;
                            $Url_en->page_id = $mPid;
                            $Url_en->language = 'en';
                            $Url_en->path = $mPrnF."/".$request->slug;
                            $Url_en->save();

                            $Url_ur = new Urls;
                            $Url_ur->page_id = $mPid;
                            $Url_ur->language = 'ur';
                            $Url_ur->path = $mPrnF."/".$request->slug;
                            $Url_ur->save();
                        }
                        return "success";
                    } else {
                        $count = DB::table('pages')->where('slug','=','/'.$request->slug)->where('view_en','=',$request->view_en)->where("status", "=", 1)->count();
                        if($count == 0){
                            $Page = Page::firstOrNew(['id' => $Page->id]);
                            $Page->title_en = $request->title_en;
                            $Page->sub_title_en = $request->sub_title_en;
                            $Page->description_en = $request->descen;
                            $Page->short_desc_en = $request->short_descen;
                            $Page->meta_title_en = $request->meta_title_en;
                            $Page->meta_desc_en = $request->meta_desc_en;
                            $Page->meta_keywords_en = $request->meta_keywords_en;
                            $Page->view_en = $request->view_en;
                            $Page->position_en = $request->position_en;
                            $Page->grid_class_en = $request->grid_class_en;
                            $Page->title_ur = $request->title_ur;
                            $Page->sub_title_ur = $request->sub_title_ur;
                            $Page->description_ur = $request->descur;
                            $Page->short_desc_ur = $request->short_descur;
                            $Page->meta_title_ur = $request->meta_title_ur;
                            $Page->meta_desc_ur = $request->meta_desc_ur;
                            $Page->meta_keywords_ur = $request->meta_keywords_ur;
                            $Page->view_ur = $request->view_ur;
                            $Page->position_ur = $request->position_ur;
                            $Page->grid_class_ur = $request->grid_class_ur;
                            $Page->parent_id = $request->parent;
                            $Page->author = $request->author;
                            $Page->update_by = $id;
                            $Page->visible = $request->status;
                            $Page->show_in_menu = $request->show_in_menu;
                            $Page->sorting = $request->sorting;
                            $Page->slug = "/".$request->slug;
                            if($request->imgur != ""){
                                $Page->image_ur =  explode(",",$request->imgur);
                            } else {
                                $Page->image_ur = [];
                            }
                            if($request->imgen != ""){
                                $Page->image_en =  explode(",",$request->imgen);
                            } else {
                                $Page->image_en = [];
                            }
                            if($request->imgmoben != ""){
                                $Page->image_mobile_en =  explode(",",$request->imgmoben);
                            } else {
                                $Page->image_mobile_en = [];
                            }
                            if($request->imgmobur != ""){
                                $Page->image_mobile_ur =  explode(",",$request->imgmobur);
                            } else {
                                $Page->image_mobile_ur = [];
                            }
                            if($request->fileen != ""){
                                $Page->file_en =  explode(",",$request->fileen);
                            } else {
                                $Page->file_en = [];
                            }
                            if($request->fileur != ""){
                                $Page->file_ur =  explode(",",$request->fileur);
                            } else {
                                $Page->file_ur = [];
                            }
                            // Page data saving
                            $Page->save();
        
                            $mid = DB::table('pages')->select('id')->where('slug', '=', "/".$request->slug)->where('id', '=', $Page->id)->where("status", "=", 1)->get();
                            $mPid = $mid[0]->id;
                            $mPrnF = "";
                            $mCnt = $request->parent;
                            if($mCnt == 0){
                                Urls::where('page_id', $mPid)->delete();
                                $Url_en = new Urls;
                                $Url_en->page_id = $mPid;
                                $Url_en->language = 'en';
                                $Url_en->path = "/".$request->slug;
                                $Url_en->save();
    
                                $Url_ur = new Urls;
                                $Url_ur->page_id = $mPid;
                                $Url_ur->language = 'ur';
                                $Url_ur->path = "/".$request->slug;
                                $Url_ur->save();
                            } else {
                                $mC3 = 0;
                                while (1){
                                        $mCh2 = DB::table('pages')->select('slug')->where('id', '=', $mCnt)->where("status", "=", 1)->get();
                                        $mPrt = $mCh2[0]->slug;
                                        $mPrs = DB::table('pages')->select('parent_id')->where('slug', '=', $mPrt)->where("status", "=", 1)->get();
                                        $mCh1 = $mPrs[0]->parent_id;
                                        $Temp = $mPrnF;
                                        $mPrnF = "";
                                        $mPrnF = $mPrt . $Temp;
                                        $mCnt = $mCh1;
                                        if($mCnt == 0){
                                            break;
                                        } else {
                                            continue;
                                        }
                                }
                                Urls::where('page_id', $mPid)->delete();
                                $Url_en = new Urls;
                                $Url_en->page_id = $mPid;
                                $Url_en->language = 'en';
                                $Url_en->path = $mPrnF."/".$request->slug;
                                $Url_en->save();
    
                                $Url_ur = new Urls;
                                $Url_ur->page_id = $mPid;
                                $Url_ur->language = 'ur';
                                $Url_ur->path = $mPrnF."/".$request->slug;
                                $Url_ur->save();
                            }
                            return response()->json('success');
                            // return $request->imgen;
                        } else{
                            return response()->json('Please use different slug.');
                            // return "data found";
                        }
                    }
                } else {
                     return response()->json('error');
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function destroy(Page $Page)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                $mPage = Page::firstOrNew(['id' => $Page->id]);
                $mPage->status = 0;
                $mPage->save();
                return response()->json('success');
            } catch (\Throwable $th) {
                
                return response()->json('error');
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function incCount(Request $request){
        $Page = Page::firstOrNew(['id' => $request->id]);
        $Page->sorting = $Page->sorting + 1;
        $Page->save();
        return response()->json('success');
    }
    
    public function descCount(Request $request){
        $Page = Page::firstOrNew(['id' => $request->id]);
        $Page->sorting = $Page->sorting - 1;
        $Page->save();
        return response()->json('success');
    }
}
