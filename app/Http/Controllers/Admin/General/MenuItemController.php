<?php

namespace App\Http\Controllers\Admin\General;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Admin\General\MenuItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use DataTables;

class MenuItemController extends Controller
{
    public function index(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            if ($request->ajax()) {
                $data =  MenuItem::latest()->where("menu_id", "=", $request->id)->where("status", "=", 1)->get();
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="menuitem/'.$row->id.'/edit?id='.$row->menu_id.'" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
                            return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
            return view('admin.general.menuitem.index');
        } else {
            return view('admin.auth.login');
        }
    }

    public function create(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            $parents = DB::table('pages')
            ->join('url', 'pages.id', '=', 'url.page_id')
            ->select('url.path','pages.title_en')->orderBy('title_en','ASC')->where('status','1')
            ->get();
            $menus = DB::table('menu_item')->where('menu_id','=',$request->id)->orderBy('title_en','ASC')->get();
            return view('admin.general.menuitem.create',compact('parents','menus'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != ""){
                    $menuitem = new MenuItem;
                    $menuitem->menu_id = $request->menu_id;
                    $menuitem->title_en = $request->title_en;
                    $menuitem->description_en = $request->descen;
                    $menuitem->link_type_en = $request->linktype_en;
                    $menuitem->title_ur = $request->title_ur;
                    $menuitem->description_ur = $request->descur;
                    $menuitem->menu_parent_item_id = $request->menu_parent_item_id;
                    if($request->linktxt_ur != NULL){
                        $menuitem->link_url_ur = $request->linktxt_ur;
                    }else {
                        $menuitem->link_url_ur = $request->linksdp_ur;
                    }
                    if($request->linktxt_en != NULL){
                        $menuitem->link_url_en = $request->linktxt_en;
                    }else {
                        $menuitem->link_url_en = $request->linksdp_en;
                    }
                    $menuitem->link_type_ur = $request->linktype_ur;
                    if($request->image_ur != ""){
                        if( strpos($request->image_ur, ',') !== false ) {
                            $menuitem->image_ur = $request->image_ur; 
                        }else{
                            $menuitem->image_ur = '["'.$request->image_ur.'"]';
                        }
                    } else {
                        $menuitem->image_ur = "[]";
                    }
                    if($request->image_en != ""){
                        if( strpos($request->image_en, ',') !== false ) {
                            $menuitem->image_en = $request->image_en; 
                        }else{
                            $menuitem->image_en = '["'.$request->image_en.'"]';
                        }
                    } else {
                        $menuitem->image_en = "[]";
                    }
                    $menuitem->save();
                    return response()->json('success');
                    //return "success";
                } else {
                    return response()->json('error');
                    // return "error";
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function show(MenuItem $menuitem)
    {
        //
    }

    public function edit(MenuItem $menuitem,Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            $parents = DB::table('pages')
            ->join('url', 'pages.id', '=', 'url.page_id')
            ->select('url.path','pages.title_en')->orderBy('title_en','ASC')->where('status','1')
            ->get();
            $menus = DB::table('menu_item')->where('menu_id','=',$request->id)->where('id','<>',$menuitem->id)->orderBy('title_en','ASC')->get();
            return view('admin.general.menuitem.edit',compact('menuitem','parents','menus'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function update(Request $request, MenuItem $menuitem)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != ""){
                    $menuitem = MenuItem::firstOrNew(['id' => $menuitem->id]);
                    $menuitem->title_en = $request->title_en;
                    $menuitem->description_en = $request->descen;
                    $menuitem->link_type_en = $request->linktype_en;
                    $menuitem->title_ur = $request->title_ur;
                    $menuitem->description_ur = $request->descur;
                    $menuitem->menu_parent_item_id = $request->menu_parent_item_id;
                    if($request->linktxt_ur != NULL){
                        $menuitem->link_url_ur = $request->linktxt_ur;
                    }else {
                        $menuitem->link_url_ur = $request->linksdp_ur;
                    }
                    if($request->linktxt_en != NULL){
                        $menuitem->link_url_en = $request->linktxt_en;
                    }else {
                        $menuitem->link_url_en = $request->linksdp_en;
                    }
                    $menuitem->link_type_ur = $request->linktype_ur;
                    if($request->imgur != ""){
                        $menuitem->image_ur =  explode(",",$request->imgur);
                    } else {
                        $menuitem->image_ur = [];
                    }
                    if($request->imgen != ""){
                        $menuitem->image_en =  explode(",",$request->imgen);
                    } else {
                        $menuitem->image_en = [];
                    }
                    $menuitem->save();
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function destroy(MenuItem $menuitem)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                $menuitem = MenuItem::firstOrNew(['id' => $menuitem->id]);
                $menuitem->status = 0;
                $menuitem->save();
                return "success";
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function incCount(Request $request){
        $MenuItem = MenuItem::firstOrNew(['id' => $request->id]);
        $MenuItem->sorting = $MenuItem->sorting + 1;
        $MenuItem->save();
        return "success";
    }
    
    public function descCount(Request $request){
        $MenuItem = MenuItem::firstOrNew(['id' => $request->id]);
        $MenuItem->sorting = $MenuItem->sorting - 1;
        $MenuItem->save();
        return "success";
    }
}
