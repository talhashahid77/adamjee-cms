<?php

namespace App\Http\Controllers\Admin\General;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Admin\General\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DataTables;

class ArticleController extends Controller
{

    public function index(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            if ($request->ajax()) {
                $data =  Article::latest()->where('page_id','=',$request->id)->where("status", "=", 1)->get();
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="../article/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
                            return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
            return view('admin.general.article.index');
        } else {
            return view('admin.auth.login');
        }
    }

    public function create()
    {
        $id = Auth::id();
        if($id)
        {
            
            $parents = DB::table('pages')
            ->join('url', 'pages.id', '=', 'url.page_id')
            ->select('url.path','pages.title_en')
            ->get();
            return view('admin.general.article.create',compact('parents'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != ""){
                    $mArticle = new Article;
                    $mArticle->page_id = $request->page_id;
                    $mArticle->title_en = $request->title_en;
                    $mArticle->description_en = $request->descen;
                    $mArticle->link_type_en = $request->linktype_en;
                    $mArticle->link_text_en = $request->link_text_en;
                    $mArticle->position_en = $request->position_en;
                    $mArticle->grid_class_en = $request->grid_class_en;
                    $mArticle->title_ur = $request->title_ur;
                    $mArticle->description_ur = $request->descur;
                    $mArticle->position_ur = $request->position_ur;
                    $mArticle->grid_class_ur = $request->grid_class_ur;
                    $mArticle->classes = $request->classes;
                    $mArticle->link_text_ur = $request->link_text_ur;
                    $mArticle->buy_link = $request->buy_link;
                    if($request->linktxt_ur != NULL){
                        $mArticle->link_ur = $request->linktxt_ur;
                    }else {
                        $mArticle->link_ur = $request->linksdp_ur;
                    }
                    if($request->linktxt_en != NULL){
                        $mArticle->link_en = $request->linktxt_en;
                    }else {
                        $mArticle->link_en = $request->linksdp_en;
                    }
                    $mArticle->link_type_ur = $request->linktype_ur;
                    if($request->doc_en != ""){
                        if( strpos($request->doc_en, ',') !== false ) {
                            $mArticle->image_en = $request->doc_en; 
                        }else{
                            $mArticle->image_en = '["'.$request->doc_en.'"]';  
                        }
                    } else {
                        $mArticle->image_en = "[]";
                    }
                    if($request->doc_ur != ""){
                        if( strpos($request->doc_ur, ',') !== false ) {
                            $mArticle->image_ur = $request->doc_ur; 
                        }else{
                            $mArticle->image_ur = '["'.$request->doc_ur.'"]';  
                        }
                    } else {
                        $mArticle->image_ur = "[]";
                    }
                    if($request->image_mobile_en != ""){
                        if( strpos($request->image_mobile_en, ',') !== false ) {
                            $mArticle->image_mobile_en = $request->image_mobile_en; 
                        }else{
                            $mArticle->image_mobile_en = '["'.$request->image_mobile_en.'"]';
                        }
                    } else {
                        $mArticle->image_mobile_en = "[]";
                    }
                    if($request->image_mobile_ur != ""){
                        if( strpos($request->image_mobile_ur, ',') !== false ) {
                            $mArticle->image_mobile_ur = $request->image_mobile_ur; 
                        }else{
                            $mArticle->image_mobile_ur = '["'.$request->image_mobile_ur.'"]';
                        }
                    } else {
                        $mArticle->image_mobile_ur = "[]";
                    }
                    if($request->file_en != ""){
                        if( strpos($request->file_en, ',') !== false ) {
                            $mArticle->file_en = $request->file_en; 
                        }else{
                            $mArticle->file_en = '["'.$request->file_en.'"]';
                        }
                    } else {
                        $mArticle->file_en = "[]";
                    }
                    if($request->file_ur != ""){
                        if( strpos($request->file_ur, ',') !== false ) {
                            $mArticle->file_ur = $request->file_ur; 
                        }else{
                            $mArticle->file_ur = '["'.$request->file_ur.'"]';
                        }
                    } else {
                        $mArticle->file_ur = "[]";
                    }
                    $mArticle->save();
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function show(Article $article)
    {
        //
    }

    public function edit(Article $article)
    {
        $id = Auth::id();
        if($id)
        {
            $parents = DB::table('pages')
            ->join('url', 'pages.id', '=', 'url.page_id')
            ->select('url.path','pages.title_en')->distinct()
            ->get();
            $custom_posts = DB::table('custom_fields')->select("*")->where("status", "=", 1)->where('parent_type','=','article')->where("parent_id", "=", $article->id)->get();
            return view('admin.general.article.edit',compact('article','parents','custom_posts'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function update(Request $request, Article $Article)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != ""){
                    $mArticle = Article::firstOrNew(['id' => $Article->id]);
                    $mArticle->page_id = $request->page_id;
                    $mArticle->title_en = $request->title_en;
                    $mArticle->description_en = $request->descen;
                    $mArticle->link_type_en = $request->linktype_en;
                    $mArticle->link_text_en = $request->link_text_en;
                    $mArticle->position_en = $request->position_en;
                    $mArticle->grid_class_en = $request->grid_class_en;
                    $mArticle->title_ur = $request->title_ur;
                    $mArticle->description_ur = $request->descur;
                    $mArticle->position_ur = $request->position_ur;
                    $mArticle->grid_class_ur = $request->grid_class_ur;
                    $mArticle->classes = $request->classes;
                    $mArticle->link_text_ur = $request->link_text_ur;
                    $mArticle->buy_link = $request->buy_link;
                    if($request->linktxt_ur != ""){
                        $mArticle->link_ur = $request->linktxt_ur;
                    }else {
                        $mArticle->link_ur = $request->linksdp_ur;
                    }
                    if($request->linktxt_en != NULL){
                        $mArticle->link_en = $request->linktxt_en;
                    }else {
                        $mArticle->link_en = $request->linksdp_en;
                    }
                    $mArticle->link_type_ur = $request->linktype_ur;
                    if($request->docen != ""){
                        $mArticle->image_en =  explode(",",$request->docen);
                    } else {
                        $mArticle->image_en = [];
                    }
                    if($request->docur != ""){
                        $mArticle->image_ur =  explode(",",$request->docur);
                    } else {
                        $mArticle->image_ur = [];
                    }
                    if($request->imgmoben != ""){
                        $mArticle->image_mobile_en =  explode(",",$request->imgmoben);
                    } else {
                        $mArticle->image_mobile_en = [];
                    }
                    if($request->imgmobur != ""){
                        $mArticle->image_mobile_ur =  explode(",",$request->imgmobur);
                    } else {
                        $mArticle->image_mobile_ur = [];
                    }
                    if($request->fileen != ""){
                        $mArticle->file_en =  explode(",",$request->fileen);
                    } else {
                        $mArticle->file_en = [];
                    }
                    if($request->fileur != ""){
                        $mArticle->file_ur =  explode(",",$request->fileur);
                    } else {
                        $mArticle->file_ur = [];
                    }
                    $mArticle->save();
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function destroy(Article $Article)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                $Article = Article::firstOrNew(['id' => $Article->id]);
                $Article->status = 0;
                $Article->save();
                return "success";
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }
    public function incCount(Request $request){
        $Article = Article::firstOrNew(['id' => $request->id]);
        $Article->sorting = $Article->sorting + 1;
        $Article->save();
        return "success";
    }
    
    public function descCount(Request $request){
        $Article = Article::firstOrNew(['id' => $request->id]);
        $Article->sorting = $Article->sorting - 1;
        $Article->save();
        return "success";
    }
}
