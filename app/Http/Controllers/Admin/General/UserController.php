<?php

namespace App\Http\Controllers\Admin\General;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Admin\General\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DataTables;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            if ($request->ajax()) {
                $data =  User::latest()->get();
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){

                            $btn = '<a href="user/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
        
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
            return view('admin.general.user.index');
        } else {
            return view('admin.auth.login');
        }
    }

    public function create()
    {
        $id = Auth::id();
        if($id)
        {
            return view('admin.general.user.create');
        } else {
            return view('admin.auth.login');
        }
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->name != "" && $request->email != "" && $request->pass != "" && $request->role != ""){
                    $User = new User;
                    $User->name = $request->name;
                    $User->email = $request->email;
                    $User->role = $request->role;
                    $User->password = Hash::make($request->pass);
                    $User->save();
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        $id = Auth::id();
        if($id)
        {
            return view('admin.general.user.edit', compact('user'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function update(Request $request, User $user)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->name != "" && $request->email != "" && $request->role != ""){
                    $User = User::firstOrNew(['id' => $user->id]);
                    $User->name = $request->name;
                    $User->email = $request->email;
                    $User->role = $request->role;
                    if($request->pass != "") {
                        $User->password = Hash::make($request->pass);
                    }
                    $User->save();
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function destroy(User $user)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                $mUser = User::firstOrNew(['id' => $user->id]);
                $mUser->delete();
                return "success";
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }
}
