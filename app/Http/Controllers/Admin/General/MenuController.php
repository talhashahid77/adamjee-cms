<?php

namespace App\Http\Controllers\Admin\General;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Admin\General\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use DataTables;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            if ($request->ajax()) {
                $data =  Menu::latest()->where("status", "=", 1)->get();
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="menuitem?id='.$row->id.'" class="edit btn btn-blue btn-sm">Menu Item</a><a href="menu/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
                            return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
            return view('admin.general.menu.index');
        } else {
            return view('admin.auth.login');
        }
    }

    public function create()
    {
        $id = Auth::id();
        if($id)
        {
            return view('admin.general.menu.create');
        } else {
            return view('admin.auth.login');
        }
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != ""){
                    $menu = Menu::create([
                        'title_en'=>$request->input('title_en'),
                        'title_ur'=>$request->input('title_ur')
                    ]);
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function show(Article $article)
    {
        //
    }

    public function edit(Menu $menu)
    {
        $id = Auth::id();
        if($id)
        {
            return view('admin.general.menu.edit',compact('menu'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function update(Request $request, Menu $menu)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title_en != ""){
                    $menu = Menu::firstOrNew(['id' => $menu->id]);
                    $menu->title_en = $request->title_en;
                    $menu->title_ur = $request->title_ur;
                    $menu->save();
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function destroy(Menu $menu)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                $menu = Menu::firstOrNew(['id' => $menu->id]);
                $menu->status = 0;
                $menu->save();
                return "success";
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }
}
