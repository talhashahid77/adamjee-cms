<?php

namespace App\Http\Controllers\Admin\General;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Admin\General\CustomPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use DataTables;

class CustomPostController extends Controller
{
    public function addCustomPost(Request $request){
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->name != ""){
                    $mCustomPost = new CustomPost;
                    $mCustomPost->parent_id = $request->id;
                    $mCustomPost->parent_type = $request->type;
                    $mCustomPost->name = $request->name;
                    $mCustomPost->description = $request->desc;
                    $mCustomPost->url = $request->url;
                    $mCustomPost->language = $request->languages;
                    if($request->image_custom != ""){
                        $mCustomPost->image =  json_encode(explode(",",$request->image_custom));
                    } else {
                        $mCustomPost->image = json_encode([]);
                    }
                    if($request->file_custom != ""){
                        $mCustomPost->file =  json_encode(explode(",",$request->file_custom));
                    } else {
                        $mCustomPost->file = json_encode([]);
                    }
                    $mCustomPost->save();
                    return "success";
                } else {
                    return "error";
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
        return "success";
    }

    public function listCustomPost(Request $request){
        $listCustomPost =  CustomPost::latest()->where("status", "=", 1)->where("parent_id", "=", $request->id)->get();
        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => $listCustomPost,
        ],200);
    }

    public function deleteCustomPost(Request $request){
        $id = Auth::id();
        if($id)
        {
            try {
                $customPost = CustomPost::firstOrNew(['id' => $request->id]);
                $customPost->status = 0;
                $customPost->save();
                return "success";
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function getCustomPost(Request $request){
        $id = Auth::id();
        if($id)
        {
            try {
                $custom_posts = DB::table('custom_fields')->select("*")->where("status", "=", 1)->where("id", "=", $request->id)->get();
                return response()->json([
                    'status' => true,
                    'code' => 200,
                    'message' => $custom_posts,
                ],200);
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function updateCustomPost(Request $request){
        $id = Auth::id();
        if($id)
        {
            try {
                $mCustomPost = CustomPost::firstOrNew(['id' => $request->custom_post_id]);
                $mCustomPost->name = $request->name;
                $mCustomPost->description = $request->desc;
                $mCustomPost->url = $request->url;
                $mCustomPost->language = $request->languages;
                if($request->image_custom != ""){
                    $mCustomPost->image =  json_encode(explode(",",$request->image_custom));
                } else {
                    $mCustomPost->image = [];
                }
                if($request->file_custom != ""){
                    $mCustomPost->file =  json_encode(explode(",",$request->file_custom));
                } else {
                    $mCustomPost->file = [];
                }
                $mCustomPost->save();
                return "success";
            } catch (\Throwable $th) {
                return "error";
            }
        } else {
            return view('admin.auth.login');
        }
    }
}
