<?php

namespace App\Http\Controllers\Admin\General;

use App\Http\Controllers\Controller;
use App\Models\Admin\General\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DataTables;

class ViewController extends Controller
{
    public function index(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            if ($request->ajax()) {
                $data =  View::latest()->where("status", "=", 1)->get()->unique('slug');
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){

                            $btn = '<a href="view/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
        
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
            return view('admin.general.view.index');
        } else {
            return view('admin.auth.login');
        }
    }

    public function create()
    {
        $id = Auth::id();
        if($id)
        {
            return view('admin.general.view.create');
        } else {
            return view('admin.auth.login');
        }
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title != "" && $request->slug != ""){
                    $count = DB::table('view')->where('slug','=','/'.$request->slug)->count();
                    if($count == 0){
                        $View = new View;
                        $View->title = $request->title;
                        $View->slug = '/'.$request->slug;
                        $View->save();
                        return response()->json('success');
                        // return "success";
                    } else {
                        return response()->json('data found');
                        // return "data found";
                    }
                } else {
                      return response()->json('error');
                }
            } catch (\Throwable $th) {
                  return response()->json('error');
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function show(View $View)
    {
        //
    }

    public function edit(View $view)
    {
        $id = Auth::id();
        if($id)
        {

            return view('admin.general.view.edit', compact('view'));
        } else {
            return view('admin.auth.login');
        }
    }

    public function update(Request $request, View $View)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->title != "" && $request->slug != ""){
                    $view = DB::table('view')->select('slug')->where('id', '=', $View->id)->get();
                    $mSlug = $view[0]->slug;

                    if($mSlug == $request->slug){
                        $viewen = View::firstOrNew(['slug' => $request->slug]);
                        $viewen->title = $request->title;
                        $viewen->slug = '/'.$request->slug;
                        $viewen->save();
                        return "success";
                    } else {
                        $count = DB::table('view')->where('slug','=','/'.$request->slug)->count();
                        if($count == 0){
                            $view = DB::table('view')->select('slug')->where('id', '=', $View->id)->get();
                            $mSlug = $view[0]->slug;
                            $viewen = View::firstOrNew(['slug' => $mSlug, 'id' => $View->id]);
                            $viewen->title = $request->title;
                            $viewen->slug = '/'.$request->slug;
                            $viewen->save();
                            return response()->json('success');
                        } else {
                            return response()->json('data found');
                            // return "data found";
                        }
                    }                    
                } else {
                    return response()->json('error');
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    public function destroy(View $View)
    {
        // echo '<pre>'; print_r($View); echo '</pre>'; die('----CALL----');
        $id = Auth::id();
        if($id)
        {
            try {
                // $mViewen = View::firstOrNew(['id' => $View->id]);
                // $mViewur = View::firstOrNew(['id' => $View->id+1]);

                // $View->update(['status' => 0]);
                $View->status = 0;
                $View->save();

                // $mViewur->status = 0;
                // $mViewur->save();
                return response()->json('success');
            } catch (\Throwable $th) {
                return response()->json($th->getMessage());
            }
        } else {
            return view('admin.auth.login');
        }
    }
}
