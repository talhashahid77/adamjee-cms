<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        // echo '<pre>'; print_r(url()); echo '</pre>'; die('----CALL----');
        $id = Auth::id();
        if($id)
        {
            if ($request->ajax()) {
                $data =  Test::latest()->get();
                return DataTables::of($data)
                 
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="test/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a><button onclick="mdelete('.$row->id.',this)" class="delete btn btn-danger btn-sm">Delete</button>';
                        return $btn;
                            })->addColumn('image', function($row){

                            $url= URL::asset('public/uploads') . '/' .$row->image;
                                return '<img src='.$url.' width="400" height="700">';
                        })
                        ->rawColumns(['image','action'])
                        ->make(true);
                    }   
                    return view('test.index');
                }else{
                    return view('admin.auth.login');
            }
        }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('test.create');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     // 'name' => 'required',
        //     // 'status' => 'required',
        //     // 'image' => 'required',
        // ]);

        // if ($validator->fails()){
        //     return redirect()->back()
        //         ->withInput($request->all())
        //                 ->withErrors($validator);       
        // }
        $id = Auth::id();
        if($id){
            try {
        if($request->name !=""){     
        $data = new Test;
        $data->name = $request->name;
        $data->status = $request->status;
        if (!empty($request->image)) {
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension(); 
        $filename = time().'.' . $extension;
        $file->move(public_path('uploads/'), $filename);
        $data->image = $filename;
        }else{

            return response()->json('error');
        
        }
        $data->save();
            return response()->json('success');
        }else{
            return response()->json('error');
        }

        } catch (\Throwable $th) {
            return "error";
        }


        }else{
        return view('admin.auth.login');
        }
       
         }
    
    //return redirect()->route('test.index')->with('alert-success', 'Data has been save');
          
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        // echo '<pre>'; print_r(URL::asset('public/public/uploads/')); echo '</pre>'; die('----CALL----');
        $id = Auth::id();
        if($id)
        {
            return view('test.edit', compact('test'));
        } else {
            return view('admin.auth.login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        
        $id = Auth::id();
        if($id)
        {
            try {
                if($request->name != "" && $request->status != ""){
                    $Test = $test;
                    $Test->name = $request->name;
                    $Test->status = $request->status;
                    if (!empty($request->image)) {
                        $file =$request->file('image');
                        $extension = $file->getClientOriginalExtension(); 
                        $filename = time().'.' . $extension;
                        $file->move(public_path('uploads/'), $filename);
                        $Test->image = $filename;
                    }
                    
                    $Test->save();
                    return response()->json('success');
                } else {
                    return response()->json('error');
                }
            } catch (\Throwable $th) {
                return $th;
            }
        } else {
            return view('admin.auth.login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        $id = Auth::id();
        if($id)
        {
            try {
                $mTest = Test::firstOrNew(['id' => $test->id]);
                $mTest->delete();
                return response()->json('success');
            } catch (\Throwable $th) {
                return response()->json('error');
            }
        } else {
            return view('admin.auth.login');
        }
    }
}
