<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClaimRequest;
use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{

        public function contact(ContactRequest $request){
        
        try {
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->policy_number = $request->policy_number;
            $contact->email = $request->email;
            $contact->cnic = $request->cnic;
            $contact->contact_number = $request->contact_number;
            $contact->city = $request->city;
            $contact->category = $request->category;
            $contact->sub_category = $request->sub_category;
            $contact->summary = $request->summary;
            $contact->form_type = "Contact_us";

            $category = $request->category;
        
            $files = [];
            if (!empty($request->file)) {
    
                foreach ($request->file as $key => $fileName) {
    
                    $file = $key . '-' . time().'.'.$fileName->extension();  
                    $fileName->move(public_path('uploads/contact'), $file);
                    $files[] = $file;
                }
                    $contact->file = implode(',', $files);
    
            }else{
                return back()->with('error', 'Please Select the File!');
            }
            
            $contact->save();
            $data = $contact->toArray();
           
            $toEmails = [
                'Complains' => "complain", 
                'Service Request' => "help_csd", 
                'Claims' => "help_Claims"
            ];
            

            Mail::send('email.contact-form',$data,
            function($message) use ($category, $files, $toEmails){
                $message->to($toEmails[$category] . '@adamjeelife.com')
                ->subject($category . ' | Adaamjee' );
    
                foreach($files as $item){
                    $message->attach(public_path('uploads/contact/'.$item));
                }
                    
            });

            
            return back()->with('success', 'Thanks for contacting me, I will get back to you soon!');
        } catch (Exception $e) {
            return back()->with('error','Something Went Wrong');
        }



    }



        public function contactClaim(Request $request){
            // die('end');
            // ClaimRequest

        
        try {
            $contact = new Contact();
            $contact->claim_type = $request->claim_type;
            $contact->name = $request->name;
            $contact->policy_number = $request->policy_number;
            $contact->email = $request->email;
            $contact->cnic = $request->cnic;
            $contact->contact_number = $request->contact_number;
            $contact->summary = $request->summary;
            $contact->claim_type = $request->claim_type;
            $contact->form_type = "Claim_Contact";

            $claim = $request->claim_type;
        
            $files = [];
            if (!empty($request->file)) {
    
                foreach ($request->file as $key => $fileName) {
    
                    $file = $key . '-' . time().'.'.$fileName->extension();  
                    $fileName->move(public_path('uploads/claim'), $file);
                    $files[] = $file;
                }
                    $contact->file = implode(',', $files);
                
            }else{
                return response()->json(['status' => false, 'message' => 'Please Select the file']);
                // return back()->with('error', 'Please Select the File!');
            }
            
            $contact->save();
            $data = $contact->toArray();
           
            $toEmails = [
                'Banca' => "talha.shahid", 
                // 'Service Request' => "help_csd", 
                // 'Claims' => "help_Claims"
            ];
            

            Mail::send('email.claim-contact',$data,
            function($message) use ($claim, $files, $toEmails){
                $message->to($toEmails[$claim] . '@convexinteractive.com')
                ->subject($claim . ' | Adaamjee' );
    
                foreach($files as $item){
                    $message->attach(public_path('uploads/claim/'.$item));
                }
                    
            });

            return response()->json(['status' => true, 'message' => 'Thanks for contacting me, I will get back to you soon!']);
            // return back()->with('success', 'Thanks for contacting me, I will get back to you soon!');
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => 'Something Went Wrong']);
        }



    }







    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
