<?php

namespace App\Http\Controllers;
use App\Models\Admin\General\Page;
use App\Models\ApplicantDetail;
use App\Models\ApplicantEducation;

use App\Models\Slider;
use App\Models\FundPrices;
use App\Models\FundPricesLog;
use App\Models\Plan;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ApiController extends Controller
{
    public function listBreadcrump(Request $request){
        $query = Page::select('title_en','title_ur')->where('slug','/'.$request->slug);
        $data = $query->get();
        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => $data[0],
        ],200);
    }

    public function listPlans(Request $request){
        if(ucwords($request->planType) == "Conventional"){
            $id = 78;
        } else if(ucwords($request->planType) == "Takaful"){
            $id = 79;
        }
        $query = DB::table('pages')
            ->join('article', 'pages.id', '=' , 'article.page_id')
            ->select('article.*')
            ->where('pages.parent_id', '=', $id)
            ->where('pages.title_en','like',''.ucwords($request->subType).'%')
            ->where('article.status','=',1)
            ->orderBy('article.sorting', 'ASC')->get();

        foreach($query as $innerCustom=>$custom){
            $subInnerPost = DB::table('custom_fields')
                ->select('*')
                ->where('parent_id','=',$custom->id)
                ->where('status','=',1)
                ->orderBy('id', 'ASC')
                ->get();
            $subInnerCustomPosts = $subInnerPost;
            
            if(!empty($subInnerCustomPosts)){
                $query[$innerCustom]->custompost =  $subInnerCustomPosts;
            }
        }
        // $data = $query->get();
        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => $query,
        ],200);
    }

    public function sliderInfo(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://alpos.adamjeelife.com/ALACLWeb_API/api/Main_/GetWebsiteInfo");
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);
        $server_output = json_decode($server_output);
        if(is_array($server_output)){
            $slider = new Slider;
            $slider->response = $server_output;
            $slider->save();
        }
        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => $server_output,
        ],200);
    }

    public function plan(Request $request){
        try {
                    // die('talha');
                $plan = new Plan();
                $plan->plan_type = $request->plan_type;
                $plan->plan_date = $request->plan_date;
                $plan->premium_mode = $request->premium_mode;
                $plan->amount = $request->amount;
                $plan->name = $request->name;
                $plan->date_of_birth = $request->date_of_birth;
                $plan->gender = $request->gender;
                $plan->mobile_no = $request->mobile_no;
                $plan->city = $request->city;
                $plan->plan_sub_type = $request->plan_sub_type;
                $plan->plan_name = $request->plan_name;

                // print_r($request->all());
                // die('end');

                $plan->save();
                $data = $plan->toArray();
            
                Mail::send('email.plan',$data,
                function($message){
                    $message->to('talha.shahid@convexinteractive.com')
                    ->subject('Adaamjee' );      
                    });

                    return response()->json([
                        'status' => true,
                        'code' => 200,
                        'message' => $data,
                    ],200);

            }catch (Exception $e) {
                return response()->json([
                    'status' => false,
                    'code' => 400,
                    'message' => $e->getMessage(),
                ],400);
                    
            }
    }

    public function openVacancy(Request $request){
        try {
        // die('talha');
        $Applicant_detail = new ApplicantDetail();
        $Applicant_detail->full_name = $request->full_name;
        $Applicant_detail->email = $request->email;
        $Applicant_detail->mobile_no = $request->mobile_no;
        $Applicant_detail->cnic = $request->cnic;
        $Applicant_detail->date_of_birth = $request->date_of_birth;
        $Applicant_detail->emergency_no = $request->emergency_no; 
        $Applicant_detail->address = $request->address;
        $Applicant_detail->city = $request->city;
        $Applicant_detail->save();

        $applicant_id = $Applicant_detail->id;        

        $Applicant_education = new ApplicantEducation();
        $Applicant_education->applicant_id = $applicant_id;
        $Applicant_education->current_qualification = $request->current_qualification;
        $Applicant_education->institution = $request->institution;
        $Applicant_education->additional_certification = $request->additional_certification;
        $Applicant_education->start_date = $request->start_date; 
        $Applicant_education->end_date = $request->end_date;
        $Applicant_education->in_progress = $request->in_progress;
        $Applicant_education->save();




        
            // print_r($request->all());
            // die('end');

            // $plan->save();
            // $data = $plan->toArray();
        
            // Mail::send('email.plan',$data,
            // function($message){
            //     $message->to('talha.shahid@convexinteractive.com')
            //     ->subject('Adaamjee' );      
            //     });

                return response()->json([
                    'status' => true,
                    'code' => 200,
                    // 'message' => $data,
                ],200);

        }catch (Exception $e) {
            return response()->json([
                'status' => false,
                'code' => 400,
                'message' => $e->getMessage(),
            ],400);
        
        }
    }


    public function listAnnouncement(Request $request){
        // Conventional Start
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://alpos.adamjeelife.com/ALACLWeb_API/api/Main_/GetFunds_Conventional");
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $conventional_data = curl_exec($ch);
        $conventional_data = json_decode($conventional_data);
        curl_close ($ch);

        if(is_array($conventional_data)){
            $conventional_data = $conventional_data[0];

            $conventionalArrKey = array();
            $conventionalArrList = array();
            
            foreach ($conventional_data as $key => $conventional_single) {
                if (!in_array($conventional_single->SFUND_DESC, $conventionalArrKey)){
                    array_push($conventionalArrKey, $conventional_single->SFUND_DESC);
                    array_push($conventionalArrList, $conventional_single);
                }
            }
    
            // Data saving in database.
            foreach ($conventionalArrList as $conventionalKey => $conventional) {
                if($conventionalKey == 0){
                    FundPrices::where('type','=','conventional')->delete();
                }
                $FundPrices = new FundPrices;
                $FundPrices->type = 'conventional';
                $FundPrices->fund_code = $conventional->SFUND_FUNDCD;
                $FundPrices->fund_type = $conventional->SFUND_DESC;
                $FundPrices->bid_price = $conventional->BOFR_BIDPRICE; 
                $FundPrices->offer_price = $conventional->BOFR_OFFRPRICE;
                $FundPrices->fund_date = $conventional->BOFR_VALDT;
                $FundPrices->save();
    
                $FundPrices = new FundPricesLog;
                $FundPrices->type = 'conventional';
                $FundPrices->fund_code = $conventional->SFUND_FUNDCD;
                $FundPrices->fund_type = $conventional->SFUND_DESC;
                $FundPrices->bid_price = $conventional->BOFR_BIDPRICE; 
                $FundPrices->offer_price = $conventional->BOFR_OFFRPRICE;
                $FundPrices->fund_date = $conventional->BOFR_VALDT;
                $FundPrices->save();
            }    
        }

        // Conventional End

        // Takaful Start
        $tk = curl_init();
        curl_setopt($tk, CURLOPT_URL,"https://alpos.adamjeelife.com/ALACLWeb_API/api/Main_/GetFunds_Takaful");
        curl_setopt($tk, CURLOPT_POST, 0);
        curl_setopt($tk, CURLOPT_RETURNTRANSFER, true);
        $takaful_data = curl_exec($tk);
        $takaful_data = json_decode($takaful_data);
        curl_close ($tk);

        if(is_array($takaful_data)){
            $takaful_data = $takaful_data[0];

            $takafulArrKey = array();
            $takafulArrList = array();
            
            foreach ($takaful_data as $key => $takaful_single) {
                if (!in_array($takaful_single->DECODE_FUND, $takafulArrKey)){
                    array_push($takafulArrKey, $takaful_single->DECODE_FUND);
                    array_push($takafulArrList, $takaful_single);
                }
            }

            foreach ($takafulArrList as $takafulKey => $takaful) {
                if($takafulKey == 0){
                    FundPrices::where('type','=','takaful')->delete();
                }
                
                $FundPrices = new FundPrices;
                $FundPrices->type = 'takaful';
                $FundPrices->fund_code = $takaful->SFUND_FUNDCD;
                $FundPrices->fund_type = $takaful->DECODE_FUND;
                $FundPrices->bid_price = $takaful->BOFR_BIDPRICE; 
                $FundPrices->offer_price = $takaful->BOFR_OFFRPRICE;
                $FundPrices->fund_date = $takaful->BOFR_VALDT;
                $FundPrices->save();

                $FundPrices = new FundPricesLog;
                $FundPrices->type = 'takaful';
                $FundPrices->fund_code = $takaful->SFUND_FUNDCD;
                $FundPrices->fund_type = $takaful->DECODE_FUND;
                $FundPrices->bid_price = $takaful->BOFR_BIDPRICE; 
                $FundPrices->offer_price = $takaful->BOFR_OFFRPRICE;
                $FundPrices->fund_date = $takaful->BOFR_VALDT;
                $FundPrices->save();
            }           
        }
        // Takaful End

        $conArray = null;
        $tkArray = null;
        
        $announcementArr = array();
        if(is_array($conventional_data)){
            $conArray = $conventionalArrList;
        } else {
            $conArray = $conventional_data;
        }

        if(is_array($takaful_data)){
            $tkArray = $takafulArrList;
        } else {
            $tkArray = $takaful_data;
        }

        $announcementArr = ['conventional' => $conArray,'takaful' => $tkArray];

        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => $announcementArr,
        ],200);
    }

    public function fundList(Request $request){
        $query = DB::table('fund_prices')
            ->select('fund_prices.fund_type as fund_name','fund_code')
            ->where('type','=',$request->fund_type)
            ->orderBy('id', 'ASC')->get();
            
        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => $query,
        ],200);
    }
}
