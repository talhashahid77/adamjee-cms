<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Admin\General\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MultiController extends Controller
{
    public function enShow($x,Request $request){ 
        try {
            $device_type = $this->deviceType();
            $url_type = 'en';
            Session::put('url', 'en');
            $url = explode('/', $x);
            $url = implode('/', $url);
            
            if(isset($_GET['type'])){
                $type = $_GET['type'];
            } else {
                $type = "";
            }
            
            $id = DB::table('url')->select('page_id')->where('path','=','/'.$url)->where('language','=','en')->get();
            // Page id retrieve..
            $page_id = $id[0]->page_id;
    
            $pagecon = DB::table('pages')->select('*')->where('id','=',$page_id)->where('status','=',1)->where('visible','=','Y')->get();
            $pagesdata = $pagecon;

            if(count($pagesdata) > 0){

                // Post data retrieve
                $posts = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $postsdata = $posts;

                //Add Custom post type in Posts
                foreach($postsdata as $PostKey=>$postdata){
                    $PostCustomPost = DB::table('custom_fields')
                            ->select('*')
                            ->where('parent_id','=',$postdata->id)
                            ->where('status','=',1)
                            ->where('parent_type','=','article')
                            ->where('language', '=', $url_type)
                            ->orderBy('id', 'ASC')
                            ->get();
                    $PostCustomPosts = $PostCustomPost;
                    if(!empty($PostCustomPosts)){
                        $postsdata[$PostKey]->custompost =  $PostCustomPosts;
                    }
                }

                $menus = DB::table('menu_item')->select('*')->where('menu_id','=',11)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $menu_items = $menus;
                
                $menus_parents = DB::table('menu_item')->select('*')->where('menu_id','=',12)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $menus_parents = $menus_parents;

                $footers_menu = DB::table('menu_item')->select('*')->where('menu_id','=',14)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $footer_menu = $footers_menu;

                // View retrive..
                $view = $pagesdata[0]->view_en;

                // Inner Pages
                $InPagesDesc = DB::table('pages')->select('pages.*','url.path')->where('pages.parent_id','=',$page_id)->where('pages.status','=',1)->where('url.language','=','en')->join('url', 'url.page_id', '=', 'pages.id')->orderBy('pages.sorting', 'ASC')->get();
                $innerPages = $InPagesDesc;
                
                //Add Custom post type in Pages
                foreach($pagesdata as $PageKey=>$pagedata){
                    $PageCustomPost = DB::table('custom_fields')
                            ->select('*')
                            ->where('parent_id','=',$pagedata->id)
                            ->where('status','=',1)
                            ->where('parent_type','=','page')
                            ->where('language', '=', $url_type)
                            ->orderBy('id', 'ASC')
                            ->get();
                    $PageCustomPosts = $PageCustomPost;
                    if(!empty($PageCustomPosts)){
                        $pagesdata[$PageKey]->custompost =  $PageCustomPosts;
                    }
                }

                //Add child inner pages in Pages
                foreach($innerPages as $innerPageKey=>$innerPage){
                    $subInnerPage = DB::table('pages')
                        ->join('url', 'pages.id', '=' , 'url.page_id')
                        ->select('pages.*','url.path')
                        ->where('url.language', '=', $url_type)
                        ->where('parent_id','=',$innerPage->id)
                        ->where('status','=',1)
                        ->orderBy('sorting', 'ASC')
                        ->get();
                    $subInnerArticle = DB::table('article')
                        ->select('*')
                        ->where('page_id','=',$innerPage->id)
                        ->where('status','=',1)
                        ->orderBy('sorting', 'ASC')
                        ->get();

                    $innerCustomPost = DB::table('custom_fields')
                        ->select('*')
                        ->where('parent_id','=',$innerPage->id)
                        ->where('status','=',1)
                        ->where('parent_type','=','page')
                        ->where('language', '=', $url_type)
                        ->orderBy('id', 'ASC')
                        ->get();
                    $innerCustomPosts = $innerCustomPost;
                    
                    
                    //Add sub inner pages article
                    foreach($subInnerPage as $subInnerPageKey => $subInnerPages){
                            $subPagesArticle = DB::table('article')
                                ->select('*')
                                ->where('page_id','=',$subInnerPages->id)
                                ->where('status','=',1)
                                ->orderBy('sorting', 'ASC')
                                ->get();
                            $subInnerPageArticle = $subPagesArticle;

                            $subChildInnerPage = DB::table('pages')
                                ->join('url', 'pages.id', '=' , 'url.page_id')
                                ->select('pages.*','url.path')
                                ->where('url.language', '=', $url_type)
                                ->where('parent_id','=',$subInnerPages->id)
                                ->where('status','=',1)
                                ->orderBy('sorting', 'ASC')
                                ->get();

                            $subinnerCustomPost = DB::table('custom_fields')
                                ->select('*')
                                ->where('parent_id','=',$subInnerPages->id)
                                ->where('status','=',1)
                                ->where('parent_type','=','page')
                                ->where('language', '=', $url_type)
                                ->orderBy('id', 'ASC')
                                ->get();
                            $subinnerCustomPosts = $subinnerCustomPost;

                            foreach($subInnerPageArticle as $subInnerPagePostKey => $subInnerPageArticles){
                                $subInnerPageArticlePost = DB::table('custom_fields')
                                    ->select('*')
                                    ->where('parent_id','=',$subInnerPageArticles->id)
                                    ->where('status','=',1)
                                    ->where('parent_type','=','article')
                                    ->where('language', '=', $url_type)
                                    ->orderBy('id', 'ASC')
                                    ->get();
                                $subInnerPageArticlePosts = $subInnerPageArticlePost;
                                
                                if(!empty($subInnerPageArticlePosts)){
                                    $subInnerPageArticle[$subInnerPagePostKey]->custompost =  $subInnerPageArticlePosts;
                                }
                            }

                            foreach($subChildInnerPage as $subChildInnerKey => $subChildInnerPages){
                                $ChildPagesArticle = DB::table('article')
                                    ->select('*')
                                    ->where('page_id','=',$subChildInnerPages->id)
                                    ->where('status','=',1)
                                    ->orderBy('sorting', 'ASC')
                                    ->get();

                                $subChildPagesArticles = $ChildPagesArticle;

                                $subinnerPageCustomPost = DB::table('custom_fields')
                                    ->select('*')
                                    ->where('parent_id','=',$subChildInnerPages->id)
                                    ->where('status','=',1)
                                    ->where('parent_type','=','page')
                                    ->where('language', '=', $url_type)
                                    ->orderBy('id', 'ASC')
                                    ->get();
                                $subinnerPageCustomPosts = $subinnerPageCustomPost;

                                foreach($subChildPagesArticles as $subChildArticleKey => $subChildPagesArticle){
                                    $subChildInnerCustomPost = DB::table('custom_fields')
                                        ->select('*')
                                        ->where('parent_id','=',$subChildPagesArticle->id)
                                        ->where('status','=',1)
                                        ->where('parent_type','=','article')
                                        ->where('language', '=', $url_type)
                                        ->orderBy('id', 'ASC')
                                        ->get();
                                    $subChildInnerCustomPosts = $subChildInnerCustomPost;
                                    
                                    if(!empty($subChildInnerCustomPosts)){
                                        $subChildPagesArticles[$subChildArticleKey]->custompost =  $subChildInnerCustomPosts;
                                    }
                                }

                                if(!empty($subinnerPageCustomPosts)){
                                    $subChildInnerPage[$subChildInnerKey]->custompost =  $subinnerPageCustomPosts;
                                }

                                if(!empty($subChildPagesArticles)){
                                    $subChildInnerPage[$subChildInnerKey]->article =  $subChildPagesArticles;
                                }
                            }
                            
                            if(!empty($subinnerCustomPosts)){
                                $subInnerPage[$subInnerPageKey]->custompost =  $subinnerCustomPosts;
                            }

                            if(!empty($subChildInnerPage)){
                                $subInnerPage[$subInnerPageKey]->child =  $subChildInnerPage;
                            }
                                
                            if(!empty($subInnerPageArticle)){
                                $subInnerPage[$subInnerPageKey]->article =  $subInnerPageArticle;
                            }
                    }

                    //Add custom post type in sub inner pages
                    foreach($subInnerArticle as $innerArticleKey => $innerArticle){
                        $subInnerPost = DB::table('custom_fields')
                            ->select('*')
                            ->where('parent_id','=',$innerArticle->id)
                            ->where('status','=',1)
                            ->where('parent_type','=','article')
                            ->where('language', '=', $url_type)
                            ->orderBy('id', 'ASC')
                            ->get();
                        $subInnerCustomPosts = $subInnerPost;
                        
                        if(!empty($subInnerCustomPosts)){
                            $subInnerArticle[$innerArticleKey]->custompost =  $subInnerCustomPosts;
                        }
                    }
                   
                    if(!empty($innerCustomPost)){
                        $innerPages[$innerPageKey]->custompost =  $innerCustomPost;
                    }
                   
                    if(!empty($subInnerPage)){
                        $innerPages[$innerPageKey]->child =  $subInnerPage;
                    }
                    if(!empty($subInnerArticle)){
                        $innerPages[$innerPageKey]->article =  $subInnerArticle;
                    }
                }

                // Inner Pages Detail
                $InPagesDetail = DB::table('pages')->select('*')->where('id','=',$request->id)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $innerPagesDetails = $InPagesDetail;

                // Get Parent Page Data -----------------------------------------------------------
                $ParentPageData = DB::table('pages')->select('*')->where('id','=',$page_id)->where('status','=',1)->get();
                if($ParentPageData[0]->mlevel > 2){
                    $ParentPageData = DB::table('pages')->select('*')->where('id','=',$ParentPageData[0]->parent_id)->where('mlevel','=',$ParentPageData[0]->mlevel - 1)->where('status','=',1)->get();
                    if($ParentPageData[0]->id != ""){
                        $InPagesDesc = DB::table('pages')->select('pages.*','url.path')->where('pages.id','<>',$page_id)->where('pages.parent_id','=',$ParentPageData[0]->id)->where('pages.status','=',1)->where('url.language','=','en')->join('url', 'url.page_id', '=', 'pages.id')->orderBy('pages.sorting', 'ASC')->get();
                        $ParentPageData = $InPagesDesc;
        
                        foreach($ParentPageData as $innerPageKey=>$innerPage){
                            $subInnerPage = DB::table('pages')
                                ->join('url', 'pages.id', '=' , 'url.page_id')
                                ->select('pages.*','url.path')
                                ->where('url.language', '=', $url_type)
                                ->where('parent_id','=',$innerPage->id)
                                ->where('status','=',1)
                                ->orderBy('sorting', 'ASC')
                                ->get();
                            $subInnerArticle = DB::table('article')
                                ->select('*')
                                ->where('page_id','=',$innerPage->id)
                                ->where('status','=',1)
                                ->orderBy('sorting', 'ASC')
                                ->get();
                            foreach($subInnerArticle as $innerArticleKey=>$innerArticle){
                                $subInnerPost = DB::table('custom_fields')
                                    ->select('*')
                                    ->where('parent_id','=',$innerArticle->id)
                                    ->where('status','=',1)
                                    ->where('parent_type','=','article')
                                    ->where('language', '=', $url_type)
                                    ->orderBy('id', 'ASC')
                                    ->get();
                                $subInnerCustomPosts = $subInnerPost;
                                
                                if(!empty($subInnerCustomPosts)){
                                    $subInnerArticle[$innerArticleKey]->custompost =  $subInnerCustomPosts;
                                }
                            }
                           
                            if(!empty($subInnerPage)){
                                $ParentPageData[$innerPageKey]->child =  $subInnerPage;
                            }
                            if(!empty($subInnerArticle)){
                                $ParentPageData[$innerPageKey]->article =  $subInnerArticle;
                            }
                        }
                    }
                }

                $slider = DB::table('slider')->select('*')->orderBy('id','DESC')->first();
                $sliderInfo = $slider;

                $fund_price = DB::table('fund_prices')->select('*')->get();
                $fund_prices = $fund_price;

                $knowledgePage = DB::table('pages')->select('*')->where('parent_id','=',248)->orderBy('id','DESC')->get();
                $knowledgePages = $knowledgePage;

                //Add Inner Pages in Knowledge Center
                foreach($knowledgePages as $knowledgeId => $knowledgedata){
                    $knowledgeInnerPage = DB::table('pages')
                        ->join('url', 'pages.id', '=' , 'url.page_id')
                        ->select('*')
                        ->where('url.language', '=', $url_type)
                        ->where('parent_id','=',$knowledgedata->id)
                        ->where('status','=',1)
                        ->orderBy('sorting', 'ASC')
                        ->limit(1)->get();
                    $knowledgeInnerData = $knowledgeInnerPage;

                    if(!empty($knowledgeInnerData)){
                        $knowledgePages[$knowledgeId]->child =  $knowledgeInnerData;
                    }
                }

                $latestAdamjee = DB::table('pages')->select('*')->where('parent_id','=',501)->orderBy('id','DESC')->get();
                $latestAdamjees = $latestAdamjee;

                //Add Inner Pages in latest Adamjee
                foreach($latestAdamjees as $latestId => $latestdata){
                    $LatestInnerPage = DB::table('pages')
                        ->join('url', 'pages.id', '=' , 'url.page_id')
                        ->select('pages.*','url.path')
                        ->where('url.language', '=', $url_type)
                        ->where('parent_id','=',$latestdata->id)
                        ->where('status','=',1)
                        ->orderBy('sorting', 'ASC')
                        ->limit(1)->get();
                    $latestInnerData = $LatestInnerPage;

                    foreach($latestInnerData as $latestCustomKey => $latestCustom){
                        $sublatestPost = DB::table('custom_fields')
                            ->select('custom_fields.*')
                            ->where('parent_id','=',$latestCustom->id)
                            ->where('status','=',1)
                            ->where('parent_type','=','page')
                            ->where('language', '=', $url_type)
                            ->orderBy('id', 'ASC')
                            ->get();

                        $sublatestCustomPosts = $sublatestPost;
                        
                        if(!empty($sublatestCustomPosts)){
                            $latestInnerData[$latestCustomKey]->custompost =  $sublatestCustomPosts;
                        }
                    }

                    if(!empty($latestInnerData)){
                        $latestAdamjees[$latestId]->child =  $latestInnerData;
                    }
                }

                return view(substr($view,1),compact('pagesdata','sliderInfo', 'footer_menu', 'latestAdamjees', 'fund_prices', 'knowledgePages' ,'device_type','postsdata','url','ParentPageData','url_type','menu_items','menus_parents','innerPages','innerPagesDetails'));
            } else {
                return abort(404);
            }
        } catch (\Throwable $th) {
            abort(404);
        }
    }

    public function urShow($x,Request $request){
        try {
            $device_type = $this->deviceType();
            $url_type = 'ur';
            Session::put('url', 'ur');
            $url = explode('/', $x);
            $url = implode('/', $url);
            $id = DB::table('url')->select('page_id')->where('path','=','/'.$url)->where('language','=','ur')->get();
            
            // Page id retrieve..
            $page_id = $id[0]->page_id;
    
            $pagecon = DB::table('pages')->select('*')->where('id','=',$page_id)->where('status','=',1)->where('visible','=','Y')->get();
            $pagesdata = $pagecon;
           
            if(count($pagesdata) > 0){

                // Post data retrieve
                $posts = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $postsdata = $posts;

                // $postsDataDesc = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('id', 'DESC')->get();
                $postsDataDesc = DB::table('article')->select('*')->where('page_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $postsdesc = $postsDataDesc;
               
                $menus = DB::table('menu_item')->select('*')->where('menu_id','=',11)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $menu_items = $menus;
                
                $menus_parents = DB::table('menu_item')->select('*')->where('menu_id','=',12)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $menus_parents = $menus_parents;

                
              
                // View retrive..
                if($pagesdata[0]->view_ur == ""){
                    $view = $pagesdata[0]->view_en;
                    $url_type = 'en';
                    Session::put('url', 'en');
                } else {
                    $view = $pagesdata[0]->view_ur;
                }

                // Inner Pages
                $InPages = DB::table('pages')->select('*')->where('parent_id','=',$page_id)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $innerPages = $InPages;

                // Inner Pages
                $InPagesDesc = DB::table('pages')->select('*')->where('parent_id','=',$page_id)->where('status','=',1)->orderBy('sorting', 'ASC')->get();
                $innerPagesDesc = $InPagesDesc;

                // Inner Pages Detail
                $InPagesDetail = DB::table('pages')->select('*')->where('id','=',$request->id)->where('status','=',1)->orderBy('id', 'ASC')->get();
                $innerPagesDetails = $InPagesDetail;

                return view(substr($view,1),compact('pagesdata','device_type','postsdesc','postsdata','url','url_type','menu_items','menus_parents','innerPages','innerPagesDesc','innerPagesDetails', 'BodAndMangment'));
            } else {
                return abort(404);
            }
        } catch (\Throwable $th) {
            abort(404);
        }
    }

}
