<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{

        public function contact(Request $request){

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->policy_number = $request->policy_number;
        $contact->email = $request->email;
        $contact->cnic = $request->cnic;
        $contact->contact_number = $request->contact_number;
        $contact->city = $request->city;
        $contact->category = $request->category;
        // $contact->sub_category = $request->sub_category;
        $contact->summary = $request->summary;

        $files = [];
        if (!empty($request->file)) {

                $fileName = time().'.'.$request->file->extension();  
                $path = $request->file->move(public_path('uploads/contact'), $fileName);
                $contact->file = $fileName;
          
        }else{
            echo "Please select the file ";
        }
        $contact->save();
       
        $data = $contact->toArray();
        $public_path = public_path('uploads/contact/'.$fileName);


        Mail::send('email.contact-form',$data,
        function($message) use ($public_path){
            $message->to('talha.shahid@convexinteractive.com', 'Talha Shahid')
            ->subject('Your Website Contact Form')
            ->attach($public_path);
            
        });
        
        return back()->with('success', 'Thanks for contacting me, I will get back to you soon!');

    }





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
