{{-- @dd($pagesdata) --}}
{{-- @dd($postsdata); --}}

@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])


<!-- Section Start -->
@foreach($pagesdata as $key => $pagedata)
@php 
$postImg =  getImageFile($device_type,$pagedata,"image","inner");
@endphp
<section class="HeaderInnerPage">
	<img src="{{ $postImg }}" />
	@include('partials.breadcrumb')
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>{{$pagedata->title_en}}</h1>
			{!! $pagedata->short_desc_en !!}
		</div>
	</div>
</section>
@endforeach
<!-- Section End -->

<!-- Section Start -->
<section class="SecWrap SecTopSpace whiteTabsSec">
	<div class="uk-container containCustom">
		<div class="searchList">
			<form class="uk-search uk-search-default">
			    <span uk-search-icon></span>
			    <input class="uk-search-input" type="search" placeholder="Search for articles">
			</form>
		</div>
		<div class="DownloadList">
			<ul>
				@foreach($postsdata as $key => $postdata)
					@php 
						$postImg =  getImageFile($device_type,$postdata,"file","inner");
					@endphp
				<li><a href="{{ $postImg }}" download>{{ $postdata->title_en }}<img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
				@endforeach
				
			</ul>
		</div>
	</div>
</section>


@include('partials.footer')
@endif