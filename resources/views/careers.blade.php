{{-- @dd($postsdata); --}}
{{-- @dd($pagesdata); --}}
{{-- @dd($postsdesc); --}}
{{-- @dd($innerPages); --}}

@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])


@foreach($pagesdata as $key => $pagedata)
    @php 
        $postImg =  getImageFile($device_type,$pagedata,"image","inner");
    @endphp
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="{{ $postImg }}" />
		@include('partials.breadcrumb')
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>{{$pagedata->title_en}}</h1>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur</p> -->
		</div>
	</div>
</section>
@endforeach
<!-- Section End -->

@foreach($postsdata as $key => $postdata)
		@php 
			$postImg =  getImageFile($device_type,$postdata,"image","banner");
		@endphp
		 @if($key == 1)
	<!-- Section Start -->
	<section class="SecWrap SecTopSpace SecBottomSpace">
		<div class="uk-container containCustom">
			<div class="boxyDesign">
				<div class="boxyDesignImg">
					<img src="{{ $postImg }}" />
				</div>
				<div class="boxyDesignTxt">
					<h3>@if(session()->get('url') == 'en')  {{ $postdata->title_en }} @else {{ $postdata->title_ur }} @endif </h3>
					@if(session()->get('url') == 'en') {!! $postdata->description_en !!} @elseif (session()->get('url') == 'ur') {!! $postdata->description_ur !!} @endif
					<a class="blueBtn" href="{{ url(session()->get('url').$postdata->link_en) }}">View all open vacancies <img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></a>
					<!-- <a class="blueBtn" href="javascript:;">Takaful <img src="images/icons/newtab.svg" uk-svg /></a> -->
				</div>
			</div>
		</div>
	</section>
	@endif
@endforeach
<!-- Section End -->

<!-- Section Start -->
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
		<div class="innerPageContent">
			<h4 class="BlockHeading">Explore our departments</h4>
			<p>Tell us about your ideal role and we will suggest some departments to explore. <br/> Start typing below and select a match.</p>
		</div>
        <div class="searchList SvgBlue">
			<form class="uk-search uk-search-default">
			    <div class="formInput">
                    <span uk-search-icon></span>
                    <input class="uk-search-input" type="search" placeholder="Search for articles" />
                </div>

               
                
                <div class="uk-margin-medium FormBtn">
                <a class="" href="careers-all-functions.php"><u>See all departments </u> <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></a>
                </div> 
			</form>
		</div>
	</div>
</section>
<!-- Section End -->




<!-- Section Start -->
@foreach($postsdata as $key => $postdata)
    @php 
        $postImg =  getImageFile($device_type,$postdata,"image","banner");
    @endphp
    @if($key == 0)
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <!-- <div class="uk-width-1-2@m">
                <div class="ContentImage">
                    <img src="images/career/Con1.jpg"/>
                </div>
            </div>    
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Experienced Professionals</h5>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed  <br/> diam nonumy eirmod tempor invidunt ut labore et dolore <br/> magna aliquyam erat, sed diam voluptua.</p>
                            <a class="blueBtn" href="javascript:;">Learn more <img src="images/icons/chevron_right.svg" uk-svg /></a>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>@if(session()->get('url') == 'en') {{$postdata->title_en}} @elseif (session()->get('url') == 'ur') {{$postdata->title_ur}} @endif</h5>
							@if(session()->get('url') == 'en') {!! $postdata->description_en !!} @elseif (session()->get('url') == 'ur') {!! $postdata->description_ur !!} @endif
                            <!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing <br/> elitr, sed diam nonumy eirmod tempor invidunt ut <br/> labore et dolore magna aliquyam erat, sed diam  <br/>voluptua. At vero</p> -->
                            <!-- <a class="blueBtn" href="careers-enableship-internship-program.php">Learn more <img src="images/icons/chevron_right.svg" uk-svg /></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-2@m">
				<div class="mdl">
                    <div class="mdl_inner">
						<div class="ContentImage">
							<img src="{{ $postImg }}"/>
						</div>
					</div>
				</div>		
            </div>    
        </div>
	</div>
</section>
@endif
@endforeach
<!-- Section End -->

<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
        <div class="innerPageContent uk-margin-medium-bottom">
			<h4 class="BlockHeading">Adamjee Lifestyle</h4>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, <br/> sed diam nonumy eirmod tempor invidunt ut labore et</p> -->
		</div>
		<ul class="uk-grid-small uk-marign-medium-top" uk-grid>
			@foreach($innerPages as $key => $innerPage)
				@php 
					$postImg =  getImageFile($device_type,$innerPage,"image","banner");
				@endphp

					<li class="{{ $innerPage->grid_class_en }}">
						<div class="GridBox" style="background-image: url({{ $postImg }});">
							<div class="GridBoxTxt">
								<h3>{{ $innerPage->title_en }}</h3>
								<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
								<a class="blueBtn" href="{{ url(session()->get('url').$innerPage->path) }}">Learn more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /> </a>
							</div>
						</div>
					</li>
			@endforeach
		</ul>
		
	</div>
</section>
<!-- Section Start -->

		{{-- @if($key == 2) --}}
			{{-- <li class="{{ $innerPage->grid_class_en }}">
				<div class="GridBox" style="background-image: url({{ $postImg }});">
					<div class="GridBoxTxt">
						<h3>{{ $innerPage->title_en }}</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="{{ $innerPage->path }}">Learn more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /> </a>
					</div>
				</div>
			</li> --}}
		{{-- @endif --}}
			{{-- <li class="uk-width-1-2@m">
				<div class="GridBox" style="background-image: url(images/career/grid2.jpg);">
					<div class="GridBoxTxt">
						<h3>Cricket Match</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="careers-cricket-match.php">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-1">
				<div class="GridBox" style="background-image: url(images/career/grid3.jpg);">
					<div class="GridBoxTxt">
						<h3>Organizational Development</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="javascript:;">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-2@m">
				<div class="GridBox" style="background-image: url(images/career/grid4.jpg);">
					<div class="GridBoxTxt">
						<h3>Women's Day</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="window-takaful-operations.php">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-2@m">
				<div class="GridBox" style="background-image: url(images/career/grid5.jpg);">
					<div class="GridBoxTxt">
						<h3>Town-Hall</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="csr.php">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
            <li class="uk-width-1-1">
				<div class="GridBox" style="background-image: url(images/career/grid3.jpg);">
					<div class="GridBoxTxt">
						<h3>Employee Engagement Activities</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="javascript:;">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
            
		
@endforeach --}}
<!-- Section End -->


<!-- Section Start -->
<!-- <section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="ContentImage">
                    <img src="images/career/Con1.jpg"/>
                </div>
            </div>    
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Message from the <br/> Head of HR</h5>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing <br/> elitr, sed diam nonumy eirmod tempor invidunt ut <br/> labore et dolore magna aliquyam erat, sed diam  <br/> voluptua. At vero eos et accusam et justo duo dolores <br/> et ea rebum. Stet clita kasd gubergren, no sea <br/> takimata sanctus est Lorem ipsum dolor sit amet. <br/> Lorem ipsum dolor sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section> -->
<!-- Section End -->


<!-- Section Start -->
<!-- <section class="SecWrap SecTopSpace coverSection" style="background-image:url(images/career/sectionmain.jpg)">
	<div class="uk-container containCustom">
		<div  uk-grid>
            <div class="uk-width-1-1">
            <div class="CoverBgContent">
				<h3>Ready to apply?</h3>
				<p>If you’re looking for a career with purpose and want to work for a bank making a difference, we’d love to hear from you.</p>
				<a class="transparentbtn" href="javascript:;">View all open vacancies <img src="images/icons/chevron_right.svg" uk-svg /></a>
				<a class="blueBtn" href="javascript:;">Search jobs <img src="images/icons/chevron_right.svg" uk-svg /></a>
			</div>
            </div>    
        </div>
	</div>
</section> -->
<!-- Section End -->





<!-- Section Start -->
@foreach($postsdata as $key => $postdata)
		@php 
			$postImg =  getImageFile($device_type,$postdata,"image","banner");
		@endphp

		@if($key == 2)
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                           <h5>@if(session()->get('url') == 'en')  {{ $postdata->title_en }} @else {{ $postdata->title_ur }} @endif </h5>
							@if(session()->get('url') == 'en') {!! $postdata->description_en !!} @elseif (session()->get('url') == 'ur') {!! $postdata->description_ur !!} @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-2@m">
                <div class="ContentImage">
                    <img src="{{ $postImg }}"/>
                </div>
            </div>  
        </div>
	</div>
</section>
@endif
@endforeach
<!-- Section End -->




<!-- Section Start -->
@foreach($postsdata as $key => $postdata)
		@php 
			$postImg =  getImageFile($device_type,$postdata,"file","banner");
		@endphp

@if($key == 3)
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
		<div class="innerPageContent">
			<h3>{{$postdata->title_en}}</h3>
			{!! $postdata->description_en !!}

            <div class="DownloadList">
			<ul>
				<li><a href="{{ $postImg }}" download>{{$postdata->title_en}} (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
			</ul>
		</div>
		    <!-- <p>With a strong and highly trained team of relationship managers and Financial Advisors, state of the art policy management and claims processing system and strong financial backing Adamjee life is geared to provide complete and exclusive round the clock service to its customers and clients. Our long term commitment ensures that the needs of our customers are always taken into account and is our prime consideration. Adamjee Life is redefining the life insurance paradigm by focusing on customers first. The service process is responsive, personalized, humane and compassionate and is developed to provide complete peace of mind to our clients. So that you can focus on what you do best and leave the rest to us.</p> -->
		</div>
	</div>
</section>
@endif

<!-- Section End -->

<!-- Section Start -->
@if($key == 4)
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>{{$postdata->title_en}}</h5>
                            {!! $postdata->description_en !!}
							<a class="blueBtn" href="corporate-governance-content-critical.php">{{$postdata->title_en}}<img src="{{asset('public/website/images/right.svg')}}" uk-svg /> </a>
						</div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
@endif
@endforeach
<!-- Section End -->


<!-- Section Start -->
<!-- <section class="SecWrap SecTopSpace coverSection" style="background-color:#999">
	<div class="uk-container containCustom">
		<div  uk-grid>
            <div class="uk-width-1-1">
            <div class="CoverBgContent">
				<h3>Ready to apply?</h3>
				<p>If you’re looking for a career with purpose and want to work for a bank making a difference, we’d love to hear from you.</p>
				<a class="transparentbtn" href="javascript:;">View all open vacancies <img src="images/icons/chevron_right.svg" uk-svg /></a>
		
			</div>
            </div>    
        </div>
	</div>
</section> -->
<!-- Section End -->


<!-- Section Start -->
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-1@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Sign up for job alerts</h5>
                            <p>Make sure you see job opportunities when they become  available. Just leave a few details below to stay <br/> up to date with jobs that suit you and your skills.</p>
                        </div>
						<form>
							<div class="Employee-Form_main">
								<h5></h5>
								<div class="uk-margin category" id="category">
									<h6>Select Job Category</h6>
									<label><input class="uk-checkbox" type="checkbox" name="category" checked> Design</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Marketing</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Finance</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Admin</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Actuary</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Sales agent</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Claims adjuster</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Underwriter</label>
								</div>

								<div class="uk-margin category" id="city">
									<h6>Select City</h6>
									<label><input class="uk-checkbox" type="checkbox" name="city" > Karachi</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Lahore</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Islamabad</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Multan</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Faisalabad</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Peshawar</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Other</label>
								</div>

								<div class="uk-margin email">
									<label>Email address</label>
									<input class="uk-input" type="text" placeholder="What's your email"> 
								</div>

								<div class="uk-margin email">
									<!-- <label>Email address</label> -->
									<button type="submit" class="blueBtn">Sign up for alerts<img src="{{asset('public/website/images/right.svg')}}" uk-svg /> </a>
								</div>
							</div>	
						</form>
					</div>
                </div>
            </div>  
        </div>
	</div>
</section>
<!-- Section End -->
@include('partials.footer')
@endif