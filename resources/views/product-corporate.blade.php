@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        @php 
            if(isset($_GET['type'])){
                $type = $_GET['type'];
                if($type == "takaful"){
                    $postImg =  getImageFile($device_type,$pagedata,"image","inner");
                } else {
                    $postImg =  getImageFile($device_type,$pagedata,"image","banner");
                }
            } else {
                $type = "";
                $postImg =  getImageFile($device_type,$pagedata,"image","banner");
            }
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->sub_title_en}}</h1>
                    {!! $pagedata->description_en !!}
                </div>
            </div>
        </section>
    @endforeach

    @if(isset($innerPages))
        @foreach($innerPages as $innerkey => $innerPage)
            <section class="SecWrap SecTopSpace">
                <div class="uk-container containCustom">
                    <div class="innerPageContent2">
                        <h2>@if(session()->get('url') == 'en') {{$innerPage->title_en}} @elseif (session()->get('url') == 'ur') {{$innerPage->title_ur}} @endif</h2>
                        <div class="NewsFilter">
                            <form>
                                <select class="uk-select" id="type" name="type">
                                    @foreach($innerPage->child as $key => $childInnerPage)
                                        @if(str_contains(ucfirst($type),$childInnerPage->title_en) == true)
                                            <option value="{{ substr($childInnerPage->slug,1) }}" selected>{{ $childInnerPage->title_en }}</option>
                                        @else
                                            <option value="{{ substr($childInnerPage->slug,1) }}">{{ $childInnerPage->title_en }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <select class="uk-select" id="subtype" name="subtype">
                                    <option value="individual-life">Individual Life</option>
                                    <option value="bancassurance">Bancassurance</option>
                                    <option value="corporate" selected>Corporate</option>
                                    <option value="online">Online</option>
                                </select>
                                <input type="button" onClick="FilterClick()" class="blueBtn" value="Filter"/> 
                            </form>
                        </div>
                    </div>    
                    <div class="NewsSec">
                        <ul class="uk-grid-medium" uk-grid  uk-height-match=".uk-card-body">
                            @foreach($innerPage->child as $key => $childInnerPage)
                                @if(ucfirst($type) == $childInnerPage->title_en)
                                    @if($innerkey == 0)
                                        @foreach($childInnerPage->article as $childkey => $article)
                                            @php 
                                                $postImg =  getImageFile($device_type,$article,"image","banner");
                                            @endphp
                                            <li class="uk-width-1-2@m" data-individual="{{ substr($childInnerPage->slug,1) }}">
                                                <div class="uk-card uk-card-default newsCard">
                                                    <div class="uk-card-media-top">
                                                        <img src="{{ $postImg }}" alt="">
                                                    </div>
                                                    <div class="uk-card-body">
                                                        <h3>@if(session()->get('url') == 'en') {{$article->title_en}} @elseif (session()->get('url') == 'ur') {{$article->title_ur}} @endif</h3>
                                                        <ul class="productContentList">
                                                            {!! $article->description_en !!}                                                        
                                                        </ul>
                                                        @foreach($article->custompost as $customKey => $custompost)
                                                            @php 
                                                                $file =  getImageFile($device_type,$custompost,"customfile","banner");
                                                            @endphp
                                                            @if($customKey == 0)
                                                                <a href="{{$custompost->url}}" target="_blank" class="blueBtn">{{$custompost->name}} <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
                                                            @elseif($customKey == 1)
                                                                @if(!empty(json_decode($custompost->file)))
                                                                    <a href="{{ $file }}" class="dateNews" target="_blank"><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> {{$custompost->name}}</a>
                                                                @endif
                                                            @endif
                                                        @endforeach 
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                @else
                                    @if($innerkey == 0 && $type == "" && $key == 0)
                                        @foreach($childInnerPage->article as $childkey => $article)
                                            @php 
                                                $postImg =  getImageFile($device_type,$article,"image","banner");
                                            @endphp
                                            <li class="uk-width-1-2@m" data-individual="{{ substr($childInnerPage->slug,1) }}">
                                                <div class="uk-card uk-card-default newsCard">
                                                    <div class="uk-card-media-top">
                                                        <img src="{{ $postImg }}" alt="">
                                                    </div>
                                                    <div class="uk-card-body">
                                                        <h3>@if(session()->get('url') == 'en') {{$article->title_en}} @elseif (session()->get('url') == 'ur') {{$article->title_ur}} @endif</h3>
                                                        <ul class="productContentList">
                                                            {!! $article->description_en !!}                                                        
                                                        </ul>
                                                        @foreach($article->custompost as $customKey => $custompost)
                                                            @php 
                                                                $file =  getImageFile($device_type,$custompost,"customfile","banner");
                                                            @endphp
                                                            @if($customKey == 0)
                                                                <a href="{{$custompost->url}}" target="_blank" class="blueBtn">{{$custompost->name}} <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
                                                            @elseif($customKey == 1)
                                                                @if(!empty(json_decode($custompost->file)))
                                                                    <a href="{{ $file }}" class="dateNews" target="_blank"><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> {{$custompost->name}}</a>
                                                                @endif
                                                            @endif
                                                        @endforeach 
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </section>
        @endforeach
    @endif

    <script>
        function FilterClick(){
            let type = $("#type").val();
            let subtype = $("#subtype").val();
            var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';
            if(window.location.pathname.includes(subtype)){
                window.location.href = base_url+'<?php echo session()->get('url') ?>'+'/'+subtype+`?type=${type}&subtype=${subtype}`;
            } else {
                window.location.href = base_url+'<?php echo session()->get('url') ?>'+'/'+subtype+`?type=${type}&subtype=${subtype}`;
            }
        }
    </script>

    @include('partials.footer')
@endif