<?php include('header.php'); ?>
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="images/career/banner.jpg" />
	<div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="javascript:;">Careers</a></li>
		    <li><span>Content Critical</span></li>
		</ul>
	</div>
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>Careers</h1>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur</p> -->
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace SecBottomSpace">
	<div class="uk-container containCustom">
		<div class="boxyDesign">
			<div class="boxyDesignImg">
				<img src="images/career/1.jpg" />
			</div>
			<div class="boxyDesignTxt">
				<h3>Expand your horizons</h3>
				<p>If you’re looking for a career with purpose and want to work for a company making a difference, we’d love to hear from you.</p>
				<a class="blueBtn" href="careers-open-vacancy.php">View all open vacancies <img src="images/icons/chevron_right.svg" uk-svg /></a>
				<!-- <a class="blueBtn" href="javascript:;">Takaful <img src="images/icons/newtab.svg" uk-svg /></a> -->
			</div>
		</div>
	</div>
</section>
<!-- Section End -->

<!-- Section Start -->
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
		<div class="innerPageContent">
			<h4 class="BlockHeading">Explore our departments</h4>
			<p>Tell us about your ideal role and we will suggest some departments to explore. <br/> Start typing below and select a match.</p>
		</div>
        <div class="searchList SvgBlue">
			<form class="uk-search uk-search-default">
			    <div class="formInput">
                    <span uk-search-icon></span>
                    <input class="uk-search-input" type="search" placeholder="Search for articles" />
                </div>

               
                
                <div class="uk-margin-medium FormBtn">
                <a class="" href="careers-all-functions.php"><u>See all departments </u> <img src="images/right.svg" uk-svg /></a>
                </div> 
			</form>
		</div>
	</div>
</section>
<!-- Section End -->



<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <!-- <div class="uk-width-1-2@m">
                <div class="ContentImage">
                    <img src="images/career/Con1.jpg"/>
                </div>
            </div>    
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Experienced Professionals</h5>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed  <br/> diam nonumy eirmod tempor invidunt ut labore et dolore <br/> magna aliquyam erat, sed diam voluptua.</p>
                            <a class="blueBtn" href="javascript:;">Learn more <img src="images/icons/chevron_right.svg" uk-svg /></a>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Enableship  Program</h5>
							<p>Adamjee Life introduced its first ever Summer Internship Program named Enableship Program, to help cultivate young talent and boost AL's presence in the industry. Enableship basically accumulates the feel of a traditional internship with additional skill development and understanding that will help mold these young talents into future industry leaders.</p>

							<p>Through this program, the interns are not overwhelmed by being singled out. Here they form a sort of bond with each other, to help, grow and shine as team and as individuals. By the end of this program these young minds will be strategic thinkers and sustainable action takers through their learnings.</p>

							<p>The team of young enablers have been assigned 3 mentors from the different departments to help these young talents grow and feel a sense of fulfillment and accomplishment at the end of the whole Enableship program.</p>
                            <!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing <br/> elitr, sed diam nonumy eirmod tempor invidunt ut <br/> labore et dolore magna aliquyam erat, sed diam  <br/>voluptua. At vero</p> -->
                            <!-- <a class="blueBtn" href="careers-enableship-internship-program.php">Learn more <img src="images/icons/chevron_right.svg" uk-svg /></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-2@m">
				<div class="mdl">
                    <div class="mdl_inner">
						<div class="ContentImage">
							<img src="images/career/Con2.jpg"/>
						</div>
					</div>
				</div>		
            </div>    
        </div>
	</div>
</section>
<!-- Section End -->

<!-- Section Start -->
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
        <div class="innerPageContent uk-margin-medium-bottom">
			<h4 class="BlockHeading">Adamjee Lifestyle</h4>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, <br/> sed diam nonumy eirmod tempor invidunt ut labore et</p> -->
		</div>
		<ul class="uk-grid-small uk-marign-medium-top" uk-grid>
            <li class="uk-width-1-1">
				<div class="GridBox" style="background-image: url(images/career/grid3.jpg);">
					<div class="GridBoxTxt">
						<h3>Careers Fairs 2022</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="javascript:;">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-2@m">
				<div class="GridBox" style="background-image: url(images/career/grid1.jpg);">
					<div class="GridBoxTxt">
						<h3>Social Impact</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="javascript:;">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-2@m">
				<div class="GridBox" style="background-image: url(images/career/grid2.jpg);">
					<div class="GridBoxTxt">
						<h3>Cricket Match</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="careers-cricket-match.php">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-1">
				<div class="GridBox" style="background-image: url(images/career/grid3.jpg);">
					<div class="GridBoxTxt">
						<h3>Organizational Development</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="javascript:;">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-2@m">
				<div class="GridBox" style="background-image: url(images/career/grid4.jpg);">
					<div class="GridBoxTxt">
						<h3>Women's Day</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="window-takaful-operations.php">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
			<li class="uk-width-1-2@m">
				<div class="GridBox" style="background-image: url(images/career/grid5.jpg);">
					<div class="GridBoxTxt">
						<h3>Town-Hall</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="csr.php">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
            <li class="uk-width-1-1">
				<div class="GridBox" style="background-image: url(images/career/grid3.jpg);">
					<div class="GridBoxTxt">
						<h3>Employee Engagement Activities</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p> -->
						<a class="blueBtn" href="javascript:;">Learn more <img src="images/right.svg" uk-svg /> </a>
					</div>
				</div>
			</li>
            
		</ul>
		
	</div>
</section>
<!-- Section End -->


<!-- Section Start -->
<!-- <section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="ContentImage">
                    <img src="images/career/Con1.jpg"/>
                </div>
            </div>    
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Message from the <br/> Head of HR</h5>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing <br/> elitr, sed diam nonumy eirmod tempor invidunt ut <br/> labore et dolore magna aliquyam erat, sed diam  <br/> voluptua. At vero eos et accusam et justo duo dolores <br/> et ea rebum. Stet clita kasd gubergren, no sea <br/> takimata sanctus est Lorem ipsum dolor sit amet. <br/> Lorem ipsum dolor sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section> -->
<!-- Section End -->


<!-- Section Start -->
<!-- <section class="SecWrap SecTopSpace coverSection" style="background-image:url(images/career/sectionmain.jpg)">
	<div class="uk-container containCustom">
		<div  uk-grid>
            <div class="uk-width-1-1">
            <div class="CoverBgContent">
				<h3>Ready to apply?</h3>
				<p>If you’re looking for a career with purpose and want to work for a bank making a difference, we’d love to hear from you.</p>
				<a class="transparentbtn" href="javascript:;">View all open vacancies <img src="images/icons/chevron_right.svg" uk-svg /></a>
				<a class="blueBtn" href="javascript:;">Search jobs <img src="images/icons/chevron_right.svg" uk-svg /></a>
			</div>
            </div>    
        </div>
	</div>
</section> -->
<!-- Section End -->


<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Recruitment scams & fraud warning</h5>
                            <p>We will never ask for the exchange of money or credit card details in the Recruitment process. Please be aware of any suspicious email activity from people who could be pretending to be recruiters or senior individuals at AdamjeeLife. If in doubt, please ignore the message.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-2@m">
                <div class="ContentImage">
                    <img src="images/career/Con2.jpg"/>
                </div>
            </div>  
        </div>
	</div>
</section>
<!-- Section End -->


<!-- Section Start -->
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
		<div class="innerPageContent">
			<h3>Whistleblowing policy</h3>
			<p>Whistle Blowing exposes information or activity within the organization that is deemed illegal, unethical, or not correct. In order to meet regulatory requirements of SECP to maintain confidentiality of information shared and anonymity of whistle blower, separate email addresses are created-independent of the Company’s domain. </p>
            <ul>
                <li>Whistles may be blown at whistleblow.adamjeelife@gmail.com which will be reviewed by HoHR.</li>
                <li>For concerns related to the HoHR and CEO, whistles shall be blown directly to Chairman of HR Committee Board of Directors at whistleblow.hrc@gmail.com</li>
            </ul>
            <p>For detailed report please click the button below :</p>

            <div class="DownloadList">
			<ul>
				<li><a href="javascript:;">Whistleblowing policy (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
			</ul>
		</div>
		    <!-- <p>With a strong and highly trained team of relationship managers and Financial Advisors, state of the art policy management and claims processing system and strong financial backing Adamjee life is geared to provide complete and exclusive round the clock service to its customers and clients. Our long term commitment ensures that the needs of our customers are always taken into account and is our prime consideration. Adamjee Life is redefining the life insurance paradigm by focusing on customers first. The service process is responsive, personalized, humane and compassionate and is developed to provide complete peace of mind to our clients. So that you can focus on what you do best and leave the rest to us.</p> -->
		</div>
	</div>
</section>
<!-- Section End -->

<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Code of conduct</h5>
                            <p>For detailed Code of Conduct and other policies please click the button below :</p>
							<a class="blueBtn" href="corporate-governance-content-critical.php">Code of conduct<img src="images/right.svg" uk-svg /> </a>
						</div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!-- Section End -->


<!-- Section Start -->
<!-- <section class="SecWrap SecTopSpace coverSection" style="background-color:#999">
	<div class="uk-container containCustom">
		<div  uk-grid>
            <div class="uk-width-1-1">
            <div class="CoverBgContent">
				<h3>Ready to apply?</h3>
				<p>If you’re looking for a career with purpose and want to work for a bank making a difference, we’d love to hear from you.</p>
				<a class="transparentbtn" href="javascript:;">View all open vacancies <img src="images/icons/chevron_right.svg" uk-svg /></a>
		
			</div>
            </div>    
        </div>
	</div>
</section> -->
<!-- Section End -->


<!-- Section Start -->
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
		<div class="uk-grid-large" uk-grid>
            <div class="uk-width-1-1@m">
                <div class="mdl">
                    <div class="mdl_inner">
                        <div class="Content">
                            <h5>Sign up for job alerts</h5>
                            <p>Make sure you see job opportunities when they become  available. Just leave a few details below to stay <br/> up to date with jobs that suit you and your skills.</p>
                        </div>
						<form>
							<div class="Employee-Form_main">
								<h5></h5>
								<div class="uk-margin category" id="category">
									<h6>Select Job Category</h6>
									<label><input class="uk-checkbox" type="checkbox" name="category" checked> Design</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Marketing</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Finance</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Admin</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Actuary</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Sales agent</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Claims adjuster</label>
									<label><input class="uk-checkbox" type="checkbox" name="category"> Underwriter</label>
								</div>

								<div class="uk-margin category" id="city">
									<h6>Select City</h6>
									<label><input class="uk-checkbox" type="checkbox" name="city" > Karachi</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Lahore</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Islamabad</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Multan</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Faisalabad</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Peshawar</label>
									<label><input class="uk-checkbox" type="checkbox" name="city"> Other</label>
								</div>

								<div class="uk-margin email">
									<label>Email address</label>
									<input class="uk-input" type="text" placeholder="What's your email"> 
								</div>

								<div class="uk-margin email">
									<!-- <label>Email address</label> -->
									<button type="submit" class="blueBtn">Sign up for alerts<img src="images/right.svg" uk-svg /> </a>
								</div>
							</div>	
						</form>
					</div>
                </div>
            </div>  
        </div>
	</div>
</section>
<!-- Section End -->
<?php include('footer.php'); ?>