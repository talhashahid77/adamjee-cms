<?php include('header.php'); ?>
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="images/awardsachievements/inside/banner.jpg" />
	<div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="about-us.php">About us</a></li>
		    <li><a href="awards-and-achievements.php">Awards & Achievements</a></li>
		    <li><span>Adamjee Life partners with Govt. of Sindh to Improve Healthcare Infrastructure</span></li>
		</ul>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap ovverflowAuto">
	<div class="uk-container containCustom">
		<div class="detailsPage">
			<h1>Adamjee Life partners with Govt. of Sindh to Improve Healthcare Infrastructure</h1>
			<h6>Nov 01st, 2021</h6>
			<p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province. This initiative is part of the Nigehbaan CSR platform of Adamjee Life that has led various education, environmental and health programs in the past.</p> 
			<p>Adamjee Life has always been a strong advocate of advancing Pakistan’s Sustainable Development Goals (SGD’s) and has pledged to provide assistance to the state in other such initiatives as well. During the unveiling of the renovated facility, Mr. Ahson Nasim, GM HR, General and Corporate affairs of Adamjee Life received a token of gratitude from District Health Officer Central Dr. Bashir Ahmed Mangi. Following the unveiling of the new facility, representatives from both sides paid a visit to the facility.</p> 
			<p>Speaking on the occasion Ahson Nasim, GM HR, General and Corporate affairs – Adamjee Life stated, “As a socially responsible entity, we feel this initiative is our national duty and a symbol of our commitment to the country. We are deeply grateful to the Government of Sindh for their support in making this collaboration a success. We hope that this initiative will result in a measurable impact and help citizens receive prompt healthcare facilities.”</p> 
			<p>Dr. Ahmed Mangi, DHO Central – Karachi stated, “In Collaboration with the Government of Sindh & Adamjee Life, “This renovation should benefit not only our patients but our staff as well. We know improved continuity of care will be very valuable to our beneficiaries and we are very thankful to Adamjee Life for their pledge as support.”</p> 
			<p>Adamjee Life will continue to play its due role towards safeguarding the safety and health of the people of Pakistan and will continue to contribute positively to the society at large.</p>

			<ul class="uk-grid-small uk-flex uk-flex-center uk-child-width-1-5@m uk-child-width-1-3@s uk-child-width-1-2 uk-margin-large-top" uk-grid uk-lightbox>
			    <li class="uk-active"><a href="images/awardsachievements/inside/1.jpg"><img src="images/awardsachievements/inside/1.jpg" alt=""></a></li>
			    <li><a href="images/awardsachievements/inside/2.jpg"><img src="images/awardsachievements/inside/2.jpg" alt=""></a></li>
			    <li><a href="images/awardsachievements/inside/3.jpg"><img src="images/awardsachievements/inside/3.jpg" alt=""></a></li>
			</ul>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace" style="background: #fff;">
	<div class="uk-container containCustom">
		<div class="innerPageContent2">
			<h2>More Awards & Achievements <a href="javascript:;">See all</a></h2>
			<div class="NewsSec">
				<ul uk-grid uk-height-match=".uk-card-body">
					<!-- Card Start -->
		    		<li class="uk-width-1-2@m">
	    				<a href="news-inner.php" class="uk-card uk-card-default newsCard">
				            <div class="uk-card-media-top">
				                <img src="images/awardsachievements/1.jpg" alt="">
				            </div>
				            <div class="uk-card-body">
				                <h3>Adamjee Life partners with Govt. of Sindh to Improve Healthcare Infrastructure</h3>
				                <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p>
				                <span class="blueBtn">Read more <img src="images/right.svg" uk-svg /></span>
				                <span class="dateNews">2nd April 2022</span>
				            </div>
		    			</a>
		    		</li>
		    		<!-- Card End -->
		    		<!-- Card Start -->
		    		<li class="uk-width-1-2@m">
	    				<a href="news-inner.php" class="uk-card uk-card-default newsCard">
				            <div class="uk-card-media-top">
				                <img src="images/awardsachievements/2.jpg" alt="">
				            </div>
				            <div class="uk-card-body">
				                <h3>Adamjee Life wins 15th Consumer Choice Award 2021</h3>
				                <p>Adamjee Life, one of Pakistan's most recognized life insurance companies, received the coveted "Icon Award" for "Excellence in Customer Service" at the Consumers Association of Pakistan's 15th Consumers Choice Awards 2021 in Karachi.</p>
				                <span class="blueBtn">Read more <img src="images/right.svg" uk-svg /></span>
				                <span class="dateNews">27th May 2022</span>
				            </div>
		    			</a>
		    		</li>
		    		<!-- Card End -->
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- Section End -->
<?php include('footer.php'); ?>