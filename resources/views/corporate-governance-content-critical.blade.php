@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php
        if(session()->get('url') == "en"){
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_en); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_en); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_en); 
            }
        } else { 
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_ur); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_ur); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_ur); 
            }
        }
        if(isset($postImageBanner[1])){
            $Img = $postImageBanner[1];
        }else{
            $Img = "";
        }
        $postImg = URL::to('/')."/public/source/".$Img."";
        $url = URL::to('/').session()->get('url');
    @endphp
	<!-- Section Start -->
	<section class="HeaderInnerPage">
		<img src="{{ $postImg }}" />
		@include('partials.breadcrumb')
		<div class="HeaderInnerTxt">
			<div class="uk-container containCustom">
				<h1>{{ $pagedata->title_en}}</h1>
				{!! $pagedata->description_en !!}
			</div>
		</div>
	</section>
@endforeach
<!-- Section End -->

@foreach($postsdata as $key => $postdata)
    @php 
        if(session()->get('url') == "en"){
            if($device_type == "mobile"){
                $postImageSection = json_decode($postdata->image_mobile_en);
                if(count($postImageSection) == 0){
                    $postImageSection = json_decode($postdata->image_en); 
                } 
            } else{
                $postImageSection = json_decode($postdata->image_en); 
            }
        } else { 
            if($device_type == "mobile"){
                $postImageSection = json_decode($postdata->image_mobile_ur); 
                if(count($postImageSection) == 0){
                    $postImageSection = json_decode($postdata->image_ur); 
                } 
            } else{
                $postImageSection = json_decode($postdata->image_ur); 
            }
        }

        if(isset($postImageSection[0])){
            $Img = $postImageSection[0];
        }else{
            $Img = "";
        }
        $postImg = URL::to('/')."/public/source/".$Img."";
        $url = URL::to('/').session()->get('url');

        
    @endphp
	<!-- Section Start -->
	<section class="SecWrap uk-padding-large">
		<div class="uk-container containCustom">

			<div class="boxyDesign">
				<div class="boxyDesignImg">
					<img src="{{ $postImg }}" />
				</div>
				<div class="boxyDesignTxt">
					<h3>@if(session()->get('url') == 'en') {{$postdata->title_en}} @elseif (session()->get('url') == 'ur') {{$postdata->title_ur}} @endif</h3>
					<!-- <h4>Vision</h4> -->
					@if(session()->get('url') == 'en') {!! $postdata->description_en !!} @elseif (session()->get('url') == 'ur') {!! $postdata->description_ur !!} @endif
				
					<a class="blueBtn" href="{{ url(session()->get('url').$postdata->link_en) }}">View profiles<img src="{{asset('public/website/images/right.svg')}}" uk-svg /></a>
				</div>
				
			</div>
		</div>
	</section>
	<!-- Section End -->
@endforeach


<!-- Section Start -->
<section class="SecWrap SecTopSpace whiteTabsSec">
	<div class="uk-container containCustom">
		<!-- <h2>Funds</h2> -->
		<div class="tabsSec">
			<!-- This is the nav containing the toggling elements -->
			<ul class="tabBtn2" uk-switcher="connect: .TabBox2">
				@foreach($innerPages as $innerPage)
			    	<li><a href="javascript:;">{{ $innerPage->title_en }}</a></li>
				@endforeach
			</ul>
			
			<!-- This is the container of the content items -->
			<ul class="uk-switcher TabBox2">
				@foreach($innerPages as $innerPage)
					<li>
						@if($innerPage->sorting == 1 || $innerPage->sorting == 2)
							@foreach($innerPage->article as $key=>$article)
								@php 
									$postImg =  getImageFile($device_type,$article,"file","banner");
								@endphp
								<div class="tabsContent">
									@if($innerPage->sorting == 1)
										<div class="NewsFilter">
											<form>
												<select class="uk-select">
													<option>Year</option>
													<option>2022</option>
													<option>2021</option>
													<option>2020</option>
													<option>2019</option>
												</select>
											</form>
										</div>
									@endif
									<div class="DownloadList">
										<ul>
											<li><a href="{{ $postImg }}" download>{{ $article->title_en }}<img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
										</ul>
									</div>
								</div>
							@endforeach
						@endif

						@if($innerPage->sorting == 3)
							<div class="tabsContent">
								<div>
									<ul uk-grid uk-height-match=".uk-card-body">
										@foreach($innerPage->article as $key=>$article)
											<li class="uk-width-1-2@s uk-width-1-4@m">
												<a href="javascript:;"class="uk-card uk-card-default tabInner CorporateCard">
													<div class="uk-card-body">
														<h3>{{$article->title_en}}</h3>
														{!! $article->description_en !!}
													</div>
												</a>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						@endif
						@if($innerPage->sorting == 4)
							<div class="tabsContent">
								<div class="NewsFilter">
									<form>
										<select class="uk-select">
											@foreach($innerPage->child as $key=>$child)
												<option value="{{ substr($child->slug,1) }}">{{ $child->title_en }}</option>
											@endforeach
										</select>
									</form>
								</div>
								<div>
									<ul uk-grid uk-height-match=".uk-card-body">
										@foreach($innerPage->child as $key=>$child)
											@foreach($child->article as $key=>$articleCommity)
												<li class="uk-width-1-2@s uk-width-1-4@m">
													<a href="{{ url(session()->get('url').$articleCommity->link_en)}}" class="uk-card uk-card-default tabInner CorporateCard">
														<div class="uk-card-body">
															<h3>{{$articleCommity->title_en}}</h3>
															{!! $articleCommity->description_en !!}
														</div>
													</a>
												</li>
											@endforeach
										@endforeach
									</ul>
								</div>
							</div>
						@endif

						@if($innerPage->sorting == 5)
							<div class="tabsContent">
								<div class="DownloadList">
									<ul>
										@foreach($innerPage->article as $key=>$article)
											@php 
												$postImg =  getImageFile($device_type,$article,"file","banner");
											@endphp
										<li><a href="{{ $postImg }}" download>{{ $article->title_en }}<img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
										@endforeach
									</ul>
								</div>
							</div>
						@endif
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</section>
<!-- Section End -->

@include('partials.footer')
@endif