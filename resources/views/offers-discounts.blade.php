@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","inner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->sub_title_en}}</h1>
                    {!! $pagedata->description_en !!}
                </div>
            </div>
        </section>
    @endforeach
    @if(isset($innerPages))
        <section class="SecWrap SecTopSpace">
            <div class="uk-container containCustom">
                <div class="NewsFilter ">

                    <form>
                        <div class="uk-form-controls GuideSearchInput">
                            <input class="uk-input" id="email" type="text" placeholder="Enter keyword">
                        </div>
                        <div class="uk-form-controls">
                        <select class="uk-select">
                            <option value="">Select</option>
                        </select>
                        </div>
                        <div class="uk-form-controls">
                        <select class="uk-select">
                            <option value="">Select</option>
                        </select>
                        </div>
                        <div class="uk-form-controls">
                        <select class="uk-select">
                            @foreach($innerPages as $innerkey => $innerPage)
                                <option value="{{ substr($innerPage->slug,1) }}">{{ $innerPage->title_en }}</option>
                            @endforeach
                        </select>
                        </div>
                        <button class="blueBtn">Filter</button>
                    </form>
                </div>
                <div class="innerPageContent2">
                    <h2>All results</h2>
                </div>
                    <ul class="uk-grid-small" uk-grid uk-height-match=".uk-card-body">
                        @foreach($innerPages as $innerPagekey => $innerPage)
                            {{-- @if($innerPagekey == 0) --}}
                                @foreach($innerPage->child as $innerchildkey => $child)
                                    @foreach($child->child as $childkey => $innerChild)
                                        @php 
                                            $postImg =  getImageFile($device_type,$innerChild,"image","banner");
                                        @endphp
                                        <li class="uk-width-1-4@m uk-width-1-2@s">
                                            <a href="{{ url(session()->get('url').$innerChild->path) }}" class="uk-card uk-card-default newsCard discounts">
                                                <div class="uk-card-media-top discountBadge">
                                                    <img src="{{ $postImg }}" alt="" />
                                                    @foreach($innerChild->custompost as $customkey => $custompost)
                                                        @if($customkey == 0)
                                                            <div class="badgeBox">{{ $custompost->name }}</div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                                
                                                <div class=" uk-card-body ">
                                                    <div class="badgesBar badgesBarDiscount">
                                                        @foreach($innerChild->custompost as $customkey => $custompost)
                                                            @if($customkey > 0 && $customkey < 3)
                                                                <div class="badgeBox">{{ $custompost->name }}</div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <h3>{{ $innerChild->title_en }}</h3>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                @endforeach
                            {{-- @endif --}}
                        @endforeach
                       
                    </ul>
                    <div class="see-all uk-margin-medium-top">
                        <a><u>See All</u></a>
                    </div>
                </div>    
            </div>
        </section>
    @endif
    @include('partials.footer')
@endif