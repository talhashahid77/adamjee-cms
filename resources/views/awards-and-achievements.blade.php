@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php 
        $postImg = getImageFile($device_type,$pagedata,"image","inner");
    @endphp
    <section class="HeaderInnerPage">
        <img src="{{ $postImg }}" />
        @include('partials.breadcrumb')
        <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
            <div class="uk-container containCustom">
                <h1>{{ $pagedata->title_en}}</h1>
                {!! $pagedata->description_en !!}
            </div>
        </div>
    </section>
@endforeach

<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="NewsFilter">
			<form>
			    <select class="uk-select">
			        <option>2022</option>
			        <option>2021</option>
			        <option>2020</option>
			        <option>2019</option>
			    </select>
			    <select class="uk-select">
			        <option>Latest first</option>
			        <option>Oldest first</option>
			    </select>
			</form>
		</div>
		<div class="NewsSec">
			<ul uk-grid uk-height-match=".uk-card-body">
				<!-- Card Start -->
                @foreach($innerPages as $key => $innerPage)
                    @php 
                        $postImg = getImageFile($device_type,$innerPage,"image","banner");
                    @endphp
                    <li class="uk-width-1-2@m">
                        <a href="{{ url(session()->get('url').$innerPage->path) }}" class="uk-card uk-card-default newsCard">
                            <div class="uk-card-media-top">
                                <img src="{{ $postImg }}" alt="">
                            </div>
                            <div class="uk-card-body">
                                <h3>@if(session()->get('url') == 'en') {{$innerPage->title_en}} @elseif (session()->get('url') == 'ur') {{$innerPage->title_ur}} @endif</h3>
                                @if(session()->get('url') == 'en') {!! $innerPage->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerPage->description_ur !!} @endif
                                <span class="blueBtn">Read more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></span>
                                <span class="dateNews">{{date('d F Y', strtotime($innerPage->created_at)) }}</span>
                            </div>
                        </a>
                    </li>
                @endforeach
	    		<!-- Card End -->
			</ul>
		</div>

	</div>
</section>
<!-- Section End -->

@include('partials.footer')
@endif