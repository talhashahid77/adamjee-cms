{{-- @dd($pagesdata); --}}
{{-- @dd($postsdesc); --}}


@include('partials.header')
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="{{asset('public/website/images/bod/bodbanner/1.jpg')}}" />
	<div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="about-us.php">About us</a></li>
		    <li><a href="bod-and-management.php">BODs & Management</a></li>
		    <li><span>Mr. Manzar Mushtaq</span></li>
		</ul>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap ovverflowAuto">
	<div class="uk-container containCustom">
		<div class="detailsPage">

			@foreach($pagesdata as $pagesdataa)
			<h1>{{$pagesdataa->title_en}}</h1>
			<h6>{{$pagesdataa->sub_title_en}}</h6>	
			 {!! $pagesdataa->short_desc_en !!}
			{{-- <p>Mr. Mushtaq has held many leadership roles over the span of 19 years of his career. From 2011 to 2013, he served as the Managing Director/CEO of Adamjee Insurance Company. He then moved onto hold the position of Chief Executive Officer at Habib Metropolitan Financial Services Ltd in 2013 and served 8 years in the same company. Previously, he was also associated with Security General Insurance Company.</p> --}}
			@endforeach
			
			@foreach($postsdesc as $postsdescrption)
			@if($postsdescrption->description_en != "")
			<div class="uk-panel">
			    <img class="uk-align-left uk-margin-remove-adjacent" src="{{asset('public/website/images/bod/inner/1.jpg')}}" width="360" alt="Example image">
				{!! $postsdescrption->description_en !!}
				{{-- <h3>Message from our CEO</h3>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.</p> --}}
			</div>
			@endif
			
			@endforeach
			
		</div>
	</div>
</section>
<!-- Section End -->

@include('partials.footer')