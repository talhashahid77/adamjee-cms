<style>
	.planFinder label.error, .Calculation label.error {
    color: red !important;
}
</style>
@if(isset($pagesdata))
	@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])
	<!-- Home Slider Start -->
	<section class="homeBanner">
		<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider>
			<ul class="uk-slider-items">
				@foreach($innerPages as $innerPagekey => $innerPage)
					@if($innerPagekey == 0)
						@foreach($innerPage->article as $innerAriclekey => $article)
							@php 
								$postImg =  getImageFile($device_type,$article,"image","banner");
							@endphp
							<li>
								<div class="uk-panel">
									<img src="{{ $postImg }}" width="2732" height="1100" alt="">
									<div class="homeBannerTxt">
										<div class="mdl">
											<div class="mdl_inner">
												<div class="uk-container containCustom">
													<div uk-grid>
														<div class="uk-width-1-2@m">
															<h2 uk-slider-parallax="x: 200,-200">{{ $article->title_en }}</h2>
															{!! $article->description_en !!}
															<div class="homeBanneBtn">
																@foreach($article->custompost as $innercustompost => $custompost)
																	<a href="{{$custompost->url}}"  uk-slider-parallax="x: 350,-350" uk-toggle>{{$custompost->name}} <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
																@endforeach
															</div>
														</div>
														<div class="uk-width-1-2@m uk-visible@m">
															<div class="homeBanneFull">
																<div class="homeBanneSqrBox" uk-slider-parallax="x: 200,-200" id="live-protected">
																@php
																	$sliderRespose = json_decode($sliderInfo->response);
																@endphp
																	<h4>{{ $sliderRespose[0]->LIVES_PROTECTED }}</h4>
																	<p>LIVES PROTECTED</p>
																</div>
															</div>
															<div class="homeBanneFull">
																<div class="homeBanneSqrBox" uk-slider-parallax="x: 300,-300">
																	<h4>A++</h4>
																	<p>PACRA RATING</p>
																</div>
															</div>
														</div>
													</div>           
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
						@endforeach
					@endif
				@endforeach
			</ul>
			<ul class="uk-slider-nav uk-dotnav homeBullets">			
			</ul>
		</div>
	</section>
	<!-- Home Slider End -->
	<!-- Ticker Section Start -->
	<section class="TickerSec">
		<div class="uk-container uk-container-expand">
			<div class="TickerBox">
				<ul class="NewsTicker">
					@foreach($fund_prices as $fuelKey => $fund_price)
						<li><p><strong>{{ $fund_price->fund_type }}</strong></p><p><span>Bid price (Rs)</span><span>{{ $fund_price->bid_price }}</span></p><p><span>Bid price (Rs)</span><span>{{ $fund_price->offer_price }}</span></p></li>
					@endforeach
				</ul>
				<a class="viewAll" href="javascript:;">View all fund prices <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
			</div>
		</div>
	</section>
	<!-- Ticker Section End -->
	<!-- Section Start -->
	<section class="SecWrap SecTopSpace">
		<div class="uk-container containCustom">
			<div class="HeadingBox">
				<h3>Want to know which plan suits your needs?</h3>
				<p>Use our planner to find information relevant to your requirements</p>
				<!-- <a class="blueBtn mobHide" href="javascript:;">Book an appointment <img src="images/right.svg" uk-svg /> </a> -->
			</div>
			<div class="ScrollCarousal">
				<div uk-slider="finite: true;">
					<ul class="uk-slider-items">
						<li>
							<a href="javascript:;" onClick="PlansPopup('education')" class="ScrollCarousalBox">
								<img src="{{ asset('public/website/images/homebanner/Secondslider/3.jpg')}}" alt="">
								<div class="ScrollCarousalBoxHover">
									<h4>Education </h4>
									<!-- <p>Save for your child's education to to ensure they achieve their dreams</p> -->
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" onClick="PlansPopup('wedding')" class="ScrollCarousalBox">
								<img src="{{ asset('public/website/images/homebanner/Secondslider/2.jpg')}}" alt="">
								<div class="ScrollCarousalBoxHover">
									<h4>Wedding </h4>
									<!-- <p>Save and invest to give your child the perfect wedding they deserve</p> -->
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" onClick="PlansPopup('comprehensive')" class="ScrollCarousalBox">
								<img src="{{ asset('public/website/images/homebanner/Secondslider/1.jpg')}}" alt="">
								<div class="ScrollCarousalBoxHover">
									<h4>Comprehensive Coverage</h4>
									<!-- <p>Plan your retirement now so that you don't have to worry about your future</p> -->
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- <div class="HeadingBox formob">
				<a class="blueBtn mobShow" href="javascript:;">Book an appointment <img src="images/right.svg" uk-svg /> </a>
			</div> -->
		</div>
	</section>
	<!-- Section End -->
	<!-- Section Start -->
	<section class="SecWrap">
		<div class="uk-container containCustom2">
			<div class="SecBan">
				<img src="{{ asset('public/website/images/getcoverage.png')}}" />
				<div class="SecBanTxt">
					<div class="mdl">
						<div class="mdl_inner">
							<ul class="uk-grid-small uk-flex uk-flex-right" uk-grid>
								<li class="uk-width-1-2@m">
									<div class="SecBanInner">
										<h3>How much life insurance do you need?</h3>
										<p>Just answer a few questions to get the estimated coverage you need</p>
										<a class="blueBtn" href="#modal-calculation" uk-toggle> Calculate<img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Section End -->
	<!-- Section Start -->
	<section class="SecWrap">
		<div class="uk-container containCustom">
			<div class="HeadingBox">
				@foreach($postsdata as $postKey => $postdata)
					@if($postKey == 0)
						<h3>{{ $postdata->title_en }}</h3>
						{!! $postdata->description_en !!}
						<a class="LinkBtn mobHide" href="{{ url(session()->get('url').$postdata->link_en) }}">Show more<img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
					@endif
				@endforeach
			</div>
			<div class="ScrollCarousal">
				<div uk-slider="finite: true;">
					<ul class="uk-slider-items">
					@foreach($postsdata as $postKey => $postdata)
						@if($postKey > 0)
							@php 
								$postImg =  getImageFile($device_type,$postdata,"image","banner");
							@endphp
							<li>
								<a href="{{ url(session()->get('url').$postdata->link_en) }}" class="ScrollCarousalBox">
									<img src="{{ $postImg }}" alt="">
									<div class="ScrollCarousalBoxHover">
										<h4>{{ $postdata->title_en }}</h4>
										{!! $postdata->description_en !!}
									</div>
								</a>
							</li>
						@endif
					@endforeach
						<!-- <li>
							<a href="educational-reimbursement.php" class="ScrollCarousalBox">
								<img src="{{ asset('public/website/images/homebanner/education.jpg')}}" alt="">
								<div class="ScrollCarousalBoxHover">
									<h4>Education</h4>
									<p>Doing our best to help your children achieve their best</p>
								</div>
							</a>
						</li> -->
					</ul>
					<div class="ScrollBtl">
						<ul class="uk-slider-nav uk-dotnav"></ul>
					</div>
					
				</div>
			</div>
			<div class="HeadingBox formob">
				<a class="LinkBtn mobShow" href="offers-discounts.php">Show more<img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
			</div>
		</div>
	</section>
	<!-- Section End -->
	<!-- Section Start -->
	<section class="SecWrap">
		<div class="uk-container containCustom2">
			<div class="SecBan textLight">
				<img src="{{ asset('public/website/images/controlpolicy.png')}}" />
				<div class="SecBanTxt">
					<div class="mdl">
						<div class="mdl_inner">
							<ul class="uk-grid-small" uk-grid>
								<li class="uk-width-1-2@m">
									<div class="SecBanInner">
										<h3>Control your policy</h3>
										<p>Now you can take control of your insurance policies and access your policy details anywhere, anytime.</p>
										<a class="whiteBtn" href="https://alpos.adamjeelife.com/Eservices" target="_blank">Conventional <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
										<a class="whiteBtn" href="https://alpos.adamjeelife.com/tkfeservice" target="_blank">Takaful <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Section End -->
	<!-- Section Start -->
	<section class="SecWrap">
		<div class="uk-container containCustom">
			<div class="HeadingBox">
				<h3>Knowledge Center</h3>
				<p>Here to help you in advice, claims and everything in between <a class="LinkBtn mobHide" href="{{ url(session()->get('url').'/articles') }}">See all <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a></p>
			</div>
			<div class="ScrollCarousal cardsCarousel">
				<div uk-slider="finite: true;">
					<ul class="uk-slider-items" uk-height-match=".uk-card-body">
						@foreach($knowledgePages as $knowledgekey => $knowledgePage)
							@foreach($knowledgePage->child as $subknowledgekey => $subknowledgePage)
								<li>
									<a href="{{ url(session()->get('url').$subknowledgePage->path) }}" class="uk-card uk-card-default">
										<div class="uk-card-media-top">
											<img src="{{ asset('public/website/images/cardimg.png')}}" width="1800" height="1200" alt="">
										</div>
										<div class="uk-card-body">
											<div class="badgeBox">{{ $knowledgePage->title_en }}</div>
											<h3>{{ $subknowledgePage->title_en }}</h3>
											{!! $subknowledgePage->short_desc_en !!}
										</div>
									</a>
								</li>
							@endforeach
						@endforeach
					</ul>
				</div>
			</div>
			<div class="HeadingBox formob">
				<a class="LinkBtn mobShow" href="{{ url(session()->get('url').'/articles') }}">See all <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
			</div>
		</div>
	</section>
	<!-- Section End -->
	<!-- Section Start -->
	<section class="prSecCar">
		<div class="uk-container containCustom2">
			<div class="SecBan textLight">
				<img src="{{ asset('public/website/images/latestadamjee.png')}}" />
				<div class="SecBanTxt">
					<div class="mdl">
						<div class="mdl_inner">
							<ul class="uk-grid-small" uk-grid>
								<li class="uk-width-1-1">
									<div class="SecBanInner fullWidth">
										<h3>Latest from Adamjee Life</h3>
										<p>Here to help you in advice, claims and everything in between <a class="LinkBtn mobHide" href="./latest-from-adamjee-life.php">See all <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a></p>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="ScrollCarousal cardsCarousel Newsbox">
				<div uk-slider="finite: true;">
					<ul class="uk-slider-items" uk-height-match=".uk-card-body">
						@foreach($latestAdamjees as $latestKey => $latestNewsCategory)
							@foreach($latestNewsCategory->child as $latestKey => $latestNews)
								<li>
									<a href="{{ url(session()->get('url').$latestNews->path) }}" class="uk-card uk-card-default">
										<div class="uk-card-body">
											
											<h3>{{ $latestNews->title_en }}</h3>
											{!! $latestNews->short_desc_en !!}
											<div class="infoBox">
												<div class="badgeBox">{{ $latestNewsCategory->title_en }}</div>
												<div class="flrigt">
													<!-- <div class="autherName">Sofia Mayer</div> -->
													@foreach($latestNews->custompost as $customKey => $customPost)
														<div class="dateInfo">{!! $customPost->description !!}</div>
													@endforeach
												</div>	
											</div>
										</div>
									</a>
								</li>
							@endforeach
						@endforeach
						
					</ul>
				</div>
				<div class="HeadingBox formob">
				<a class="LinkBtn mobShow" href="./latest-from-adamjee-life.php">See all <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
			</div>
			</div>
		</div>
	</section>
	<!-- Section End -->
	<!-- plans modal start -->
	<div id="modal-education" uk-modal>
		<div class="uk-modal-dialog">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<div class="uk-modal-header">
				<a href="index.php" class="logo">
					<img src="{{ asset('public/website/images/logo.svg')}}" alt="Adamjee Life">
				</a>
			</div>
			<div class="uk-modal-body">
				<div class="uk-container containCustom">
					<div class="homeStep">
					<!-- multistep form -->
					<form class="wrappermultiform planFinder" id="planFinder" method="POST" return="false">
						<div class="header">
							<ul>
								<li class="active form_1_progessbar">
								</li>
								<li class="form_2_progessbar">
								
								</li>
								<li class="form_3_progessbar">
									
								</li>
								<li class="form_4_progessbar">
									
								</li>
							</ul>
						</div>
						<input type="hidden" name="planType" id="planType" />
						<div class="form_wrap NewsFilter JobForm">
							<div class="form_1 data_info" >
								<div id="education" style="display:none">
									<h5>Question 1 of 4</h5>
									<h4><b>That’s so exciting. When do you need funds for <br/> your child’s admission?</b></h4>
									<div class="uk-grid-medium" uk-grid >
										<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
											<label for="full_name">Date</label>
											<input class="uk-input" type="date" onkeydown="return false" placeholder="DD/MM/YYYY" name="educationDate"  id="educationDate" required >
										</div>
									</div>
								</div>
								<div id="wedding" style="display:none">
									<h5>Question 1 of 4</h5>
									<h4><b>That’s so exciting. When is the wedding?</b></h4>
									<div class="uk-grid-medium" uk-grid >
										<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
											<label for="full_name">Date</label>
											<input class="uk-input" type="date" placeholder="DD/MM/YYYY" onkeydown="return false" name="weddingDate"  id="weddingDate" required >
										</div>
									</div>
								</div>
								<div id="comprehensive" style="display:none">
									<h5>Question 1 of 4</h5>
									<h4><b>That’s so exciting. Till when do you want yourself to becovered?</b></h4>
									<div class="uk-grid-medium" uk-grid >
										<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
											<label for="full_name">Date</label>
											<input class="uk-input" type="date" placeholder="DD/MM/YYYY" onkeydown="return false" name="comprehensiveDate"  id="comprehensiveDate" required >
										</div>
									</div>
								</div>
							</div>
							<div class="form_2 data_info" style="display: none;">
								<h5>Question 2 of 4</h5>
								<h4><b>How much can you pay and when?</b></h4>
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label" id="premium_mode" name="premium_mode">Select a premium mode</label>
										<select class="uk-select" id="premium_mode_input" name="premium_mode_input" required >
											<option value="">Select Mode</option>
											<option value="Premium">Premium mode</option>
										</select>
									</div>

									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label for="pkr">Enter an amount</label>
										<input class="uk-input" type="number" placeholder="Enter Amount" id="pkr" name="pkr" required >
									</div>
								</div> 
							</div>
							<div class="form_3 data_info" style="display: none;">
								<h5>Question 3 of 4</h5>
								<h4><b>How much can you pay and when?</b></h4>
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
										<label for="fullname">Name</label>
										<input class="uk-input" type="text" id="fullname" name="fullname" placeholder="Your full name" required>
									</div>

									<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
										<label for="dob">Date of Birth</label>
										<input class="uk-input" type="date" onkeydown="return false" id="dob" name="dob" placeholder="DD/MM/YYYY" required >
									</div>

									<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label">Gender</label>
										<select class="uk-select" id="gender" name="gender" required>
											<option value="">Select Gender</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
									</div>

									<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
										<label for="mobile_no">Mobile number</label>
										<input class="uk-input" type="number"  minLength="11" maxLength="11" id="mobile_no" name="mobile_no" placeholder="03xx xxxxxxx" required >
									</div>

									<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label">City</label>
										<select class="uk-select" id="city" name="city" required >
											<option value="">Select City</option>
											@foreach (getCities() as $item)
											<option value="{{$item}}">{{$item}}</option>    
											@endforeach
										</select>
									</div>
									
								</div>  
							</div>
							<div class="form_4 data_info" style="display: none;">
								<h5>Question 4 of 4</h5>
								<h4><b>Select your desired plan type</b></h4>
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-2@m  uk-grid-margin uk-first-column" id="conventional">
										<label for="Conventional-coverage">
											<img src="{{ asset('public/website/images/homebanner/Conventional-coverage.jpg')}}"/>
										</label>
										<input class="uk-radio" type="radio" name="Conventional-coverage" id="Conventional-coverage" required >Conventional coverage</label>
										<div class="CustomToolTip" uk-tooltip="Takaful coverage is commonly referred to as Islamic insurance. Takaful means a scheme based on brotherhood, solidarity and mutual assistance which provides for mutual financial aid and assistance to the participants in case of need."><img src="{{ asset('public/website/images/icons/tooltip.svg')}}" /></div>
										<!-- <input class="uk-input" type="button" placeholder="Conventional coverage" value="Conventional coverage" > -->
										
									</div>

									<div class="uk-width-1-2@m  uk-grid-margin uk-first-column" id="takaful">
										<label for="takaful-coverage">
											<img src="{{ asset('public/website/images/homebanner/Takaful-coverage.jpg')}}"/>
										</label>
										<input class="uk-radio" type="radio" name="takaful-coverage"  id="takaful-coverage" required>Takaful coverage</label>
				
										<!-- <input class="uk-input" type="button" placeholder="Conventional coverage" value="Conventional coverage" > -->
										<div class="CustomToolTip" uk-tooltip="Takaful coverage is commonly referred to as Islamic insurance. Takaful means a scheme based on brotherhood, solidarity and mutual assistance which provides for mutual financial aid and assistance to the participants in case of need."><img src="{{ asset('public/website/images/icons/tooltip.svg')}}" /></div>
										<!-- <a id="js-modal-dialog" class="uk-button uk-button-default" ><img src="{{ asset('public/website/images/icons/tooltip.svg')}}" /></a>  -->
									</div>
									

									<div id="Conventional-coverage-plans" style="display: none">
										<ul class="uk-grid-small plan_dynamic"  uk-grid uk-height-match=".uk-card-body">
											<!-- Card Start -->
											<li class="uk-width-1-3@m" >
												<div class="uk-card uk-card-default newsCard">
													<div class="uk-card-media-top">
														<img src="{{ asset('public/website/images/products/1.jpg')}}" alt="">	
													</div>
													<div class="uk-card-body">
														<div class="badgesBar">
																<div class="badgeBox">TRENDING</div>	
															</div>
														<h3>Mehfooz Munafa</h3>
														<ul>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														</ul>
														<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
														<a href="javascript:;" class=""><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> Brochure (PDF)</a><br/>
														<button type="submit" class="packageBtn" uk-toggle="#thank-you" >Contact an expert <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
														
													</div>
												</div>
											</li>
											<!-- Card Start -->
											<li class="uk-width-1-3@m" >
												<div class="uk-card uk-card-default newsCard">
													<div class="uk-card-media-top">
														<img src="{{ asset('public/website/images/products/1.jpg')}}" alt="">
													</div>
													<div class="uk-card-body">
														<div class="badgesBar">
																<div class="badgeBox">TRENDING</div>	
															</div>
														<h3>Mehfooz Munafa</h3>
														<ul>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														</ul>
														<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
														<a href="javascript:;" class=""><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> Brochure (PDF)</a><br/>
														<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
														
													</div>
												</div>
											</li>
											<!-- Card Start -->
											<li class="uk-width-1-3@m" >
												<div class="uk-card uk-card-default newsCard">
													<div class="uk-card-media-top">
														<img src="{{ asset('public/website/images/products/1.jpg')}}" alt="">
													</div>
													<div class="uk-card-body">
														<div class="badgesBar">
																<div class="badgeBox">TRENDING</div>	
															</div>
														<h3>Mehfooz Munafa</h3>
														<ul>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														</ul>
														<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
														
														<a href="javascript:;" class=""><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> Brochure (PDF)</a><br/>
														<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
													</div>
												</div>
											</li>
										</ul>
									</div>



									<div id="Takaful-coverage-plans" style="display: none">
										<ul class="uk-grid-small plan_dynamic"  uk-grid uk-height-match=".uk-card-body">
											<!-- Card Start -->
											<li class="uk-width-1-3@m" >
												<div class="uk-card uk-card-default newsCard">
													<div class="uk-card-media-top">
														<img src="{{ asset('public/website/images/products/1.jpg')}}" alt="">
													</div>
													<div class="uk-card-body">
														<div class="badgesBar">
																<div class="badgeBox">TRENDING</div>	
															</div>
														<h3>Mehfooz Munafa takaful-coverage</h3>
														<ul>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														</ul>
														<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
														<a href="javascript:;" class=""><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> Brochure (PDF)</a><br/>
														<button type="submit" class="packageBtn" uk-toggle="#thank-you" >Contact an expert <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
														
													</div>
												</div>
											</li>
											<!-- Card Start -->
											<li class="uk-width-1-3@m" >
												<div class="uk-card uk-card-default newsCard">
													<div class="uk-card-media-top">
														<img src="{{ asset('public/website/images/products/1.jpg')}}" alt="">
													</div>
													<div class="uk-card-body">
														<div class="badgesBar">
																<div class="badgeBox">TRENDING</div>	
															</div>
														<h3>Mehfooz Munafa</h3>
														<ul>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														</ul>
														<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
														<a href="javascript:;" class=""><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> Brochure (PDF)</a><br/>
														<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
														
													</div>
												</div>
											</li>
											<!-- Card Start -->
											<li class="uk-width-1-3@m" >
												<div class="uk-card uk-card-default newsCard">
													<div class="uk-card-media-top">
														<img src="{{ asset('public/website/images/products/1.jpg')}}" alt="">
													</div>
													<div class="uk-card-body">
														<div class="badgesBar">
																<div class="badgeBox">TRENDING</div>	
															</div>
														<h3>Mehfooz Munafa</h3>
														<ul>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
															<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														</ul>
														<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
														
														<a href="javascript:;" class=""><img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /> Brochure (PDF)</a><br/>
														<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a>
													</div>
												</div>
											</li>
										</ul>
									</div>
									
								</div>
							</div>
							
						</div>
						<div class="btns_wrap">
							<div class="common_btns form_1_btns">
								<button type="button" class="btn_next blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
							</div>
							<div class="common_btns form_2_btns" style="display: none;">
								<button type="button" class="btn_back transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
								<button type="button" class="btn_next blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
							</div>
							<div class="common_btns form_3_btns" style="display: none;">
								<button type="button" class="btn_back transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
								<button type="button" class="btn_next blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
							</div>
							<div class="common_btns form_4_btns" style="display: none;">
								<button type="button" class="btn_back transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
								<button type="button" class="btn_next blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
								
							</div>
							<!-- <div class="common_btns form_5_btns" style="display: none;">
								<button type="button" class="btn_back transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
								<button type="button" class="btn_done">Done</button>
							</div> -->
						</div>
					</form>

					
					<div class="modal_wrapper" >
						<div class="shadow"></div>
						<div class="success_wrap">
							<span class="modal_icon"><ion-icon name="checkmark-sharp"></ion-icon></span>
							<p>You have successfully completed the process.</p>
						</div>
					</div>
				</div>    
			</div>
			</div>
		</div>
	</div>
	<!-- plan modal end -->
	<!-- calculation modal start -->
	<div id="modal-calculation" class="modal-calculation" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
			<a href="index.php" class="logo">
                <img src="{{ asset('public/website/images/logo.svg')}}" alt="Adamjee Life">
            </a>
        </div>
        <div class="uk-modal-body">
			<div class="uk-container containCustom">
				<div class="homeStep">
				<!-- multistep form -->
				<form class="wrappermultiform Calculation" id="Calculation" >
					<div class="header">
						<ul>
							<li class="active form_1_progessbar_calculation">
								
							</li>
							<li class="form_2_progessbar_calculation">
							
							</li>
							<li class="form_3_progessbar_calculation">
								
							</li>
							<li class="form_4_progessbar_calculation">
								
							</li>
							<li class="form_5_progessbar_calculation">
								
							</li>
							<li class="form_6_progessbar_calculation">
								
							</li>
							<li class="form_7_progessbar_calculation">
								
							</li>
						</ul>
					</div>
					<div class="form_wrap NewsFilter JobForm">
						<div class="form_1_calculation data_info_calculation">
							<h5>Question 1 of 7</h5>
							<h4><b>Tell us a bit about yourself...</b></h4>
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label for="full_name">full</label> -->
										<input class="uk-input" type="text" placeholder="Full Name" name="full_Name" id="full_Name" >
									</div>
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label for="full_name">Date</label> -->
										<input class="uk-input" type="number" placeholder="Mobile Number" minLength="11" maxLength="11" name="mobile_Number" id="mobile_Number" >
									</div>
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label class="uk-form-label">Select a premium mode</label> -->
										<select class="uk-select" id="gender" name="gender">
											<option value="" >Gender</option>
											<option value="female">Male</option>
											<option value="male">Female</option>
										</select>
									</div>

									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label class="uk-form-label">Select a premium mode</label> -->
										<select name="city" class="uk-select" required>
											<option value="">Select City</option>
											@foreach (getCities() as $item)
											<option value="{{$item}}">{{$item}}</option>    
											@endforeach
                                		</select>
									</div>
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label class="uk-form-label">Select a premium mode</label> -->
										<input class="uk-input" type="date" placeholder="Date of Birth" onkeydown="return false" id="birth" name="birth" >
									</div>
								</div>
						</div>
						<div class="form_2_calculation data_info_calculation" style="display: none;">
							<h5>Question 2 of 7</h5>
							<h4><b>How much annual income would you like to provide, if you were no longer here?</b></h4>
							<!-- <p>Think about how much money your family will need to cover daily living expenses. This is typically 60-80% of your individual post-tax income. Don’t include college savings or any debts that you would like to pay off immediately (such as your mortgage), since those are covered in other questions</p> -->
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="number" placeholder="Enter Amount" name="annual_income" id="annual_income" required >
								</div>
							</div> 
						</div>
						<div class="form_3_calculation data_info_calculation" style="display: none;">
							<h5>Question 3 of 7</h5>
							<h4><b>How many years should income be provided after you’re gone?</b></h4>
							<!-- <p>Think about how long you’ll need the additional income to support you and your family</p> -->
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="number" placeholder="Years" name="years" id="years" required >
								</div>	
							</div>  
						</div>

						

						<div class="form_4_calculation data_info_calculation" style="display: none;">
							<h5>Question 4 of 7</h5>
							<h4><b>How much debt would you like to pay off immediately?</b></h4>
							<!-- <p>Consider things like outstanding mortgage, credit card balances and car loans.</p> -->
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="number" placeholder="Enter Amount" name="debit_value" id="debit_value" required >
								</div>	
							</div> 
						</div>

						<div class="form_5_calculation data_info_calculation" style="display: none;">
							<h5>Question 5 of 7</h5>
							<h4><b>How much funding you need for your children's education?</b></h4>
							<!-- <p>Consider things like outstanding mortgage, credit card balances and car loans.</p> -->
							<div class="childInfo">
								<h5><b>Child </b><a class="childremove">x</a></h5>
								
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label">What is their current age?</label>
										<input class="uk-input" type="number" placeholder="Enter Your Child's Age" id="age" name="age" required >
									</div>
									
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label">Total Funding required</label>
										<input class="uk-input" type="number" placeholder="Enter Amount" name="total_Funding" id="total_Funding" >
									</div>

									<!-- <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label">Enter Amount</label>
										<input class="uk-input" type="number" placeholder="Enter Amount" name="enter_Amount" id="enter_Amount" >
									</div> -->
								</div>
							</div>
							<div class="childInfomore"></div>
							<a id="addChild">Add Child</a>
							 
						</div>

						<div class="form_6_calculation data_info_calculation" style="display: none;">
							<h5>Question 6 of 7</h5>
							<h4><b>How much would you like to set aside for an emergency fund?</b></h4>
							<!-- <p>If you don’t already have one, it’s a good idea to set aside at least three to six months’ worth of expenses in your emergency fund.</p> -->
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="number" placeholder="Enter Amount" name="emergency_fund" id="emergency_fund" >
								</div>	
							</div> 
						</div>

						<div class="form_7_calculation data_info_calculation" style="display: none;">
							<h5>Question 6 of 7</h5>
							<h4><b>How much personal life insurance do you already have?</b></h4>
							<!-- <p>Do not include any life insurance policies you may have through work, since these policies will likely be eliminated when your job changes.</p> -->
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="number" placeholder="Enter Amount" name="insurance_Number" id="insurance_Number" >
								</div>	
							</div> 
						</div>
					</div>
					<div class="btns_wrap">
						<div class="common_btns_calculation form_1_btns_calculation">
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
						</div>
						<div class="common_btns_calculation form_2_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
						</div>
						<div class="common_btns form_3_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
						</div>
						<div class="common_btns_calculation form_4_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
							
						</div>
						<div class="common_btns_calculation form_5_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn ">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
							<button type="button" class="btn_skip_calculation">Skip<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
						</div>

						<div class="common_btns_calculation form_6_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
							<button type="button" class="btn_skip_calculation">Skip<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
						</div>
						<div class="common_btns_calculation form_7_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Back </button>
							<button type="button" class="btn_done_calculation blueBtn" uk-toggle="#modal-calculation-result" onclick="calculationForm()">Submit<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
							<!-- <button type="button" class="btn_skip_calculation">Skip<img src="images/icons/chevron_right.svg" uk-svg /></button> -->
						</div>
					</div>
				</form>
			</div>    
		</div>
        </div>
    </div>
</div>
<!-- calculator modal end -->

<!-- calculate result start -->
<div id="modal-calculation-result" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
			<a href="index.php" class="logo">
                <img src="{{ asset('public/website/images/logo.svg')}}" alt="Adamjee Life">
            </a>
        </div>
        <div class="uk-modal-body">
			<div class="uk-container uk-container-small mtauto containCustom">
				<h4><b>Your estimated life insurance need:</b></h4>
				<table class="uk-table uk-table-small Calculateresult" >
					<tbody>
						<tr>
							<td><b>Income replacement</b></td>
							<td id="incomeReplacement">-</td>
						</tr>
						<tr>
							<td>Debt to pay off</td>
							<td><span id="debtOff">-</span></td>
						</tr>
						
						<tr>
							<td>College fund</td>
							<td><span id="collegeFund">-</span></td>
						</tr>

						<tr>
							<td>Emergency fund</td>
							<td><span id="emergencyFund">-</span></td>
						</tr>

						<tr>
							<td>Current insurance</td>
							<td><span id="currentInsurance">-</span></td>
						</tr>
						<tr>
							<td>Total need</td>
							<td><span id="totalAmount">0</span></td>
						</tr>
						
					</tbody>
				</table>	

				<div class="common_btns_calculation form_7_btns_calculation form_7_btns_calculation_result">
					<button type="button" class="btn_back_calculation transparentbtn" uk-toggle=".modal-calculation"><img src="{{ asset('public/website/images/icons/chevron_left.svg')}}" uk-svg />Edit Response </button>
					<button class="uk-modal-close-default blueBtn reset-btn" type="button" >Done<img src="{{ asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
					<!-- <button type="button" class="btn_done_calculation blueBtn calculationDone">Done<img src="images/icons/chevron_right.svg" uk-svg /></button> -->
				</div>
			</div>    
		</div>

		
        </div>
    </div>
</div>
<!-- calculator modal end -->

	<!-- thank you  start -->
	<div id="thank-you" class="uk-flex-top" uk-modal>
		<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

			<button class="uk-modal-close-default" type="button" uk-close></button>
			
			<h4>Thank you for choosing us</h4>
			<p>An expert will be in contact with you soon.</p>

		</div>
	</div>
	<script type="text/javascript" src="{{ asset('public/website/js/acmeticker.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$('.NewsTicker').AcmeTicker({
				type:'marquee',/*horizontal/horizontal/Marquee/type*/
				direction: 'left',/*up/down/left/right*/
				speed: 0.05,/*true/false/number*/ /*For vertical/horizontal 600*//*For marquee 0.05*//*For typewriter 50*/
			});
		})
	</script>
<!-- plans form -->
<script>
		var form_1 = document.querySelector(".form_1");
		var form_2 = document.querySelector(".form_2");
		var form_3 = document.querySelector(".form_3");
		var form_4 = document.querySelector(".form_4");
		// var form_5 = document.querySelector(".form_5");


		var form_1_btns = document.querySelector(".form_1_btns");
		var form_2_btns = document.querySelector(".form_2_btns");
		var form_3_btns = document.querySelector(".form_3_btns");
		var form_4_btns = document.querySelector(".form_4_btns");
		// var form_5_btns = document.querySelector(".form_5_btns");


		var form_1_next_btn = document.querySelector(".form_1_btns .btn_next");
		var form_2_back_btn = document.querySelector(".form_2_btns .btn_back");
		var form_2_next_btn = document.querySelector(".form_2_btns .btn_next");
		var form_3_back_btn = document.querySelector(".form_3_btns .btn_back");
		var form_3_next_btn = document.querySelector(".form_3_btns .btn_next");
		var form_4_back_btn = document.querySelector(".form_4_btns .btn_back");
		var form_4_next_btn = document.querySelector(".form_4_btns .btn_next");
		// var form_5_back_btn = document.querySelector(".form_5_btns .btn_back");

		var form_2_progessbar = document.querySelector(".form_2_progessbar");
		var form_3_progessbar = document.querySelector(".form_3_progessbar");
		var form_4_progessbar = document.querySelector(".form_4_progessbar");
		// var form_5_progessbar = document.querySelector(".form_5_progessbar");

		var btn_done = document.querySelector(".btn_done");
		var modal_wrapper = document.querySelector(".modal_wrapper");
		var shadow = document.querySelector(".shadow");

		form_1_next_btn.addEventListener("click", function(){
			var form = $(".planFinder");
			form.validate({
			rules: {
				educationDate: {
				required: true,
				},	
			}
		});
  	if (form.valid() === true) {
			form_1.style.display = "none";
			form_2.style.display = "block";

			form_1_btns.style.display = "none";
			form_2_btns.style.display = "flex";

			form_2_progessbar.classList.add("active");
		}
	});

		form_2_back_btn.addEventListener("click", function(){
			form_1.style.display = "block";
			form_2.style.display = "none";

			form_1_btns.style.display = "flex";
			form_2_btns.style.display = "none";

			form_2_progessbar.classList.remove("active");
		});

		form_2_next_btn.addEventListener("click", function(){
			var form = $(".planFinder");
			form.validate({
			rules: {
				premium_mode_input: {
				required: true,
				},
				pkr: {
				required: true,
				},
				
			},
		});
  	if (form.valid() === true) {
			form_2.style.display = "none";
			form_3.style.display = "block";

			form_3_btns.style.display = "flex";
			form_2_btns.style.display = "none";

			form_3_progessbar.classList.add("active");
		}
		});

		form_3_back_btn.addEventListener("click", function(){
			form_2.style.display = "block";
			form_3.style.display = "none";

			form_3_btns.style.display = "none";
			form_2_btns.style.display = "flex";

			form_3_progessbar.classList.remove("active");
		});


		form_3_next_btn.addEventListener("click", function(){
			var form = $(".planFinder");
			form.validate({
			rules: {
				fullname: {
				required: true,
				},
				dob: {
				required: true,
				},
				gender: {
				required: true,
				},
				mobile_no: {
				required: true,
				minlength: 11,
            	maxlength: 11,
				},
				city: {
				required: true,
				}
				
			},
		});
  	if (form.valid() === true) {
			form_3.style.display = "none";
			form_4.style.display = "block";

			form_4_btns.style.display = "flex";
			form_3_btns.style.display = "none";

			form_4_progessbar.classList.add("active");
		}
		});

			// Conventional-coverage
			$(function() {
				$("input[name='Conventional-coverage']").click(function() {
					$("#Conventional-coverage-plans ul").empty();
					if ($("#Conventional-coverage").is(":checked")) {
						var formData  = new FormData();
            			formData.append("planType", "conventional");
            			formData.append("subType", $("#planType").val());
						$.ajax({
							type:'POST',
							url:'<?php echo url('/');?>/api/planFetch',
							contentType: false,
							processData: false,
							data: formData,
							success:function(response){
								if(response.code == 200){
									for (let index = 0; index < response.message.length; index++) {
										let fileUrl = "";
										var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';	
										(response.message[index].custompost.length > 1) ? fileUrl = base_url+'public/source/'+JSON.parse(response.message[index].custompost[1].file) : fileUrl = "";
										let imageUrl = base_url+'public/source/'+JSON.parse(response.message[index].image_en);
										$("#Conventional-coverage-plans ul.plan_dynamic").append("<li class='uk-width-1-3@m' ><div class='uk-card uk-card-default newsCard'><div class='uk-card-media-top'><img src='"+imageUrl+"' alt=''></div><div class='uk-card-body'><div class='badgesBar'><div class='badgeBox'>TRENDING</div></div><h3>"+response.message[index].title_en+"</h3>"+response.message[index].description_en+"<a href='"+fileUrl+"' class='' target='_blank'><img src='{{ asset('public/website/images/icons/download.svg')}}' uk-svg /> Brochure (PDF)</a><br/><button type='button' class='packageBtn' onclick=\"submitPlan(\'" + response.message[index].title_en + "\');\" >Contact an expert <img src='{{ asset('public/website/images/right.svg')}}' uk-svg /></a></div></div></li>");										
									}
								}
							}
						});
						$("#Conventional-coverage-plans").show();
						$("#conventional").hide();
						$("#takaful").hide();
					} else {
						$("#Conventional-coverage-plans").hide();
					}
				});
			});
		//   takaful-coverage
			$(function() {
					$("input[name='takaful-coverage']").click(function() {
					$("#Takaful-coverage-plans ul").empty();
					if ($("#takaful-coverage").is(":checked")) {
						var formData  = new FormData();
							formData.append("planType", "takaful");
							formData.append("subType", $("#planType").val());
							$.ajax({
								type:'POST',
								url:'<?php echo url('/');?>/api/planFetch',
								contentType: false,
								processData: false,
								data: formData,
								success:function(response){
									if(response.code == 200){
										for (let index = 0; index < response.message.length; index++) {
											let fileUrl = "";
											var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';	
											(response.message[index].custompost.length > 1) ? fileUrl = base_url+'public/source/'+JSON.parse(response.message[index].custompost[1].file) : fileUrl = "";
											let imageUrl = base_url+'public/source/'+JSON.parse(response.message[index].image_en);
											$("#Takaful-coverage-plans ul.plan_dynamic").append("<li class='uk-width-1-3@m' ><div class='uk-card uk-card-default newsCard'><div class='uk-card-media-top'><img src='"+imageUrl+"' alt=''></div><div class='uk-card-body'><div class='badgesBar'><div class='badgeBox'>TRENDING</div></div><h3>"+response.message[index].title_en+"</h3>"+response.message[index].description_en+"<a href='"+fileUrl+"' class='' target='_blank'><img src='{{ asset('public/website/images/icons/download.svg')}}' uk-svg /> Brochure (PDF)</a><br/><button type='button' class='packageBtn' onclick=\"submitPlan(\'" + response.message[index].title_en + "\');\" >Contact an expert <img src='{{ asset('public/website/images/right.svg')}}' uk-svg /></a></div></div></li>");										
										}
									}
								}
							});
						$("#Takaful-coverage-plans").show();
						$("#conventional").hide();
						$("#takaful").hide();
					}
					else {
					$("#Takaful-coverage-plans").hide();
				}
				});
			});
</script>
<!-- plans end -->

<!-- calculation modal start -->
<!-- calculation modal start -->
<script>
var form_1_calculation = document.querySelector(".form_1_calculation");
var form_2_calculation = document.querySelector(".form_2_calculation");
var form_3_calculation = document.querySelector(".form_3_calculation");
var form_4_calculation = document.querySelector(".form_4_calculation");
var form_5_calculation = document.querySelector(".form_5_calculation");
var form_6_calculation = document.querySelector(".form_6_calculation");
var form_7_calculation = document.querySelector(".form_7_calculation");


var form_1_btns_calculation = document.querySelector(".form_1_btns_calculation");
var form_2_btns_calculation = document.querySelector(".form_2_btns_calculation");
var form_3_btns_calculation = document.querySelector(".form_3_btns_calculation");
var form_4_btns_calculation = document.querySelector(".form_4_btns_calculation");
var form_5_btns_calculation = document.querySelector(".form_5_btns_calculation");
var form_6_btns_calculation = document.querySelector(".form_6_btns_calculation")
var form_7_btns_calculation = document.querySelector(".form_7_btns_calculation")


var form_1_next_btn_calculation = document.querySelector(".form_1_btns_calculation .btn_next_calculation");
var form_2_back_btn_calculation = document.querySelector(".form_2_btns_calculation .btn_back_calculation");
var form_2_next_btn_calculation = document.querySelector(".form_2_btns_calculation .btn_next_calculation");
var form_3_back_btn_calculation = document.querySelector(".form_3_btns_calculation .btn_back_calculation");
var form_3_next_btn_calculation = document.querySelector(".form_3_btns_calculation .btn_next_calculation");
var form_4_back_btn_calculation = document.querySelector(".form_4_btns_calculation .btn_back_calculation");
var form_4_next_btn_calculation = document.querySelector(".form_4_btns_calculation .btn_next_calculation");
var form_5_back_btn_calculation = document.querySelector(".form_5_btns_calculation .btn_back_calculation");
var form_5_next_btn_calculation = document.querySelector(".form_5_btns_calculation .btn_next_calculation");
var form_5_skip_btn_calculation = document.querySelector(".form_5_btns_calculation .btn_skip_calculation");
var form_6_back_btn_calculation = document.querySelector(".form_6_btns_calculation .btn_back_calculation");
var form_6_next_btn_calculation = document.querySelector(".form_6_btns_calculation .btn_next_calculation");
var form_6_skip_btn_calculation = document.querySelector(".form_6_btns_calculation .btn_skip_calculation");
var form_7_back_btn_calculation = document.querySelector(".form_7_btns_calculation .btn_back_calculation");
var form_7_next_btn_calculation = document.querySelector(".form_7_btns_calculation .btn_next_calculation");
var form_7_skip_btn_calculation = document.querySelector(".form_7_btns_calculation .btn_skip_calculation");


var form_2_progessbar_calculation = document.querySelector(".form_2_progessbar_calculation");
var form_3_progessbar_calculation = document.querySelector(".form_3_progessbar_calculation");
var form_4_progessbar_calculation = document.querySelector(".form_4_progessbar_calculation");
var form_5_progessbar_calculation = document.querySelector(".form_5_progessbar_calculation");
var form_6_progessbar_calculation = document.querySelector(".form_6_progessbar_calculation");
var form_7_progessbar_calculation = document.querySelector(".form_7_progessbar_calculation");


var btn_done = document.querySelector(".btn_done");
var modal_wrapper = document.querySelector(".modal_wrapper");
var shadow = document.querySelector(".shadow");

form_1_next_btn_calculation.addEventListener("click", function(){
	var form = $(".Calculation");
  	form.validate({
    rules: {
        full_Name: {
        required: true,
        },
        
        mobile_Number:{
            required: true,
            minlength: 11,
            maxlength: 11,
        },

		gender:{
        	required: true,
        },

		cities:{
        	required: true,
        },
		birth:{
			required: true,
		}
    },
    messages: {
        full_Name: {
        required: "This field is required.",
        },
        mobile_Number:{
            required: "This field is required.",
            // maxlength: "Must be 12 number",
            // minlength: "Must be 12 number"
        },
		gender:{
        	required: "This field is required.",
        },
		cities:{
			required: "This field is required.",
        },
		birth:{
			required: "This field is required.",
		}
    }
  });
  if (form.valid() === true) {
	form_1_calculation.style.display = "none";
	form_2_calculation.style.display = "block";

	form_1_btns_calculation.style.display = "none";
	form_2_btns_calculation.style.display = "flex";

	form_2_progessbar_calculation.classList.add("active");
    
  }
	
});

form_2_back_btn_calculation.addEventListener("click", function(){
	
	form_1_calculation.style.display = "block";
	form_2_calculation.style.display = "none";

	form_1_btns_calculation.style.display = "flex";
	form_2_btns_calculation.style.display = "none";

	form_2_progessbar_calculation.classList.remove("active");
});

form_2_next_btn_calculation.addEventListener("click", function(){
	var form1 = $(".Calculation");
  	form1.validate({
    rules: {
        annual_income: {
        	required: true,
			number: true
        },
    },
    messages: {
        annual_income: {
        required: "This is required.",
        minlength: "Too short."
        },
    }
  });
	if (form1.valid() === true) {
		form_2_calculation.style.display = "none";
		form_3_calculation.style.display = "block";

		form_3_btns_calculation.style.display = "flex";
		form_2_btns_calculation.style.display = "none";

		form_3_progessbar_calculation.classList.add("active");
}
});

form_3_back_btn_calculation.addEventListener("click", function(){
	form_2_calculation.style.display = "block";
	form_3_calculation.style.display = "none";

	form_3_btns_calculation.style.display = "none";
	form_2_btns_calculation.style.display = "flex";

	form_3_progessbar_calculation.classList.remove("active");
});


form_3_next_btn_calculation.addEventListener("click", function(){
	var form2 = $(".Calculation");
  	form2.validate({
    rules: {
        years: {
        	required: true,
			number: true
        },
    },
    messages: {
        years: {
        required: "This is required.",
        },
    }
  });
  if (form2.valid() === true) {
	form_3_calculation.style.display = "none";
	form_4_calculation.style.display = "block";

	form_4_btns_calculation.style.display = "flex";
	form_3_btns_calculation.style.display = "none";

	form_4_progessbar_calculation.classList.add("active");
}
});

form_4_back_btn_calculation.addEventListener("click", function(){
	form_3_calculation.style.display = "block";
	form_4_calculation.style.display = "none";

	form_4_btns_calculation.style.display = "none";
	form_3_btns_calculation.style.display = "flex";

	form_4_progessbar_calculation.classList.remove("active");
});


form_4_next_btn_calculation.addEventListener("click", function(){
	var form3 = $(".Calculation");
  	form3.validate({
    rules: {
        debit_value: {
        	required: true,
			number: true
        },
	},
		messages: {
			debit_value: {
			required: "This is required.",
			},
		}
	});
  	if (form3.valid() === true) {
	form_4_calculation.style.display = "none";
	form_5_calculation.style.display = "block";

	form_5_btns_calculation.style.display = "flex";
	form_4_btns_calculation.style.display = "none";

	form_5_progessbar_calculation.classList.add("active");
}
});

form_5_back_btn_calculation.addEventListener("click", function(){
	form_4_calculation.style.display = "block";
	form_5_calculation.style.display = "none";

	form_5_btns_calculation.style.display = "none";
	form_4_btns_calculation.style.display = "flex";

	form_5_progessbar_calculation.classList.remove("active");
});


form_5_next_btn_calculation.addEventListener("click", function(){
	form_5_calculation.style.display = "none";
	form_6_calculation.style.display = "block";

	form_6_btns_calculation.style.display = "flex";
	form_5_btns_calculation.style.display = "none";

	form_6_progessbar_calculation.classList.add("active");
});

form_5_skip_btn_calculation.addEventListener("click", function(){
	form_5_calculation.style.display = "none";
	form_6_calculation.style.display = "block";

	form_6_btns_calculation.style.display = "flex";
	form_5_btns_calculation.style.display = "none";

	form_6_progessbar_calculation.classList.add("active");
});

form_6_back_btn_calculation.addEventListener("click", function(){
	form_5_calculation.style.display = "block";
	form_6_calculation.style.display = "none";

	form_6_btns_calculation.style.display = "none";
	form_5_btns_calculation.style.display = "flex";

	form_6_progessbar_calculation.classList.remove("active");
});


form_6_next_btn_calculation.addEventListener("click", function(){
	form_6_calculation.style.display = "none";
	form_7_calculation.style.display = "block";

	form_7_btns_calculation.style.display = "flex";
	form_6_btns_calculation.style.display = "none";

	form_7_progessbar_calculation.classList.add("active");
});

form_6_skip_btn_calculation.addEventListener("click", function(){
	form_6_calculation.style.display = "none";
	form_7_calculation.style.display = "block";

	form_7_btns_calculation.style.display = "flex";
	form_6_btns_calculation.style.display = "none";

	form_7_progessbar_calculation.classList.add("active");
});


form_7_back_btn_calculation.addEventListener("click", function(){
	form_6_calculation.style.display = "block";
	form_7_calculation.style.display = "none";

	form_7_btns_calculation.style.display = "none";
	form_6_btns_calculation.style.display = "flex";

	form_7_progessbar_calculation.classList.remove("active");
});



$("#addChild").click(function(){
	$(".childInfomore").append(
		`<div class="childInfo">
			<h5><b>Another Child </b><a class="childremove">x</a></h5>
			
			<div class="uk-grid-medium uk-grid" uk-grid="">
				<div class="uk-width-1-3@m uk-first-column">
					<label class="uk-form-label">What is their current age?</label>
					<input class="uk-input" type="number" placeholder="Enter Your Age" id="age" name="age" required="">
				</div>
				
				<div class="uk-width-1-3@m">
					<label class="uk-form-label">Total Funding required</label>
					<input class="uk-input" type="number" placeholder=""Enter Amount" name="total_Funding" id="total_Funding">
				</div>

			</div>
		</div>`);
});

  
// $('.childremove').click(function() {
//     $('.childInfomore .childInfo').remove();
//     	// return false;
// 	});

	$(document).on('click', '.childremove', function () {
	$(this).parent().parent().remove();
});

$('.calculationDone').click(function() {
$('#modal-calculation-result').removeClass('uk-open');

});
</script>
<!-- calculation modal end -->
<!-- calculator form start -->
<script>
	function calculationForm(){
 	let fullname = $('#full_Name').val();
	let mobile_Number = $('#mobile_Number').val();
	let gender = $('#gender').val();
	let cities = $('#cities').val();
	let birth = $('#birth').val();
	// let birth = $('#birth').val();
	let annual_income = $('#annual_income').val();
	let years = $('#years').val();
	let debit_value = ($('#debit_value').val() == "") ? 0 : $('#debit_value').val();
	let age = $('#age').val();
	let total_Funding = ($('#total_Funding').val() == "") ? 0 : $('#total_Funding').val();
	let enter_Amount = ($('#enter_Amount').val() == "") ? 0 : $('#enter_Amount').val();
	let emergency_fund = ($('#emergency_fund').val() == "") ? 0 : $('#emergency_fund').val();
	let insurance_Number = ($('#insurance_Number').val() == "") ? 0 : $('#insurance_Number').val();
	let inflation = 1.1;
	let incomeReplacement = 0;
	let mTotal = 0;
	
	for(let i=0; i < years; i++){
		annual_income = annual_income/inflation;
		incomeReplacement += Math.round(parseFloat(annual_income));
	}
	// console.log("clear"+mTotal);
	if(debit_value != "-"){
		mTotal += parseInt(debit_value);
		$('#debtOff').html("Rs. "+debit_value+"");
	} else {
		$('#debtOff').html("-");
	} 
	// console.log("debtOff"+mTotal);
	if(total_Funding != "-"){
		mTotal += parseInt(total_Funding);
		$('#collegeFund').html("Rs. "+total_Funding+"");
	} else {
		$('#collegeFund').html("-");
	} 
	// console.log("collegeFund"+mTotal);
	if(emergencyFund != "-"){
		mTotal += parseInt(emergencyFund);
		$('#emergencyFund').html("Rs. "+emergency_fund+"");
	} else {
		$('#emergencyFund').html("-");
	}
	// console.log("emergencyFund"+mTotal);
	if(insurance_Number != "-"){
		mTotal -= parseInt(insurance_Number);
		$('#currentInsurance').html("Rs. "+insurance_Number+"");
	} else {
		$('#currentInsurance').html("-");
	}

	let total= parseInt(incomeReplacement) + parseInt(debit_value) + parseInt(total_Funding) + parseInt(emergency_fund) - parseInt(insurance_Number)
	console.log(parseInt(total));
	if(total != 0){
		total=parseInt(total);
		$('#totalAmount').html("Rs. "+total+"");
	} else {
		$('#totalAmount').html(0);
	}
	// mTotal = parseInt(mTotal) + parseInt(incomeReplacement);
	// console.log(mTotal);
	// console.log(incomeReplacement);
	// if()

	$('#incomeReplacement').html("Rs. "+incomeReplacement+"");
	
	// $('#emergencyFund').html("Rs. "+emergency_fund+"");
	// $('#currentInsurance').html("(Rs. "+insurance_Number+" )");
	// $('#totalAmount').html(mTotal);
	
	console.log(incomeReplacement);
}

$(document).ready(function(){
    $(".reset-btn").click(function(){
    	$("#Calculation").trigger("reset");
			
			$(".form_7_calculation").css({"display": "none"});
			$(".form_6_calculation").css({"display": "none"});
			$(".form_5_calculation").css({"display": "none"});
			$(".form_4_calculation").css({"display": "none"});
            $(".form_3_calculation").css({"display": "none"});
            $(".form_2_calculation").css({"display": "none"});
            $(".form_1_calculation").css({"display": "block"});
            $(".form_1_btns_calculation").css({"display": "block"});
			$(".form_2_btns_calculation").css({"display": "none"});
			$(".form_3_btns_calculation").css({"display": "none"});
			$(".form_4_btns_calculation").css({"display": "none"});
			$(".form_5_btns_calculation").css({"display": "none"});
			$(".form_6_btns_calculation").css({"display": "none"});
			$(".form_7_btns_calculation").css({"display": "none"});
            $(".form_1_progessbar_calculation").addClass("active");
			$(".form_2_progessbar_calculation").removeClass("active");
			$(".form_3_progessbar_calculation").removeClass("active");
			$(".form_4_progessbar_calculation").removeClass("active");
			$(".form_5_progessbar_calculation").removeClass("active");
			$(".form_6_progessbar_calculation").removeClass("active");
			$(".form_7_progessbar_calculation").removeClass("active");
    });
});

document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('[maxlength]').forEach(input => {
    input.addEventListener('input', e => {
    let val = e.target.value, len = +e.target.getAttribute('maxlength');
      e.target.value = val.slice(0,len);
    })
  })
})



</script>
<!-- calculation modal end -->

<script>
	function submitPlan(planName){
		UIkit.modal("#modal-education").hide();
		UIkit.modal("#thank-you").show();
		let question1 = "";
		let planSubType = "";
		if($("#educationDate").val() != ""){
			question1 = $("#educationDate").val();
		} else if($("#weddingDate").val() != ""){
			question1 = $("#weddingDate").val();
		} else if($("#educationDate").val() != ""){
			question1 = $("#comprehensiveDate").val();
		}
		let premium_mode = $("#premium_mode_input").val();
		let premium_amount = $("#pkr").val();
		let name = $("#fullname").val();
		let dob = $("#dob").val();
		let gender = $("#gender").val();
		let mobile_no = $("#mobile_no").val();
		let city = $("#city").val();
		let planType = $("#planType").val();
		if($("#takaful-coverage").is(":checked")){
			planSubType = "takaful";
		} else if($("#Conventional-coverage").is(":checked")){
			planSubType = "conventional";
		}

		var formData  = new FormData();
		formData.append("plan_type", planType);
		formData.append("plan_date", question1);
		formData.append("premium_mode", premium_mode);
		formData.append("amount", premium_amount);
		formData.append("name", name);
		formData.append("date_of_birth", dob);
		formData.append("gender", gender);
		formData.append("mobile_no", mobile_no);
		formData.append("city", city);
		formData.append("plan_sub_type", planSubType);
		formData.append("plan_name", planName);
		$.ajax({
			type:'POST',
			url:'<?php echo url('/');?>/api/plan',
			contentType: false,
			processData: false,
			data: formData,
			headers: {
				'X-CSRF-Token': '{{ csrf_token() }}',
			},
			success:function(response){
				if(response.code == 200){
					$(".form_4").css({"display": "none"});
					$(".form_3").css({"display": "none"});
					$(".form_2").css({"display": "none"});
					$(".form_1").css({"display": "block"});
					$(".form_1_btns").css({"display": "block"});
					$(".form_2_btns").css({"display": "none"});
					$(".form_3_btns").css({"display": "none"});
					$(".form_4_btns").css({"display": "none"});
					$(".form_1_progessbar").addClass("active");
					$("#conventional").css({"display": "block"});
					$("#takaful").css({"display": "block"});
					$("#Conventional-coverage-plans").css({"display": "none"});
					$("#Takaful-coverage-plans").css({"display": "none"});
					$(".form_2_progessbar").removeClass("active");
					$(".form_3_progessbar").removeClass("active");
					$(".form_4_progessbar").removeClass("active");
					document.getElementById("planFinder").reset();
				}
			}
		});


	}
</script>
<script>
	function FormReset(){
		$(".form_4").css({"display": "none"});
		$(".form_3").css({"display": "none"});
		$(".form_2").css({"display": "none"});
		$(".form_1").css({"display": "block"});
		$(".form_1_btns").css({"display": "block"});
		$(".form_2_btns").css({"display": "none"});
		$(".form_3_btns").css({"display": "none"});
		$(".form_4_btns").css({"display": "none"});
		$(".form_1_progessbar").addClass("active");
		$("#conventional").css({"display": "block"});
		$("#takaful").css({"display": "block"});
		$("#Conventional-coverage-plans").css({"display": "none"});
		$("#Takaful-coverage-plans").css({"display": "none"});
		$(".form_2_progessbar").removeClass("active");
		$(".form_3_progessbar").removeClass("active");
		$(".form_4_progessbar").removeClass("active");
		document.getElementById("planFinder").reset();
	}

	function PlansPopup(params){
            UIkit.modal("#modal-education").show();
            $("#educationDate").val("");
            $("#weddingDate").val("");
            $("#comprehensiveDate").val("");
            if(params == 'education'){
                $("#education").css("display","block");
                $("#wedding").css("display","none");
                $("#comprehensive").css("display","none");
				FormReset();
            }else if(params == 'wedding'){
                $("#education").css("display","none");
                $("#wedding").css("display","block");
                $("#comprehensive").css("display","none");
				FormReset();
            }else if(params == 'comprehensive'){
                $("#education").css("display","none");
                $("#wedding").css("display","none");
                $("#comprehensive").css("display","block");
				FormReset();
            }
            $("#planType").val(params);
        }
</script>
<!-- current date start -->
<script>
	$(function(){
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var minDate= year + '-' + month + '-' + day;

    $('#educationDate').attr('min', minDate);
	$('#weddingDate').attr('min', minDate);
	$('#comprehensiveDate').attr('min', minDate);
	// $('#birth').attr('min', minDate);
});
</script>
<!-- current date end -->
	@include('partials.footer')
@endif