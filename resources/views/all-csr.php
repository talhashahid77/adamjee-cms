<?php include('header.php'); ?>
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="images/csr/banner.jpg" />
	<div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="about-us.php">About us</a></li>
		    <li><a href="csr.php">Corporate Social Responsibility</a></li>
		    <li><span>All</span></li>
		</ul>
	</div>
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>All CSR Campaigns</h1>
			<p>Lorem ipsum dolor sit amet, consetetur</p>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="NewsFilter">
			<form>
			    <select class="uk-select">
			        <option>2022</option>
			        <option>2021</option>
			        <option>2020</option>
			        <option>2019</option>
			    </select>
			    <select class="uk-select">
			        <option>Community</option>
			        <option>Education</option>
			        <option>Healthcare</option>
			    </select>
			    <select class="uk-select">
			        <option>Latest first</option>
			        <option>Oldest first</option>
			    </select>
			    <button class="blueBtn">Filter</button>
			</form>
		</div>
		<div class="NewsSec">
			<ul uk-grid uk-height-match=".uk-card-body">
				<!-- Card Start -->
	    		<li class="uk-width-1-2@m">
    				<a href="csr-inner.php" class="uk-card uk-card-default newsCard">
			            <div class="uk-card-media-top">
			                <img src="images/awardsachievements/1.jpg" alt="">
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar">
				            		<div class="badgeBox">COMMUNITY</div>	
				            	</div>
			                <h3>Adamjee Life partners with Govt. of Sindh to Improve Healthcare Infrastructure</h3>
			                <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p>
			                <span class="blueBtn">Read more <img src="images/right.svg" uk-svg /></span>
			                <span class="dateNews">2nd April 2022</span>
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
	    		<!-- Card Start -->
	    		<li class="uk-width-1-2@m">
    				<a href="csr-inner.php" class="uk-card uk-card-default newsCard">
			            <div class="uk-card-media-top">
			                <img src="images/awardsachievements/2.jpg" alt="">
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar">
				            		<div class="badgeBox">COMMUNITY</div>	
				            	</div>
			                <h3>Adamjee Life wins 15th Consumer Choice Award 2021</h3>
			                <p>Adamjee Life, one of Pakistan's most recognized life insurance companies, received the coveted "Icon Award" for "Excellence in Customer Service" at the Consumers Association of Pakistan's 15th Consumers Choice Awards 2021 in Karachi.</p>
			                <span class="blueBtn">Read more <img src="images/right.svg" uk-svg /></span>
			                <span class="dateNews">27th May 2022</span>
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
	    		<!-- Card Start -->
	    		<li class="uk-width-1-2@m">
    				<a href="csr-inner.php" class="uk-card uk-card-default newsCard">
			            <div class="uk-card-media-top">
			                <img src="images/awardsachievements/3.jpg" alt="">
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar">
				            		<div class="badgeBox">COMMUNITY</div>	
				            	</div>
			                <h3>Adamjee Life 14th NFEH’s CSR Award 2022</h3>
			                <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p>
			                <span class="blueBtn">Read more <img src="images/right.svg" uk-svg /></span>
			                <span class="dateNews">17th Nov 2021</span>
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
	    		<!-- Card Start -->
	    		<li class="uk-width-1-2@m">
    				<a href="csr-inner.php" class="uk-card uk-card-default newsCard">
			            <div class="uk-card-media-top">
			                <img src="images/awardsachievements/4.jpg" alt="">
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar">
				            		<div class="badgeBox">COMMUNITY</div>	
				            	</div>
			                <h3>Adamjee Life wins Brand of the Year Award, 2021</h3>
			                <p>In order to educate adults about the pressure that is unconsciously and unwillingly put on their children, Adamjee Life has started a campaign called #DreamOutLoud!</p>
			                <span class="blueBtn">Read more <img src="images/right.svg" uk-svg /></span>
			                <span class="dateNews">28th Nov 2017</span>
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
			</ul>
		</div>
		<div class="NewsPagination">
			<ul class="uk-pagination uk-flex-center" uk-margin>
			    <li><a href="javascript:;"><span uk-pagination-previous></span></a></li>
			    <li class="uk-active"><span>1</span></li>
			    <li><a href="javascript:;">2</a></li>
			    <li class="uk-disabled"><span>…</span></li>
			    <li><a href="javascript:;">8</a></li>
			    <li><a href="javascript:;">9</a></li>
			    <li><a href="javascript:;"><span uk-pagination-next></span></a></li>
			</ul>
		</div>
	</div>
</section>
<!-- Section End -->

<?php include('footer.php'); ?>