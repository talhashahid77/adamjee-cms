<style>
    div#errorDateRange .uk-modal-dialog {
        width: 380px;
        text-align: center;
        border-radius: 10px;
    }

    div#errorDateRange h4 {
        margin: 5px 0px 0px 0px ;
    }
    div#errorDateRange p {
        margin-top: 5px;
    }

    div#errorApi .uk-modal-dialog {
        width: 380px;
        text-align: center;
        border-radius: 10px;
    }

    div#errorApi h4 {
        margin: 5px 0px 0px 0px ;
    }
    div#errorApi p {
        margin-top: 5px;
    }

    svg.errorcross.uk-svg {
        width: 85px;
    }
    .errorcross .s0 {
        fill: #F27474 !important;
    }
    .tabsFormBox .uk-spinner>* {
        animation: uk-spinner-rotate 1.4s linear infinite;
        color: #1000ff;
    }
</style>
@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","inner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt fullWidth {{ $pagedata->grid_class_en}}">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->title_en}}</h1>
                    @foreach($pagedata->custompost as  $innerCustompost)
                        <a href="{{ $innerCustompost->url }}">{{ $innerCustompost->name }}<img src="{{asset('public/website/images/right.svg') }}" uk-svg /></a>
                    @endforeach
                </div>
            </div>
        </section>
    @endforeach

    @foreach($innerPages as $key => $innerPage)
        @if($key < 2 )
            <section class="SecWrap SecTopSpace whiteTabsSec">
                <div class="uk-container containCustom">
                    <h2>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif </h2>
                    @if(session()->get('url') == 'en') {!! $innerPage->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerPage->description_ur !!} @endif
                    <div class="tabsSec">
                        <ul class="tabBtn2" uk-switcher="connect: .TabBox{{ $key }}">
                            @foreach($innerPage->child as $childKey => $childs)
                                <li><a href="javascript:;">@if(session()->get('url') == 'en')  {{ $childs->title_en }} @else {{ $childs->title_ur }} @endif </a></li>
                            @endforeach
                        </ul>
                        <ul class="uk-switcher TabBox{{ $key }}">
                            @if($innerPage->sorting == 2)
                                @foreach($innerPage->child as $childKey => $child)
                                    @if($innerPage->sorting == 2 && $childKey == 0)
                                        <li>
                                            @foreach($child->article as $article)
                                                <div class="tabsContent financialTabli">
                                                    @if(session()->get('url') == 'en')  {!! $article->description_en !!} @else {!! $article->description_ur !!} @endif
                                                </div>
                                            @endforeach
                                        </li>
                                    @elseif($innerPage->sorting == 2 && $childKey == 1)
                                        <li>
                                            <div class="tabsContent">
                                                <h3>Conventional Funds</h3>
                                                @foreach($fund_prices as $fuelKey => $fund_price)
                                                    @if($fuelKey == 0)
                                                        <p>Note: Unit Price Date is {{ date('d-F-Y', strtotime($fund_price->fund_date)) }} (Policy issued till 5:30 PM)</p>
                                                    @endif
                                                @endforeach
                                                <table class="uk-table uk-table-divider">
                                                    <thead>
                                                        <tr>
                                                            <th>Fund type</th>
                                                            <th>Bid price (Rs)</th>
                                                            <th>Offer price (Rs)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($fund_prices as $fuelKey => $fund_price)
                                                            @if($fund_price->type == 'conventional')
                                                                <tr>
                                                                    <td>{{ $fund_price->fund_type }}</td>
                                                                    <td>{{ $fund_price->bid_price }}</td>
                                                                    <td>{{ $fund_price->offer_price }}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <h3>Takaful Funds</h3>
                                                @foreach($fund_prices as $fuelKey => $fund_price)
                                                    @if($fuelKey == 0)
                                                        <p>Note: Unit Price Date is {{ date('d-F-Y', strtotime($fund_price->fund_date)) }} (Policy issued till 5:30 PM)</p>
                                                    @endif
                                                @endforeach
                                                <table class="uk-table uk-table-divider">
                                                    <thead>
                                                        <tr>
                                                            <th>Fund type</th>
                                                            <th>Bid price (Rs)</th>
                                                            <th>Offer price (Rs)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($fund_prices as $fuelKey => $fund_price)
                                                            @if($fund_price->type == 'takaful')
                                                                <tr>
                                                                    <td>{{ $fund_price->fund_type }}</td>
                                                                    <td>{{ $fund_price->bid_price }}</td>
                                                                    <td>{{ $fund_price->offer_price }}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    @elseif($innerPage->sorting == 2 && $childKey == 2)
                                        <li>
                                            <div class="tabsContent">
                                                <div class="tabsFormBox">
                                                    <p>Select the date range to view performance & past prices</p>
                                                    <form class="uk-grid-small" uk-grid action="javascript:;">
                                                        <div class="uk-width-1-2@s uk-width-1-4@m">
                                                            <label class="uk-form-label">Select fund type:</label>
                                                            <select class="uk-select" id="fund_type">
                                                                <option value="conventional">Conventional</option>
                                                                <option value="takaful">Takaful</option>
                                                            </select>
                                                        </div>
                                                        <div class="uk-width-1-2@s uk-width-1-4@m">
                                                            <label class="uk-form-label">Fund Name:</label>
                                                            <select class="uk-select" id="fund_code">
                                                                @foreach($fund_prices as $fuelKey => $fund_price)
                                                                    @if($fund_price->type == 'conventional')
                                                                        <option value="{{ $fund_price->fund_code }}">{{ $fund_price->fund_type }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="uk-width-1-2@s uk-width-1-4@m">
                                                            <label class="uk-form-label">From:</label>
                                                            <input class="uk-input" id="date_from" type="date" placeholder="(DD-MM-YY)">
                                                        </div>
                                                        <div class="uk-width-1-2@s uk-width-1-4@m">
                                                            <label class="uk-form-label">To:</label>
                                                            <input class="uk-input" id="date_to" type="date" placeholder="(DD-MM-YY)">
                                                        </div>
                                                        <div class="uk-width-1-2@s">
                                                            <button class="blueBtn" onClick="generateGraph()">Generate result <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></button>
                                                        </div>
                                                        <div class="uk-width-1-2@s uk-width-1-1@m uk-text-center">
                                                            <div uk-spinner="ratio: 2" id="loadingSpinner" style="display:none"></div>
                                                        </div>    
                                                    </form>
                                                </div>
                                                <div class="chartSec">
                                                    <canvas id="chartBox" width="1050" height="160"></canvas>
                                                </div>
                                            </div>
                                        </li>
                                    @elseif($innerPage->sorting == 2 && $childKey == 3)
                                        <li>
                                        <div class="tabsContent">
                                            <div class="NewsFilter">
                                                <form>
                                                    <select class="uk-select" name="year" id="year">
                                                        <option>Select</option>
                                                    </select>
                                                    <select class="uk-select" name="month" id="month">
                                                        <option>Select</option>
                                                    </select>
                                                    <select class="uk-select" name="type" id="type">
                                                        @foreach($child->child as $innerchildKey => $innerchild)
                                                            <option value="{{ substr($innerchild->slug,1) }}">{{ $innerchild->title_en }}</option>
                                                        @endforeach
                                                    </select>
                                                    <button class="blueBtn">Filter</button>
                                                </form>
                                            </div>
                                            <div class="DownloadList">
                                                <ul>
                                                    @foreach($child->child as $innerchildKey => $innerchild)
                                                        @foreach($innerchild->article as $innerarticleKey => $innerarticle)
                                                            @php 
                                                                $postImg =  getImageFile($device_type,$innerarticle,"file","banner");
                                                            @endphp
                                                            <li><a href="{{ $postImg }}" target="_blank">{{ $innerarticle->title_en }} <img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
                                                        @endforeach 
                                                    @endforeach
                                                    
                                                </ul>
                                            </div>
                                            <div class="seeAll">
                                                <a href="javascript:;" >See all <img src="{{ asset('public/website/images/down.svg')}}" uk-svg /></a>
                                            </div>
                                        </div>
                                        </li>
                                    @endif
                                @endforeach
                            @elseif($innerPage->sorting == 1)
                                @foreach($innerPage->child as $childKey => $child)
                                    @if($innerPage->sorting == 1 && $childKey == 0)
                                        <li>
                                            <div class="tabsContent">
                                                <div class="NewsFilter">
                                                    <form>
                                                        <select class="uk-select">
                                                            <option>Select</option>
                                                        </select>
                                                        <select class="uk-select">
                                                            <option>Year</option>
                                                            <option>2022</option>
                                                            <option>2021</option>
                                                            <option>2020</option>
                                                            <option>2019</option>
                                                        </select>
                                                        <select class="uk-select">
                                                            @foreach($child->child as $innerchildKey => $innerchild)
                                                                <option value="{{ substr($innerchild->slug,1) }}">{{ $innerchild->title_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <button class="blueBtn">Filter</button>
                                                    </form>
                                                </div>
                                                <div class="DownloadList">
                                                    <ul>
                                                        @foreach($child->child as $innerchildKey => $innerchild)
                                                            @foreach($innerchild->article as $innerarticleKey => $innerarticle)
                                                                @php 
                                                                    $postImg =  getImageFile($device_type,$innerarticle,"file","banner");
                                                                @endphp
                                                                <li><a href="{{ $postImg }}" target="_blank">{{ $innerarticle->title_en }} <img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
                                                            @endforeach 
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="seeAll">
                                                    <a href="javascript:;" >See all <img src="{{ asset('public/website/images/down.svg')}}" uk-svg /></a>
                                                </div>
                                                <p><a class="clrblue" href="#financialModal" uk-toggle>Request form for financial statements <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /></a></p>
                                            </div>
                                        </li>
                                    @elseif($innerPage->sorting == 1 && $childKey == 1)
                                        <li>
                                            <div class="tabsContent">
                                                <div class="NewsFilter">
                                                    <form>
                                                        <select class="uk-select">
                                                            @foreach($child->child as $innerchildKey => $innerchild)
                                                                <option value="{{ substr($innerchild->slug,1) }}">{{ $innerchild->title_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <button class="blueBtn">Filter</button>
                                                    </form>
                                                </div>
                                                <div class="DownloadList">
                                                    <ul>
                                                        @foreach($child->child as $innerchildKey => $innerchild)
                                                            @foreach($innerchild->article as $innerarticleKey => $innerarticle)
                                                                @php 
                                                                    $postImg =  getImageFile($device_type,$innerarticle,"file","banner");
                                                                @endphp
                                                                <li><a href="{{ $postImg }}" target="_blank">{{ $innerarticle->title_en }} <img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
                                                            @endforeach 
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="seeAll">
                                                    <a href="javascript:;" >See all <img src="{{ asset('public/website/images/down.svg')}}" uk-svg /></a>
                                                </div>
                                            </div>
                                        </li>
                                    @elseif($innerPage->sorting == 1 && $childKey == 2)
                                        <li>
                                            <div class="tabsContent">
                                                <div class="NewsFilter">
                                                    <form>
                                                        <select class="uk-select">
                                                            <option>Year</option>
                                                            <option>2022</option>
                                                            <option>2021</option>
                                                            <option>2020</option>
                                                            <option>2019</option>
                                                        </select>
                                                        <button class="blueBtn">Filter</button>
                                                    </form>
                                                </div>
                                                <div class="DownloadList">
                                                    <ul>
                                                        @foreach($child->article as $innerarticleKey => $innerarticle)
                                                            @php 
                                                                $postImg =  getImageFile($device_type,$innerarticle,"file","banner");
                                                            @endphp
                                                            <li><a href="{{ $postImg }}" target="_blank">{{ $innerarticle->title_en }} <img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
                                                        @endforeach 
                                                    </ul>
                                                </div>
                                                <div class="seeAll">
                                                    <a href="javascript:;" >See all <img src="{{ asset('public/website/images/down.svg')}}" uk-svg /></a>
                                                </div>
                                            </div>
                                        </li>
                                    @elseif($innerPage->sorting == 1 && $childKey == 3)
                                        <li>
                                            <div class="tabsContent">
                                                <h3>Financial Ratios</h3>
                                                <div class="NewsFilter">
                                                    <form>
                                                        <select class="uk-select">
                                                            <option>Year</option>
                                                            <option>2022</option>
                                                            <option>2021</option>
                                                            <option>2020</option>
                                                            <option>2019</option>
                                                        </select>
                                                        <button class="blueBtn">Filter</button>
                                                    </form>
                                                </div>
                                                <p>Yearly earning per share as on 31st December 2020 <span class="clrblue">Rs. 17.84</span></p>
                                                <p>Price earnings ratio as on 31st December 2020 (in times) <span class="clrblue">12.16</span></p>
                                                <p>Breakup value per share as on 31st December 2020 <span class="clrblue">Rs. 61.75</span></p>
                                            </div>
                                        </li>
                                    @elseif($innerPage->sorting == 1 && $childKey == 4)
                                        <li>
                                            @foreach($child->article as $article)
                                                <div class="tabsContent">
                                                    @if(session()->get('url') == 'en')  {!! $article->description_en !!} @else {!! $article->description_ur !!} @endif
                                                </div>
                                            @endforeach
                                        </li>
                                    @elseif($innerPage->sorting == 1 && $childKey == 5)
                                        <li>
                                            <div class="tabsContent">
                                                <ul uk-accordion>
                                                    @foreach($child->article as $articleKey => $article)
                                                        <li class="@if($articleKey == 0) uk-open @endif">
                                                            <a class="uk-accordion-title" href="#">{{ $article->title_en }}</a>
                                                            <div class="uk-accordion-content">
                                                                @if(session()->get('url') == 'en')  {!! $article->description_en !!} @else {!! $article->description_ur !!} @endif
                                                                    @foreach($article->custompost as $customKey => $custompost)
                                                                        <p><a href="javascript:;">{{$custompost->name }}</a></p>
                                                                    @endforeach
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </section>
        @elseif($key > 1)
            <section class="SecWrap whiteTabsSec">
                <div class="uk-container containCustom">
                    <h2>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif</h2>
                    <div class="DownloadList">
                        <ul>
                            @foreach($innerPage->article as $articleKey => $article)
                                @php 
                                    $postImg =  getImageFile($device_type,$article,'file','banner');
                                @endphp
                                <li><a href="{{ $postImg }}" target="_blank">{{ $article->title_en }} <img src="{{ asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </section>
        @endif

    @endforeach

    <div id="financialModal" class="uk-flex-top financialModal uk-modal-container" uk-modal>
        <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h3>Request form for financial statements</h3>
            <p>Dear Shareholder,<br> 
            The Securities and Exchange Commission of Pakistan, vide S.R.O 470(I)/2016 dated May 31, 2016 read with Section 223 of the Companies Act 2017, has allowed companies to circulate their annual balance sheet, profit and loss account, auditor’s report, directors’ report and ancillary statements/notes/documents (“Annual Audited Accounts”) along with notice of general meeting to its shareholders in electronic form.</p> 
            <p>However, Shareholders may request a hard copy of the Annual Audited Accounts along with notice of general meetings to be sent to their registered address instead of receiving the same in electronic form on CD/DVD/USB. If you require a hard copy of the Annual Audited Accounts, please fill the following form and send it to our Share Registrar or Company Secretary at the address given below.</p>
            <p>I/We request that a hard copy of the Annual Audited Accounts along with notice of general meetings be sent to me through post. My/our particulars in this respect are as follows:</p>
            <form uk-grid>				    
                <div class="uk-width-1-2@s uk-width-1-3@m">
                    <label class="uk-form-label">Full name *</label>
                    <input class="uk-input" type="text" placeholder="Your full name" required="">
                </div>				    
                <div class="uk-width-1-2@s uk-width-1-3@m">
                    <label class="uk-form-label">Folio/CDC A/c No *</label>
                    <input class="uk-input" type="number" placeholder="xxxx-x-xxxxx-x" required="">
                </div>
                <div class="uk-width-1-2@s uk-width-1-3@m">
                    <label class="uk-form-label">Postal Address:</label>
                    <input class="uk-input" type="text" placeholder="Your full postal address">
                </div>
                <div class="uk-width-1-2@s uk-width-1-3@m">
                    <label class="uk-form-label">Contact</label>
                    <input class="uk-input" type="number" placeholder="Enter your contact number">
                </div>
                <div class="uk-width-1-2@s uk-width-1-3@m">
                    <label class="uk-form-label">CNIC *</label>
                    <input class="uk-input" type="number" placeholder="xxxx-xxxxxxx-x" required="">
                </div>
                <div class="uk-width-1-2@s uk-width-1-3@m">
                    <label class="uk-form-label">Email Address:</label>
                    <input class="uk-input" type="Email" placeholder="Enter your email">
                </div>
                
                <div class="uk-width-1-1">
                    <button class="blueBtn uk-margin-remove-top">Submit <img src="images/right.svg" uk-svg /></button>
                </div>
            </form>
            <p>The form may be sent directly to Adamjee Life Assurance Company Limited or Share Registrar at the following address</p>
            <div uk-grid>
                <div class="uk-width-1-2@s">
                    <p><strong>CDC Share Registrar Services Limited</strong></p>
                    <p>CDC House, 99-B, Block ‘B’ S.M.C.H.S. Main Shahrah-e-Faisal, Karachi</p>
                    <p>Website: www.cdcpakistan.com</p>
                </div>
                <div class="uk-width-1-2@s">
                    <p><strong>Adamjee Life Assurance Co Limited</strong></p>
                    <p>3rd & 4th Floor, Adamjee House, I.I Chundrigar Road, Karachi</p>
                    <p>Website: www.adamjeelife.com</p>
                </div>
            </div>
            <p>If you are a CDC Account Holder, you should submit your request directly to your CDC Participant through which you maintain your CDC account.</p>
        </div>
    </div>

    <div id="errorDateRange" class="uk-flex-top" uk-modal>
		<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

			<button class="uk-modal-close-default" type="button" uk-close></button>
            
			<img class="errorcross" src="{{asset('public/website/images/financial-highlights/cross-icon.svg') }}"  uk-svg />
			<h4><b>Error</b></h4>
            <p>Please select date range.</p>

		</div>
	</div>

    <div id="errorApi" class="uk-flex-top" uk-modal>
		<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

			<button class="uk-modal-close-default" type="button" uk-close></button>
            
			<img class="errorcross" src="{{asset('public/website/images/financial-highlights/cross-icon.svg') }}"  uk-svg />
			<h4><b>Error</b></h4>
            <p>Something went wrong.</p>

		</div>
	</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.9.1/chart.min.js"></script>
    <script>
    // function getMonthName(monthNumber) {
    //     const date = new Date();
    //     date.setMonth(monthNumber - 1);

    //     return date.toLocaleString('en-US', { month: 'short' });
    // }

    // function getMonths (fromDate, toDate) {
    //     const fromYear = fromDate.getFullYear();
    //     const fromMonth = fromDate.getMonth();
    //     const toYear = toDate.getFullYear();
    //     const toMonth = toDate.getMonth();
    //     const months = [];

    //     for(let year = fromYear; year <= toYear; year++) {
    //         let monthNum = year === fromYear ? fromMonth : 0;
    //         const monthLimit = year === toYear ? toMonth : 11;

    //         for(; monthNum <= monthLimit; monthNum++) {
    //             let a = [{day: 'numeric'}, {month: 'short'}, {year: 'numeric'}];
    //             let s = join(new Date, a, '-');
    //             let month = monthNum + 1;
    //             month = getMonthName(month);
    //             months.push({ year, month });
    //         }
    //     }
    //     return months;
    // }

    function join(t, a, s) {
        function format(m) {
            let f = new Intl.DateTimeFormat('en', m);
            return f.format(t);
        }
        return a.map(format).join(s);
    }

    function getDatesInRange(startDate, endDate) {
        const date = new Date(startDate.getTime());

        const dates = [];

        while (date <= endDate) {
            let a = [{day: 'numeric'}, {month: 'short'}, {year: 'numeric'}];
            let s = join(new Date(date), a, '-');
            dates.push(s);
            date.setDate(date.getDate() + 1);
        }

        return dates;
    }

    $('#fund_type').on('change', function() {
        var formData  = new FormData();
        formData.append("fund_type", this.value);
        $.ajax({
            type:'POST',
            url:'<?php echo url('/');?>/api/fund_list',
            contentType: false,
            processData: false,
            data: formData,
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success:function(response){
                if(response.code == 200){
                    $("#fund_code").empty();
                    for (let index = 0; index < response.message.length; index++) {
                        $("#fund_code").append("<option value='"+response.message[index].fund_code+"'>"+response.message[index].fund_name+"</option>")
                    }
                    
                }
            }
        });

    });

    function generateGraph(){
        if($("#date_from").val() != '' && $("#date_from").val() != ''){
            $("#loadingSpinner").css("display","block");
            var date_from = new Date($("#date_from").val());
            var date_from= date_from.getDate() + '-' + date_from.toLocaleString('en-US', {month: 'short'}) + '-' +  date_from.getFullYear();

            var date_to = new Date($("#date_to").val());
            var date_to= date_to.getDate() + '-' + date_to.toLocaleString('en-US', {month: 'short'}) + '-' +  date_to.getFullYear();

            // let monthsArr = getMonths(new Date(date_from), new Date(date_to));
            let labels = [];

            labels = getDatesInRange(new Date(date_from), new Date(date_to));
            // console.log(monthsArr);
            // monthsArr.forEach(element => {
            //     labels.push(element.month+'/'+element.year);
            // });


            let apiUrl = "";
            if($("#fund_type").val() == "conventional"){
                apiUrl = "https://alpos.adamjeelife.com/ALACLWeb_API/api/Main_/GetNAVHistory_Conventional?frm_date="+date_from+"&to_date="+date_to+"&fundCode="+$("#fund_code").val()+"";
            } else {
                apiUrl = "https://alpos.adamjeelife.com/ALACLWeb_API/api/Main_/GetNAVHistory_Takaful?frm_date="+date_from+"&to_date="+date_to+"&fundCode="+$("#fund_code").val()+"";
            }
            $.ajax({
                type:'GET',
                url: apiUrl,
            }).done(function(rs, textStatus, xhr) {
                if(xhr.status == 200){
                    $("#loadingSpinner").css("display","none");
                    let BOFR_BIDPRICE = [];
                    let BOFR_OFFRPRICE = [];
                    let graphArr = rs[0];
                    
                    for (let i = 0; i < graphArr.length; i++) {
                        BOFR_BIDPRICE.push(graphArr[i].BOFR_BIDPRICE);                  
                    }
                    for (let k = 0; k < graphArr.length; k++) {
                        BOFR_OFFRPRICE.push(graphArr[k].BOFR_OFFRPRICE);                  
                    }
                    
                    var data = {
                        labels: labels,
                        datasets: [{
                        label: 'Before Bid Price',
                        backgroundColor: '#0064BF',
                        borderColor: '#0064BF',
                        data: BOFR_BIDPRICE,
                        },
                        {
                        label: 'Before Offer Price',
                        backgroundColor: '#12845a',
                        borderColor: '#12845a',
                        data: BOFR_OFFRPRICE,
                        }]
                    };

                    var config = {
                        type: 'line',
                        data: data,
                        options: {}
                    };

                    let chartStatus = Chart.getChart("chartBox");
                    if (chartStatus != undefined) {
                        chartStatus.destroy();
                    }

                    var chartCanvas = $('#chartBox');
                    chartInstance = new Chart(chartCanvas, config);
                }
            }).fail(function (jqXHR, exception) {
                let chartStatus = Chart.getChart("chartBox");
                if (chartStatus != undefined) {
                    chartStatus.destroy();
                }
                $("#loadingSpinner").css("display","none");
                UIkit.modal("#errorApi").show();
            });
        } else {
            let chartStatus = Chart.getChart("chartBox");
            if (chartStatus != undefined) {
                chartStatus.destroy();
            }
            UIkit.modal("#errorDateRange").show();
        }
        
    }

    </script>

    @include('partials.footer')
@endif

