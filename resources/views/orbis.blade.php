@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","banner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->sub_title_en}}</h1>
                    {!! $pagedata->description_en !!}
                </div>
            </div>
        </section>
    @endforeach

    @if(isset($innerPages))
    <section class="SecWrap SecTopSpace WhiteBgSection">
        <div class="uk-container containCustom">
            <ul class="uk-grid-small" uk-grid>
                @foreach($innerPages as $innerkey => $innerPage)
                    @if($innerPage->sorting > 0)
                        @php 
                            $postImg =  getImageFile($device_type,$innerPage,"image","banner");
                        @endphp
                        <li class="{{ $innerPage->grid_class_en }}">
                            <div class="GridBox" style="background-image: url({{ $postImg }});">
                                <div class="GridBoxTxt">
                                    <h3>{{ $innerPage->title_en }}</h3>
                                    {!! $innerPage->short_desc_en !!}
                                    <a class="blueBtn" href="{{ url(session()->get('url').$innerPage->path) }}">View more <img src="{{ asset('public/website/images/right.svg')}}" uk-svg /> </a>
                                </div>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </section>
    <!-- Section Start -->
    <section class="SecWrap SecTopSpace">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>Eligibility criteria</h2>
                <div class="orbisContent">
                    <table class="uk-table uk-table-divider">
                        <thead>
                            <tr>
                                <th>Card type</th>
                                <th>Premium/contribution amount</th>
                                <th>Premium/Contribution Tenure</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Orbis Gold - Conventional</td>
                                <td>PKR. 200,000 & above</td>
                                <td>Basic annual premium only*</td>
                            </tr>
                            <tr>
                                <td>Orbis Gold - Takaful</td>
                                <td>PKR. 200,000 & above</td>
                                <td>Basic annual contribution only*</td>
                            </tr>
                        </tbody>
                    </table>
                </div>      
                <p>* Adhoc premium/contribution and/or any other premium/contribution payment mode, other than annualized premium/contribution will not be considered for Orbis</p>      
            </div>
        </div>
    </section>
    <!-- Section End -->

    <!-- Section Start -->
    <section class="SecWrap SecTopSpace WhiteBgSection">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>Apply for an ORBIS card</h2>
                <div class="tabsSec">
                <!-- This is the nav containing the toggling elements -->
                <ul class="tabBtn" uk-switcher="connect: .TabBox">
                    <li><a href="javascript:;">New Card</a></li>
                    <li><a href="javascript:;">Report a Lost Card</a></li>
                </ul>

                <!-- This is the container of the content items -->
                <ul class="uk-switcher TabBox FormTabs JobForm NewsFilter">
                    <!-- Tab li Start -->
                    <li>
                        <ul uk-grid uk-height-match=".uk-card-body">
                            <!-- Card Start -->
                            <li class="uk-width-1-1@s uk-width-1-1@m">
                                <div>
                                    <h6>Personal Details</h6>
                                <form uk-grid>				    
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Your full name*</label>
                                        <input class="uk-input" type="text" placeholder="Enter your full name" required="">
                                    </div>				    
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Mobile number*</label>
                                        <input class="uk-input" type="number" placeholder="Enter your mobile number" required="">
                                    </div>
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">CNIC*</label>
                                        <input class="uk-input" type="number" placeholder="xxxxx-xxxxxxx-x" required="">
                                    </div>
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Policy number*</label>
                                        <input class="uk-input" type="number" placeholder="MM/DD/YYYY" required="">
                                    </div>
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Address</label>
                                        <input class="uk-input" type="number" placeholder="Enter your full address" >
                                    </div>
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Please select City*</label>
                                        <select class="uk-select">

                                            <option value="">Select City</option>
                                            @foreach (getCities() as $item)
                                            <option value="{{$item}}">{{$item}}</option>    
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="uk-width-1-1@s uk-width-1-1@m">
                                        <h6>Disclaimer:</h6>
                                        <ul>
                                            <li>I certify that the information provided in my application is as per actual and correct</li>
                                            <!-- <li>I understand that any information provided by me that is found to be false or misrepresented in any respect, will be sufficient cause to (i) eliminate me from further consideration for employment, or(ii) may be subject to disciplinary action, whenever it is discovered.</li> -->
                                        </ul>
                                    </div>
                                    <div class="uk-width-1-1@s uk-width-1-1@m">
                                        <label><input class="uk-checkbox" type="checkbox" checked>Yes, I have read and consent to the terms and conditions</label>
                                    </div>
                                    
                                    <div class="uk-width-1-1">
                                        <button class="blueBtn uk-margin-remove-top">Apply now <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></button>
                                    </div>
                                </form>
                                </div>
                            </li>
                            <!-- Card End -->
                        </ul>   
                    </li>
                    <li>
                        <ul uk-grid uk-height-match=".uk-card-body">
                        <!-- Card Start -->
                            <li class="uk-width-1-1@s uk-width-1-1@m">
                                <div>
                                    <h6>Personal Details</h6>
                                <form uk-grid>				    
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Your full name*</label>
                                        <input class="uk-input" type="text" placeholder="Enter your full name" required="">
                                    </div>
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">CNIC*</label>
                                        <input class="uk-input" type="number" placeholder="xxxxx-xxxxxxx-x" required="">
                                    </div>				    
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Mobile number*</label>
                                        <input class="uk-input" type="number" placeholder="Enter your mobile number" required="">
                                    </div>
                                    
                                    <div class="uk-width-1-2@s uk-width-1-3@m">
                                        <label class="uk-form-label">Policy number*</label>
                                        <input class="uk-input" type="number" placeholder="MM/DD/YYYY" required="">
                                    </div>
                                    
                                    
                                    <div class="uk-width-1-1">
                                        <button class="blueBtn uk-margin-remove-top">Report now <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></button>
                                    </div>
                                </form>
                                </div>
                            </li>
                            <!-- Card End -->
                        </ul>
                    </li>        
            </div>     
        </div>
    </section>
    <!-- Section End -->

    <!-- Section Start -->
    <section class="SecWrap SecTopSpace">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>How to use an Orbis Reward Card?</h2>
                <ul class="uk-grid-small uk-margin-medium-top" uk-grid uk-height-match=".uk-card-body">
                    <!-- Card Start -->
                    <li class="uk-width-1-2@s uk-width-1-4@m">
                        <a href="javascript:;" class="uk-card uk-card-default tabInner">
                            <div class="uk-card-media-top">
                                <img src="{{ asset('public/website/images/orbis/faq1.jpg')}}" alt="">
                            </div>
                            <div class="uk-card-body">
                                <h3>Step 1</h3>
                                <p>Visit any of the mentioned outlets (as per deals and discount page) with your Orbis Card</p>
                                <!-- <span>Read more <img src="images/right.svg" uk-svg /></span> -->
                            </div>
                        </a>
                    </li>
                    <!-- Card End -->

                    <!-- Card Start -->
                    <li class="uk-width-1-2@s uk-width-1-4@m">
                        <a href="javascript:;" class="uk-card uk-card-default tabInner">
                            <div class="uk-card-media-top">
                                <img src="{{ asset('public/website/images/orbis/faq2.jpg')}}" alt="">
                            </div>
                            <div class="uk-card-body">
                                <h3>Step 2</h3>
                                <p>At the time of bill payment, please provide your card to the staff at the counter of the merchant outlet</p>
                                <!-- <span>Read more <img src="images/right.svg" uk-svg /></span> -->
                            </div>
                        </a>
                    </li>
                    <!-- Card End -->

                    <!-- Card Start -->
                    <li class="uk-width-1-2@s uk-width-1-4@m">
                        <a href="javascript:;" class="uk-card uk-card-default tabInner">
                            <div class="uk-card-media-top">
                                <img src="{{ asset('public/website/images/orbis/faq3.jpg')}}" alt="">
                            </div>
                            <div class="uk-card-body">
                                <h3>Step 3</h3>
                                <p>The merchant/outlet will swipe the card, and provide the discounted bill</p>
                                <!-- <span>Read more <img src="images/right.svg" uk-svg /></span> -->
                            </div>
                        </a>
                    </li>
                    <!-- Card End -->

                    <!-- Card Start -->
                    <li class="uk-width-1-2@s uk-width-1-4@m">
                        <a href="javascript:;" class="uk-card uk-card-default tabInner">
                            <div class="uk-card-media-top">
                                <img src="{{ asset('public/website/images/orbis/faq4.jpg')}}" alt="">
                            </div>
                            <div class="uk-card-body">
                                <h3>Step 4</h3>
                                <p>Pay the remaining amount either by cash or credit/debit card</p>
                                <!-- <span>Read more <img src="images/right.svg" uk-svg /></span> -->
                            </div>
                        </a>
                    </li>
                    <!-- Card End -->
                </ul>          
            </div>
        </div>
    </section>
    <!-- Section End -->

    @foreach($innerPages as $innerkey => $innerPage)
        @if($innerPage->sorting == 0)
            <section class="SecWrap SecTopSpace WhiteBgSection">
                <div class="uk-container containCustom">
                    <div class="innerPageContent2">
                        <h2>{{ $innerPage->title_en }}</h2>
                        <ul class="faqsitems uk-margin-medium-top" uk-accordion >
                        @foreach($innerPage->article as $subkey => $subChildInnerPage)
                            <li class="@if($subkey == 0) uk-open @endif"> 
                                <a class="uk-accordion-title" href="#">{{ $subChildInnerPage->title_en }}</a>
                                <div class="uk-accordion-content">
                                {!! $subChildInnerPage->description_en !!}
                                </div>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                    
                </div>
            </section>
        @endif
    @endforeach
    @endif

    @include('partials.footer')
@endif