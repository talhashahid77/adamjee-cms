@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

<!-- Section Start -->
@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php
        if(session()->get('url') == "en"){
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_en); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_en); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_en); 
            }
        } else { 
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_ur); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_ur); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_ur); 
            }
        }
        if(isset($postImageBanner[1])){
            $Img = $postImageBanner[1];
        }else{
            $Img = "";
        }
        $postImg = URL::to('/')."/public/source/".$Img."";
        $url = URL::to('/').session()->get('url');
    @endphp
   
	<section class="HeaderInnerPage">
		{{-- @if($key == 1) --}}
		<img src="{{ $postImg }}" />
		{{-- @endif --}}
	@include('partials.breadcrumb')
@endforeach

@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php
        if(session()->get('url') == "en"){
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_en); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_en); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_en); 
            }
        } else { 
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_ur); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_ur); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_ur); 
            }
        }
        if(isset($postImageBanner[0])){
            $Img = $postImageBanner[0];
        }else{
            $Img = "";
        }
        $postImg = URL::to('/')."/public/source/".$Img."";
        $url = URL::to('/').session()->get('url');
    @endphp


	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>{{ $pagedata->title_en}}</h1>
            <p>{{ $pagedata->sub_title_en }}</p>
		</div>
	</div>

@endforeach
</section>
<!-- Section End -->
@if(!empty($postsdata))
	<!-- Section Start -->
	<section class="SecWrap SecTopSpace">
		<div class="uk-container containCustom">
			<div class="innerPageContent2">
				@foreach($postsdata as $key => $postdata)
					@php 
					if(session()->get('url') == "en"){
						$postImageSection = json_decode($postdata->file_en);
					} else { 
						$postImageSection = json_decode($postdata->file_ur);
					}

					if(isset($postImageSection[0])){
						$Img = $postImageSection[0];
					}else{
						$Img = "";
					}
					$postImg = URL::to('/')."/public/source/".$Img."";
					$url = URL::to('/').session()->get('url');
					@endphp

					@if($key == 0)
						<h2>{{$postdata->title_en}}</h2>
						{!! $postdata->description_en !!}
						<div class="DownloadList">
							<ul>
					@else
						<li><a href="{{ $postImg }}" download>{{$postdata->title_en}}<img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					@endif
				@endforeach
				
				
					<!-- <li><a href="javascript:;">Description Type (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">PMD (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">Takaful Fund (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">PTF Policy (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">Shari’ah Advisor (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">Group Takaful Waqaf Rules (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">Takaful License (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">PMD Group Takaful (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">Takaful Fatwa (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">Window Takaful Operations (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li>
					<li><a href="javascript:;">Waqf Deed (PDF) <img src="{{asset('public/website/images/icons/download.svg')}}" uk-svg /></a></li> -->
				</ul>
			</div>
			</div>
		</div>
	</section>
	<!-- Section End -->
@endif

@include('partials.footer')
@endif