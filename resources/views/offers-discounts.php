<?php include('header.php'); ?>
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="images/orbis/education/banner.jpg" />
	<div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="javascript:;">Orbis</a></li>
		    <li><span>Offers & Discounts</span></li>
		</ul>
	</div>
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>Offers & Discounts</h1>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur</p> -->
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
        <div class="NewsFilter ">

			<form>
                <div class="uk-form-controls GuideSearchInput">
                    <input class="uk-input" id="email" type="text" placeholder="Enter keyword">
                </div>
                <div class="uk-form-controls">
			    <select class="uk-select">
			        <option>City</option>
			        <option>City</option>
			        <option>City</option>
			    </select>
                </div>
                <div class="uk-form-controls">
			    <select class="uk-select">
			        <option>Eateries</option>
			        <option>Healthcare & Wellness</option>
			        <option>Lifestyle</option>
			    </select>
                </div>
                <div class="uk-form-controls">
			    <select class="uk-select">
			        <option>Orbis Gold Conventional</option>
			        <option>Orbis Gold Takaful</option>
			    </select>
                </div>
			    <button class="blueBtn">Filter</button>
			</form>
		</div>
        <div class="innerPageContent2">
            <h2>All results</h2>
        </div>
        
			<ul class="uk-grid-small" uk-grid uk-height-match=".uk-card-body">
				<!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/1.jpg" alt="" />
                            <div class="badgeBox">20%</div>
			            </div>
                        
			            <div class=" uk-card-body ">
			            	<div class="badgesBar badgesBarDiscount">
				            	<div class="badgeBox">22 OUTLETS</div>
                                <div class="badgeBox">TERMS APPLIED</div>
                                	
				            </div>
                            
			                <h3>Pie in the Sky</h3>
			                
			                
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
	    		<!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts ">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/2.jpg" alt="">
                            <div class="badgeBox">20%</div>
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar badgesBarDiscount">
                                <div class="badgeBox">22 OUTLETS</div>
                                <div class="badgeBox">TERMS APPLIED</div>
				            </div>
			                <h3>Hobnob bakery</h3>
			              
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
	    		<!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts ">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/3.jpg" alt="">
                            <div class="badgeBox">20%</div>
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar badgesBarDiscount">
                                <div class="badgeBox">22 OUTLETS</div>
                                <div class="badgeBox">TERMS APPLIED</div>
				            </div>
			                <h3>Dunkin Donuts</h3>
			              
			                
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->

                <!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts ">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/4.jpg" alt="">
                            <div class="badgeBox">20%</div>
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar badgesBarDiscount">
                                <div class="badgeBox">22 OUTLETS</div>
                                <div class="badgeBox">TERMS APPLIED</div>
				            </div>
			                <h3>Chaai Khana</h3>
			                
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
                <!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts ">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/5.jpg" alt="">
                            <div class="badgeBox">20%</div>
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar badgesBarDiscount">
                                <div class="badgeBox">22 OUTLETS</div>
                                <div class="badgeBox">TERMS APPLIED</div>
				            </div>
			                <h3>Kababjees</h3>
			              
			                
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
	    		<!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts ">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/6.jpg" alt="">
                            <div class="badgeBox">20%</div>
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar badgesBarDiscount">
                                <div class="badgeBox">22 OUTLETS</div>
                                <div class="badgeBox">TERMS APPLIED</div>	
				            </div>
			                <h3>Howdy</h3>
			               
			                
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
                <!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts ">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/7.jpg" alt="">
                            <div class="badgeBox">20%</div>
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar badgesBarDiscount">
                                <div class="badgeBox">22 OUTLETS</div>
                                <div class="badgeBox">TERMS APPLIED</div>	
				            </div>
			                <h3>Burger Lab</h3>
			                
			                
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
                   <!-- Card Start -->
	    		<li class="uk-width-1-4@m uk-width-1-2@s">
    				<a href="pie-in-the-sky.php" class="uk-card uk-card-default newsCard discounts ">
			            <div class="uk-card-media-top discountBadge">
			                <img src="images/orbis/discount/8.jpg" alt="">
                            <div class="badgeBox">20%</div>
			            </div>
			            <div class="uk-card-body">
			            	<div class="badgesBar badgesBarDiscount">
				            		<div class="badgeBox">PRE SALES</div>	
				            	</div>
			                <h3>Burger King</h3>
			                
			                
			            </div>
	    			</a>
	    		</li>
	    		<!-- Card End -->
			</ul>

            <div class="see-all uk-margin-medium-top">
                <a><u>See All</u></a>
            </div>
            
		</div>    
	</div>
</section>
<!-- Section End -->


<?php include('footer.php'); ?>