<?php include('header.php'); ?>
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="images/window/banner.jpg" />

	@include('partials.breadcrumb')
	{{-- <div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="about-us.php">About us</a></li>
		    <li><span>Window Takaful Operations</span></li>
		</ul>
	</div> --}}
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>Window Takaful Operations</h1>
			<p>Lorem ipsum dolor sit amet, consetetur</p>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="innerPageContent2">
			<h2>Introduction to Window Takaful Operations</h2>
			<p>Window Takaful Operations were introduced by Adamjee Life in 2016 after receiving grant of operations for commencement of Window Takaful Operations from SECP; Deeming us eligible for providing Shahriah Compliant products and services.</p> 
			<p>The word Takaful is derived from the Arabic verb Kafala, which means to guarantee; to help; to take care of one’s needs. Takaful is a system based on the principle of Ta’awun (mutual assistance) and Tabarru (voluntary contribution), where risk is shared collectively by a group of participants, who by paying contributions to a common fund, agree to jointly guarantee themselves against loss or damage to any one of them as defined in the pact.</p> 
			<p>Takaful is operated on the basis of shared responsibility, brotherhood, solidarity and mutual cooperation.</p> 
			<p>The Window Takaful Operations of Adamjee Life offers two Shahriah Compliant Funds; Tameen Fund and Maza’af Fund. Tameen Fund has a moderate to low risk profile that generates stable and secure returns by balancing the investment in long term money markets instruments including term deposits in Islamic Banks and Sukuk Bonds. Whereas, Maza’af Fund has a moderate to high risk profile that generates higher returns over the long run in Shahriah Compliant Equities and Islamic mutual funds.</p>
			<div class="DownloadList">
			<ul>
				<li><a href="javascript:;">Description Type (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">PMD (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Takaful Fund (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">PTF Policy (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Shari’ah Advisor (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Group Takaful Waqaf Rules (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Takaful License (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">PMD Group Takaful (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Takaful Fatwa (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Window Takaful Operations (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Waqf Deed (PDF) <img src="images/icons/download.svg" uk-svg /></a></li>
			</ul>
		</div>
		</div>
	</div>
</section>
<!-- Section End -->

<?php include('footer.php'); ?>