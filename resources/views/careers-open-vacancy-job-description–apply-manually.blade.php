<style>
    label.error {
    color: red !important;
}
label#file\[\]-error {
    position: absolute;
    bottom: -30px;
}
#group1 input[type="radio"],#group2 input[type="radio"],#group3 input[type="radio"] {
    height: 15px;
}
</style>

@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

<!-- Section Start -->
@foreach($pagesdata as $key => $pagedata)
    @php 
        $postImg =  getImageFile($device_type,$pagedata,"image","inner");
    @endphp
<section class="HeaderInnerPage">
    <img src="{{ $postImg }}" />
    @include('partials.breadcrumb')
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>{{ $pagedata->title_en}}</h1>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur</p> -->
		</div>
	</div>
</section>
@endforeach
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="innerPageContent">
			<h2>22/6/2022</h2>
			<h3>Deputy Manager - New Business Operations</h3>
		</div>  		
        <table class="uk-table uk-table-divider">
            <tbody>
                <tr>
                    <td><b>Department</b></td>
                    <td>Agency Distribution</td>
                </tr>
                <tr>
                    <td><b>Designation</b></td>
                    <td>SENIOR OFFICER AGENCY OPERATIONS</td>
                </tr>
                <tr>
                    <td><b>Nature of Employment</b></td>
                    <td>Permanent</td>
                </tr>
                <tr>
                    <td><b>Total Positions</b></td>
                    <td>1</td>
                </tr>
                <tr>
                    <td><b>Experience Required</b></td>
                    <td>1-2 Years</td>
                </tr>
                <tr>
                    <td><b>Key Responsibilities</b></td>
                    <td>Prepare commission statements for disbursement to the sales force <br/> Provide timely and useful information to sales force to facilitate achievement of sales targets.<br/> Facilitate branches pan-Pakistan, related to the queries raised and guide them with the solutions. Monitor the internal software related issues and resolve ISD errors in coordination with ISD. <br/> Provide backup support for the post issuance matters. <br/> Resolve issues on a regular basis as communicated by the sales force in in reference to inter- departmental problems.</td>
                </tr>
                <tr>
                    <td><b>Competencies</b></td>
                    <td>Communication Skills -Multitasking -Pressure handling</td>
                </tr>
                <tr>
                    <td><b>Qualification Required</b></td>
                    <td>Masters in Business Administration (MBA)</td>
                </tr>
                <tr>
                    <td><b>Location</b></td>
                    <td>Karachi</td>
                </tr>
                <tr>
                    <td><b>Last Date to Apply</b></td>
                    <td>22/6/2022</td>
                </tr>
                
            </tbody>
        </table>
    </div>	
</section>
<!-- Section End -->

<!-- Section Start -->
<section class="SecWrap SecTopSpace WhiteBgSection">
	<div class="uk-container containCustom">
		<div class="innerPageContent2">
			<h2>Employment Application Form</h2>            
        </div>
        <div class="employeeForm">
            <!-- multistep form -->
            <form id="career_form"class="wrappermultiform">
                @csrf
                <div class="header">
                    <ul>
                        <li class="active form_1_progessbar">
                            <!-- <div> -->
                                <!-- <p>1</p> -->
                            <!-- </div> -->
                        </li>
                        <li class="form_2_progessbar">
                            <!-- <div>
                                <p>2</p>
                            </div> -->
                        </li>
                        <li class="form_3_progessbar">
                            <!-- <div>
                                <p>3</p>
                            </div> -->
                        </li>
                        <li class="form_4_progessbar">
                            <!-- <div>
                                <p>3</p>
                            </div> -->
                        </li>
                        <li class="form_5_progessbar">
                            <!-- <div>
                                <p>3</p>
                            </div> -->
                        </li>
                    </ul>
                </div>
                <div class="form_wrap NewsFilter JobForm">
                    <div class="form_1 data_info" >
                        <h5>Personal Detail</h5>
                            <div class="uk-grid-medium" uk-grid >
                                <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                    <label for="full_name">Your full name</label>
                                    <input class="uk-input" type="text" placeholder="Enter your full name" name="name" id="full_name" required >
                                </div>

                                <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                    <label for="address">Email address</label>
                                    <input class="uk-input" type="email" placeholder="What's your email" name="email" id="address" required>
                                </div>

                                <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                    <label for="mobile_number">Mobile number</label>
                                    <input class="uk-input" type="number" placeholder="Mobile number" minlength="13" maxlength="13" name="mobile_no" id="mobile_number" required >
                                </div>

                                <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">                                    
                                    <label for="Cnic">CNIC</label>
                                    <input class="uk-input" type="number" placeholder="xxxxx-xxxxxxx-x" minlength="13" maxlength="13" name="cnic" id="Cnic"  required>
                                </div>
                                <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                    <label for="birth">Date of birth</label>
                                    <input class="uk-input" type="date" placeholder="MM/DD/YYYY" name="date_of_birth" id="birth" required >
                                </div>
                                <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                    <label for="Cnic">Emergency number</label>
                                    <input class="uk-input" type="number" placeholder="Enter your emergency contact" name="emergency_no" id="Emergency"  required>
                                </div>
                                <div class="uk-width-2-3@m  uk-grid-margin uk-first-column">
                                    <label for="Cnic">Address</label>
                                    <input class="uk-input" type="text" placeholder="Enter your full address" name="address" id="Address" required >
                                </div>
                                <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                    <label class="uk-form-label">City</label>
                                    <select name="city" class="uk-select" required>
                                        <option value="">Select City</option>
                                        @foreach (getCities() as $item)
                                        <option value="{{$item}}">{{$item}}</option>    
                                        @endforeach
                                       
                                    </select>
                                </div>  
                            </div>
                    </div>
                    <div class="form_2 data_info" style="display: none;">
                        <h5>Education</h5>
                        <div class="uk-grid-medium" uk-grid >
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label class="uk-form-label">Last/current qualification</label>
                                <select class="uk-select" name="qualification" required>
                                    <option value="">Select</option>
                                    <option>Enter your latest qualification</option>
                                    <option>Enter your latest qualification</option>
                                </select>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="address">Institution</label>
                                <input class="uk-input" type="text" placeholder="Enter your institution" name="institution" id="" required>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="mobile_number">Additional certification</label>
                                <input class="uk-input" type="number" placeholder="Enter any additional certifications" name="certification" id="" required>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">                                    
                                <label for="Cnic">Start date</label>
                                <input class="uk-input" type="date" placeholder="DD/MM/YYYY" name="start_date" id="" required>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="birth">End date</label>
                                <input class="uk-input" type="date" placeholder="End date" name="end_date" id="" required>
                            </div>
                            <div class="uk-width-1-3@m uk-grid-margin uk-first-column uk-margin-large-top">
                                <label>
                                    <input class="uk-checkbox" type="checkbox"  name="progress" required>
                                    in Progress
                                </label>
                            </div>
                        </div> 
                    </div>
                    <div class="form_3 data_info" style="display: none;">
                        <h5>Employment Details</h5>
                        <div class="uk-grid-medium" uk-grid >
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label class="uk-form-label">Last/current employer name</label>
                                <select class="uk-select" name="current_employe_name" required>
                                    <option value="">Select</option>
                                    <option>Last/current employer name</option>
                                    <option>Last/current employer name</option>
                                </select>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="Designation">Designation</label>
                                <input class="uk-input" type="text" placeholder="Enter your last designation" name="designation" id="" required>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label class="uk-form-label" for="mobile_number">Functional area</label>
                                <select class="uk-select" name="functional_area" id="" required>
                                    <option value="">Select</option>
                                    <option>Last/current employer name</option>
                                    <option>Last/current employer name</option>
                                </select>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">                                    
                                <label for="start_date">Start date</label>
                                <input class="uk-input" type="text" placeholder="DD/MM/YYYY" name="startDate"  id="" required >
                            </div>
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="end_date">End date</label>
                                <input class="uk-input" type="text" placeholder="End date" name="endDate" id=""  required >
                            </div>
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column uk-margin-large-top">
                                <label>
                                    <input class="uk-checkbox" type="checkbox"  name="process" id=""  required >
                                    in Progress
                                </label>
                            </div>
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="salary">Gross salary (Exc. allowances)</label>
                                <input class="uk-input" type="text" placeholder="Your salary excluding allowances" id="salary" name="gross_salary" id=""  required>
                            </div>
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="Benefits">Benefits details</label>
                                <input class="uk-input" type="text" placeholder="Full details of your benefits" id="Benefits" name="benefits_details" id=""  required>
                            </div>
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="expected_salary">Expected salary</label>
                                <input class="uk-input" type="text" placeholder="Enter your expected salary" id="expected_salary" name="expected_salary" id=""  required>
                            </div>
                        </div>  
                    </div>
                    <div class="form_4 data_info" style="display: none;">
                        <h5>Additional details</h5>
                        <div class="uk-grid-medium" uk-grid >
                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="Skills">Skills</label>
                                <input class="uk-input" type="text" placeholder="Enter skills relevant to the job"   name="Skills" id=""  required >
                            </div>

                            <div class="uk-width-1-1@m  uk-grid-margin uk-first-column">
                                <p>Are you related to / acquainted with anybody working for Adamjee Life? If yes, please mention his / her name, designation and relationship.</p>

                                <div id="group1">
                                    <input  type="radio" name="group1" id="11yes"  required >
                                    <label for="11yes"> Yes</label>
                                    <input  type="radio" name="group1" id="11no"  required >
                                    <label for="11no">No</label>
                                </div>
                            </div>

                            <div class="uk-width-1-1@m  uk-grid-margin uk-first-column">
                                <p>Have you been employed in our organization before? If yes, kindly mention the designation.</p>
                                <div id="group2">
                                <label for="22yes"><input  type="radio" name="group2" id="22yes" required  > Yes</label>
                                <label for="22no"><input  type="radio" name="group2" id="22no"  required > No</label>
                                </div>
                            </div>

                            <div class="uk-width-1-1@m  uk-grid-margin uk-first-column">
                                <p>Do you have any special needs? If yes, please specify</p>
                                <div id="group3">
                                <label for="33yes"><input  type="radio" name="group3" id="33yes"  required > Yes</label>
                                <label for="44no"><input  type="radio" name="group3"  id="44no" required  > No</label>
                                </div>
                            </div>

                            <div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
                                <label for="Skills">Please specify your special needs</label>
                                <input class="uk-input" type="text" placeholder="Please specify any special needs" name="special_Skills" id=""  required >
                            </div>
                           
                           
                        </div>
                    </div>
                    
                    <div class="form_5 data_info" style="display: none;">
                    <div class="uk-upload-box">
                                
                        <h5>Upload Cv</h5>
                        <div class="uk-grid-medium" uk-grid >
                            <div class="uk-width-1-1@m">
                                <div class="uk-upload-box">
                                    <div id="error-alert" class="uk-alert-danger uk-margin-top uk-hidden" uk-alert>
                                        <p id="error-messages"></p>
                                    </div>
                                    <div class="drop__zone custom uk-placeholder uk-text-center">
                                        <span uk-icon="icon: cloud-upload"></span>
                                        <span class="uk-text-middle uk-margin-small-left">Drop file here or</span>
                                        <br/>
                                        <div class="selectForm" uk-form-custom >
                                            <input name="file[]" type="file" accept="image/png, image/jpeg, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple required >
                                            <span class="uk-link">Select file</span>
                                        </div>
        
                                    
                                        <ul id="preview" class="uk-list uk-grid-match uk-child-width-1-5@m uk-text-center uk-grid-small" uk-grid uk-scrollspy="cls: uk-animation-scale-up; target: .list-item; delay: 80"></ul>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@m">
                                <div class="employeeDis">
                                    <h5>Disclaimer:</h5>
                                    <ul>
                                        <li>I certify that all information I have provided in my application is true and correct.</li>
                                        <li>I understand that any information provided by me that is found to be false or misrepresented in any respect, will be sufficient cause to (i) eliminate me from further consideration for employment, or(ii) may be subject to disciplinary action, whenever it is discovered.</li>
                                        <li>I understand and agree that all my personal information provided above will be used by Adamjee Life for employment purpose only</li>  
                                    </ul>
                                </div>
                            </div>
                            <div class="uk-width-1-1@m">
                                 <label>
                                    <input class="uk-checkbox" type="checkbox" checked="">
                                    Yes, I have read and consent to the terms and conditions
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btns_wrap">
                    <div class="common_btns form_1_btns">
                        <button type="button" class="btn_next blueBtn">Next<img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
                    </div>
                    <div class="common_btns form_2_btns" style="display: none;">
                        <button type="button" class="btn_back transparentbtn"><img src="{{asset('public/website/images/icons/chevron_left.svg')}}" uk-svg /> Back</button>
                        <button type="button" class="btn_next blueBtn">Next<img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
                    </div>
                    <div class="common_btns form_3_btns" style="display: none;">
                        <button type="button" class="btn_back transparentbtn"><img src="{{asset('public/website/images/icons/chevron_left.svg')}}" uk-svg /> Back</button>
                        <button type="button" class="btn_next blueBtn">Next<img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
                    </div>
                    <div class="common_btns form_4_btns" style="display: none;">
                        <button type="button" class="btn_back transparentbtn"><img src="{{asset('public/website/images/icons/chevron_left.svg')}}" uk-svg /> Back</button>
                        <button type="button" class="btn_next blueBtn">Next<img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /></button>
                    </div>
                    <div class="common_btns form_5_btns" style="display: none;">
                        <button type="button" class="btn_back transparentbtn"><img src="{{asset('public/website/images/icons/chevron_left.svg')}}" uk-svg /> Back</button>
                        <button type="button" class="btn_done blueBtn">Done <img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg /> </button>
                    </div>
                </div>
            </form>

            <div class="modal_wrapper">
                <div class="shadow"></div>
                <div class="success_wrap">
                    <span class="modal_icon"><ion-icon name="checkmark-sharp"></ion-icon></span>
                    <p>You have successfully completed the process.</p>
                </div>
            </div>
        </div>    
	</div>
</section>
<!-- Section End -->

<script>
var form_1 = document.querySelector(".form_1");
var form_2 = document.querySelector(".form_2");
var form_3 = document.querySelector(".form_3");
var form_4 = document.querySelector(".form_4");
var form_5 = document.querySelector(".form_5");


var form_1_btns = document.querySelector(".form_1_btns");
var form_2_btns = document.querySelector(".form_2_btns");
var form_3_btns = document.querySelector(".form_3_btns");
var form_4_btns = document.querySelector(".form_4_btns");
var form_5_btns = document.querySelector(".form_5_btns");


var form_1_next_btn = document.querySelector(".form_1_btns .btn_next");
var form_2_back_btn = document.querySelector(".form_2_btns .btn_back");
var form_2_next_btn = document.querySelector(".form_2_btns .btn_next");
var form_3_back_btn = document.querySelector(".form_3_btns .btn_back");
var form_3_next_btn = document.querySelector(".form_3_btns .btn_next");
var form_4_back_btn = document.querySelector(".form_4_btns .btn_back");
var form_4_next_btn = document.querySelector(".form_4_btns .btn_next");
var form_5_back_btn = document.querySelector(".form_5_btns .btn_back");

var form_2_progessbar = document.querySelector(".form_2_progessbar");
var form_3_progessbar = document.querySelector(".form_3_progessbar");
var form_4_progessbar = document.querySelector(".form_4_progessbar");
var form_5_progessbar = document.querySelector(".form_5_progessbar");

var btn_done = document.querySelector(".btn_done");
var modal_wrapper = document.querySelector(".modal_wrapper");
var shadow = document.querySelector(".shadow");

form_1_next_btn.addEventListener("click", function(){

    var form1 = $('#career_form');
     form1.validate({ // initialize the plugin
            rules: {
                name: {
                    required: true,
                   
                },
                email: {
                	required: true,
                	 email: true,
                },

                mobile_no: {
                	required: true,
                	minlength: 13,
                    maxlength: 13,
                },
                cnic: {
                	required: true,
                	minlength: 13,
                    maxlength: 13,
                },
                date_of_birth: {
                	required: true,
                	
                },
                emergency_no: {
                	required: true,
                	minlength: 13,
                    maxlength: 13,
                },
                address: {
                	required: true,
                	
                },
                city: {
                	required: true,
                	
                },
            }
        });
       
        if(form1.valid() === true) {

            form_1.style.display = "none";
            form_2.style.display = "block";

            form_1_btns.style.display = "none";
            form_2_btns.style.display = "flex";

            form_2_progessbar.classList.add("active");
        
        }
});

form_2_back_btn.addEventListener("click", function(){

   
            form_1.style.display = "block";
            form_2.style.display = "none";

            form_1_btns.style.display = "flex";
            form_2_btns.style.display = "none";

            form_2_progessbar.classList.remove("active");
        
});

form_2_next_btn.addEventListener("click", function(){


    var form2 = $('#career_form');
     form2.validate({ // initialize the plugin
            rules: {
                qualification: {
                    required: true,
                   
                },
                institution: {
                	required: true,
                	 
                },

                certification: {
                	required: true,
                	
                },
                start_date: {
                	required: true,
                	
                },
                end_date: {
                	required: true,
                	
                },
                progress: {
                	required: true,
                	
                },

            }
        });
       
        if(form2.valid() === true) {
    
            form_2.style.display = "none";
            form_3.style.display = "block";

            form_3_btns.style.display = "flex";
            form_2_btns.style.display = "none";

            form_3_progessbar.classList.add("active");
        }
});

form_3_back_btn.addEventListener("click", function(){
	form_2.style.display = "block";
	form_3.style.display = "none";

	form_3_btns.style.display = "none";
	form_2_btns.style.display = "flex";

	form_3_progessbar.classList.remove("active");
});


form_3_next_btn.addEventListener("click", function(){

    var form3 = $('#career_form');
     form3.validate({ // initialize the plugin
            rules: {
                qualification: {
                    required: true,
                   
                },
                institution: {
                	required: true,
                	 
                },

                certification: {
                	required: true,
                	
                },
                startDate: {
                	required: true,
                	
                },
                endDate: {
                	required: true,
                	
                },
                process: {
                	required: true,
                	
                },

                functional_area: {
                	required: true,
                	
                },

                

            }
        });
       
        if(form3.valid() === true) {
            form_3.style.display = "none";
            form_4.style.display = "block";

            form_4_btns.style.display = "flex";
            form_3_btns.style.display = "none";

            form_4_progessbar.classList.add("active");
    
    }
});

form_4_back_btn.addEventListener("click", function(){
	form_3.style.display = "block";
	form_4.style.display = "none";

	form_4_btns.style.display = "none";
	form_3_btns.style.display = "flex";

	form_4_progessbar.classList.remove("active");
});


form_4_next_btn.addEventListener("click", function(){
    var form4 = $('#career_form');
     form4.validate({ // initialize the plugin
            rules: {
                Skills: {
                    required: true,
                   
                },
                group1:{
                    required: true
                },
                group2:{
                    required: true
                },
                group3:{
                    required: true
                },

                special_Skills: {
                	required: true,
                	
                },
    

            }
        });
       
        if(form4.valid() === true) {

        form_4.style.display = "none";
        form_5.style.display = "block";

        form_5_btns.style.display = "flex";
        form_4_btns.style.display = "none";

        form_5_progessbar.classList.add("active");
        }

    });

form_5_back_btn.addEventListener("click", function(){
	form_4.style.display = "block";
	form_5.style.display = "none";

	form_5_btns.style.display = "none";
	form_4_btns.style.display = "flex";

	form_5_progessbar.classList.remove("active");
});
btn_done.addEventListener("click", function(){
	modal_wrapper.classList.add("active");
})

shadow.addEventListener("click", function(){
	modal_wrapper.classList.remove("active");
})
</script>



<script>

const UPLOAD = {
	BASE_PATH: window.location.origin,
	ACCEPTED_DOC_MIMES: [
		"image/png",
		"image/jpeg",
		"application/pdf",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/msword"
	],
	DOC_MIMES: [
		"application/pdf",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/msword"
	],
	FileInputs: [...document.querySelectorAll("input[type='file']")],
	Init: () => {
		UPLOAD.AddEventListeners();
	},
	AddEventListeners: () => {
		UPLOAD.FileInputs.map((fileInput) => {
			fileInput.addEventListener("change", UPLOAD.HandleFileChange);
		});
		UPLOAD.FileInputs.map((fileInput) => {
			fileInput.addEventListener("blur", (e) => {
				// console.log(e.target);
			});
		});
		UPLOAD.FileInputs.map((fileInput) => {
			const box = fileInput.closest(".uk-placeholder");
			box.addEventListener(
				"dragover",
				(e) => {
					e.preventDefault();
					e.stopPropagation();
					box.classList.add("uk-box-shadow-medium");
				},
				false
			);
		});
		UPLOAD.FileInputs.map((fileInput) => {
			const box = fileInput.closest(".uk-placeholder");
			box.addEventListener(
				"dragleave",
				(e) => {
					e.preventDefault();
					e.stopPropagation();
					box.classList.remove("uk-box-shadow-medium");
				},
				false
			);
		});
		UPLOAD.FileInputs.map((fileInput) => {
			const box = fileInput.closest(".uk-placeholder");
			box.addEventListener(
				"drop",
				(e) => {
					e.preventDefault();
					e.stopPropagation();
					box.classList.remove("uk-box-shadow-medium");
					const dropZone = e.target.closest(".drop__zone");
					const componentContainer = dropZone.closest(".uk-upload-box");
					let preview, alertBox, alertMsg;
					preview = dropZone.querySelector(`#preview`);
					alertBox = componentContainer.querySelector(`#error-alert`);
					alertMsg = componentContainer.querySelector(`#error-messages`);
					let files;
					let isMultiple = false;
					if (fileInput.hasAttribute("multiple")) {
						isMultiple = true;
					}
					if (!e.dataTransfer.files.length) return;
					/** If items dropped are more than one file and the input doesn't accept multiple, do not accept the files. Clear preview field, show notification and add shake animatin to dropzone */
					if (isMultiple === false && e.dataTransfer.files.length > 1) {
						UPLOAD.RemoveChild(preview);
						UPLOAD.AddShakeAnimation(dropZone);
						UIkit.notification({
							message: "Cannot accept multiple files when expecting a single file!",
							status: "danger",
							pos: "center",
							timeout: 4000
						});
						return;
					}
					files = e.dataTransfer.files;
					fileInput.files = files;
					const options = {
						isMultiple,
						files,
						preview,
						alertBox,
						alertMsg,
						fileInput,
						dropZone
					};
					if (isMultiple) {
						UPLOAD.PreviewMultipleDocs(options);
					} else {
						UPLOAD.PreviewSingleDoc(options);
					}
				},
				false
			);
		});
	},
	Reset: (e) => {
		if (e.target.files.length == 0) {
			const alertWrapper = e.target.closest("#error-alert");
			let alertMessage = alertWrapper.querySelector("#error-messages");
			alertWrapper.classList.add("uk-hidden");
			alertMessage.textContent = "";
			input.value = "";
			return;
		}
	},
	AddShakeAnimation: (parent) => {
		let timeout;
		clearTimeout(timeout);
		parent.classList.add("uk-animation", "uk-animation-shake");
		timeout = setTimeout(() => {
			parent.classList.remove("uk-animation", "uk-animation-shake");
		}, 1000);
	},
	HandleFileChange: (e) => {
		let files;
		let isMultiple = false;
		if (e.target.hasAttribute("multiple")) {
			isMultiple = true;
		}
		const dropZone = e.target.closest(".drop__zone");
		const componentContainer = dropZone.closest(".uk-upload-box");
		const documentCategory = dropZone.dataset.documentCategory;
		let preview, alertBox, alertMsg;
		preview = dropZone.querySelector(`#preview`);
		alertBox = componentContainer.querySelector(`#error-alert`);
		alertMsg = componentContainer.querySelector(`#error-messages`);
		/** Remove existing files and preview files if files lenght is 0 */
		if (!e.target.files.length) {
			UPLOAD.RemoveChild(preview);
			return;
		}
		files = e.target.files;
		const options = {
			files,
			fileInput: e.target,
			isMultiple,
			preview,
			alertBox,
			alertMsg,
			dropZone
		};
		if (isMultiple) {
			UPLOAD.PreviewMultipleDocs(options);
		} else {
			UPLOAD.PreviewSingleDoc(options);
		}
	},
	RemoveChild: (preview) => {
		while (preview.firstChild) {
			preview.removeChild(preview.firstChild);
		}
	},
	ImgPreviewLi: (readerResult, filename) => {
		const li = document.createElement("li");
		const div = document.createElement("div");
		const img = document.createElement("img");
		const span = document.createElement("span");
		li.className = "list-item uk-margin-medium-top";
		// div.className = "uk-cover-container";
		// img.setAttribute("id", "img-preview-responsive");
		// img.setAttribute("src", "images/icons/download.svg");
		// img.setAttribute("data-name", filename);
		// img.setAttribute("alt", "file-image-preview");
		span.className = "uk-text-meta uk-text-break file-upload-name";
		span.textContent = filename;
		// div.append(img);
		li.append(div, span);
		return li;
	},
	ValidateFileType: ({
		fileType,
		fileInput,
		alertBox,
		alertMsg,
		preview,
		dropZone
	}) => {
		if (!UPLOAD.ACCEPTED_DOC_MIMES.includes(fileType)) {
			alertMsg.textContent =
				"Sorry, one or more of your file type is not allowed.";
			alertMsg.classList.add("uk-text-danger");
			alertBox.classList.remove("uk-hidden");
			fileInput.value = "";
			UPLOAD.AddShakeAnimation(dropZone);
			UPLOAD.RemoveChild(preview);
			return false;
		}
		return true;
	},
	ValidateFileSize: ({
		fileInput,
		size,
		alertBox,
		alertMsg,
		preview,
		dropZone
	}) => {
		if (size > 2000000) {
			alertMsg.textContent =
				"Sorry, one or more of your files has exceeded the file size limit of 2MB.";
			alertMsg.classList.add("uk-text-danger");
			alertBox.classList.remove("uk-hidden");
			fileInput.value = "";
			UPLOAD.AddShakeAnimation(dropZone);
			UPLOAD.RemoveChild(preview);
			return false;
		}
		return true;
	},
	PreviewMultipleDocs: ({
		files,
		fileInput,
		preview,
		alertBox,
		alertMsg,
		dropZone
	}) => {
		const docFiles = [...files];
		docFiles.forEach((file) => {
			const size = file["size"];
			const fileType = file["type"];
			if (docFiles.length !== 0) {
				UPLOAD.RemoveChild(preview);
			}
			const fileOptions = {
				fileInput,
				preview,
				alertBox,
				alertMsg,
				fileType,
				size,
				dropZone
			};
			if (!UPLOAD.ValidateFileSize(fileOptions)) return;
			if (!UPLOAD.ValidateFileType(fileOptions)) return;
			alertMsg.textContent = "";
			alertBox.classList.add("uk-hidden");
			fileInput.files = files;
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => {
				let filename = file["name"];
				let imgPreview = "";
				if (UPLOAD.DOC_MIMES.includes(fileType)) {
					if (fileType === "application/pdf") {
						imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
						imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
					} else {
						imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
						imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
					}
				} else {
					imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
				}
				preview.append(imgPreview);
			};
		});
	},
	PreviewSingleDoc: ({
		files,
		fileInput,
		preview,
		alertBox,
		alertMsg,
		dropZone
	}) => {
		const size = files[0]["size"];
		const fileType = files[0]["type"];
		let filename = files[0]["name"];
		if (files[0].length !== 0) {
			UPLOAD.RemoveChild(preview);
		}
		const fileOptions = {
			fileInput,
			preview,
			alertBox,
			alertMsg,
			fileType,
			size,
			dropZone
		};
		if (!UPLOAD.ValidateFileSize(fileOptions)) return;
		if (!UPLOAD.ValidateFileType(fileOptions)) return;
		alertMsg.textContent = "";
		alertBox.classList.add("uk-hidden");
		const reader = new FileReader();
		reader.readAsDataURL(files[0]);
		reader.onload = () => {
			let imgPreview = "";
			let imagePath = "";
			if (UPLOAD.DOC_MIMES.includes(fileType)) {
				if (fileType === "application/pdf") {
					imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
					imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
				} else {
					imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
					imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
				}
			} else {
				imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
			}
			preview.append(imgPreview);
		};
	}
};
document.addEventListener("DOMContentLoaded", UPLOAD.Init());
</script>
@include('partials.footer')
@endif