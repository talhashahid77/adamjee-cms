{{-- dd($pagesdata) --}}
{{-- dd($ParentPageData) --}}
@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","inner");
            $postImgBanner =  getImageFile($device_type,$pagedata,"image","banner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
        </section>
        <section class="SecWrap ovverflowAuto">
            <div class="uk-container containCustom">
                <div class="detailsPage">
                    <h1>{{ $pagedata->title_en}}</h1>
                    <h5>{{ date('F d, Y', strtotime($pagedata->created_at)) }}</h5>
                    {!! $pagedata->description_en !!}
                    <img src="{{ $postImgBanner }}" />
                </div>
            </div>
        </section>
    @endforeach
    <section class="SecWrap SecTopSpace" style="background: #fff;">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>More Guides <a href="all-csr.php">See all</a></h2>
                <div class="NewsSec">
                    <ul uk-grid uk-height-match=".uk-card-body">
                        @foreach($ParentPageData as $key => $ParentData)
                            @php 
                                $postImg =  getImageFile($device_type,$ParentData,"image","banner");
                            @endphp
						<li class="uk-width-1-3@m">
							<a href="{{ url(session()->get('url').$ParentData->path) }}" class="uk-card uk-card-default newsCard">
								<div class="uk-card-media-top">
									<img src="{{ $postImg }}" alt="">
								</div>
								<div class="uk-card-body">
									<div class="badgesBar">
										<div class="badgeBox">@if(session()->get('url') == 'en')  {{ $ParentData->sub_title_en }} @else {{ $ParentData->sub_title_ur }} @endif</div>	
									</div>
									<h3>{{ $ParentData->title_en}}</h3>
									@if(session()->get('url') == 'en') {!! $ParentData->short_desc_en !!} @elseif (session()->get('url') == 'ur') {!! $ParentData->short_desc_ur !!} @endif
									<span class="blueBtn">Read more <img src="{{ asset('public/website/images/right.svg') }} " uk-svg /></span>
								</div>
							</a>
						</li>

					@endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @include('partials.footer')
@endif  