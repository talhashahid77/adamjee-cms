@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","banner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->sub_title_en}}</h1>
                    {!! $pagedata->description_en !!}
                </div>
            </div>
        </section>
    @endforeach
    @if(isset($innerPages))
            <section class="SecWrap SecTopSpace">
                <div class="uk-container containCustom">
                    <div class="searchList">
                        <form class="uk-search uk-search-default">
                            <span uk-search-icon></span>
                            <input class="uk-search-input" type="search" placeholder="Search for articles">
                        </form>
                    </div>
                    <div class="tabsSec">
                        <ul class="tabBtn BankTab" uk-switcher="connect: .TabBox">
                            @foreach($innerPages as $innerkey => $innerPage)
                                <li><a href="javascript:;">{{ $innerPage->title_en }}</a></li>
                            @endforeach
                    </ul>
                        <ul class="uk-switcher TabBox"> 
                        @foreach($innerPages as $innerkey => $innerPage)     
                            <li>
                                <div class="FaqAccordion">
                                    <ul uk-accordion>
                                        @foreach($innerPage->article as $subkey => $subChildInnerPage)
                                            <li class="@if($subkey == 0) uk-open @endif"> 
                                                <a class="uk-accordion-title" href="#">{{ $subChildInnerPage->title_en }}</a>
                                                <div class="uk-accordion-content">
                                                {!! $subChildInnerPage->description_en !!}
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>	
                            </li>   
                        @endforeach    
                        </ul>
                    </div>
                </div>
            </section>
    @endif

    @include('partials.footer')
@endif