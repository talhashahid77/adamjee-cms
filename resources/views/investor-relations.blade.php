@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php 
        $postImg =  getImageFile($device_type,$pagedata,"image","inner");
    @endphp
    <section class="HeaderInnerPage">
        <img src="{{ $postImg }}" />
        @include('partials.breadcrumb')
        <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
            <div class="uk-container containCustom">
                <h1>{{ $pagedata->title_en}}</h1>
                {!! $pagedata->description_en !!}
            </div>
        </div>
    </section>
@endforeach

@if(isset($innerPages))
    @foreach($innerPages as $key => $innerPage)
        @php 
            $postImg =  getImageFile($device_type,$innerPage,"image","inner");
        @endphp
        @if($innerPage->sorting == 1)
            <!-- Section Start -->
            <section class="SecWrap SecTopSpace">
                <div class="uk-container containCustom">
                    <div class="innerPageContent2">
                        <h2>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif</h2>
                        <div uk-grid>
                            <div class="uk-width-1-2@m">
                                <div class="cbox">
                                    @foreach($innerPage->article as $innerkey => $innerArticle)
                                        @if($innerArticle->position_en == "uk-flex-left@m")
                                            <h4>@if(session()->get('url') == 'en')  {{ $innerArticle->title_en }} @else {{ $innerArticle->title_ur }} @endif</h4>
                                            @if(session()->get('url') == 'en') {!! $innerArticle->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerArticle->description_ur !!} @endif
                                            @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                                @php 
                                                    $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                                @endphp
                                                @if(session()->get('url') == 'en') <img src="{{ $postImg }}" uk-svg/>{!! $innerCustompost->description !!} @elseif (session()->get('url') == 'ur') <img src="{{ $postImg }}" uk-svg/>{!! $innerCustompost->description !!} @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </div>
                                
                            </div>
                            <div class="uk-width-1-2@m">
                                <div class="cbox">
                                    @foreach($innerPage->article as $innerkey => $innerArticle)
                                        @if($innerArticle->position_en == "uk-flex-right@m")
                                            <h4>@if(session()->get('url') == 'en')  {{ $innerArticle->title_en }} @else {{ $innerArticle->title_ur }} @endif</h4>
                                            @if(session()->get('url') == 'en') {!! $innerArticle->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerArticle->description_ur !!} @endif
                                            @if(count($innerArticle->custompost)  > 0)
                                                <div class="addressicons addressiconsleft">
                                                    @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                                        @php 
                                                            $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                                        @endphp
                                                        @if(session()->get('url') == 'en') <img src="{{ $postImg }}" uk-svg />{!! $innerCustompost->description !!} @elseif (session()->get('url') == 'ur') <img src="{{ $postImg }}" uk-svg />{!! $innerCustompost->description !!} @endif
                                                    @endforeach
                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Section End -->
        @endif
    @endforeach
        
    @foreach($innerPages as $key => $innerPage)
        @php 
            $postImg =  getImageFile($device_type,$innerPage,"image","inner");
        @endphp
        @if($innerPage->sorting == 2)
            <!-- Section Start -->
            <section class="SecWrap SecTopSpace" style="background: #fff;">
                <div class="uk-container containCustom">
                    <div class="innerPageContent2">
                        <h2>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif</h2>
                        @foreach($innerPage->article as $innerkey => $innerArticle)
                            @if($innerArticle->position_en == "uk-flex-left@m")
                                @php 
                                    $postImg =  getImageFile($device_type,$innerArticle,"image","banner");
                                @endphp
                                
                                <h4>@if(session()->get('url') == 'en')  {{ $innerArticle->title_en }} @else {{ $innerArticle->title_ur }} @endif</h4>
                                @if(session()->get('url') == 'en') {!! $innerArticle->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerArticle->description_ur !!} @endif
                                @if($innerArticle->image_en != "[]") <img src="{{ $postImg}}"> @endif
                                @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                    @php 
                                        $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                    @endphp
                                    @if(session()->get('url') == 'en') <img src="{{ $postImg }}" uk-svg/>{!! $innerCustompost->description !!} @elseif (session()->get('url') == 'ur') <img src="{{ $postImg }}" uk-svg/>{!! $innerCustompost->description !!} @endif
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

@if(isset($innerPages))
    <!-- Section Start -->
    <section class="SecWrap">
        <div class="uk-container containCustom">
            <ul class="uk-grid-small" uk-grid>
                @foreach($innerPages as $key => $innerPage)
                    @php 
                        $postImg =  getImageFile($device_type,$innerPage,"image","banner");
                    @endphp
                    @if($innerPage->sorting > 2)
                        <li class="{{ $innerPage->grid_class_en}}">
                            <div class="GridBox" style="background-image: url({{ $postImg }});">
                                <div class="GridBoxTxt">
                                <h3>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif </h3>
                                @if(session()->get('url') == 'en') {!! $innerPage->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerPage->description_ur !!} @endif
                                <a class="blueBtn" href="{{ url(session()->get('url').$innerPage->path) }}">Learn more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /> </a>
                                </div>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </section>
    <!-- Section End -->
@endif

@include('partials.footer')
@endif