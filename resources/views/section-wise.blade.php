@if(isset($pagesdata))
	@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])
	@foreach($pagesdata as $key => $pagedata)
		<!-- Banner Start -->
		@php
			if(session()->get('url') == "en"){
				if($device_type == "mobile"){
					$postImageBanner = json_decode($pagedata->image_mobile_en); 
					if(count($postImageBanner) == 0){
						$postImageBanner = json_decode($pagedata->image_en); 
					} 
				} else{
					$postImageBanner = json_decode($pagedata->image_en); 
				}
			} else { 
				if($device_type == "mobile"){
					$postImageBanner = json_decode($pagedata->image_mobile_ur); 
					if(count($postImageBanner) == 0){
						$postImageBanner = json_decode($pagedata->image_ur); 
					} 
				} else{
					$postImageBanner = json_decode($pagedata->image_ur); 
				}
			}
			if(isset($postImageBanner[0])){
				$Img = $postImageBanner[0];
			}else{
				$Img = "";
			}
			$postImg = URL::to('/')."/public/source/".$Img."";
			$url = URL::to('/').session()->get('url');
		@endphp
		<section class="innerBanner" style="background-image: url({{ $postImg; }})">
			<div class="mdl">
				<div class="mdl_inner">
					<div class="uk-container">
						<div class="uk-flex @if(session()->get('url') == 'en') {{ $pagedata->position_en }} @elseif (session()->get('url') == 'ur') {{ $pagedata->position_ur }} @endif" uk-grid>
							<div class="@if(session()->get('url') == 'en') {{ $pagedata->grid_class_en }} @elseif (session()->get('url') == 'ur') {{ $pagedata->grid_class_ur }} @endif">
								<h1>@if(session()->get('url') == 'en')  {{ $pagedata->title_en }} @else {{ $pagedata->title_ur }} @endif</h1>
								<div class="dbfull">
									@if(session()->get('url') == 'en')  {!! $pagedata->description_en !!} @else {!! $pagedata->description_ur !!} @endif
								</div>
								<!-- <div class="dbfull">
									<a href="javascript:;" class="moreBtn">Watch more</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Banner End -->
	@endforeach
	@foreach($postsdata as $key => $postdata)
		<!-- Section Start -->
		@php 
			if(session()->get('url') == "en"){
				if($device_type == "mobile"){
					$postImageSection = json_decode($postdata->image_mobile_en);
					if(count($postImageSection) == 0){
						$postImageSection = json_decode($postdata->image_en); 
					} 
				} else{
					$postImageSection = json_decode($postdata->image_en); 
				}
			} else { 
				if($device_type == "mobile"){
					$postImageSection = json_decode($postdata->image_mobile_ur); 
					if(count($postImageSection) == 0){
						$postImageSection = json_decode($postdata->image_ur); 
					} 
				} else{
					$postImageSection = json_decode($postdata->image_ur); 
				}
			}

			if(isset($postImageSection[0])){
				$Img = $postImageSection[0];
			}else{
				$Img = "";
			}
			$postImg = URL::to('/')."/public/source/".$Img."";
			$url = URL::to('/').session()->get('url');

			if(session()->get('url') == "en"){
				if($postdata->link_text_en != ""){
					$linkText = $postdata->link_text_en;
				} else {
					$linkText = "Learn more";
				}
			} else{
				if($postdata->link_text_ur != ""){
					$linkText = $postdata->link_text_ur;
				} else {
					$linkText = "اورجانیے";
				}
			}
			
		@endphp
		<section class="blockSec {{ $postdata->classes }}" style="background-image: url({{ $postImg; }});@php if($Img == '') { echo 'background-color: #266666'; } @endphp">
				<div class="mdl">
					<div class="mdl_inner">
						<div class="uk-container">
							<div class="uk-flex @if(session()->get('url') == 'en') {{ $postdata->position_en }} @elseif (session()->get('url') == 'ur') {{ $postdata->position_ur }} @endif" uk-grid>
								<div class="@if(session()->get('url') == 'en') {{ $postdata->grid_class_en}} @elseif (session()->get('url') == 'ur') {{ $postdata->grid_class_ur}} @endif">
									<h2 class="blockTitle">@if(session()->get('url') == 'en') {{$postdata->title_en}} @elseif (session()->get('url') == 'ur') {{$postdata->title_ur}} @endif</h2>
									<div class="dbfull">
										@if(session()->get('url') == 'en') {!! $postdata->description_en !!} @elseif (session()->get('url') == 'ur') {!! $postdata->description_ur !!} @endif
									</div>
									<div class="dbfull">
										@if(session()->get('url') == 'en')
											@if($postdata->link_en != "")
												@if($postdata->link_type_en == "external")
													@php $ext = pathinfo($postdata->link_en, PATHINFO_EXTENSION); @endphp
													@if ($ext == 'pdf')
														
														<a href="{{ $postdata->link_en }}" target="_blank" class="pdfBtn">{{ $linkText }}<img src="{{asset('public/public/assets/images/aboutpso/download.svg')}}" uk-svg /></a>
													@else
														<a href="{{ $postdata->link_en }}" target="_blank" class="blockBtn">{{ $linkText }}</a>
													@endif
												@elseif($postdata->link_type_en == "internal")
													<a href="{{ url(session()->get('url').$postdata->link_en) }}" class="blockBtn">{{ $linkText }}</a>
													@if($postdata->buy_link != "")
														<a href="{{ $postdata->buy_link }}" target="_blank" class="blockBtn">Buy Now</a>
													@endif
												@endif
											@endif
										@else
											@if($postdata->link_ur != "")
												@if($postdata->link_type_ur == "external")
													@php $ext = pathinfo($postdata->link_ur, PATHINFO_EXTENSION); @endphp
													@if ($ext == 'pdf')
														
														<a href="{{ $postdata->link_ur }}" target="_blank" class="pdfBtn">{{ $linkText }}<img src="{{asset('public/public/assets/images/aboutpso/download.svg')}}" uk-svg /></a>
													@else
														<a href="{{ $postdata->link_ur }}" target="_blank" class="blockBtn">{{ $linkText }}</a>
													@endif
												@elseif($postdata->link_type_ur == "internal")
													<a href="{{ url(session()->get('url').$postdata->link_ur) }}" class="blockBtn">{{ $linkText }}</a>
													@if($postdata->buy_link != "")
														<a href="{{ $postdata->buy_link }}" target="_blank" class="blockBtn">ابھی خریدیں</a>
													@endif
												@endif
											@endif
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> 
		<!-- Section End -->
	@endforeach
	
	@include('partials.footer')
@endif