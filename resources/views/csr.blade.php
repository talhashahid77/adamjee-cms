@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php 
        $postImg = getImageFile($device_type,$pagedata,"image","inner");
    @endphp
    <section class="HeaderInnerPage">
        <img src="{{ $postImg }}" />
        @include('partials.breadcrumb')
        <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
            <div class="uk-container containCustom">
                <h1>{{ $pagedata->title_en}}</h1>
                {!! $pagedata->description_en !!}
            </div>
        </div>
    </section>
@endforeach

@foreach($postsdata as $key => $postdata)
    @php 
        $postImg = getImageFile($device_type,$postdata,"image","inner");
    @endphp
    <!-- Section Start -->
    <section class="SecWrap SecTopSpace">
        <div class="uk-container containCustom">
            <div class="innerPageContent">
                <h2>@if(session()->get('url') == 'en') {{$postdata->title_en}} @elseif (session()->get('url') == 'ur') {{$postdata->title_ur}} @endif</h2>
                @if(session()->get('url') == 'en') {!! $postdata->description_en !!} @elseif (session()->get('url') == 'ur') {!! $postdata->description_ur !!} @endif
            </div>
        </div>
    </section>
    <!-- Section End -->
@endforeach

@if(isset($innerPages))
    <!-- Section Start -->
    <section class="SecWrap">
        <div class="uk-container containCustom">
            <ul class="uk-grid-small" uk-grid>
                @foreach($innerPages as $key => $innerPage)
                    @php 
                        $postImg = getImageFile($device_type,$innerPage,"image","inner");
                    @endphp
                    @if($innerPage->sorting < 4)
                        <li class="{{ $innerPage->grid_class_en}}">
                            <div class="GridBox" style="background-image: url({{ $postImg }});">
                                <div class="GridBoxTxt">
                                <h3>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif </h3>
                                        @if(session()->get('url') == 'en') {!! $innerPage->short_desc_en !!} @elseif (session()->get('url') == 'ur') {!! $innerPage->short_desc_ur !!} @endif
                                    <a class="blueBtn" href="{{ url(session()->get('url').$innerPage->path) }}">Learn more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /> </a>
                                </div>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </section>
    <!-- Section End -->
@endif

@if(isset($innerPages))

@php
    $campaigns = array();
    
@endphp

@foreach($innerPages as $key => $innerPage)
    
    

    @foreach( $innerPage->child as $child )
    
    @php
        $child = (array) $child;
        $child['campaignPath'] = $innerPage->path . $child['slug'];
        $campaigns[] = $child;
        
    @endphp

    @endforeach
@endforeach

@php
    $sorted = array_column($campaigns, 'id');
    array_multisort($sorted, SORT_DESC, $campaigns);
@endphp
    <section class="SecWrap">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>Our latest CSR campaigns <a href="all-csr.php">See all</a></h2>
                <div class="NewsSec">
                    <ul uk-grid uk-height-match=".uk-card">
                        @foreach( $campaigns as $key=>$child  )
                            @php 
                            //       print_r($child);
                            // die;
                                if(session()->get('url') == "en"){
                                    if($device_type == "mobile"){
                                        $postImageBanner = json_decode($child['image_mobile_en']); 
                                        if(count($postImageBanner) == 0){
                                            $postImageBanner = json_decode($child['image_en']); 
                                        } 
                                    } else{
                                        $postImageBanner = json_decode($child['image_en']); 
                                    }
                                } else { 
                                    if($device_type == "mobile"){
                                        $postImageBanner = json_decode($child['image_mobile_ur']); 
                                        if(count($postImageBanner) == 0){
                                            $postImageBanner = json_decode($child['image_ur']); 
                                        } 
                                    } else{
                                        $postImageBanner = json_decode($child['image_ur']); 
                                    }
                                }

                                if(isset($postImageBanner[0])){
                                    if(count($postImageBanner) == 0){
                                        $Img = $postImageBanner[1];
                                    } else{
                                        $Img = $postImageBanner[0];
                                    }
                                }else{
                                    $Img = "";
                                }
                                $postImg = URL::to('/')."/public/source/".$Img."";
                                $url = URL::to('/').session()->get('url');
                            @endphp
                            {{-- @if($child->sorting > 3) --}}
                             
                                    <li class="{{ $child['grid_class_en']}}">
                                        <a href="{{ url(session()->get('url').$child['campaignPath']) }}" class="uk-card uk-card-default newsCard">
                                            <div class="uk-card-media-top">
                                                <img src="{{ $postImg }}" alt="">
                                            </div>
                                            <div class="uk-card-body">
                                                <div class="badgesBar">
                                                    <div class="badgeBox">@if(session()->get('url') == 'en')  {{ $child['sub_title_en'] }} @else {{ $child['sub_title_ur'] }} @endif</div>	
                                                </div>
                                                <h3>@if(session()->get('url') == 'en')  {{ $child['title_en'] }} @else {{ $child['title_ur'] }} @endif</h3>
                                                @if(session()->get('url') == 'en') {!! $child['short_desc_en'] !!} @elseif (session()->get('url') == 'ur') {!! $child['short_desc_ur'] !!} @endif
                                                <span class="blueBtn">Read more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></span>
                                                {{-- <span class="dateNews"> {{ $innerPage->created_at }}</span> --}}
                                                <span class="dateNews">@php echo date('d F Y', strtotime($child['created_at'])) @endphp </span>
                                            </div>
                                        </a>
                                    </li>
                                <!-- Card End -->
                            {{-- @endif --}}
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Section End -->

@endif

@include('partials.footer')
@endif