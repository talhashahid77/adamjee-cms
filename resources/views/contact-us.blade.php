<style>
    label.error {
    color: red !important;
}
label#file\[\]-error {
    position: absolute;
    bottom: -30px;
}

</style>
{{-- @dd($innerPages); --}}
@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

<!-- Section Start -->
@foreach($pagesdata as $key => $pagedata)
    @php 
        $postImg =  getImageFile($device_type,$pagedata,"image","inner");
    @endphp
    <section class="HeaderInnerPage">
        <img src="{{ $postImg }}" />
        @include('partials.breadcrumb')
        <div class="HeaderInnerTxt">
            <div class="uk-container containCustom">
                <h1>{{ $pagedata->title_en}}</h1>
                {!! $pagedata->description_en !!}
                <div class="homeBanneBtn">
                    @foreach($pagedata->custompost as $key => $customposts)
                        {{-- <a class="transparentbtn" href="https://www.adamjeelife.com/appointment-booking/" target="_blank">{{$customposts->name }}<img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg ></a> --}}
                        <a class="transparentbtn" href="{{ $customposts->url }}" target="_blank" >{{$customposts->name }} <img src="{{asset('public/website/images/icons/chevron_right.svg')}}" uk-svg ></a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endforeach
<!-- Section End -->



<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
        <div class="uk-grid uk-grid-large" uk-grid>
            <div class="uk-width-1-3@l">
                <div class="contactLeft">
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-body">
                            
                            @foreach($innerPages as $key => $innerPage)
                            @php 
                                $postImg =  getImageFile($device_type,$innerPage,"image","inner");
                            @endphp
                            
                            <div class="contactLeftContent">

                                 @if($innerPage->sorting == 1)     
                                
                                 <div class="cbox">
                                    <h4> <b> {{ $innerPage->title_en }}</b></h4>

                                    @foreach($innerPage->article as $innerkey => $innerArticle)
                                        @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                            @php 
                                                $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                            @endphp  

                                    <div class="addressicons addressiconsleft">
                                        <img src="{{ $postImg }}" uk-svg /> {!! $innerCustompost->description !!}
                                    </div>
                                    @endforeach

                                    @endforeach

                                    @endif



                                    @if($innerPage->sorting == 2)    
                                     <h4> <b> {{ $innerPage->title_en }}</b></h4>

                                     <div class="addressicons">
                                    @foreach($innerPage->article as $innerkey => $innerArticle)
                                    
                                  
                                        <span><strong>{{  $innerArticle->title_en }}</strong></span>

                                        @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                        @php 
                                            $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                        @endphp

                                         <p><img src="{{ $postImg }}" uk-svg />{!! $innerCustompost->description !!}</p>
                                       {{-- <span><strong>Head office</strong></span>
                                        <p><img src="images/icons/phone.svg" uk-svg /> (92-21) 37134900</p>
                                        <p><img src="images/icons/phone.svg" uk-svg /> (92-21) 38677100</p> --}}
                                        @endforeach
                                        @endforeach
                                    </div> 
                                    
                                    @endif
                                    

                                    @if($innerPage->sorting == 3)    
                                     <h4> <b>{{ $innerPage->title_en }}</b></h4>
                                     <div class="addressicons">
                                        @foreach($innerPage->article as $innerkey => $innerArticle)
                                   
                                            <span><strong>{{  $innerArticle->title_en }}</strong></span><br/>

                                                @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                                    @php 
                                                        $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                                    @endphp

                                                    <p><img src="{{ $postImg }}" uk-svg /><a href="{{ $innerCustompost->url }}">{!! $innerCustompost->description !!}</a></p>
                                                    {{--  <span><strong>Group life</strong></span>
                                                        <p><img src="images/contact/whatsapp.svg" uk-svg /><a href="mailto:info@adamjeelife.com">info@adamjeelife.com</a></p>
                                                        <span><strong>Help desk</strong></span>
                                                        <p><img src="images/contact/whatsapp.svg" uk-svg /><a href="mailto:info@adamjeelife.com">info@adamjeelife.com</a></p>
                                                        --}}
                                                    
                                                
                                                @endforeach 
                                        @endforeach 
                                    </div> 
                                   
                                  
                                    @endif

                                    
                                    @if($innerPage->sorting == 4)    
                                     <h4> <b>{{ $innerPage->title_en }}</b></h4>
                                    
                                    <div class="addressicons">
                                    @foreach($innerPage->article as $innerkey => $innerArticle)
                                         <span><strong>{{   $innerArticle->title_en }}</strong></span><br/>
                                            
                                             {!!  $innerArticle->description_en !!}
                                        @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                        @php 
                                            $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                        @endphp
                                         {{-- <p><img src="{{ $postImg }}" uk-svg />{!! $innerCustompost->description !!}</p> --}}
                                       
                                         {{-- <span><strong>Group life</strong></span> --}}
                                       <p><img src="{{ $postImg }}" uk-svg /><a href="{{ $innerCustompost->url }}">{!! $innerCustompost->description !!}</a></p>
                                       {{--  <span><strong>Help desk</strong></span>
                                        <p><img src="images/contact/whatsapp.svg" uk-svg /><a href="tel:+92-346-8285388">+92-346-8285388</a></p>
                                    --}}
                                    @endforeach
                                    @endforeach
                                    </div> 
                                    @endif

                                    @if($innerPage->sorting == 5)    
                                     <h4> <b>{{ $innerPage->title_en }}</b></h4>
                                     @foreach($innerPage->article as $innerkey => $innerArticle)
                                    <div class="addressicons">
                                        <span><strong>{{   $innerArticle->title_en }}</strong></span>

                                        @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                                            @php 
                                                $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                                            @endphp
                                        <p><img src="{{ $postImg }}" uk-svg /><a href="{{ $innerCustompost->url }}">{!! $innerCustompost->description !!}</a></p>
                                        {{--<span><strong>Complains</strong></span>
                                        <p><img src="images/contact/whatsapp.svg" uk-svg /><a href="tel:+92-346-8285388">+92-346-8285388</a></p>
                                        <span><strong>Claims</strong></span>
                                        <p><img src="images/contact/whatsapp.svg" uk-svg /><a href="tel:+92-346-8209366">+92-346-8209366</a></p>
                                        --}}
                                    </div> 
                                    @endforeach
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="contactLeftContent">
                         @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="uk-width-2-3@l">
                <div class="contactRightContent JobForm NewsFilter">
                    <h3>Send us a message</h3>
                    <p>We're here to help you every step of the way, so please don't hesitate to reach out to us via our online or offline touchpoints.</p>
                    
                        <form id="contact_us" uk-grid action="{{ route('contact') }}" method="post" enctype="multipart/form-data">		
                         
                            @csrf
                            <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Your full name</label>
                            <input class="uk-input" type="text" placeholder="Enter your full name" name="name" required>
                        </div>
                        			    
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Policy Number</label>
                            <input class="uk-input" type="number" placeholder="Your policy number" name="policy_number" required>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Email address</label>
                            <input class="uk-input" type="email" placeholder="What's your email" name="email" required>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">CNIC Number</label>
                            <input class="uk-input" type="number" placeholder="Enter your CNIC Number" minlength="13" maxlength="13" name="cnic" required>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Contact number</label>
                            <input class="uk-input" type="number" placeholder="Enter your contact number" minlength="13" maxlength="13" name="contact_number" required>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <div class="NewsFilter1">
                            <label class="uk-form-label">City</label>
                                <select name="city" class="uk-select" required>
                                    <option value="">Select City</option>
                                    @foreach (getCities() as $item)
                                    <option value="{{$item}}">{{$item}}</option>    
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <div class="NewsFilter1">
                            <label class="uk-form-label">Category</label>
                                <select  name="category" class="uk-select" id="categorySelect" required>
                                    
									{{-- <option value="">Select Category</option> --}}
									<option value="Service Request">Service Request</option>
									<option value="Complains">Complains</option>
                                    <option value="Claims">Claims</option>

                                </select>
                            </div>
                        </div>


						<div class="uk-width-1-2@s uk-width-1-2@m Service_Request subCats">
                            <div class="NewsFilter1">
                            <label class="uk-form-label">Sub Category</label>
                                <select class="uk-select" name="sub_category" required>
									{{-- <option value="">Select Sub Category</option> --}}
                                    <option value="Change of account number">Change of account number</option>
                                    <option value="Adhoc adjustment request">Adhoc adjustment request</option>
                                    <option value="Adhoc partial surrender">Adhoc partial surrender</option>
                                    <option value="Adhoc surrender">Adhoc surrender</option>
                                    <option value="Adhoc transfer (MTV)">Adhoc transfer (MTV)</option>
                                    <option value="Agent change request">Agent change request</option>
                                    <option value="Beneficiary Add/Change/Remove">Beneficiary Add/Change/Remove</option>
                                    <option value="Change of address business">Change of address business</option>
                                    <option value="Change of address correspondence">Change of address correspondence</option>
                                    <option value="Change of address permanent">Change of address permanent</option>
                                    <option value="Contact number change"> Contact number change</option>
                                    <option value="Duplicate documents">Duplicate documents</option>
                                    <option value="Full surrender request">Full surrender request</option>
                                    <option value="Guardian detail Add/Change/Remove">Guardian detail Add/Change/Remove</option>
                                    <option value="Loan">Loan</option>
                                    <option value="Maturity"> Maturity</option>
                                    <option value="Mode change request (big to small)">Mode change request (big to small)</option>
                                    <option value="Mode change request (small to big)">Mode change request (small to big)</option>
                                    <option value="Name correction">Name correction</option>
                                    <option value="No correspondence Yes/No">No correspondence Yes/No</option>
                                    <option value="Partial policy surrender"> Partial policy surrender</option>
                                    <option value="Paying terms/benefit term alteration (Decrease)">Paying terms/benefit term alteration (Decrease)</option>
                                    <option value="Paying terms/benefit term alteration (Increase)">Paying terms/benefit term alteration (Increase)</option>
                                    <option value="Premium enhancement/Reduction request">Premium enhancement/Reduction request</option>
                                    <option value="Premium refund">Premium refund</option>
                                    <option value="Reinstatement/Activation">Reinstatement/Activation</option>
                                    <option value="Rider addition">Rider addition</option>
                                    <option value="Rider deletion">Rider deletion</option>
                                    <option value="Sum assured decrease">Sum assured decrease</option>
                                    <option value="Sum assured increase">Sum assured increase</option>
                                    
                                </select>
                            </div>
                        </div>

                        <div class="uk-width-1-2@s uk-width-1-2@m Complains subCats">
                            <div class="NewsFilter1">
                            <label class="uk-form-label">Sub Category</label>
                                <select class="uk-select" name="sub_category" required>
									{{-- <option value="">Select Sub Category</option> --}}
                                    <option value="Mis-selling">Mis-selling</option>
                                    <option value="Premium refund">Premium refund</option>
                                    <option value="Premium deduction">Premium deduction</option>

                                    <option value="Service Issues">Service Issues</option>
                                    <option value="Loan">Loan</option>
                                    <option value="Premium pocketing">Premium pocketing</option>

                                    <option value="Cancellation">Cancellation</option>
                                    <option value="Agent complain">Agent complain</option>
                                    <option value="Return issues">Return issues</option>

                                    <option value="Policy documents">Policy documents</option>
                                    <option value="Alteration issues">Alteration issues</option>
                                    <option value="Payment issue">Payment issue</option>

                                    <option value="Letter request ">Letter request </option>
                                    <option value="Proposal cancellation">Proposal cancellation</option>
                                    <option value="Orbis">Orbis</option>


                                    <option value="Surrender">Surrender</option>
                                    <option value="Maturity Claim">Maturity Claim</option>
                                    <option value="Policy information">Policy information</option>

                                    <option value="Death claim">Death claim</option>
                                    <option value="Partial surrender">Partial surrender</option>
                                    <option value="Penalty waiver">Penalty waiver</option>

                                </select>
                            </div>
                        </div>

                    
                        <div class="uk-width-1-2@s uk-width-1-2@m Claims subCats">
                            <div class="NewsFilter1">
                            <label class="uk-form-label">Sub Category</label>
                                <select class="uk-select" name="sub_category" required>
									{{-- <option value="">Select Sub Category</option> --}}
                                    <option value="Death">Death</option>
                                    <option value="Disability">Disability</option>
                                   



                                </select>
                            </div>
                        </div>



                        <div class="uk-width-1-2@s uk-width-1-1@m">
                            <label class="uk-form-label">Summary</label>
                            <textarea name="summary" rows="10" class="uk-textarea" required></textarea>
                        </div>

                        <div class="uk-width-1-2@s uk-width-1-1@m">
                            {{-- <div class="js-upload uk-placeholder uk-text-center" >
                                <span uk-icon="icon: cloud-upload"></span>
                                <span class="uk-text-middle">Drop files here or</span>
                                <div  class="selectForm"uk-form-custom>
                                    <input name="file[]" type="file" multiple >
                                    <span class="uk-link">Select File</span>
                                </div>
                            </div>
                            <span>Supported file formats: PDF, Doc, Docx, Jpg</span>

                           
                            <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress> --}}


                            <div class="uk-upload-box">
                                <div id="error-alert" class="uk-alert-danger uk-margin-top uk-hidden" uk-alert>
                                    <p id="error-messages"></p>
                                </div>
                                <div class="drop__zone custom uk-placeholder uk-text-center">
                                    <span uk-icon="icon: cloud-upload"></span>
                                    <span class="uk-text-middle uk-margin-small-left">Drop file here or</span>
                                    <br/>
                                    <div class="selectForm" uk-form-custom >
                                        <input name="file[]" type="file" accept="image/png, image/jpeg, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple required >
                                        <span class="uk-link">select file</span>
                                    </div>

                                   
                                    <ul id="preview" class="uk-list uk-grid-match uk-child-width-1-5@m uk-text-center uk-grid-small" uk-grid uk-scrollspy="cls: uk-animation-scale-up; target: .list-item; delay: 80"></ul>
                                </div>
                            </div>
                            <span>Supported file formats: PDF, Doc, Docx, Jpg</span>
                            
                            
                    
                                                        
                        </div>    
						@include('partials.alart')	

                        <div class="uk-width-1-1">
                            <button type="submit" class="blueBtn uk-margin-remove-top">Send message <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></button>
                          
                        </div>
                   
                        <div class="uk-width-1-1">
                            <span><b>Disclaimer:</b> In case your complaint has not been properly redressed by us, you may lodge your complaint with Securities and Exchange Commission of Pakistan (the “SECP”). However, please note that SECP will entertain only those complaints which were at first directly requested to be redressed by the company and the company has failed to redress the same. Further, the complaints that are not relevant to SECP’s regulatory domain/competence shall not be entertained by the SECP.”</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<script>

const UPLOAD = {
	BASE_PATH: window.location.origin,
	ACCEPTED_DOC_MIMES: [
		"image/png",
		"image/jpeg",
		"application/pdf",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/msword"
	],
	DOC_MIMES: [
		"application/pdf",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/msword"
	],
	FileInputs: [...document.querySelectorAll("input[type='file']")],
	Init: () => {
		UPLOAD.AddEventListeners();
	},
	AddEventListeners: () => {
		UPLOAD.FileInputs.map((fileInput) => {
			fileInput.addEventListener("change", UPLOAD.HandleFileChange);
		});
		UPLOAD.FileInputs.map((fileInput) => {
			fileInput.addEventListener("blur", (e) => {
				// console.log(e.target);
			});
		});
		UPLOAD.FileInputs.map((fileInput) => {
			const box = fileInput.closest(".uk-placeholder");
			box.addEventListener(
				"dragover",
				(e) => {
					e.preventDefault();
					e.stopPropagation();
					box.classList.add("uk-box-shadow-medium");
				},
				false
			);
		});
		UPLOAD.FileInputs.map((fileInput) => {
			const box = fileInput.closest(".uk-placeholder");
			box.addEventListener(
				"dragleave",
				(e) => {
					e.preventDefault();
					e.stopPropagation();
					box.classList.remove("uk-box-shadow-medium");
				},
				false
			);
		});
		UPLOAD.FileInputs.map((fileInput) => {
			const box = fileInput.closest(".uk-placeholder");
			box.addEventListener(
				"drop",
				(e) => {
					e.preventDefault();
					e.stopPropagation();
					box.classList.remove("uk-box-shadow-medium");
					const dropZone = e.target.closest(".drop__zone");
					const componentContainer = dropZone.closest(".uk-upload-box");
					let preview, alertBox, alertMsg;
					preview = dropZone.querySelector(`#preview`);
					alertBox = componentContainer.querySelector(`#error-alert`);
					alertMsg = componentContainer.querySelector(`#error-messages`);
					let files;
					let isMultiple = false;
					if (fileInput.hasAttribute("multiple")) {
						isMultiple = true;
					}
					if (!e.dataTransfer.files.length) return;
					/** If items dropped are more than one file and the input doesn't accept multiple, do not accept the files. Clear preview field, show notification and add shake animatin to dropzone */
					if (isMultiple === false && e.dataTransfer.files.length > 1) {
						UPLOAD.RemoveChild(preview);
						UPLOAD.AddShakeAnimation(dropZone);
						UIkit.notification({
							message: "Cannot accept multiple files when expecting a single file!",
							status: "danger",
							pos: "center",
							timeout: 4000
						});
						return;
					}
					files = e.dataTransfer.files;
					fileInput.files = files;
					const options = {
						isMultiple,
						files,
						preview,
						alertBox,
						alertMsg,
						fileInput,
						dropZone
					};
					if (isMultiple) {
						UPLOAD.PreviewMultipleDocs(options);
					} else {
						UPLOAD.PreviewSingleDoc(options);
					}
				},
				false
			);
		});
	},
	Reset: (e) => {
		if (e.target.files.length == 0) {
			const alertWrapper = e.target.closest("#error-alert");
			let alertMessage = alertWrapper.querySelector("#error-messages");
			alertWrapper.classList.add("uk-hidden");
			alertMessage.textContent = "";
			input.value = "";
			return;
		}
	},
	AddShakeAnimation: (parent) => {
		let timeout;
		clearTimeout(timeout);
		parent.classList.add("uk-animation", "uk-animation-shake");
		timeout = setTimeout(() => {
			parent.classList.remove("uk-animation", "uk-animation-shake");
		}, 1000);
	},
	HandleFileChange: (e) => {
		let files;
		let isMultiple = false;
		if (e.target.hasAttribute("multiple")) {
			isMultiple = true;
		}
		const dropZone = e.target.closest(".drop__zone");
		const componentContainer = dropZone.closest(".uk-upload-box");
		const documentCategory = dropZone.dataset.documentCategory;
		let preview, alertBox, alertMsg;
		preview = dropZone.querySelector(`#preview`);
		alertBox = componentContainer.querySelector(`#error-alert`);
		alertMsg = componentContainer.querySelector(`#error-messages`);
		/** Remove existing files and preview files if files lenght is 0 */
		if (!e.target.files.length) {
			UPLOAD.RemoveChild(preview);
			return;
		}
		files = e.target.files;
		const options = {
			files,
			fileInput: e.target,
			isMultiple,
			preview,
			alertBox,
			alertMsg,
			dropZone
		};
		if (isMultiple) {
			UPLOAD.PreviewMultipleDocs(options);
		} else {
			UPLOAD.PreviewSingleDoc(options);
		}
	},
	RemoveChild: (preview) => {
		while (preview.firstChild) {
			preview.removeChild(preview.firstChild);
		}
	},
	ImgPreviewLi: (readerResult, filename) => {
		const li = document.createElement("li");
		const div = document.createElement("div");
		const img = document.createElement("img");
		const span = document.createElement("span");
		li.className = "list-item uk-margin-medium-top";
		// div.className = "uk-cover-container";
		// img.setAttribute("id", "img-preview-responsive");
		// img.setAttribute("src", "images/icons/download.svg");
		// img.setAttribute("data-name", filename);
		// img.setAttribute("alt", "file-image-preview");
		span.className = "uk-text-meta uk-text-break file-upload-name";
		span.textContent = filename;
		// div.append(img);
		li.append(div, span);
		return li;
	},
	ValidateFileType: ({
		fileType,
		fileInput,
		alertBox,
		alertMsg,
		preview,
		dropZone
	}) => {
		if (!UPLOAD.ACCEPTED_DOC_MIMES.includes(fileType)) {
			alertMsg.textContent =
				"Sorry, one or more of your file type is not allowed.";
			alertMsg.classList.add("uk-text-danger");
			alertBox.classList.remove("uk-hidden");
			fileInput.value = "";
			UPLOAD.AddShakeAnimation(dropZone);
			UPLOAD.RemoveChild(preview);
			return false;
		}
		return true;
	},
	ValidateFileSize: ({
		fileInput,
		size,
		alertBox,
		alertMsg,
		preview,
		dropZone
	}) => {
		if (size > 2000000) {
			alertMsg.textContent =
				"Sorry, one or more of your files has exceeded the file size limit of 2MB.";
			alertMsg.classList.add("uk-text-danger");
			alertBox.classList.remove("uk-hidden");
			fileInput.value = "";
			UPLOAD.AddShakeAnimation(dropZone);
			UPLOAD.RemoveChild(preview);
			return false;
		}
		return true;
	},
	PreviewMultipleDocs: ({
		files,
		fileInput,
		preview,
		alertBox,
		alertMsg,
		dropZone
	}) => {
		const docFiles = [...files];
		docFiles.forEach((file) => {
			const size = file["size"];
			const fileType = file["type"];
			if (docFiles.length !== 0) {
				UPLOAD.RemoveChild(preview);
			}
			const fileOptions = {
				fileInput,
				preview,
				alertBox,
				alertMsg,
				fileType,
				size,
				dropZone
			};
			if (!UPLOAD.ValidateFileSize(fileOptions)) return;
			if (!UPLOAD.ValidateFileType(fileOptions)) return;
			alertMsg.textContent = "";
			alertBox.classList.add("uk-hidden");
			fileInput.files = files;
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => {
				let filename = file["name"];
				let imgPreview = "";
				if (UPLOAD.DOC_MIMES.includes(fileType)) {
					if (fileType === "application/pdf") {
						imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
						imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
					} else {
						imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
						imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
					}
				} else {
					imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
				}
				preview.append(imgPreview);
			};
		});
	},
	PreviewSingleDoc: ({
		files,
		fileInput,
		preview,
		alertBox,
		alertMsg,
		dropZone
	}) => {
		const size = files[0]["size"];
		const fileType = files[0]["type"];
		let filename = files[0]["name"];
		if (files[0].length !== 0) {
			UPLOAD.RemoveChild(preview);
		}
		const fileOptions = {
			fileInput,
			preview,
			alertBox,
			alertMsg,
			fileType,
			size,
			dropZone
		};
		if (!UPLOAD.ValidateFileSize(fileOptions)) return;
		if (!UPLOAD.ValidateFileType(fileOptions)) return;
		alertMsg.textContent = "";
		alertBox.classList.add("uk-hidden");
		const reader = new FileReader();
		reader.readAsDataURL(files[0]);
		reader.onload = () => {
			let imgPreview = "";
			let imagePath = "";
			if (UPLOAD.DOC_MIMES.includes(fileType)) {
				if (fileType === "application/pdf") {
					imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
					imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
				} else {
					imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
					imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
				}
			} else {
				imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
			}
			preview.append(imgPreview);
		};
	}
};
document.addEventListener("DOMContentLoaded", UPLOAD.Init());

$(document).ready(()=>{
    $('.subCats').each((i, element) => {
        if(i > 0){
            $(element).hide();
            $(element).find('select').attr('disabled', true);
        }
    })
});

$('#categorySelect').change(()=>{
    let selectedOption = $("#categorySelect option:selected").val().replace(' ', '_');
    $('.subCats').each((i, element) => {
		$(element).hide();
		$(element).find('select').attr('disabled', true); 
    });
    
		$('.'+selectedOption).show();
    	$('.'+selectedOption).find('select').attr('disabled', false);
    
})
</script>

<script>
   $(document).ready(function () {

$('#contact_us').validate({ // initialize the plugin
	rules: {
		name: {
			required: true,
			// email: true
		},
		// field2: {
		// 	required: true,
		// 	minlength: 5
		// }
	}
});

});
</script>



@include('partials.footer')
@endif