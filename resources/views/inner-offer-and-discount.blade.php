@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","inner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->sub_title_en}}</h1>
                    {!! $pagedata->description_en !!}
                </div>
            </div>
        </section>
    @endforeach

    <section class="SecWrap SecTopSpace">
        <div class="uk-container containCustom">
            <div class="uk-grid" uk-grid>
                <div class="uk-width-1-2@m">
                    <div class="innerPageContent2">
                        <h2>Outlets</h2>
                    </div>
                </div>
                <div class="uk-width-1-2@m">
                    <div class="searchFormDiscount">
                        <form>
                            <div class="uk-form-controls GuideSearchInput">
                                <input class="uk-input" id="email" type="text" placeholder="Search">
                            </div>
                        <form>    
                    </div>
                </div>
                @foreach($postsdata as $key => $postdata)
                    <div class="uk-width-1-2@m">
                        <div class="innerPageContent2">
                            <h2>{{ $postdata->title_en }}</h2>
                        </div>
                    </div>
                    @if(isset($postdata->custompost))
                        <div class="uk-width-1-1">
                            <ul class="uk-grid-small" uk-grid uk-height-match=".uk-card-body">
                                @foreach($postdata->custompost as $customkey => $customPost)
                                        <li class="uk-width-1-4@m uk-width-1-2@s">
                                            <a href="javascript:;" class="uk-card uk-card-default newsCard discounts">
                                                <div class="uk-card-body">
                                                    <h3>{{ $customPost->name }}</h3>
                                                    {!! $customPost->description !!}
                                                </div>
                                            </a>
                                        </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                @endforeach
                <!-- <div class="uk-width-1-2@m">
                    <div class="innerPageContent2">
                        <h2>Outlets</h2>
                    </div>
                </div>
                <ul class="uk-grid-small" uk-grid uk-height-match=".uk-card-body">
                    <li class="uk-width-1-4@m uk-width-1-2@s">
                        <a href="javascript:;" class="uk-card uk-card-default newsCard discounts">
                            <div class="uk-card-body">
                                <h3>Bahadurabad, Karachi</h3>
                                <p>Plot# 120, Below Bahar-e-Shariat Masjid, Alamgir Road, Bahadurabad, Karachi - Pakistan</p>
                                
                            </div>
                        </a>
                    </li>
                </ul> -->
            </div>    
        </div>
    </section>
    <section class="SecWrap SecTopSpace uk-padding-remove-top">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>Terms & Conditions</h2>
                <ul>
                    <li>These Terms and Conditions (the “Terms and Conditions”) shall apply to all customers availing the discount (the “Customer”) and the Customer hereby consents to these Terms and Conditions through any such participation in the discount campaign and agrees to HBL’s right as the final decision-making authority in all cases, with its decision being final and binding for all.</li>
                    <li>Customers may avail 40% Discount per transaction with the Cap amount of 1000 PKR.</li>
                    <li>The discount is applicable on transactions conducted only through Visa QR from HBL Mobile and Konnect by HBL Application.</li>
                    <li>This offer is valid across all PITS outlets in Karachi and Hyderabad (including Café chatter box, Aztech Chocolate.)</li>
                    <li>The discounts are not valid in conjunction with other special promotions, discount programs or vouchers defined by HBL or alliance partners.</li>
                    <li>The discounts cannot be combined with any group discounts, parties, special events or any other offers.</li>
                    <li>This discount cannot be availed through cash or card.</li>
                    <li>Any disputes on the quality of products and services that the alliance partner provides, is the sole responsibility of the alliance partner and not of HBL.</li>
                    <li>In case of any issue with the product, please contact Pie in the sky management on their helpline. HBL will not be responsible for the order management.</li>
                    <li>HBL reserves the right to terminate or withdraw or change the criteria of any discount offering with prior notice</li>
                    <li>HBL is not liable for the accuracy of the brand offers/discounts offered by alliances. For updated brand offers/discounts, please refer to the relevant alliance partner’s website or physical outlet.</li>
                    <li>HBL reserves the right to discontinue the campaign at any time and may at any time revise these Terms and Conditions for any reason whatsoever by updating HBL’s website i.e. https://www.hbl.com/ and the Konnect webpage i.e. https://www.hbl.com/personal/konnect. . Although Customer will be notified but its advised that the Customer should regularly check the website to stay updated as these changes shall be binding upon the Customers immediately.</li>
                    <li>These Terms and Conditions shall be governed by and construed in accordance with the laws of Pakistan.</li>
                </ul>
            </div>
        </div>
    </section>

    @include('partials.footer')
@endif