@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

<!-- Section Start -->
@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php
        if(session()->get('url') == "en"){
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_en); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_en); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_en); 
            }
        } else { 
            if($device_type == "mobile"){
                $postImageBanner = json_decode($pagedata->image_mobile_ur); 
                if(count($postImageBanner) == 0){
                    $postImageBanner = json_decode($pagedata->image_ur); 
                } 
            } else{
                $postImageBanner = json_decode($pagedata->image_ur); 
            }
        }
        if(isset($postImageBanner[0])){
            $Img = $postImageBanner[0];
        }else{
            $Img = "";
        }
        $postImg = URL::to('/')."/public/source/".$Img."";
        $url = URL::to('/').session()->get('url');
    @endphp

	<section class="HeaderInnerPage">
		<img src="{{ $postImg }}" />
		@include('partials.breadcrumb')
	</section>

	<!-- Section End -->
	<!-- Section Start -->
	<section class="SecWrap ovverflowAuto">
		<div class="uk-container containCustom">
			<div class="detailsPage">
				<h1>{{ $pagedata->title_en}}</h1>
				<h6>@php echo date('d F Y', strtotime($pagedata->created_at)) @endphp </h6>
				{!! $pagedata->description_en !!}
				<ul class="uk-grid-small uk-flex uk-flex-center uk-child-width-1-5@m uk-child-width-1-3@s uk-child-width-1-2 uk-margin-large-top" uk-grid uk-lightbox>
					@for($i=0; $i < count($postImageBanner)-2; $i++)
					@php  
						$Count = $i + 2; 
						$Imgbanner = URL::to('/')."/public/source/".$postImageBanner[$Count]."";
						
					@endphp
					<li><a href="{{ $Imgbanner }}"><img src="{{ $Imgbanner }}" alt=""></a></li>
				@endfor
				</ul>
			</div>
		</div>
	</section>
@endforeach
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace" style="background: #fff;">
	<div class="uk-container containCustom">
		<div class="innerPageContent2">
			<h2>More Awards & Achievements <a href="javascript:;">See all</a></h2>
			<div class="NewsSec">
				<ul uk-grid uk-height-match=".uk-card-body">
					@foreach($ParentPageData as $key => $ParentData)
						<li class="uk-width-1-2@m">
							<a href="{{ url(session()->get('url').$ParentData->path) }}" class="uk-card uk-card-default newsCard">
								<div class="uk-card-media-top">
									<img src="{{ $postImg }}" alt="">
								</div>
								<div class="uk-card-body">
									<div class="badgesBar">
										<div class="badgeBox">@if(session()->get('url') == 'en')  {{ $ParentData->sub_title_en }} @else {{ $ParentData->sub_title_ur }} @endif</div>	
									</div>
									<h3>{{ $ParentData->title_en}}</h3>
									@if(session()->get('url') == 'en') {!! $ParentData->short_desc_en !!} @elseif (session()->get('url') == 'ur') {!! $ParentData->short_desc_ur !!} @endif
									<span class="blueBtn">Read more <img src="{{ asset('public/website/images/right.svg') }} " uk-svg /></span>
									<span class="dateNews">@php echo date('d F Y', strtotime($ParentData->created_at)) @endphp </span>
								</div>
							</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- Section End -->

@include('partials.footer')
@endif