<?php include('header.php'); ?>
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="images/financial-highlights/banner.jpg" />
	<div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="about-us.php">About us</a></li>
		    <li><a href="investor-relations.php">Investor Relations</a></li>
		    <li><span>Financial highlights</span></li>
		</ul>
	</div>
	<div class="HeaderInnerTxt fullWidth">
		<div class="uk-container containCustom">
			<h1>Financial highlights</h1>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur</p> -->
			<a href="javascript:;">Download latest financial report <img src="images/right.svg" uk-svg /></a>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace whiteTabsSec">
	<div class="uk-container containCustom">
		<h2>Funds</h2>
		<div class="tabsSec">
			<!-- This is the nav containing the toggling elements -->
			<ul class="tabBtn2" uk-switcher="connect: .TabBox2">
			    <li><a href="javascript:;">Overview</a></li>
			    <li><a href="javascript:;">Fund Prices</a></li>
			    <li><a href="javascript:;">Fund Performance</a></li>
			    <li><a href="javascript:;">Fund Managers Report</a></li>
			</ul>

			<!-- This is the container of the content items -->
			<ul class="uk-switcher TabBox2">
				<!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent financialTabli">
			    		<p>Life Insurance can be defined as a contract between an insurance policy holder and an insurance company, where the insurer promises to pay a sum of money in exchange for a premium, upon the death of an insured person or on occurrence of an insured event or after a set period on survival. Here, at Adamjee Life Assurance, along with this promise we also enable you to save and get back significant portion of your paid premium, plus profit depending on the market performance of the selected fund, for your future long term needs. Adamjee Life continuously strives to protect financial health and provide retirement security for the people through its innovative and powerful products, which are also available in digital environment.  </p> 
			    		<!-- <p>We give you the ability to choose the kind of investments you want to make and offer you unparalleled flexibility and transparency when it comes to making those investments.</p> -->
			    		<h3>How Funds Are Managed </h3>
			    		<p>Fund management refers to the systematic process in which a fund manager operates, maintain, disposes, and upgrades assets in a cost effective manner while they ensure optimum return on investment. The fund manager has to pay close attention to the returns as well as the risk associated with the available investment opportunities. </p>
						<p>At Adamjee Life specialized investment professionals of MCB-Arif Habib Investments and Savings Limited are managing our funds. MCB Arif Habib Investments and Savings Limited is one of the largest asset managers in Pakistan with credit rating of AM1. The funds are invested in multifarious securities to maximize returns balanced with risks. The premiums paid by the policyholders are invested in funds of your choice and the units are allocated on the current price of such fund. The year wise allocable portion of premiums into the selected fund is given in the individual product brochure and structure. </p>
			    		<h3>Freedom to Select Your Desired Investment Strategy</h3>
			    		<p>We provide you complete flexibility to select your desired fund. Furthermore, depending on the fund’s performance, you can easily switch your investment from one fund to the other. </p>
			    		<p>Please Note:</p>
			    		<ul>
			    			<li>Two free switches are allowed every policy year.</li>
			    			<li>Each fund is different from the other fund. For more details, you may contact our sales representatives for the appropriate fund selection according to your investment needs.</li>
			    			
			    		</ul>
			    		<h3>Aggressive Funds: (High Risk) </h3>
			    		<p>Investment avenues include equity securities, government securities, cash in bank account, money market placements, deposits, certificate of deposits (COD), certificate of musharakas (COM) ,TDRs, commercial papers, reverse repo , TFCs/Sukuks , real estate, MTS, preference shares, units of collective investment schemes , units of ETFs, Units of REITs, equity future contracts, commodity future contracts, units of Private Funds:</p>
			    			<ul>
								<li>
									<h5><b>Investment Multiplier Fund</b> </h5>
									<p>To earn higher returns in medium to long term by investing in diversified mix of equity, fixed income instruments and real estate.</p>
								</li>

								<li>
									<h5><b>Dynamic Growth Fund</b> </h5>
									<p>To earn higher returns in medium to long term by investing in diversified mix of equity, fixed income instruments and real estate. </p>
								</li>

								<li>
									<h5><b>Mazaaf Fund</b> </h5>
									<p>To earn higher returns in medium to long term by investing in diversified mix of shariah compliant equity, debt instruments and real estate.</p>
								</li>
							</ul>
						<h3>Balanced Funds: (Medium Risk)</h3>
			    		<p>Investment avenues include equity securities, government securities, cash in bank account, money market placements, deposits, certificate of deposits (COD), certificate of musharakas (COM) ,TDRs, commercial papers, reverse repo , TFCs/Sukuks , real estate, MTS, preference shares, units of collective investment schemes , units of ETFs, Units of REITs, equity future contracts, commodity future contracts, units of Private Funds:</p>
			    			<ul>
								<li>
									<h5><b> Amaanat Fund</b> </h5>
			    					<p>The objective of the fund is to maximize returns to policyholders by investing in a diverse portfolio of asset-backed investments such as shares, term finance certificates and bank deposits.</p>
								</li>
								<li>
									<h5><b>Managed Growth Fund</b> </h5>
			    					<p>To provide growth in investment value by investing in fixed income instruments, equities and real estate. </p>
								</li>
								<li>
									<h5><b>Saman Fund</b> </h5>
			    					<p>To provide growth in investment value by investing in shariah compliant debt instruments, equities and real estate.</p>
								</li>
							</ul>
						
			    		<h3>Income Funds: (Low Risk)</h3>
			    		<p>Investment avenues include government securities, cash in bank account, money market placements, deposits, certificate of deposits (COD), certificate of musharakas (COM) ,TDRs, commercial papers, reverse repo , MTS, units of money market and income collective investment schemes .</p>
						<ul>
								<li>
									<h5><b>Investment Secure Fund</b> </h5>
			    					<p>To offer regular and steady returns from investment in wide variety of fixed income securities including bank deposits, government securities etc. without any exposure to corporate bonds and equities. </p>
								</li>
								<li>
									<h5><b>Dynamic Secure Fund</b> </h5>
			    					<p>To offer regular and steady returns from investment in wide variety of fixed income securities including bank deposits, government securities etc. without any exposure to corporate bonds and equities.  </p>
								</li>
								<li>
									<h5><b>Tameen Fund</b> </h5>
			    					<p>To offer regular and steady returns from investment in wide variety of shariah compliant debt securities including bank deposits, government securities etc. without any exposure to corporate bonds and equities</p>
								</li>
							</ul>

							<h3>Income Funds: (Moderate Risk)</h3>
			    		<p>Investment avenues include government securities, cash in bank account, money market placements, deposits, certificate of deposits (COD), certificate of musharakas (COM) ,TDRs, commercial papers, reverse repo , TFC/Sukuk, MTS, units of money market and income collective investment schemes .</p>
						<ul>
								<li>
									<h5><b><b>Investment Secure Fund II</b></b> </h5>
			    					<p>To offer regular and steady returns from investment in wide variety of fixed income securities including bank deposits, government Securities, corporate bonds etc. without any exposure to equities.</p>
								</li>
								
							</ul>
			    	</div>
			    </li>
			    <!-- Tab li End -->
			    <!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent">
			    		<h3>Conventional Funds</h3>
			    		<p>Note: Unit Price Date is 28-Jul-2022 (Policy issued till 5:30 PM)</p>
			    		<table class="uk-table uk-table-divider">
						    <thead>
						        <tr>
						            <th>Fund type</th>
						            <th>Bid price (Rs)</th>
						            <th>Offer price (Rs)</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td>Investment Secure Fund</td>
						            <td>270.3094</td>
						            <td>284.5362</td>
						        </tr>
						        <tr>
						            <td>Investment Multiplier Fund</td>
						            <td>264.2429</td>
						            <td>278.1504</td>
						        </tr>
						        <tr>
						            <td>Amaanat Fund</td>
						            <td>197.7627</td>
						            <td>208.1713</td>
						        </tr>
						        <tr>
						            <td>Investment Secure Fund II</td>
						            <td>283.3425</td>
						            <td>292.1057</td>
						        </tr>
						        <tr>
						            <td>Investment Diversifier Fund</td>
						            <td>186.0819</td>
						            <td>191.837</td>
						        </tr>
						        <tr>
						            <td>Dynamic Secure Fund</td>
						            <td>169.5977</td>
						            <td>178.5239</td>
						        </tr>
						        <tr>
						            <td>Dynamic Growth Fund</td>
						            <td>112.9516</td>
						            <td>118.8964</td>
						        </tr>
						    </tbody>
						</table>
						<h3>Takaful Funds</h3>
			    		<p>Note: Unit Price Date is 28-Jul-2022 (Policy issued till 5:30 PM)</p>
			    		<table class="uk-table uk-table-divider">
						    <thead>
						        <tr>
						            <th>Fund type</th>
						            <th>Bid price (Rs)</th>
						            <th>Offer price (Rs)</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td>Taameen Fund</td>
						            <td>148.3217</td>
						            <td>156.1281</td>
						        </tr>
						        <tr>
						            <td>Maza'af Fund</td>
						            <td>113.4349</td>
						            <td>119.4052</td>
						        </tr>
						    </tbody>
						</table>
			    	</div>
		    	</li>
			    <li>
			    	<div class="tabsContent">
			    		<div class="tabsFormBox">
			    			<p>Select the date range to view performance & past prices</p>
			    			<form class="uk-grid-small" uk-grid>
							    
							    <div class="uk-width-1-2@s uk-width-1-4@m">
							    	<label class="uk-form-label">Select fund type:</label>
							        <select class="uk-select">
										<option>Conventional</option>
										<option>Takaful</option>
								    </select>
							    </div>
							    <div class="uk-width-1-2@s uk-width-1-4@m">
							    	<label class="uk-form-label">Fund Name:</label>
							        <select class="uk-select">
								        <option>Investment Secure Fund</option>
								        <option>Taameen Fund</option>
								    </select>
							    </div>
							    <div class="uk-width-1-2@s uk-width-1-4@m">
							    	<label class="uk-form-label">From:</label>
							        <input class="uk-input" type="date" placeholder="(DD-MM-YY)">
							    </div>
							    <div class="uk-width-1-2@s uk-width-1-4@m">
							    	<label class="uk-form-label">To:</label>
							        <input class="uk-input" type="date" placeholder="(DD-MM-YY)">
							    </div>
							    <div class="uk-width-1-2@s">
							    	<button class="blueBtn">Generate result <img src="images/right.svg" uk-svg /></button>
							    </div>
							</form>
			    		</div>
			    		<div class="chartSec">
			    			<canvas id="chartBox" width="1050" height="160"></canvas>
			    		</div>
			    	</div>
		    	</li>
			    <!-- Tab li End -->
			     <!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent">
			    		<div class="NewsFilter">
							<form>
							    <select class="uk-select">
							    	<option>Year</option>
							        <option>2022</option>
							        <option>2021</option>
							        <option>2020</option>
							        <option>2019</option>
							    </select>
							    <select class="uk-select">
							        <option>Month</option>
							        <option>Option 2</option>
							        <option>Option 3</option>
							        <option>Option 4</option>
							    </select>
							    <select class="uk-select">
							        <option>Conventional</option>
							        <option>Option 2</option>
							        <option>Option 3</option>
							        <option>Option 4</option>
							    </select>
							    <button class="blueBtn">Filter</button>
							</form>
						</div>
						<div class="DownloadList">
							<ul>
								<li><a href="javascript:;">FMR June 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR June 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR May 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR May 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR April 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR April 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
							</ul>
						</div>
						<div class="seeAll">
							<a href="javascript:;" >See all <img src="images/down.svg" uk-svg /></a>
						</div>
			    	</div>
		    	</li>
			    <!-- Tab li End -->
			</ul>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap whiteTabsSec">
	<div class="uk-container containCustom">
		<h2>Investor Information</h2>
		<div class="tabsSec">
			<!-- This is the nav containing the toggling elements -->
			<ul class="tabBtn2" uk-switcher="connect: .TabBox3">
			    <li><a href="javascript:;">Financial Reports</a></li>
			    <li><a href="javascript:;">Unit Linked Statements</a></li>
			    <li><a href="javascript:;">Financial Ratios</a></li>
			    <li><a href="javascript:;">Stock Information</a></li>
			    <li><a href="javascript:;">Investor FAQs</a></li>
			</ul>

			<!-- This is the container of the content items -->
			<ul class="uk-switcher TabBox3">
				<!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent">
			    		<div class="NewsFilter">
							<form>
							    <select class="uk-select">
							    	<option>Annual</option>
							        <option>Option 2</option>
							        <option>Option 3</option>
							        <option>Option 4</option>
							    </select>
							    <select class="uk-select">
							        <option>Year</option>
							        <option>2022</option>
							        <option>2021</option>
							        <option>2020</option>
							        <option>2019</option>
							    </select>
							    <select class="uk-select">
							        <option>Conventional</option>
							        <option>Option 2</option>
							        <option>Option 3</option>
							        <option>Option 4</option>
							    </select>
							    <button class="blueBtn">Filter</button>
							</form>
						</div>
						<div class="DownloadList">
							<ul>
								<li><a href="javascript:;">FMR June 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR June 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR May 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR May 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR April 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR April 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
							</ul>
						</div>
						<div class="seeAll">
							<a href="javascript:;" >See all <img src="images/down.svg" uk-svg /></a>
						</div>
						<p><a class="clrblue" href="#financialModal" uk-toggle>Request form for financial statements <img src="images/right.svg" uk-svg /></a></p>
			    	</div>
		    	</li>
			    <!-- Tab li End -->
			    <!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent">
			    		<div class="NewsFilter">
							<form>
							    <select class="uk-select">
							        <option>Year</option>
							        <option>2022</option>
							        <option>2021</option>
							        <option>2020</option>
							        <option>2019</option>
							    </select>
							    <button class="blueBtn">Filter</button>
							</form>
						</div>
						<div class="DownloadList">
							<ul>
								<li><a href="javascript:;">FMR June 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR June 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR May 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR May 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR April 2022 Conventional <img src="images/icons/download.svg" uk-svg /></a></li>
								<li><a href="javascript:;">FMR April 2022 Takaful <img src="images/icons/download.svg" uk-svg /></a></li>
							</ul>
						</div>
						<div class="seeAll">
							<a href="javascript:;" >See all <img src="images/down.svg" uk-svg /></a>
						</div>
			    	</div>
		    	</li>
			    <!-- Tab li End -->
				<!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent">
			    		<h3>Financial Ratios</h3>
			    		<div class="NewsFilter">
							<form>
							    <select class="uk-select">
							        <option>Year</option>
							        <option>2022</option>
							        <option>2021</option>
							        <option>2020</option>
							        <option>2019</option>
							    </select>
							    <button class="blueBtn">Filter</button>
							</form>
						</div>
			    		<p>Yearly earning per share as on 31st December 2020 <span class="clrblue">Rs. 17.84</span></p>
			    		<p>Price earnings ratio as on 31st December 2020 (in times) <span class="clrblue">12.16</span></p>
			    		<p>Breakup value per share as on 31st December 2020 <span class="clrblue">Rs. 61.75</span></p>
			    	</div>
			    </li>
			    <!-- Tab li End -->
			    <!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent">
			    		<h3>Stock Information</h3>
			    		<p>Shareholders are the backbone of any business. They provide the initial capital which enables the company to commence business and to then grow that business through retention of profits and by investing additional capital when required. Year after year, we strive to achieve an above average return on the stockholders’ invested capital.</p>
			    		<p>Jubilee Life is a Public Limited company whose shares are quoted on Pakistan Stock Exchange Limited (www.psx.com.pk). The company currently operates with a Paid-Up Capital of Rs. 873 million. Total shareholder equity as at 31 December 2021 was Rs. 13,069 million.</p>
			    	</div>
			    </li>
			    <!-- Tab li End -->
			    <!-- Tab li Start -->
			    <li>
			    	<div class="tabsContent">
			    		<ul uk-accordion>
						    <li class="uk-open">
						        <a class="uk-accordion-title" href="#">Lorem ipsum dolor</a>
						        <div class="uk-accordion-content">
						            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						            <p><a href="javascript:;">Click here for further details</a></p>
						        </div>
						    </li>
						    <li>
						        <a class="uk-accordion-title" href="#">Lorem ipsum dolor sit amet</a>
						        <div class="uk-accordion-content">
						            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.</p>
						            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.</p>
						            <p><a href="javascript:;">Click here for further details</a></p>
						        </div>
						    </li>
						    <li>
						        <a class="uk-accordion-title" href="#">Duis aute irure dolor in reprehenderit</a>
						        <div class="uk-accordion-content">
						            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident. </p>
						            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident. </p>
						            <p><a href="javascript:;">Click here for further details</a></p>
						        </div>
						    </li>
						</ul>
			    	</div>
			    </li>
			    <!-- Tab li End -->
			     
			</ul>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap whiteTabsSec">
	<div class="uk-container containCustom">
		<h2>List Of Unclaimed Insurance Benefits</h2>
		<div class="DownloadList">
			<ul>
				<li><a href="javascript:;">List-of-unclaimed-insurance-benefits-for-web-placement-Advertisement-30-November-2020 <img src="images/icons/download.svg" uk-svg /></a></li>
				<li><a href="javascript:;">Consolidated-List-of-unclaimed-insurance-benefits-30-june-2020 <img src="images/icons/download.svg" uk-svg /></a></li>
			</ul>
		</div>
	</div>
</section>
<!-- Section End -->
<div id="financialModal" class="uk-flex-top financialModal uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h3>Request form for financial statements</h3>
        <p>Dear Shareholder,<br> 
        The Securities and Exchange Commission of Pakistan, vide S.R.O 470(I)/2016 dated May 31, 2016 read with Section 223 of the Companies Act 2017, has allowed companies to circulate their annual balance sheet, profit and loss account, auditor’s report, directors’ report and ancillary statements/notes/documents (“Annual Audited Accounts”) along with notice of general meeting to its shareholders in electronic form.</p> 
        <p>However, Shareholders may request a hard copy of the Annual Audited Accounts along with notice of general meetings to be sent to their registered address instead of receiving the same in electronic form on CD/DVD/USB. If you require a hard copy of the Annual Audited Accounts, please fill the following form and send it to our Share Registrar or Company Secretary at the address given below.</p>
        <p>I/We request that a hard copy of the Annual Audited Accounts along with notice of general meetings be sent to me through post. My/our particulars in this respect are as follows:</p>
        <form uk-grid>				    
		    <div class="uk-width-1-2@s uk-width-1-3@m">
		    	<label class="uk-form-label">Full name *</label>
		        <input class="uk-input" type="text" placeholder="Your full name" required="">
		    </div>				    
		    <div class="uk-width-1-2@s uk-width-1-3@m">
		    	<label class="uk-form-label">Folio/CDC A/c No *</label>
		        <input class="uk-input" type="number" placeholder="xxxx-x-xxxxx-x" required="">
		    </div>
		    <div class="uk-width-1-2@s uk-width-1-3@m">
		    	<label class="uk-form-label">Postal Address:</label>
		        <input class="uk-input" type="text" placeholder="Your full postal address">
		    </div>
		    <div class="uk-width-1-2@s uk-width-1-3@m">
		    	<label class="uk-form-label">Contact</label>
		        <input class="uk-input" type="number" placeholder="Enter your contact number">
		    </div>
		    <div class="uk-width-1-2@s uk-width-1-3@m">
		    	<label class="uk-form-label">CNIC *</label>
		        <input class="uk-input" type="number" placeholder="xxxx-xxxxxxx-x" required="">
		    </div>
		    <div class="uk-width-1-2@s uk-width-1-3@m">
		    	<label class="uk-form-label">Email Address:</label>
		        <input class="uk-input" type="Email" placeholder="Enter your email">
		    </div>
		    
		    <div class="uk-width-1-1">
		    	<button class="blueBtn uk-margin-remove-top">Submit <img src="images/right.svg" uk-svg /></button>
		    </div>
		</form>
		<p>The form may be sent directly to Adamjee Life Assurance Company Limited or Share Registrar at the following address</p>
		<div uk-grid>
			<div class="uk-width-1-2@s">
				<p><strong>CDC Share Registrar Services Limited</strong></p>
				<p>CDC House, 99-B, Block ‘B’ S.M.C.H.S. Main Shahrah-e-Faisal, Karachi</p>
				<p>Website: www.cdcpakistan.com</p>
			</div>
			<div class="uk-width-1-2@s">
				<p><strong>Adamjee Life Assurance Co Limited</strong></p>
				<p>3rd & 4th Floor, Adamjee House, I.I Chundrigar Road, Karachi</p>
				<p>Website: www.adamjeelife.com</p>
			</div>
		</div>
		<p>If you are a CDC Account Holder, you should submit your request directly to your CDC Participant through which you maintain your CDC account.</p>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.9.1/chart.min.js"></script>
<script>
  const labels = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  const data = {
    labels: labels,
    datasets: [{
      label: 'My First dataset',
      backgroundColor: '#0064BF',
      borderColor: '#0064BF',
      data: [111, 10, 5, 2, 20, 30, 15, 10, 5, 2, 20, 30],
    }]
  };

  const config = {
    type: 'line',
    data: data,
    options: {}
  };
</script>
<script>
  const chartBox = new Chart(
    document.getElementById('chartBox'),
    config
  );
</script>

<?php include('footer.php'); ?>