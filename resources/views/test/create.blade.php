@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Test Create</h4>

                          <!-- main alert @s -->
                             {{-- @include('partials.alerts') --}}
                              <!-- main alert @e -->
                                <form method="POST" action="{{ route('test.store') }}" id="testCreate" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    
                                    <div class="form-group col-md-6">
                                        <label>Name<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="" name="name" placeholder="Enter Name">
 
                                        {{-- @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror --}}
                                                                           
                                    </div>

                                 
                                    <div class="form-group col-md-3">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="">Please select an option</option>
                                            <option value="P">Pending</option>
                                            <option value="A">Active</option>
                                        </select>

                                        {{-- @error('status')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror --}}
                                    
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label>Image<span class="text-danger"></span></label>
                                        <input type="file" class="form-control" id="" name="image" >
 
                                        {{-- @error('image')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror --}}
                                                                           
                                    </div>
                                    
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" name="submit" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('test.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        function save(){
            var formData  = new FormData(jQuery('#testCreate')[0]);
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:"{{ route('test.store') }}",
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    console.log(data);
                    if(data == 'success'){
                        swal({
                            title: "Success",
                            text: "Data added successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            window.location.href = "{{ route('test.index') }}";
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Please fill required fields.",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }
    </script>
@endsection
