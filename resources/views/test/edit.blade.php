{{-- @dd($test);  --}}
@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">
                            <h4 class="card-title">Test Edit</h4>
                                <form method="POST" action="{{ route('test.update',$test->id) }}" id="testEdit" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" value="{{ $test->id }}" id="id"/>
                                    <div class="form-group col-md-6">
                                        <label>Name<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ $test->name }}" class="form-control" id="name" name="name" placeholder="Name">
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label>Status<span class="text-danger">*</span></label>
                                        <select name="status" class="form-control">
                                            <option value="">Select an option</option>
                                            <option value="P" <?= ($test->status == "P") ? "selected" : "" ?>>Pending</option>
                                            <option value="A" <?= ($test->status == "A") ? "selected" : "" ?>>Active</option>
                                        </select>
                                    </div>
                                    

                                    
                                    <div class="form-group col-md-6">
                                        <label>Image<span class="text-danger"></span></label>
                                        <input type="file" class="form-control" id="" name="image" value="{{ $test->image }}" >
                                        <img src="{{ URL::asset('public/uploads') . '/' . $test->image }}" width="200px" alt="{{$test->image }}" >
 
                                                                           
                                    </div>
                                    
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('test.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        function save(){

            // alert('okay');
            var formData = new FormData(jQuery('#testEdit')[0]);
            // console.log(formData);
            
            var id = document.getElementById("id").value;
            //  console.log(id);

            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+"/test/"+id,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
               
                    
                    if(data === 'success'){

                    
                        swal({
                            title: "success",
                            text: "Data updated successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            backUrl = (window.location+'').replace('/test/'+id+'/edit', '/test');
                            window.location.href = backUrl;
                        });
                    } else {
                        swal({
                            title: "Error",
                            text:  "Please fill required fields.",
                            icon:  "error",
                            button: "Ok",
                        });
                    }

                }
            });
        }
    </script>
@endsection
