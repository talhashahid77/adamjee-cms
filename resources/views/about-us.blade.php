
@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php 
        $postImg =  getImageFile($device_type,$pagedata,"image","banner");
    @endphp
    <section class="HeaderInnerPage">
        <img src="{{ $postImg }}" />
        @include('partials.breadcrumb')
        <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
            <div class="uk-container containCustom">
                <h1>{{ $pagedata->title_en}}</h1>
                {!! $pagedata->description_en !!}
            </div>
        </div>
    </section>
@endforeach

@foreach($postsdata as $key => $postdata)
    @php 
        $postImg =  getImageFile($device_type,$postdata,"image","banner");
    @endphp
    @if($key == 0)
    <!-- Section Start -->
    <section class="SecWrap SecTopSpace">
        <div class="uk-container containCustom">
            <div class="innerPageContent">
                <h2>@if(session()->get('url') == 'en') {{$postdata->title_en}} @elseif (session()->get('url') == 'ur') {{$postdata->title_ur}} @endif</h2>
                @if(session()->get('url') == 'en') {!! $postdata->description_en !!} @elseif (session()->get('url') == 'ur') {!! $postdata->description_ur !!} @endif
            </div>
        </div>
    </section>
    <!-- Section End -->
    @endif
@endforeach

@foreach($innerPages as $key => $innerPage)
    @php 
        $postImg =  getImageFile($device_type,$innerPage,"image","banner");
    @endphp
    @if($key == 0)
        <!-- Section Start -->
        <section class="SecWrap">
            <div class="uk-container containCustom">

                <div class="boxyDesign">
                    <div class="boxyDesignImg">
                        <img src="{{ $postImg }}" />
                    </div>
                    <div class="boxyDesignTxt">
                        <h3>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif </h3>
                        @if(session()->get('url') == 'en') {!! $innerPage->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerPage->description_ur !!} @endif
                        <!-- <a class="blueBtn" href="{{ url(session()->get('url').$innerPage->path) }}">View all <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></a> -->
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- Section End -->
    @endif
@endforeach

@if(isset($innerPages))
    <!-- Section Start -->
    <section class="SecWrap">
        <div class="uk-container containCustom">
            <ul class="uk-grid-small" uk-grid>
                @foreach($innerPages as $key => $innerPage)
                    @php 
                        $postImg = getImageFile($device_type,$innerPage,"image","banner");
                    @endphp
                    @if($key > 0)
                        <li class="{{ $innerPage->grid_class_en}}">
                            <div class="GridBox" style="background-image: url({{ $postImg }});">
                                <div class="GridBoxTxt">
                                <h3>@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif </h3>
                                        @if(session()->get('url') == 'en') {!! $innerPage->description_en !!} @elseif (session()->get('url') == 'ur') {!! $innerPage->description_ur !!} @endif
                                    <a class="blueBtn" href="{{ url(session()->get('url').$innerPage->path) }}">Learn more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /> </a>
                                </div>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </section>
    <!-- Section End -->
@endif

<!-- Section Start -->
<section class="SecWrap">
	<div class="uk-container containCustom">
		<div class="ourPartners">
			<h3>Our partners</h3>
			<div uk-slider="autoplay: true; autoplay-interval: 3000">
			    <ul class="uk-slider-items">
                @for($i = 0; $i < count(json_decode($postsdata[1]->image_en)); $i++)
                    @php 
                        $postsdata[1] = $postdata;
                        if(session()->get('url') == "en"){
                            if($device_type == "mobile"){
                                $postImageSection = json_decode($postdata->image_mobile_en);
                                if(count($postImageSection) == 0){
                                    $postImageSection = json_decode($postdata->image_en); 
                                } 
                            } else{
                                $postImageSection = json_decode($postdata->image_en); 
                            }
                        } else { 
                            if($device_type == "mobile"){
                                $postImageSection = json_decode($postdata->image_mobile_ur); 
                                if(count($postImageSection) == 0){
                                    $postImageSection = json_decode($postdata->image_ur); 
                                } 
                            } else{
                                $postImageSection = json_decode($postdata->image_ur); 
                            }
                        }

                        if(isset($postImageSection[$i])){
                            $Img = $postImageSection[$i];
                        }else{
                            $Img = "";
                        }
                        $postImg = URL::to('/')."/public/source/".$Img."";
                        $url = URL::to('/').session()->get('url');
                    @endphp
                    <li>
			        	<div class="partnersImg">
			        		<img src="{{ $postImg }}" />
			        	</div>
			        </li>
                @endfor
			    </ul>
			</div>
		</div>
	</div>
</section>
<!-- Section End -->

@include('partials.footer')
@endif