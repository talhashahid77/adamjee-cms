@extends('admin.layouts.app')
@section('content')
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
        @include('admin.partials.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
            @include('admin.partials.sidebar')
        <!-- partial -->
         <div class="main-panel">
            <div class="content-wrapper">
            <div class="row">
              <div class="col-md-8">
                <div class="mr-md-3 mr-xl-5">
                  <!-- <h2>Welcome back,</h2> -->
                </div>
              </div>
              <!-- <div class="col-md-4">
                <div class="text-right">
                  <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 102%;margin-left: -17px;">
                      <i class="mdi mdi-calendar"></i>&nbsp;
                      <span id="dt"></span> <i class="mdi mdi-arrow-down-drop-circle"></i>
                      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  </div>
                </div>
              </div> -->
            </div>

              <!-- <div class="container pt-4">
                <div class="row align-items-stretch">
                  <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                      <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Basic Members<svg
                          class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path
                            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                          </path>
                        </svg></h4><span class="hind-font caption-12 c-dashboardInfo__count" id="TotBasic">0</span>
                    </div>
                  </div>
                  <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                      <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Bronze Members<svg
                          class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path
                            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                          </path>
                        </svg></h4><span class="hind-font caption-12 c-dashboardInfo__count" id="TotBronze">0</span>
                    </div>
                  </div>
                  <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                      <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Silver Members<svg
                          class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path
                            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                          </path>
                        </svg></h4><span class="hind-font caption-12 c-dashboardInfo__count" id="TotSilver">0</span>
                    </div>
                  </div>
                  <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                      <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Gold Members<svg
                          class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path
                            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                          </path>
                        </svg></h4><span class="hind-font caption-12 c-dashboardInfo__count" id="TotGold">0</span>
                    </div>
                  </div>
                  <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                      <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Platinum Members<svg
                          class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path
                            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                          </path>
                        </svg></h4><span class="hind-font caption-12 c-dashboardInfo__count" id="TotPlatinum">0</span>
                    </div>
                  </div>
                </div>
              </div> -->

            </div>
            <!-- content-wrapper ends -->
            @include('admin.partials.footer')
        </div> 
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

  <!-- <script type="text/javascript">
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);   
    $("span").on('DOMSubtreeModified', function () {
      if($(this).html().length > 23){
        var mIndex = $(this).html().indexOf('-')
        var dtFrom = $(this).html().substr(0,mIndex-1)
        var dtTo = $(this).html().substr(mIndex+1)
        var mdtFrom= new Date(dtFrom);
        var dtFrom= moment(mdtFrom).format('YYYY-MM-DD');
        var mdtTo= new Date(dtTo);
        var dtTo= moment(mdtTo).format('YYYY-MM-DD');
        var formData = 'dtFrom='+dtFrom;
        var dt = dtFrom+'--'+dtTo;
        $.ajax({
          type:'POST',
          enctype: 'multipart/form-data',
          url:"getdashList/"+dt,
          headers: {
              'X-CSRF-Token': '{{ csrf_token() }}',
          },
          data: formData,
          contentType: false,
          processData: false,
          success:function(data){
              if(data != ''){
                  var mIndex = data.indexOf('tmember=');
                  var mIndex2 = data.indexOf('pointsRedeem=');
                  var mIndex3 = data.indexOf('totWallet=');
                  var mIndex4 = data.indexOf('totBasic=');
                  var mIndex5 = data.indexOf('totBronze=');
                  var mIndex6 = data.indexOf('totSilver=');
                  var mIndex7 = data.indexOf('totGold=');
                  var mIndex8 = data.indexOf('totPlatinum=');
                  var TotMember = data.substr(8,mIndex2-8);
                  var TotReward = data.substr(mIndex2+13,mIndex3-mIndex2-13);        
                  var TotWallet = data.substr(mIndex3+10,mIndex4-mIndex3-10);
                  var TotBasic = data.substr(mIndex4+9,mIndex5-mIndex4-9);
                  var TotBronze = data.substr(mIndex5+10,mIndex6-mIndex5-10);
                  var TotSilver = data.substr(mIndex6+10,mIndex7-mIndex6-10);
                  var TotGold = data.substr(mIndex7+8,mIndex8-mIndex7-8);
                  var TotPlatinum = data.substr(mIndex8+12);

                  //document.getElementById('TotMember').innerHTML = TotMember;
                  //document.getElementById('TotReward').innerHTML = TotReward;
                  //document.getElementById('TotWallet').innerHTML = TotWallet;
                  document.getElementById('TotBasic').innerHTML = TotBasic;
                  document.getElementById('TotBronze').innerHTML = TotBronze;
                  document.getElementById('TotSilver').innerHTML = TotSilver;
                  document.getElementById('TotGold').innerHTML = TotGold;
                  document.getElementById('TotPlatinum').innerHTML = TotPlatinum;

              }
          }
      });
      }
    });
    window.onload = function () { 
      var formData = "";
      $.ajax({
          type:'POST',
          enctype: 'multipart/form-data',
          url:"getdashListDef",
          headers: {
              'X-CSRF-Token': '{{ csrf_token() }}',
          },
          data: formData,
          contentType: false,
          processData: false,
          success:function(data){
              if(data != ''){
                  var mIndex = data.indexOf('tmember=');
                  var mIndex2 = data.indexOf('pointsRedeem=');
                  var mIndex3 = data.indexOf('totWallet=');
                  var mIndex4 = data.indexOf('totBasic=');
                  var mIndex5 = data.indexOf('totBronze=');
                  var mIndex6 = data.indexOf('totSilver=');
                  var mIndex7 = data.indexOf('totGold=');
                  var mIndex8 = data.indexOf('totPlatinum=');
                  var TotMember = data.substr(8,mIndex2-8);
                  var TotReward = data.substr(mIndex2+13,mIndex3-mIndex2-13);        
                  var TotWallet = data.substr(mIndex3+10,mIndex4-mIndex3-10);
                  var TotBasic = data.substr(mIndex4+9,mIndex5-mIndex4-9);
                  var TotBronze = data.substr(mIndex5+10,mIndex6-mIndex5-10);
                  var TotSilver = data.substr(mIndex6+10,mIndex7-mIndex6-10);
                  var TotGold = data.substr(mIndex7+8,mIndex8-mIndex7-8);
                  var TotPlatinum = data.substr(mIndex8+12);

                  //document.getElementById('TotMember').innerHTML = TotMember;
                  //document.getElementById('TotReward').innerHTML = TotReward;
                  //document.getElementById('TotWallet').innerHTML = TotWallet;
                  document.getElementById('TotBasic').innerHTML = TotBasic;
                  document.getElementById('TotBronze').innerHTML = TotBronze;
                  document.getElementById('TotSilver').innerHTML = TotSilver;
                  document.getElementById('TotGold').innerHTML = TotGold;
                  document.getElementById('TotPlatinum').innerHTML = TotPlatinum;

              }
          }
      });
    }
  </script> -->
@endsection
