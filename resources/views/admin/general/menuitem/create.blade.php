@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Menu Item Create</h4>
                                <form method="POST" action="{{ route('menuitem.store') }}" id="menuitemCreate" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="{{ request()->id }}" id="menu_id" name="menu_id"/>
                                    <div class="form-group col-md-6">
                                        <label>Title(en)<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="title_en" name="title_en" placeholder="Title English">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Title(ur)</label>
                                        <input type="text" class="form-control" id="title_ur" name="title_ur" placeholder="Title Urdu">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(en)</label>
                                        <textarea id="tinyMceExample2" class="form-control" name="desc_en"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(ur)</label>
                                        <textarea id="tinyMceExample" class="form-control"  name="desc_ur"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">Image(en)</label>
                                            <input id="inpen" name="image_en" readonly class="form-control col-md-8 type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-en-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-en-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpen&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">Image(ur)</label>
                                            <input id="inpur" name="image_ur" readonly class="form-control col-md-8 type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-ur-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Link Type(en)</label>
                                        <select name="linktype_en" id="linktype_en" class="form-control">
                                            <option value="internal">Internal</option>
                                            <option value="external">External</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Link Type(ur)</label>
                                        <select name="linktype_ur" id="linktype_ur" class="form-control">
                                            <option value="internal">Internal</option>
                                            <option value="external">External</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6" id="links_en">
                                        <label>Link(en)</label>
                                        <select name="linksdp_en" id="linksdp_en" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->path}}">{{$parent->title_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 d-none" id="link_en">
                                        <label>Link(en)</label>
                                        <input type="text" class="form-control" id="linktxt_en" name="linktxt_en" placeholder="Link English">
                                    </div>
                                    <div class="form-group col-md-6" id="links_ur">
                                        <label>Link(ur)</label>
                                        <select name="linksdp_ur" id="linksdp_ur" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->path}}">{{$parent->title_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 d-none" id="link_ur">
                                        <label>Link(ur)</label>
                                        <input type="text" class="form-control" id="linktxt_ur" name="linktxt_ur" placeholder="Link Urdu">
                                    </div>
                                    <div class="form-group col-md-6" id="links_en">
                                        <label>Menu Item</label>
                                        <select name="menu_parent_item_id" id="menu_parent_item_id" class="form-control">
                                            <option value="0">Select</option>
                                            @foreach($menus as $menu)
                                                <option value="{{$menu->id}}">{{$menu->title_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('menuitem.index') }}?id={{ request()->id }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        $("#linktype_en").change(function () {
            var val = this.value;
            if(val == "internal"){
                $("#link_en").addClass("d-none");
                $("#links_en").removeClass("d-none");
                document.getElementById("linktxt_en").value = NULL;
            } else if(val == "external"){
                $("#links_en").addClass("d-none");
                $("#link_en").removeClass("d-none");
            }
        });

        $("#linktype_ur").change(function () {
            var val = this.value;
            if(val == "internal"){
                $("#link_ur").addClass("d-none");
                $("#links_ur").removeClass("d-none");
                document.getElementById("linktxt_ur").value = NULL;
            } else if(val == "external"){
                $("#links_ur").addClass("d-none");
                $("#link_ur").removeClass("d-none");
            }
        });

        $("#links_en option").each(function() {
            $(this).siblings('[value="'+ this.value +'"]').remove();
        });
        $("#links_ur option").each(function() {
            $(this).siblings('[value="'+ this.value +'"]').remove();
        });
        function save(){
            var descen = tinymce.get("tinyMceExample2").getContent();
            var descur = tinymce.get("tinyMceExample").getContent();
            var menu_id = document.getElementById("menu_id").value;
            var formData  = new FormData(jQuery('#menuitemCreate')[0]);
            formData.append("descen", descen);
            formData.append("descur", descur);            
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:"{{ route('menuitem.store') }}",
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    console.log(data);
                    if(data == 'success'){
                        swal({
                            title: "Success",
                            text: "Data added successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            window.location.href = "{{ route('menuitem.index') }}?id="+menu_id;
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Please fill required fields.",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }
    </script>
@endsection
