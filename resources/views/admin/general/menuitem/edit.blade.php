@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">
                            <h4 class="card-title">Sub Menu Edit</h4>
                                <form method="POST" action="{{ route('menuitem.update',$menuitem->id) }}" id="menuitemEdit" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" value="{{ $menuitem->id }}" id="id"/>
                                    <input type="hidden" value="{{ request()->id }}" id="menu_id" name="menu_id"/>
                                    <div class="form-group col-md-6">
                                        <label>Title(en)<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ $menuitem->title_en }}" class="form-control" id="title_en" name="title_en" placeholder="Title English">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Title(ur)</label>
                                        <input type="text" value="{{ $menuitem->title_ur }}" class="form-control" id="title_ur" name="title_ur" placeholder="Title Urdu">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(en)</label>
                                        <textarea id="tinyMceExample2" class="form-control" name="desc_en">{{ $menuitem->description_en }}</textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(ur)</label>
                                        <textarea id="tinyMceExample" class="form-control"  name="desc_ur">{{ $menuitem->description_ur }}</textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">Image(en)</label>
                                            <input id="inpen" name="image_en" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-en-modal-lg" class="form-control col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-en-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpen&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                            $decImg = json_decode($menuitem->image_en);
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <img class='imgcus' src='".$url2."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <img class='imgcus' src='".$url3."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <img class='imgcus' src='".$url4."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }else {
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <div id='img'>
                                                                        <img class='imgcus' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }
                                                    
                                                }   
                                            }                                 
                                        ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">Image(ur)</label>
                                            <input id="inpur" name="image_ur" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-ur-modal-lg" class="form-control col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                            $decImg = json_decode($menuitem->image_ur);
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <img class='imgcus' src='".$url2."' />
                                                                <div id='img'>
                                                                    <img class='imgcus d-none' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <img class='imgcus' src='".$url3."' />
                                                                <div id='img'>
                                                                    <img class='imgcus d-none' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <img class='imgcus' src='".$url4."' />
                                                                <div id='img'>
                                                                    <img class='imgcus d-none' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    } else {
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <div id='img'>
                                                                    <img class='imgcus' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    }
                                                    
                                                }   
                                            }                           
                                        ?>
                                    </div>
                                    <input type="hidden" value="{{ $menuitem->link_url_en }}" id="type_en"/>
                                    <input type="hidden" value="{{ $menuitem->link_url_ur }}" id="type_ur"/>
                                    <input type="hidden" value="{{ $menuitem->menu_parent_item_id }}" id="menu_parent"/>
                                    <div class="form-group col-md-6">
                                        <label>Link Type(en)</label>
                                        <select name="linktype_en" id="linktype_en" class="form-control">
                                            @if($menuitem->link_type_en == "internal")
                                                <option value="internal" selected>Internal</option>
                                                <option value="external">External</option>
                                            @else if($menuitem->link_type_en == "external")
                                                <option value="internal">Internal</option>
                                                <option value="external" selected>External</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Link Type(ur)</label>
                                        <select name="linktype_ur" id="linktype_ur" class="form-control">
                                            @if($menuitem->link_type_ur == "internal")
                                                <option value="internal" selected>Internal</option>
                                                <option value="external">External</option>
                                            @else if($menuitem->link_type_ur == "external")
                                                <option value="internal">Internal</option>
                                                <option value="external" selected>External</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 {{ ($menuitem->link_type_en == 'internal') ? '' : 'd-none'}}" id="links_en">
                                        <label>Link(en)</label>
                                        <select name="linksdp_en" id="linksdp_en" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->path}}">{{$parent->title_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 {{ ($menuitem->link_type_en == 'internal') ? 'd-none' : ''}}" id="link_en">
                                        <label>Link(en)</label>
                                        <input type="text" class="form-control" value="{{ ($menuitem->link_type_en == 'internal') ? '' : $menuitem->link_url_en}}" id="linktxt_en" name="linktxt_en" placeholder="Link English">
                                    </div>
                                    <div class="form-group col-md-6 {{ ($menuitem->link_type_ur == 'internal') ? '' : 'd-none'}}" id="links_ur">
                                        <label>Link(ur)</label>
                                        <select name="linksdp_ur" id="linksdp_ur" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($parents as $parent)
                                                <option value="{{$parent->path}}">{{$parent->title_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 {{ ($menuitem->link_type_ur == 'internal') ? 'd-none' : ''}}" id="link_ur">
                                        <label>Link(ur)</label>
                                        <input type="text" class="form-control" id="linktxt_ur" value="{{ ($menuitem->link_type_ur == 'internal') ? '' : $menuitem->link_url_ur}}" name="linktxt_ur" placeholder="Link Urdu">
                                    </div>
                                    <div class="form-group col-md-6" id="links_en">
                                        <label>Menu Item</label>
                                        <select name="menu_parent_item_id" id="menu_parent_item_id" class="form-control">
                                            <option value="0">Select</option>
                                            @foreach($menus as $menu)
                                                <option value="{{$menu->id}}">{{$menu->title_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('menuitem.index') }}?id={{ request()->id }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        $(".remove").click(function(){
            $(this).parent('.product-img').fadeOut('100', function(){
    			$(this).remove();
    		});
        });
        $("#linktype_en").change(function () {
            var val = this.value;
            if(val == "internal"){
                $("#link_en").addClass("d-none");
                $("#links_en").removeClass("d-none");
                document.getElementById("linktxt_en").value = NULL;
            } else if(val == "external"){
                $("#links_en").addClass("d-none");
                $("#link_en").removeClass("d-none");
            }
        });

        $("#linktype_ur").change(function () {
            var val = this.value;
            if(val == "internal"){
                $("#link_ur").addClass("d-none");
                $("#links_ur").removeClass("d-none");
                document.getElementById("linktxt_ur").value = NULL;
            } else if(val == "external"){
                $("#links_ur").addClass("d-none");
                $("#link_ur").removeClass("d-none");
            }
        });

        $("#links_en option").each(function() {
            $(this).siblings('[value="'+ this.value +'"]').remove();
        });
        $("#links_ur option").each(function() {
            $(this).siblings('[value="'+ this.value +'"]').remove();
        });
        var linken = document.getElementById('type_en').value;
        var linkur = document.getElementById('type_ur').value;
        var menu_parent_item_id = document.getElementById('menu_parent').value;
        $('#linksdp_ur').find('option[value="'+linkur+'"]').attr('selected','selected');
        $('#linksdp_en').find('option[value="'+linken+'"]').attr('selected','selected');
        $('#menu_parent_item_id').find('option[value="'+menu_parent_item_id+'"]').attr('selected','selected');
        function save(){
            let x = 0;
            const imgCount = $.map($('#images-en > div'), div => div.id);
            while(x < imgCount.length) {
                $('#'+imgCount[x]).attr("id","imgen"+x)
                x++;
            }

            let k = 0;
            const imgCountur = $.map($('#images-ur > div'), div => div.id);
            while(k < imgCountur.length) {
                $('#'+imgCountur[k]).attr("id","imgur"+k)
                k++;
            }

            // File En Start***********************************************************
            var imgAll_en = "";
            var imgcnt = $("#images-en #img img").length;
            for (let index = 0; index < imgcnt; index++) {
                var images = $('#imgen'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        imgAll_en += images[1].substr(indexImg+8);
                    } else {
                        imgAll_en += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        imgAll_en += images[0].substr(indexImg+8);
                    } else {
                        imgAll_en += ','+images[0].substr(indexImg+8);
                    }
                }
            }
            const previousImgEn = imgAll_en.split(',');
            selectedImgEn = document.getElementById('inpen').value;
            mImgEn = selectedImgEn.replaceAll('["', "");
            mImgEn = mImgEn.replaceAll('","', ",");
            mImgEn = mImgEn.replaceAll('"]', "");
            mImgEn = mImgEn.split(",");
            var imgAlleng = "";
            if(mImgEn != '' && previousImgEn != ''){
                imgAlleng = previousImgEn.concat(mImgEn);
            } else if(mImgEn != '' && previousImgEn == '') {
                imgAlleng = mImgEn;
            } else if(mImgEn == '' && previousImgEn != '') {
                imgAlleng = previousImgEn;
            }
            // File En End*************************************************************
            
            // File Ur Start***********************************************************
            var imgAll_ur = "";
            var imgcnt = $("#images-ur #img img").length;
            for (let index = 0; index < imgcnt; index++) {
                var images = $('#imgur'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        imgAll_ur += images[1].substr(indexImg+8);
                    } else {
                        imgAll_ur += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        imgAll_ur += images[0].substr(indexImg+8);
                    } else {
                        imgAll_ur += ','+images[0].substr(indexImg+8);
                    }
                }

            }
            const previousImgUr = imgAll_ur.split(',');
            selectedImgUr = document.getElementById('inpur').value;
            mImgUr = selectedImgUr.replaceAll('["', "");
            mImgUr = mImgUr.replaceAll('","', ",");
            mImgUr = mImgUr.replaceAll('"]', "");
            mImgUr = mImgUr.split(",");

            var imgAllUr = "";
            if(mImgUr != '' && previousImgUr != ''){
                imgAllUr = previousImgUr.concat(mImgUr);
            } else if(mImgUr != '' && previousImgUr == '') {
                imgAllUr = mImgUr;
            } else if(mImgUr == '' && previousImgUr != '') {
                imgAllUr = previousImgUr;
            }
            // File Ur End***********************************************************

            var descen = tinymce.get("tinyMceExample2").getContent();
            var descur = tinymce.get("tinyMceExample").getContent();
            var formData  = new FormData(jQuery('#menuitemEdit')[0]);
            formData.append("descen", descen);
            formData.append("descur", descur);
            formData.append("imgen", imgAlleng);
            formData.append("imgur", imgAllUr);
            var id = document.getElementById("id").value;
            var menu_id = document.getElementById("menu_id").value;
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+"/menuitem/"+id,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    console.log(data);
                    if(data == 'success'){
                        swal({
                            title: "Success",
                            text: "Data updated successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            window.location.href = "{{ route('menuitem.index') }}?id="+menu_id;
                        }); 
                    } else {
                        swal({
                            title: "Error",
                            text: "Please fill required fields.",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }
    </script>
@endsection
