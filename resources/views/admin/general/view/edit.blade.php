@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">
                            <h4 class="card-title">View Edit</h4>
                                <form method="POST" action="{{ route('view.update',$view->id) }}" id="viewEdit" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" value="{{ $view->id }}" id="id"/>
                                    <div class="form-group col-md-6">
                                        <label>Title<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ $view->title }}" class="form-control" id="title" name="title" placeholder="Title">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Slug<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ substr($view->slug,1) }}" class="form-control" id="slug" name="slug" placeholder="Slug">
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('view.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        $('#slug').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9-]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
            event.preventDefault();
            return false;
            }
        });
        function save(){
            var formData  = new FormData(jQuery('#viewEdit')[0]);
            var id = document.getElementById("id").value;
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+"/view/"+id,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    console.log(data);
                    if(data == 'success'){
                        swal({
                            title: "Success",
                            text: "Data updated successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            backUrl = (window.location+'').replace('/view/'+id+'/edit', '/view');
                            window.location.href = backUrl;
                        });
                    }else if(data == 'data found') {
                        swal({
                            title: "Error",
                            text: "Please enter different slug.",
                            icon: "error",
                            button: "Ok",
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Please fill required fields.",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }
    </script>
@endsection
