@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">
                            <h4 class="card-title">User Edit</h4>
                                <form method="POST" action="{{ route('user.update',$user->id) }}" id="userEdit" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" value="{{ $user->id }}" id="id"/>
                                    <div class="form-group col-md-6">
                                        <label>Name<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ $user->name }}" class="form-control" id="name" name="name" placeholder="Name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Email<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ $user->email }}" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Password</label>
                                        <input type="password" class="form-control" id="pass" name="pass">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Role<span class="text-danger">*</span></label>
                                        <select name="role" class="form-control">
                                            <option value="">Select Role</option>
                                            @if($user->role == "admin") 
                                                <option value="admin" selected>Admin</option>
                                                <option value="editor">Editor</option>
                                                <option value="publisher">Publisher</option>
                                            @elseif($user->role == "editor")
                                                <option value="admin" >Admin</option>
                                                <option value="editor" selected>Editor</option>
                                                <option value="publisher">Publisher</option>
                                            @elseif($user->role == "publisher") 
                                                <option value="admin" >Admin</option>
                                                <option value="editor">Editor</option>
                                                <option value="publisher" selected>Publisher</option>
                                            @endif

                                        </select>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('user.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        function save(){
            var formData  = new FormData(jQuery('#userEdit')[0]);
            var id = document.getElementById("id").value;
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+"/user/"+id,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    console.log(data);
                    if(data == 'success'){
                        swal({
                            title: "Success",
                            text: "Data updated successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            backUrl = (window.location+'').replace('/user/'+id+'/edit', '/user');
                            window.location.href = backUrl;
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Please fill required fields.",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }
    </script>
@endsection
