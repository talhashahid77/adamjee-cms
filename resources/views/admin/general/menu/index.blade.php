@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 text-left">
                        <h4 class="card-title">Menu List</h4>
                    </div>
                    <div class="col-md-10 text-right mb-3">
                        <a href="{{ route('menu.create') }}" class="btn btn-primary text-right">Create</a>
                    </div>
                </div>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table table-bordered data-table">
                        <thead>
                            <tr>
                                <th width="100px">No</th>
                                <th>Title</th>
                                <th width="100px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        @include('admin.partials.footer')  
      </div>
    </div>
    <script type="text/javascript">
      setTimeout(() => {
        $(function () {
            var table = $('.data-table').DataTable({
                "order": [[ 0, "desc" ]],
                processing: true,
                serverSide: true,
                ajax: "{{ route('menu.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title_en', name: 'title_en'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
        });
      }, 1000);
    </script>
    <script>
        function mdelete(id,mId){
          swal({
            title: "Are you sure?",
            text: "You want to delete this menu?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                  type:'DELETE',
                  enctype: 'multipart/form-data',
                  url:'<?php echo url('/');?>'+"/menu/"+id,
                  headers: {
                      'X-CSRF-Token': '{{ csrf_token() }}',
                  },
                  data: {
                      id:id
                  },
                  contentType: false,
                  processData: false,
                  success:function(data){
                      if(data == 'success'){
                          swal({
                              title: "Success",
                              text: "Menu deleted successfully.",
                              icon: "success",
                              buttons: "Ok",
                          })
                          .then((willDelete) => {
                            var p=mId.parentNode.parentNode;
                            p.parentNode.removeChild(p);
                          });
                      } else {
                          swal({
                              title: "Error",
                              text: "Error occurred",
                              icon: "error",
                              button: "Ok",
                          });
                      }
                  }
              });
            } else {
              console.log("nothing delete");
            }
          });
        }
    </script>
@endsection
