@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">menu Create</h4>
                                <form method="POST" action="{{ route('menu.store') }}" id="menuCreate" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group col-md-6">
                                        <label>Title(en)<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="title_en" name="title_en" placeholder="Title English">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Title(ur)</label>
                                        <input type="text" class="form-control" id="title_ur" name="title_ur" placeholder="Title Urdu">
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('menu.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        function save(){
            var formData  = new FormData(jQuery('#menuCreate')[0]);
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:"{{ route('menu.store') }}",
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    console.log(data);
                    if(data == 'success'){
                        swal({
                            title: "Success",
                            text: "Data added successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            window.location.href = "{{ route('menu.index') }}";
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Please fill required fields.",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }
    </script>
@endsection
