@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar') 
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Page Create</h4>
                                <form method="POST" action="{{ route('page.store') }}" id="pageCreate" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="{{ request()->id }}" id="page_id"/>
                                    <div class="form-group col-md-6">
                                        <label>Title(en)<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="title_en" name="title_en" placeholder="Title English">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Title(ur)</label>
                                        <input type="text" class="form-control" id="title_ur" name="title_ur" placeholder="Title Urdu">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sub Title(en)<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="sub_title_en" name="sub_title_en" placeholder="Sub Title English">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sub Title(ur)</label>
                                        <input type="text" class="form-control" id="sub_title_ur" name="sub_title_ur" placeholder="Sub Title Urdu">
                                    </div>  
                                    <div class="form-group col-md-6">
                                        <label>Short Description(en)</label>
                                        <textarea id="tinyMceExample3" class="form-control" name="short_desc_en"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Short Description(ur)</label>
                                        <textarea id="tinyMceExample4" class="form-control"  name="short_desc_ur"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(en)</label>
                                        <textarea id="tinyMceExample2" class="form-control" name="desc_en"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(ur)</label>
                                        <textarea id="tinyMceExample" class="form-control"  name="desc_ur"></textarea>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image(en)</label>
                                            <input id="inpen" name="image_en" readonly class="form-control col-md-8 type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-en-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-en-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpen&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image Mobile(en)</label>
                                            <input id="inpmoben" name="image_mobile_en" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-mobile-en-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-mobile-en-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpmoben&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image(ur)</label>
                                            <input id="inpur" name="image_ur" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-ur-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image Mobile(ur)</label>
                                            <input id="inpmobur" name="image_mobile_ur" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-mobile-ur-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-mobile-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpmobur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">File(en)</label>
                                            <input id="inpfile_en" name="file_en" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-fileen-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-fileen-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpfile_en&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">File(ur)</label>
                                            <input id="inpfile_ur" name="file_ur" readonly class="form-control col-md-8 type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-file-ur-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-file-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpfile_ur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>View(en)<span class="text-danger">*</span></label>
                                        <select name="view_en" class="form-control">
                                            <option value="0">Select</option>
                                            @foreach($views_en as $view)
                                                <option value="{{$view->slug}}">{{$view->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Position(en)</label>
                                        <select name="position_en" class="form-control">
                                            <option value="">Left</option>
                                            <option value="uk-flex-right@m">Right</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>View(ur)</label>
                                        <select name="view_ur" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($views_en as $view)
                                                <option value="{{$view->slug}}">{{$view->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label>Position(ur)</label>
                                        <select name="position_ur" class="form-control">
                                            <option value="">Left</option>
                                            <option value="uk-flex-right@m">Right</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Grid Class(en)</label>
                                        <select name="grid_class_en" class="form-control">
                                            <option value="uk-width-1-1@m">uk-width-1-1@m</option>
                                            <option value="uk-width-1-2@m">uk-width-1-2@m</option>
                                            <option value="uk-width-1-3@m">uk-width-1-3@m</option>
                                            <option value="uk-width-1-4@m">uk-width-1-4@m</option>
                                            <option value="uk-width-1-5@m">uk-width-1-5@m</option>
                                            <option value="uk-width-1-6@m">uk-width-1-6@m</option>
                                            <option value="uk-width-2-3@m">uk-width-2-3@m</option>
                                            <option value="uk-width-2-5@m">uk-width-2-5@m</option>
                                            <option value="uk-width-3-4@m">uk-width-3-4@m</option>                                            
                                            <option value="uk-width-3-5@m">uk-width-3-5@m</option>
                                            <option value="uk-width-4-5@m">uk-width-4-5@m</option>
                                            <option value="uk-width-5-6@m">uk-width-5-6@m</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Author</label>
                                        <input type="text" class="form-control" id="author" name="author" placeholder="Author">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Grid Class(ur)</label>
                                        <select name="grid_class_ur" class="form-control">
                                            <option value="uk-width-1-1@m">uk-width-1-1@m</option>
                                            <option value="uk-width-1-2@m">uk-width-1-2@m</option>
                                            <option value="uk-width-1-3@m">uk-width-1-3@m</option>
                                            <option value="uk-width-1-4@m">uk-width-1-4@m</option>
                                            <option value="uk-width-1-5@m">uk-width-1-5@m</option>
                                            <option value="uk-width-1-6@m">uk-width-1-6@m</option>
                                            <option value="uk-width-2-3@m">uk-width-2-3@m</option>
                                            <option value="uk-width-2-5@m">uk-width-2-5@m</option>
                                            <option value="uk-width-3-4@m">uk-width-3-4@m</option>                                            
                                            <option value="uk-width-3-5@m">uk-width-3-5@m</option>
                                            <option value="uk-width-4-5@m">uk-width-4-5@m</option>
                                            <option value="uk-width-5-6@m">uk-width-5-6@m</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Slug<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug">
                                    </div>
                                    @if(request()->id)
                                        <input type="hidden" class="form-control" id="parent" name="parent" value="{{ request()->id }}">
                                    @else
                                        <div class="form-group col-md-3">
                                            <label>Parent</label>
                                            <select name="parent" class="form-control">
                                                <option value="0">Select</option>
                                                @foreach($parents as $parent)
                                                    <option value="{{$parent->id}}">{{$parent->title_en}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class="form-group col-md-3">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="Y">Show</option>
                                            <option value="N">Hide</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Show In Menu</label>
                                        <select name="show_in_menu" class="form-control">
                                            <option value="1">Show</option>
                                            <option value="0">Hide</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Sorting<span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" id="sorting" name="sorting" placeholder="Value">
                                    </div>
                                    <fieldset class="border p-2 mb-4 row" style="border: 2px solid #dfd9d9 !important;">
                                        <legend class="w-auto fs-5" style="font-size: 17px;">SEO</legend>
                                        <div class="form-group col-md-6">
                                            <label>Meta Title(en)</label>
                                            <input type="text" class="form-control" id="meta_title_en" name="meta_title_en" placeholder="Meta Title English">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Title(ur)</label>
                                            <input type="text" class="form-control" id="meta_title_ur" name="meta_title_ur" placeholder="Meta Title Urdu">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Description(en)</label>
                                            <textarea class="form-control" id="meta_desc_en" name="meta_desc_en"></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Description(ur)</label>
                                            <textarea class="form-control" id="meta_desc_ur" name="meta_desc_ur"></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Keywords(en)</label>
                                            <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" placeholder="Meta Keywords English">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Keywords(ur)</label>
                                            <input type="text" class="form-control" id="meta_keywords_ur" name="meta_keywords_ur" placeholder="Meta Keywords Urdu">
                                        </div>
                                    </fieldset>
                                    <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('page.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        $('#slug').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9-]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
            event.preventDefault();
            return false;
            }
        });
        function save(){
            var dpUr = $("select[name='view_ur'] option:selected").index();
            var txtUr = document.getElementById('title_ur').value;
            var descen = tinymce.get("tinyMceExample2").getContent();
            var descur = tinymce.get("tinyMceExample").getContent();
            var short_descen = tinymce.get("tinyMceExample3").getContent();
            var short_descur = tinymce.get("tinyMceExample4").getContent();
            var page_id = document.getElementById("page_id").value;
            if(dpUr > 0 && txtUr == ""){
                swal({
                    title: "Error",
                    text: "Please fill urdu title.",
                    icon: "error",
                    button: "Ok",
                });
            } else if(dpUr == 0 && txtUr != ""){
                swal({
                    title: "Error",
                    text: "Please select urdu view.",
                    icon: "error",
                    button: "Ok",
                });
            } else {
                var formData  = new FormData(jQuery('#pageCreate')[0]);
                formData.append("descen", descen);
                formData.append("descur", descur);
                formData.append("short_descen", short_descen);
                formData.append("short_descur", short_descur);
                $.ajax({
                    type:'POST',
                    enctype: 'multipart/form-data',
                    url:"{{ route('page.store') }}",
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    data: formData,
                    contentType: false,
                    processData: false,
                    success:function(data){
                        // console.log(data);
                        if(data == 'success'){
                            swal({
                                title: "Success",
                                text: "Data added successfully.",
                                icon: "success",
                                buttons: "Ok",
                            })
                            .then((willDelete) => {
                                // backUrl = (window.location+'').replace('/page/create', '/page');
                                if(page_id){
                                    window.location.href = "{{ route('page.index') }}?id="+page_id;
                                }else{
                                    window.location.href = "{{ route('page.index') }}";
                                }
                            });
                        } else if(data == 'data found'){
                            swal({
                                title: "Error",
                                text: "Please enter different slug.",
                                icon: "error",
                                button: "Ok",
                            });
                        } else {
                            swal({
                                title: "Error",
                                text: "Please fill required fields.",
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    }
                });
            }
        }
    </script>
@endsection
