@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">
                            <h4 class="card-title">Page Edit</h4>
                                <form method="POST" action="{{ route('page.update',$page->id) }}" id="pageEdit" class="forms-sample row" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" value="{{ $page->id }}" id="id"/>
                                    <input type="hidden" value="{{ request()->id }}" id="page_id"/>
                                    <div class="form-group col-md-6">
                                        <label>Title(en)<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ $page->title_en }}" class="form-control" id="title_en" name="title_en" placeholder="Title English">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Title(ur)</label>
                                        <input type="text" value="{{ $page->title_ur }}" class="form-control" id="title_ur" name="title_ur" placeholder="Title Urdu">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sub Title(en)<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ $page->sub_title_en }}" class="form-control" id="sub_title_en" name="sub_title_en" placeholder="Sub Title English">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sub Title(ur)</label>
                                        <input type="text" value="{{ $page->sub_title_ur }}" class="form-control" id="sub_title_ur" name="sub_title_ur" placeholder="Sub Title Urdu">
                                    </div>  
                                    <div class="form-group col-md-6">
                                        <label>Short Description(en)</label>
                                        <textarea id="tinyMceExample3" class="form-control" name="short_desc_en">{{ $page->short_desc_en }}</textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Short Description(ur)</label>
                                        <textarea id="tinyMceExample4" class="form-control"  name="short_desc_ur">{{ $page->short_desc_ur }}</textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(en)</label>
                                        <textarea id="tinyMceExample2" class="form-control" name="desc_en">{{ $page->description_en }}</textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Description(ur)</label>
                                        <textarea id="tinyMceExample" class="form-control"  name="desc_ur">{{ $page->description_ur }}</textarea>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image(en)</label>
                                            <input id="inpen" name="image_en" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-en-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-en-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpen&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image Mobile(en)</label>
                                            <input id="inpmoben" name="image_mobile_en" readonly class="form-control col-md-8 type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-mobile-en-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-mobile-en-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpmoben&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image(ur)</label>
                                            <input id="inpur" name="image_ur" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-ur-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="row">
                                            <label class="col-md-12">Image Mobile(ur)</label>
                                            <input id="inpmobur" name="image_mobile_ur" readonly class="form-control col-md-8" type="text" value=""  style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-mobile-ur-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-mobile-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpmobur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <?php 
                                            $decImg = json_decode($page->image_en);
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                $url5 = URL::to('/')."/public/source/video.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <img class='imgcus' src='".$url2."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <img class='imgcus' src='".$url3."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <img class='imgcus' src='".$url4."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "mp4"){
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <img class='imgcus' src='".$url5."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }else {
                                                        echo "<div class='product-img' id='images-en'>                                                                
                                                                <div id='imgen".$i."'>
                                                                    <div id='img'>
                                                                        <img class='imgcus' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }
                                                    
                                                }   
                                            }                                  
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <?php 
                                            $decImg = json_decode($page->image_mobile_en);
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='images-mobile-en'>                                                                
                                                                <div id='imgmobile-en".$i."'>
                                                                    <img class='imgcus' src='".$url2."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-mobile-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='images-mobile-en'>                                                                
                                                                <div id='imgmobile-en".$i."'>
                                                                    <img class='imgcus' src='".$url3."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-mobile-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='images-mobile-en'>                                                                
                                                                <div id='imgmobile-en".$i."'>
                                                                    <img class='imgcus' src='".$url4."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-mobile-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }else {
                                                        echo "<div class='product-img' id='images-mobile-en'>                                                                
                                                                <div id='imgmobile-en".$i."'>
                                                                    <div id='img'>
                                                                        <img class='imgcus' id='imgcus-mobile-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }
                                                    
                                                }   
                                            }                                  
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <?php 
                                            $decImg = json_decode($page->image_ur);                                           
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <img class='imgcus' src='".$url2."' />
                                                                <div id='img'>
                                                                    <img class='imgcus d-none' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <img class='imgcus' src='".$url3."' />
                                                                <div id='img'>
                                                                    <img class='imgcus d-none' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <img class='imgcus' src='".$url4."' />
                                                                <div id='img'>
                                                                    <img class='imgcus d-none' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    } else {
                                                        echo "<div class='product-img' id='images-ur'>                                                            
                                                            <div id='imgur".$i."'>
                                                                <div id='img'>
                                                                    <img class='imgcus' id='imgcus-ur' src='".$url.$imgarray[$i]."' />
                                                                </div>
                                                            </div>
                                                            <button type='button' class='btn btn-xs btn-danger remove'>
                                                            <span class='mdi mdi-close-circle-outline'></span></button>
                                                            <p class='text-center'>".substr($name,0,10)."</p>
                                                        </div>";
                                                    }
                                                    
                                                }   
                                            }                           
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <?php 
                                            $decImg = json_decode($page->image_mobile_ur);
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='images-mobile-ur'>                                                                
                                                                <div id='imgmobile-ur".$i."'>
                                                                    <img class='imgcus' src='".$url2."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-mobile-ur' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='images-mobile-ur'>                                                                
                                                                <div id='imgmobile-ur".$i."'>
                                                                    <img class='imgcus' src='".$url3."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-mobile-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='images-mobile-ur'>                                                                
                                                                <div id='imgmobile-ur".$i."'>
                                                                    <img class='imgcus' src='".$url4."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-mobile-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }else {
                                                        echo "<div class='product-img' id='images-mobile-ur'>                                                                
                                                                <div id='imgmobile-ur".$i."'>
                                                                    <div id='img'>
                                                                        <img class='imgcus' id='imgcus-mobile-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }
                                                    
                                                }   
                                            }                                  
                                        ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">File(en)</label>
                                            <input id="inpfile_en" name="file_en" readonly class="form-control col-md-8" type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-fileen-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-fileen-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpfile_en&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="col-md-12">File(ur)</label>
                                            <input id="inpfile_ur" name="file_ur" readonly class="form-control col-md-8 type="text" value="" style="margin-left: 10px;">
                                            <button data-toggle="modal" data-target=".bd-file-ur-modal-lg" class="form-control btn btn-primary btn-sm col-md-3 btn" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                        </div>
                                        <div class="modal fade bd-file-ur-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding: 11px 25px !important;">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="padding: 0px 0px !important;">
                                                        <iframe  width="100%" style="height:80vh" frameborder="0"
                                                            src="{{ asset('public/filemanager/dialog.php?type=2&field_id=inpfile_ur&relative_url=1') }}">
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <?php 
                                            $decImg = json_decode($page->file_en);
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                $url5 = URL::to('/')."/public/source/video.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='file-en'>                                                                
                                                                <div id='fileen".$i."'>
                                                                    <img class='imgcus' src='".$url2."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='file-en'>                                                                
                                                                <div id='fileen".$i."'>
                                                                    <img class='imgcus' src='".$url3."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='file-en'>                                                                
                                                                <div id='fileen".$i."'>
                                                                    <img class='imgcus' src='".$url4."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "mp4"){
                                                        echo "<div class='product-img' id='file-en'>                                                                
                                                                <div id='fileen".$i."'>
                                                                    <img class='imgcus' src='".$url5."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }else {
                                                        echo "<div class='product-img' id='file-en'>                                                                
                                                                <div id='fileen".$i."'>
                                                                    <div id='img'>
                                                                        <img class='imgcus' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }
                                                    
                                                }   
                                            }                                  
                                        ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <?php 
                                            $decImg = json_decode($page->file_ur);
                                            $imgarray = $decImg;
                                            if(count($imgarray) > 0){
                                                $url = URL::to('/')."/public/source/";
                                                $url2 = URL::to('/')."/public/assets/images/pdficon.png";
                                                $url3 = URL::to('/')."/public/assets/images/excelicon.jpg";
                                                $url4 = URL::to('/')."/public/assets/images/docicon.png";
                                                $url5 = URL::to('/')."/public/source/video.png";
                                                for ($i=0; $i < count($imgarray) ; $i++) { 
                                                    $path = pathinfo($imgarray[$i]);
                                                    $name = $path['filename'];
                                                    $ext = $path['extension'];
                                                    if($ext == "pdf" || $ext == "PDF"){
                                                        echo "<div class='product-img' id='file-ur'>                                                                
                                                                <div id='fileur".$i."'>
                                                                    <img class='imgcus' src='".$url2."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "xls" || $ext == "XLS" || $ext == "XLSX" || $ext == "xlsx"){
                                                        echo "<div class='product-img' id='file-ur'>                                                                
                                                                <div id='fileur".$i."'>
                                                                    <img class='imgcus' src='".$url3."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "docx" || $ext == "DOCX"){
                                                        echo "<div class='product-img' id='file-ur'>                                                                
                                                                <div id='fileur".$i."'>
                                                                    <img class='imgcus' src='".$url4."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }elseif($ext == "mp4"){
                                                        echo "<div class='product-img' id='file-ur'>                                                                
                                                                <div id='fileur".$i."'>
                                                                    <img class='imgcus' src='".$url5."' />
                                                                    <div id='img'>
                                                                        <img class='imgcus d-none' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }else {
                                                        echo "<div class='product-img' id='file-ur'>                                                                
                                                                <div id='fileur".$i."'>
                                                                    <div id='img'>
                                                                        <img class='imgcus' id='imgcus-en' src='".$url.$imgarray[$i]."' />
                                                                    </div>
                                                                </div>
                                                                <button type='button' class='btn btn-xs btn-danger remove'>
                                                                <span class='mdi mdi-close-circle-outline'></span></button>
                                                                <p class='text-center'>".substr($name,0,10)."</p>
                                                            </div>";
                                                    }
                                                    
                                                }   
                                            }                                  
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>View(en)<span class="text-danger">*</span></label>
                                        <select name="view_en" class="form-control">
                                            <option value="0">Select</option>
                                            @foreach($views_en as $view)
                                                @if($page->view_en == $view->slug)
                                                    <option value="{{$view->slug}}" selected>{{$view->title}}</option>
                                                @else
                                                    <option value="{{$view->slug}}">{{$view->title}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>View(ur)</label>
                                        <select name="view_ur" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($views_en as $view)
                                                @if($page->view_ur == $view->slug)
                                                    <option value="{{$view->slug}}" selected>{{$view->title}}</option>
                                                @else
                                                    <option value="{{$view->slug}}">{{$view->title}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="hidden" class="form-control" id="parent" name="parent" value="{{ request()->id }}">
                                
                                    <div class="form-group col-md-3">
                                        <label>Position(en)</label>
                                        <select name="position_en" class="form-control">
                                            @if($page->position_en == NULL)
                                                <option value="" selected>Left</option>
                                                <option value="uk-flex-right@m">Right</option>
                                            @else
                                                <option value="">Left</option>
                                                <option value="uk-flex-right@m" selected>Right</option>
                                            @endif
                                        </select>
                                    </div>
                                    

                                    <div class="form-group col-md-3">
                                        <label>Grid Class(en)</label>
                                        <select name="grid_class_en" class="form-control">
                                            <option value="uk-width-1-1@m"<?php if($page->grid_class_en == "uk-width-1-1@m"){ echo "selected"; } ?>>uk-width-1-1@m</option>
                                            <option value="uk-width-1-2@m"<?php if($page->grid_class_en == "uk-width-1-2@m"){ echo "selected"; } ?>>uk-width-1-2@m</option>
                                            <option value="uk-width-1-3@m"<?php if($page->grid_class_en == "uk-width-1-3@m"){ echo "selected"; } ?>>uk-width-1-3@m</option>
                                            <option value="uk-width-1-4@m"<?php if($page->grid_class_en == "uk-width-1-4@m"){ echo "selected"; } ?>>uk-width-1-4@m</option>
                                            <option value="uk-width-1-5@m"<?php if($page->grid_class_en == "uk-width-1-5@m"){ echo "selected"; } ?>>uk-width-1-5@m</option>
                                            <option value="uk-width-1-6@m"<?php if($page->grid_class_en == "uk-width-1-6@m"){ echo "selected"; } ?>>uk-width-1-6@m</option>
                                            <option value="uk-width-2-3@m"<?php if($page->grid_class_en == "uk-width-2-3@m"){ echo "selected"; } ?>>uk-width-2-3@m</option>
                                            <option value="uk-width-2-5@m"<?php if($page->grid_class_en == "uk-width-2-5@m"){ echo "selected"; } ?>>uk-width-2-5@m</option>
                                            <option value="uk-width-3-4@m"<?php if($page->grid_class_en == "uk-width-3-4@m"){ echo "selected"; } ?>>uk-width-3-4@m</option>                                            
                                            <option value="uk-width-3-5@m"<?php if($page->grid_class_en == "uk-width-3-5@m"){ echo "selected"; } ?>>uk-width-3-5@m</option>
                                            <option value="uk-width-4-5@m"<?php if($page->grid_class_en == "uk-width-4-5@m"){ echo "selected"; } ?>>uk-width-4-5@m</option>
                                            <option value="uk-width-5-6@m"<?php if($page->grid_class_en == "uk-width-5-6@m"){ echo "selected"; } ?>>uk-width-5-6@m</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Position(ur)</label>
                                        <select name="position_ur" class="form-control">
                                            @if($page->position_ur == NULL)
                                                <option value="" selected>Left</option>
                                                <option value="uk-flex-right@m">Right</option>
                                            @else
                                                <option value="">Left</option>
                                                <option value="uk-flex-right@m" selected>Right</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Grid Class(ur)</label>
                                        <select name="grid_class_ur" class="form-control">
                                            <option value="uk-width-1-1@m"<?php if($page->grid_class_ur == "uk-width-1-1@m"){ echo "selected"; } ?>>uk-width-1-1@m</option>
                                            <option value="uk-width-1-2@m"<?php if($page->grid_class_ur == "uk-width-1-2@m"){ echo "selected"; } ?>>uk-width-1-2@m</option>
                                            <option value="uk-width-1-3@m"<?php if($page->grid_class_ur == "uk-width-1-3@m"){ echo "selected"; } ?>>uk-width-1-3@m</option>
                                            <option value="uk-width-1-4@m"<?php if($page->grid_class_ur == "uk-width-1-4@m"){ echo "selected"; } ?>>uk-width-1-4@m</option>
                                            <option value="uk-width-1-5@m"<?php if($page->grid_class_ur == "uk-width-1-5@m"){ echo "selected"; } ?>>uk-width-1-5@m</option>
                                            <option value="uk-width-1-6@m"<?php if($page->grid_class_ur == "uk-width-1-6@m"){ echo "selected"; } ?>>uk-width-1-6@m</option>
                                            <option value="uk-width-2-3@m"<?php if($page->grid_class_ur == "uk-width-2-3@m"){ echo "selected"; } ?>>uk-width-2-3@m</option>
                                            <option value="uk-width-2-5@m"<?php if($page->grid_class_ur == "uk-width-2-5@m"){ echo "selected"; } ?>>uk-width-2-5@m</option>
                                            <option value="uk-width-3-4@m"<?php if($page->grid_class_ur == "uk-width-3-4@m"){ echo "selected"; } ?>>uk-width-3-4@m</option>                                            
                                            <option value="uk-width-3-5@m"<?php if($page->grid_class_ur == "uk-width-3-5@m"){ echo "selected"; } ?>>uk-width-3-5@m</option>
                                            <option value="uk-width-4-5@m"<?php if($page->grid_class_ur == "uk-width-4-5@m"){ echo "selected"; } ?>>uk-width-4-5@m</option>
                                            <option value="uk-width-5-6@m"<?php if($page->grid_class_ur == "uk-width-5-6@m"){ echo "selected"; } ?>>uk-width-5-6@m</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Author</label>
                                        <input type="text" value="{{ $page->author }}" class="form-control" id="author" name="author" placeholder="Author">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Slug<span class="text-danger">*</span></label>
                                        <input type="text" value="{{ substr($page->slug,1) }}" class="form-control" id="slug" name="slug" placeholder="Slug">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Parent</label>
                                        <select name="parent" class="form-control">
                                            <option value="0">Select</option>
                                            @foreach($parents as $parent)
                                                @if($page->parent_id == $parent->id)
                                                    <option value="{{$parent->id}}" selected>{{$parent->title_en}}</option>
                                                @else
                                                    <option value="{{$parent->id}}">{{$parent->title_en}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            @if($page->visible == 'Y')
                                                <option value="Y" selected>Show</option>
                                                <option value="N">Hide</option>
                                            @else
                                                <option value="Y">Show</option>
                                                <option value="N" selected>Hide</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Show In Menu</label>
                                        <select name="show_in_menu" class="form-control">
                                            @if($page->show_in_menu == '1')
                                                <option value="1" selected>Show</option>
                                                <option value="0">Hide</option>
                                            @else
                                                <option value="1">Show</option>
                                                <option value="0" selected>Hide</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Sorting<span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" id="sorting" value="{{ $page->sorting }}" name="sorting" placeholder="Value">
                                    </div>
                                    <fieldset class="border p-2 mb-4 row" style="border: 2px solid #dfd9d9 !important;">
                                        <legend class="w-auto fs-5" style="font-size: 17px;">SEO</legend>
                                        <div class="form-group col-md-6">
                                            <label>Meta Title(en)</label>
                                            <input type="text" value="{{ $page->meta_title_en }}" class="form-control" id="meta_title_en" name="meta_title_en" placeholder="Meta Title English">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Title(ur)</label>
                                            <input type="text" value="{{ $page->meta_title_ur }}" class="form-control" id="meta_title_ur" name="meta_title_ur" placeholder="Meta Title Urdu">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Description(en)</label>
                                            <textarea class="form-control" id="meta_desc_en" name="meta_desc_en">{{ $page->meta_desc_en }}</textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Description(ur)</label>
                                            <textarea class="form-control" id="meta_desc_ur" name="meta_desc_ur">{{ $page->meta_desc_ur }}</textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Keywords(en)</label>
                                            <input type="text" value="{{ $page->meta_keywords_en }}" class="form-control" id="meta_keywords_en" name="meta_keywords_en" placeholder="Meta Keywords English">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Meta Keywords(ur)</label>
                                            <input type="text" value="{{ $page->meta_keywords_ur }}" class="form-control" id="meta_keywords_ur" name="meta_keywords_ur" placeholder="Meta Keywords Urdu">
                                        </div>
                                    </fieldset> 
                                    <div id="custom_modal" style="display: none;">
                                        <fieldset class="border p-2 mb-4 row" style="border: 2px solid #dfd9d9 !important;">
                                            <legend class="w-auto fs-5" style="font-size: 17px;">Add Custom Post</legend>
                                            <input type="hidden" name="custom_post_id" id="custom_post_id" />
                                            <form id="custom_post" enctype="multipart/form-data">
                                                <div class="form-group col-md-6">
                                                    <label for="name">Name: <span style="color: red"> * </span></label>
                                                    <input type="text" required class="form-control" id="name" name="name">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="url">Url:</label>
                                                    <input type="text" class="form-control" id="url" name="url">
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="name">Image:</label>
                                                    <div class="container">
                                                        <div class="row">
                                                        <input id="image" name="image" readonly class="form-control col-md-8" type="text" value="">
                                                            <button data-toggle="modal" data-target=".bd-image-modal-lg" class="form-control btn btn-primary btn-sm col-md-4" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                                        </div>
                                                    </div>
                                                    <div id="customImages"></div>
                                                </div>
                                                <div class="modal fade bd-image-modal-lg" id="custom_image-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                        <div class="modal-content">
                                                            <div class="modal-header" style="padding: 11px 25px !important;">
                                                                <button type="button" class="close" onclick="$('#custom_image-modal').modal('hide');" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="padding: 0px 0px !important;">
                                                                <iframe  width="100%" style="height:80vh" frameborder="0"
                                                                    src="{{ asset('public/filemanager/dialog.php?type=2&field_id=image&relative_url=1') }}">
                                                                </iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group col-md-3">
                                                    <label for="name">File:</label>
                                                    <div class="container">
                                                        <div class="row">
                                                            <input id="file" name="file" readonly class="form-control col-md-8" type="text" value="">
                                                            <button data-toggle="modal" data-target=".bd-file-modal-lg" class="form-control btn btn-primary btn-sm col-md-4" type="button" style="color: #fff;background-color: #5b6661 !important;border-color: #5b6661 !important;font-size: 12px !important;">Select</button>
                                                        </div>
                                                    </div>
                                                    <div id="customFile"></div>
                                                </div>
                                                <div class="modal fade bd-file-modal-lg" id="custom_file-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" style="margin: 30px auto;">
                                                        <div class="modal-content">
                                                            <div class="modal-header" style="padding: 11px 25px !important;">
                                                                <button type="button" class="close" onclick="$('#custom_file-modal').modal('hide');" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="padding: 0px 0px !important;">
                                                                <iframe  width="100%" style="height:80vh" frameborder="0"
                                                                    src="{{ asset('public/filemanager/dialog.php?type=2&field_id=file&relative_url=1') }}">
                                                                </iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Language :</label>
                                                    <select name="language" id="language" class="form-control col-md-12">
                                                        <option value="en" selected>English</option>
                                                        <option value="ur">Urdu</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="description">Description:</label>
                                                    <textarea id="tinyMceExample5" class="form-control" id="description" name="description"></textarea>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <button type="button" onclick="custom_post_add()" class="btn btn-primary mr-2">Save</button>
                                                    <button type="button" onClick="$('#custom_modal').hide();$('#add_custom_post_btn').show();$('#custom_post_id').val('');" class="btn btn-light">Cancel</button>
                                                </div>
                                            </form>
                                        </fieldset>
                                    </div> 
                                    <!-- <div class="col-md-12 text-center">
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('page.index') }}" class="btn btn-light">Cancel</a>
                                    </div> -->
                                    <div class="form-group">
                                        <div class="card">
                                            <div class="card-body">
                                            <h4 class="card-title">Custom Post List</h4>
                                            <div class="table-responsive">
                                                <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sr.</th>
                                                    <th>Custom Post Name</th>
                                                    <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="custom_post_table">
                                                    @for($i=0; $i < count($custom_posts); $i++)
                                                        <tr>
                                                            <td>{{ $custom_posts[$i]->id }}</td>
                                                            <td>{{ ucwords($custom_posts[$i]->name) }}</td>
                                                            <td><button class="btn btn-primary mr-1" id="btn{{ $custom_posts[$i]->id }}" onClick="customView({{ $custom_posts[$i]->id }})">Edit</button><button class="btn btn-danger" id="btn{{ $custom_posts[$i]->id }}" onClick="customDelete({{ $custom_posts[$i]->id }},this)">Delete</button></td>
                                                        </tr>
                                                    @endfor
                                                    @if(count($custom_posts) == 0)
                                                        <tr class="odd" id="no_record" style="background: rgba(238, 238, 238, 0.57);">
                                                            <td valign="top" colspan="4" class="dataTables_empty text-center">No data available in table</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                </table>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center" id="add_custom_post_btn">
                                        <button type="button" class="btn btn-info"  onClick="$('#custom_modal').show();$('#add_custom_post_btn').hide();clearInput();$('#custom_post_id').val('');" style="background-color : #4d83ff !important;border: 1px solid #4d83ff !important;">Add Custom Post</button>
                                        <button type="button" onclick="save()" class="btn btn-primary mr-2">Save</button>
                                        <a href="{{ route('page.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.partials.footer')            
        </div>
    </div>
    <script>
        $(".remove").click(function(){
            $(this).parent('.product-img').fadeOut('100', function(){
    			$(this).remove();
    		});
        });
        $('#slug').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9-]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
            event.preventDefault();
            return false;
            }
        });
        function save(){
            let x = 0;
            const imgCount = $.map($('#images-en > div'), div => div.id);
            while(x < imgCount.length) {
                $('#'+imgCount[x]).attr("id","imgen"+x)
                x++;
            }

            let k = 0;
            const imgCountur = $.map($('#images-ur > div'), div => div.id);
            while(k < imgCountur.length) {
                $('#'+imgCountur[k]).attr("id","imgur"+k)
                k++;
            }

            x = 0;
            const imgMobileCount = $.map($('#images-mobile-en > div'), div => div.id);
            while(x < imgMobileCount.length) {
                $('#'+imgMobileCount[x]).attr("id","imgmobile-en"+x)
                x++;
            }

            k = 0;
            const imgMobileCountur = $.map($('#images-mobile-ur > div'), div => div.id);
            while(k < imgMobileCountur.length) {
                $('#'+imgMobileCountur[k]).attr("id","imgmobile-ur"+k)
                k++;
            }

            k = 0;
            const imgFileCountur = $.map($('#file-ur > div'), div => div.id);
            while(k < imgFileCountur.length) {
                $('#'+imgFileCountur[k]).attr("id","fileur"+k)
                k++;
            }

            k = 0;
            const imgFileCounten = $.map($('#file-en > div'), div => div.id);
            while(k < imgFileCounten.length) {
                $('#'+imgFileCounten[k]).attr("id","fileen"+k)
                k++;
            }

            // Image En Start***********************************************************
            var imgAll_en = "";
            var imgcnt = $("#images-en #img img").length;
            for (let index = 0; index < imgcnt; index++) {
                var images = $('#imgen'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        imgAll_en += images[1].substr(indexImg+8);
                    } else {
                        imgAll_en += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        imgAll_en += images[0].substr(indexImg+8);
                    } else {
                        imgAll_en += ','+images[0].substr(indexImg+8);
                    }
                }
            }
            const previousImgEn = imgAll_en.split(',');
            selectedImgEn = document.getElementById('inpen').value;
            mImgEn = selectedImgEn.replaceAll('["', "");
            mImgEn = mImgEn.replaceAll('","', ",");
            mImgEn = mImgEn.replaceAll('"]', "");
            mImgEn = mImgEn.split(",");
            var imgAlleng = "";
            if(mImgEn != '' && previousImgEn != ''){
                imgAlleng = previousImgEn.concat(mImgEn);
            } else if(mImgEn != '' && previousImgEn == '') {
                imgAlleng = mImgEn;
            } else if(mImgEn == '' && previousImgEn != '') {
                imgAlleng = previousImgEn;
            }
            // Image En End*************************************************************
            
            // Image Ur Start***********************************************************
            var imgAll_ur = "";
            var imgcnt = $("#images-ur #img img").length;
            for (let index = 0; index < imgcnt; index++) {
                var images = $('#imgur'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        imgAll_ur += images[1].substr(indexImg+8);
                    } else {
                        imgAll_ur += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        imgAll_ur += images[0].substr(indexImg+8);
                    } else {
                        imgAll_ur += ','+images[0].substr(indexImg+8);
                    }
                }

            }
            const previousImgUr = imgAll_ur.split(',');
            selectedImgUr = document.getElementById('inpur').value;
            mImgUr = selectedImgUr.replaceAll('["', "");
            mImgUr = mImgUr.replaceAll('","', ",");
            mImgUr = mImgUr.replaceAll('"]', "");
            mImgUr = mImgUr.split(",");

            var imgAllUr = "";
            if(mImgUr != '' && previousImgUr != ''){
                imgAllUr = previousImgUr.concat(mImgUr);
            } else if(mImgUr != '' && previousImgUr == '') {
                imgAllUr = mImgUr;
            } else if(mImgUr == '' && previousImgUr != '') {
                imgAllUr = previousImgUr;
            }
            // Image Ur End***********************************************************

            // Image Mobile En Start***********************************************************
            var imgAllMob_en = "";
            var imgmobcnten = $("#images-mobile-en #img img").length;
            for (let index = 0; index < imgmobcnten; index++) {
                var images = $('#imgmobile-en'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        imgAllMob_en += images[1].substr(indexImg+8);
                    } else {
                        imgAllMob_en += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        imgAllMob_en += images[0].substr(indexImg+8);
                    } else {
                        imgAllMob_en += ','+images[0].substr(indexImg+8);
                    }
                }
            }
            const previousImgMobileEn = imgAllMob_en.split(',');
            selectedImgMobileEn = document.getElementById('inpmoben').value;
            mImgMobEn = selectedImgMobileEn.replaceAll('["', "");
            mImgMobEn = mImgMobEn.replaceAll('","', ",");
            mImgMobEn = mImgMobEn.replaceAll('"]', "");
            mImgMobEn = mImgMobEn.split(",");
            var imgAllMobeng = "";
            if(mImgMobEn != '' && previousImgMobileEn != ''){
                imgAllMobeng = previousImgMobileEn.concat(mImgMobEn);
            } else if(mImgMobEn != '' && previousImgMobileEn == '') {
                imgAllMobeng = mImgMobEn;
            } else if(mImgMobEn == '' && previousImgMobileEn != '') {
                imgAllMobeng = previousImgMobileEn;
            }
            // Image Mobile En End*************************************************************
            
            // Image Mobile Ur Start***********************************************************
            var imgAllMob_ur = "";
            var imgmobcntur = $("#images-mobile-ur #img img").length;
            for (let index = 0; index < imgmobcntur; index++) {
                var images = $('#imgmobile-ur'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        imgAllMob_ur += images[1].substr(indexImg+8);
                    } else {
                        imgAllMob_ur += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        imgAllMob_ur += images[0].substr(indexImg+8);
                    } else {
                        imgAllMob_ur += ','+images[0].substr(indexImg+8);
                    }
                }

            }
            const previousImgMobUr = imgAllMob_ur.split(',');
            selectedImgMobUr = document.getElementById('inpmobur').value;
            mImgMobUr = selectedImgMobUr.replaceAll('["', "");
            mImgMobUr = mImgMobUr.replaceAll('","', ",");
            mImgMobUr = mImgMobUr.replaceAll('"]', "");
            mImgMobUr = mImgMobUr.split(",");

            var imgAllMobUr = "";
            if(mImgMobUr != '' && previousImgMobUr != ''){
                imgAllMobUr = previousImgMobUr.concat(mImgMobUr);
            } else if(mImgMobUr != '' && previousImgMobUr == '') {
                imgAllMobUr = mImgMobUr;
            } else if(mImgMobUr == '' && previousImgMobUr != '') {
                imgAllMobUr = previousImgMobUr;
            }
            // Image Mobile Ur End***********************************************************

            // File Ur Start***********************************************************
             var File_All_ur = "";
            var filecntur = $("#file-ur #img img").length;
            for (let index = 0; index < filecntur; index++) {
                var images = $('#fileur'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        File_All_ur += images[1].substr(indexImg+8);
                    } else {
                        File_All_ur += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        File_All_ur += images[0].substr(indexImg+8);
                    } else {
                        File_All_ur += ','+images[0].substr(indexImg+8);
                    }
                }

            }
            const previousFileUr = File_All_ur.split(',');
            selectedFileUr = document.getElementById('inpfile_ur').value;
            mFileUr = selectedFileUr.replaceAll('["', "");
            mFileUr = mFileUr.replaceAll('","', ",");
            mFileUr = mFileUr.replaceAll('"]', "");
            mFileUr = mFileUr.split(",");

            var FileAllUr = "";
            if(mFileUr != '' && previousFileUr != ''){
                FileAllUr = previousFileUr.concat(mFileUr);
            } else if(mFileUr != '' && previousFileUr == '') {
                FileAllUr = mFileUr;
            } else if(mFileUr == '' && previousFileUr != '') {
                FileAllUr = previousFileUr;
            }
            // File Ur End***********************************************************

             // File en Start***********************************************************
             var file_All_en = "";
            var filecnten = $("#file-en #img img").length;
            for (let index = 0; index < filecnten; index++) {
                var images = $('#fileen'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        file_All_en += images[1].substr(indexImg+8);
                    } else {
                        file_All_en += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        file_All_en += images[0].substr(indexImg+8);
                    } else {
                        file_All_en += ','+images[0].substr(indexImg+8);
                    }
                }

            }
            const previousFileen = file_All_en.split(',');
            selectedFileEn = document.getElementById('inpfile_en').value;
            mFileEn = selectedFileEn.replaceAll('["', "");
            mFileEn = mFileEn.replaceAll('","', ",");
            mFileEn = mFileEn.replaceAll('"]', "");
            mFileEn = mFileEn.split(",");

            var FileAllEn = "";
            if(mFileEn != '' && previousFileen != ''){
                FileAllEn = previousFileen.concat(mFileEn);
            } else if(mFileEn != '' && previousFileen == '') {
                FileAllEn = mFileEn;
            } else if(mFileEn == '' && previousFileen != '') {
                FileAllEn = previousFileen;
            }
            // File en End***********************************************************

            var descen = tinymce.get("tinyMceExample2").getContent();
            var descur = tinymce.get("tinyMceExample").getContent();
            var short_descen = tinymce.get("tinyMceExample3").getContent();
            var short_descur = tinymce.get("tinyMceExample4").getContent();
            var formData  = new FormData(jQuery('#pageEdit')[0]);
            formData.append("descen", descen);
            formData.append("descur", descur);
            formData.append("short_descen", short_descen);
            formData.append("short_descur", short_descur);
            formData.append("imgen", imgAlleng);
            formData.append("imgur", imgAllUr);
            formData.append("imgmoben", imgAllMobeng);
            formData.append("imgmobur", imgAllMobUr);
            formData.append("fileur", FileAllUr);
            formData.append("fileen", FileAllEn);
            var id = document.getElementById("id").value;
            var page_id = document.getElementById("page_id").value;
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+"/page/"+id,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    // console.log(data);
                    if(data == 'success'){
                        swal({
                            title: "Success",
                            text: "Data updated successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            if(page_id){
                                window.location.href = "{{ route('page.index') }}?id="+page_id;
                            }else{
                                window.location.href = "{{ route('page.index') }}";
                            }
                        }); 
                    } else {
                        swal({
                            title: "Error",
                            text: data,
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }
        function getCustomPost() {
            var formData  = new FormData();
            formData.append("id", document.getElementById("id").value);
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+"/list-custom-post",
                data : formData,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                contentType: false,
                processData: false,
                success:function(data){
                    // console.log(data);
                    $("#custom_post_table").empty();
                    if(data.message.length == 0){
                        $("#custom_post_table").append("<tr class='odd' id='no_record' style='background: rgba(238, 238, 238, 0.57);'><td valign='top' colspan='4' class='dataTables_empty text-center'>No data available in table</td></tr>");
                    }
                    for (let i = 0; i < data.message.length; i++) {
                        $("#custom_post_table").append("<tr><td>"+data.message[i].id+"</td><td>"+data.message[i].name+"</td><td><button class='btn btn-primary mr-1' onClick='customView("+data.message[i].id+")' id='btn"+data.message[i].id+"'>Edit</button><button class='btn btn-danger' id='btn"+data.message[i].id+"' onClick='customDelete("+data.message[i].id+",this)'>Delete</button></td>");
                    } 
                }
            });
        }

        function basename(str, sep) {
            return str.substr(str.lastIndexOf(sep) + 1);
        }
        
        function strip_extension(str) {
            return str.substr(0,str.lastIndexOf('.'));
        }

        function customView(id){
            $("#custom_post_id").val(id);
            var formData  = new FormData();
            formData.append("id", id);
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+"/get-custom-post/"+id,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data:formData,
                contentType: false,
                processData: false,
                success:function(data){
                console.log(data);
                    if(data.code == 200){
                        $('#customImages').empty();
                        $('#customFile').empty();
                        tinymce.get("tinyMceExample5").setContent((data.message[0].description == null) ? "" : data.message[0].description);
                        $("#name").val(data.message[0].name);
                        $("#url").val(data.message[0].url);
                        $("#language").val(data.message[0].language);
                        decImg = JSON.parse(data.message[0].image);
                        imgarray = decImg;
                        if(imgarray.length > 0){
                            var getUrl = window.location;
                            var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                            url  = baseUrl+"/public/source/";
                            url2 = baseUrl+"/public/assets/images/pdficon.png";
                            url3 = baseUrl+"/public/assets/images/excelicon.jpg";
                            url4 = baseUrl+"/public/assets/images/docicon.png";
                            for (i=0; i < imgarray.length ; i++) {
                                var name = basename(imgarray[i],'/');
                                name = strip_extension(name,'/');
                                var ext = basename(imgarray[i],'.');
                                if(ext == "pdf" || ext == "PDF"){
                                    $("#customImages").append("<div class='product-img' id='image_custom'><div id='img_custom"+i+"'><img class='imgcus' src='"+url2+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+imgarray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                } else if(ext == "xls" || ext == "XLS" || ext == "XLSX" || ext == "xlsx"){
                                    $("#customImages").append("<div class='product-img' id='image_custom'><div id='img_custom"+i+"'><img class='imgcus' src='"+url3+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+imgarray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                } else if(ext == "docx" || ext == "DOCX"){
                                    $("#customImages").append("<div class='product-img' id='image_custom'><div id='img_custom"+i+"'><img class='imgcus' src='"+url4+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+imgarray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                } else {
                                    $("#customImages").append("<div class='product-img' id='image_custom'><div id='img_custom"+i+"'><img class='imgcus' src='"+url+imgarray[i]+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+imgarray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                }                                
                            }   
                            $(".remove").click(function(){
                                $(this).parent('.product-img').fadeOut('100', function(){
                                    $(this).remove();
                                });
                            });
                        }       
                        decfile = JSON.parse(data.message[0].file);
                        filearray = decfile;
                        if(filearray.length > 0){
                            var getUrl = window.location;
                            var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                            url  = baseUrl+"/public/source/";
                            url2 = baseUrl+"/public/assets/images/pdficon.png";
                            url3 = baseUrl+"/public/assets/images/excelicon.jpg";
                            url4 = baseUrl+"/public/assets/images/docicon.png";
                            for (i=0; i < filearray.length ; i++) {
                                var name = basename(filearray[i],'/');
                                name = strip_extension(name,'/');
                                var ext = basename(filearray[i],'.');
                                if(ext == "pdf" || ext == "PDF"){
                                    $("#customFile").append("<div class='product-img' id='file_custom'><div id='fil_cust"+i+"'><img class='imgcus' src='"+url2+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+filearray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                } else if(ext == "xls" || ext == "XLS" || ext == "XLSX" || ext == "xlsx"){
                                    $("#customFile").append("<div class='product-img' id='file_custom'><div id='fil_cust"+i+"'><img class='imgcus' src='"+url3+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+filearray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                } else if(ext == "docx" || ext == "DOCX"){
                                    $("#customFile").append("<div class='product-img' id='file_custom'><div id='fil_cust"+i+"'><img class='imgcus' src='"+url4+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+filearray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                } else {
                                    $("#customFile").append("<div class='product-img' id='file_custom'><div id='fil_cust"+i+"'><img class='imgcus' src='"+url+filearray[i]+"' /><div id='img'><img class='imgcus d-none' id='imgcus-ur' src='"+url+filearray[i]+"' /></div></div><button type='button' class='btn btn-xs btn-danger remove'><span class='mdi mdi-close-circle-outline'></span></button><p class='text-center'>"+name.substr(0,10)+"</p></div>");
                                }                                
                            }   
                            $(".remove").click(function(){
                                $(this).parent('.product-img').fadeOut('100', function(){
                                    $(this).remove();
                                });
                            });
                        }                  
                        $('#custom_modal').show();
                        $('#add_custom_post_btn').hide();
                    //     getCustomPost();
                    } else {
                        swal({
                            title: "Error",
                            text: "Error occurred",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }

        function customDelete(id,mId){
          swal({
            title: "Are you sure?",
            text: "You want to delete this custom post?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                var formData  = new FormData();
                formData.append("id", id);
                $.ajax({
                  type:'POST',
                  enctype: 'multipart/form-data',
                  url:'<?php echo url('/');?>'+"/delete-custom-post/"+id,
                  headers: {
                      'X-CSRF-Token': '{{ csrf_token() }}',
                  },
                  data:formData,
                  contentType: false,
                  processData: false,
                  success:function(data){
                    console.log(data);
                      if(data == 'success'){
                          swal({
                              title: "Success",
                              text: "Custom post deleted successfully.",
                              icon: "success",
                              buttons: "Ok",
                          })
                          .then((willDelete) => {
                            var p=mId.parentNode.parentNode;
                            p.parentNode.removeChild(p);
                            getCustomPost();
                          });
                      } else {
                          swal({
                              title: "Error",
                              text: "Error occurred",
                              icon: "error",
                              button: "Ok",
                          });
                      }
                  }
              });
            } else {
            //   console.log("nothing delete");
            }
          });
        }

        function custom_post_add(){
            let custom_post_id = $('#custom_post_id').val();
            let x = 0;
            const image_custom = $.map($('#image_custom > div'), div => div.id);
            while(x < image_custom.length) {
                $('#'+image_custom[x]).attr("id","img_custom"+x)
                x++;
            }

            let k = 0;
            const file_custom = $.map($('#file_custom > div'), div => div.id);
            while(k < file_custom.length) {
                $('#'+file_custom[k]).attr("id","fil_cust"+k)
                k++;
            }

            // Image Custom Start***********************************************************
            var img_custom = "";
            var imgcnt = $("#image_custom #img img").length;
            for (let index = 0; index < imgcnt; index++) {
                var images = $('#img_custom'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        img_custom += images[1].substr(indexImg+8);
                    } else {
                        img_custom += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        img_custom += images[0].substr(indexImg+8);
                    } else {
                        img_custom += ','+images[0].substr(indexImg+8);
                    }
                }
            }
            const previousImg = img_custom.split(',');
            selectedImgEn = document.getElementById('image').value;
            mImg = selectedImgEn.replaceAll('["', "");
            mImg = mImg.replaceAll('","', ",");
            mImg = mImg.replaceAll('"]', "");
            mImg = mImg.split(",");
            var imgs_custom = "";
            if(mImg != '' && previousImg != ''){
                imgs_custom = previousImg.concat(mImg);
            } else if(mImg != '' && previousImg == '') {
                imgs_custom = mImg;
            } else if(mImg == '' && previousImg != '') {
                imgs_custom = previousImg;
            }
            // File En End*************************************************************
            
            // File Ur Start***********************************************************
            var file_cust = "";
            var imgcnt = $("#file_custom #img img").length;
            for (let index = 0; index < imgcnt; index++) {
                var images = $('#fil_cust'+index).find('img').map(function() { return this.src; }).get();
                if(images.length == 2){
                    var indexImg = images[1].indexOf('/source');
                    if(index == 0) {
                        file_cust += images[1].substr(indexImg+8);
                    } else {
                        file_cust += ','+images[1].substr(indexImg+8);
                    }
                } else {
                    var indexImg = images[0].indexOf('/source');
                    if(index == 0) {
                        file_cust += images[0].substr(indexImg+8);
                    } else {
                        file_cust += ','+images[0].substr(indexImg+8);
                    }
                }

            }
            const previousFile = file_cust.split(',');
            selectedImgUr = document.getElementById('file').value;
            mImgUr = selectedImgUr.replaceAll('["', "");
            mImgUr = mImgUr.replaceAll('","', ",");
            mImgUr = mImgUr.replaceAll('"]', "");
            mImgUr = mImgUr.split(",");

            var file_customs = "";
            if(mImgUr != '' && previousFile != ''){
                file_customs = previousFile.concat(mImgUr);
            } else if(mImgUr != '' && previousFile == '') {
                file_customs = mImgUr;
            } else if(mImgUr == '' && previousFile != '') {
                file_customs = previousFile;
            }
            
            // File Ur End***********************************************************

            var desc = tinymce.get("tinyMceExample5").getContent();
            var name = $("#name").val();
            var url = $("#url").val();
            var language = $("#language").val();
            var formData  = new FormData(jQuery('#custom_post')[0]);
            formData.append("desc", desc);
            formData.append("image_custom", imgs_custom);
            formData.append("name", name);
            formData.append("url", url);
            formData.append("file_custom", file_customs);
            formData.append("languages", language);
            formData.append("id", document.getElementById("id").value);
            formData.append("type", "page");
            // var page_id = document.getElementById("page_id").value;
            let apiEndPoint = "";
            if(custom_post_id == ""){
                apiEndPoint = "/add-custom-post";
            } else {
                apiEndPoint = "/update-custom-post/"+custom_post_id;
                formData.append("custom_post_id", custom_post_id);
            }
            $.ajax({
                type:'POST',
                enctype: 'multipart/form-data',
                url:'<?php echo url('/');?>'+apiEndPoint,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                data: formData,
                contentType: false,
                processData: false,
                success:function(data){
                    // console.log(data);
                    if(data == 'success'){
                        getCustomPost();
                        swal({
                            title: "Success",
                            text: "Data updated successfully.",
                            icon: "success",
                            buttons: "Ok",
                        })
                        .then((willDelete) => {
                            clearInput();
                            
                            $('#custom_modal').hide();
                            $('#add_custom_post_btn').show();
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "Please fill required fields.",
                            icon: "error",
                            button: "Ok",
                        });
                    }
                }
            });
        }

        function clearInput(){
            tinymce.get("tinyMceExample5").setContent("");
            $("#name").val("");
            $("#url").val("");
            $("#image").val("");
            $("#file").val("");
            $('#customImages').empty();
            $('#customFile').empty();
        }
    </script>
@endsection
