@extends('admin.layouts.app')
@section('content')
    @include('admin.partials.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.partials.sidebar')
        <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 text-left">
                        <h4 class="card-title">Article List</h4>
                    </div>
                    <div class="col-md-10 text-right mb-3">
                        <a href="{{ route('article.create') }}?id={{ request()->id }}" class="btn btn-primary text-right">Create</a>
                    </div>
                </div>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table table-bordered data-table">
                        <thead>
                            <tr>
                                <th width="100px">No</th>
                                <th>Title</th>
                                <th width="80px">Sorting</th>
                                <th width="100px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        @include('admin.partials.footer')  
      </div>
    </div>
    <script type="text/javascript">
      setTimeout(() => {
        $(function () {
            var table = $('.data-table').DataTable({
                "order": [[ 0, "desc" ]],
                processing: true,
                serverSide: true,
                ajax: "{{ route('article.index') }}?id={{ request()->id }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title_en', name: 'title_en'},
                    { "data": "id", "name": "id",
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<button class='btn btn-primary' onclick='decrease("+oData.id+")'>-</button><span class='p-2' id='count"+oData.id+"'>"+oData.sorting+"</span><button class='btn btn-primary' onclick='increament("+oData.id+")'>+</button>");
                        }
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    
                ]
            });
            
        });
      }, 500);
    </script>
    <script>
       function decrease(val){
          if(parseInt(document.getElementById("count"+val).innerHTML) - 1 >= 0){
            $.ajax({
                  type:'POST',
                  enctype: 'multipart/form-data',
                  url:'<?php echo url('/');?>'+"/articledesc/"+val,
                  headers: {
                      'X-CSRF-Token': '{{ csrf_token() }}',
                  },
                  data: {
                      id:val,
                  },
                  contentType: false,
                  processData: false,
                  success:function(data){
                    document.getElementById("count"+val).innerHTML = parseInt(document.getElementById("count"+val).innerHTML) - 1;
                  }
              });
          }
        }
        function increament(val){
          if(parseInt(document.getElementById("count"+val).innerHTML) + 1 >= 0){
            
            $.ajax({
                  type:'POST',
                  enctype: 'multipart/form-data',
                  url:'<?php echo url('/');?>'+"/articleinc/"+val,
                  headers: {
                      'X-CSRF-Token': '{{ csrf_token() }}',
                  },
                  data: {
                      id:val,
                  },
                  contentType: false,
                  processData: false,
                  success:function(data){
                    document.getElementById("count"+val).innerHTML = parseInt(document.getElementById("count"+val).innerHTML) + 1;
                  }
              });
          }
        }
        function mdelete(id,mId){
          swal({
            title: "Are you sure?",
            text: "You want to delete this article?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                  type:'DELETE',
                  enctype: 'multipart/form-data',
                  url:'<?php echo url('/');?>'+"/article/"+id,
                  headers: {
                      'X-CSRF-Token': '{{ csrf_token() }}',
                  },
                  data: {
                      id:id
                  },
                  contentType: false,
                  processData: false,
                  success:function(data){
                      if(data == 'success'){
                          swal({
                              title: "Success",
                              text: "Article deleted successfully.",
                              icon: "success",
                              buttons: "Ok",
                          })
                          .then((willDelete) => {
                            var p=mId.parentNode.parentNode;
                            p.parentNode.removeChild(p);
                          });
                      } else {
                          swal({
                              title: "Error",
                              text: "Error occurred",
                              icon: "error",
                              button: "Ok",
                          });
                      }
                  }
              });
            } else {
              console.log("nothing delete");
            }
          });
        }
    </script>
@endsection
