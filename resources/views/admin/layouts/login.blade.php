<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('public/admin_dashboard/css/vertical-layout-light/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('public/admin_dashboard/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/admin_dashboard/css/vendors/css/vendor.bundle.base.css') }}">

    <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.ico') }}" />
        <style type="text/css">
        .btn-primary {
            color: #fff;
            background-color: #008444 !important;
            border-color: #008444 !important;
        }
        .form-control:focus{
            border-color: #008444 !important;
        }
        .form-check .form-check-label input[type="checkbox"]:checked + .input-helper:before {
            background: #008444 !important;
        }
        .form-check .form-check-label input[type="checkbox"] + .input-helper:before {
            border: solid #008444;
        }
        .auth .brand-logo img {
            width: 100px;
        }
    </style>
</head>
<body>
    @yield('content')
    <!-- plugins:js -->
    <script src="{{ asset('public/admin_dashboard/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('public/admin_dashboard/js/off-canvas.js') }}"></script>
    <script src="{{ asset('public/admin_dashboard/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('public/admin_dashboard/js/template.js') }}"></script>
    <script src="{{ asset('public/admin_dashboard/js/settings.js') }}"></script>
    <script src="{{ asset('public/admin_dashboard/js/todolist.js') }}"></script>
    <!-- endinject -->
</body>
</html>
