<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('public/admin_dashboard/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/admin_dashboard/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('public/admin_dashboard/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.ico') }}" />
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('public/admin_dashboard/css/vertical-layout-light/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/admin_dashboard/vendors/simplemde/simplemde.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- Date Picker Links -->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


    <!-- endinject -->
    <style type="text/css">
        .btn-blue:hover {
            color: #fff;
            text-decoration: none;
        }
        .btn-blue, .wizard > .actions a {
            color: #fff;
            background-color: #034ea2 !important;
            border-color: #034ea2 !important;
        }
        #librayr a{
            float:left;
            position:relative;
            border: 1px solid #e7e7e7;
        }

        /*==================== styles........ =========================*/
        .product-img{
            float: left;
            position: relative;
            border: 3px solid #e9e9e9;
            margin-right: 10px;
            margin-top: 5px;
        }

        .product-img .btn{
            position: absolute;
            right: 0;
            bottom: 0;
            padding: 3px;
        }
        .imgcus{
            width: 100px;
            height: 70px;
        }
        #librayr img{
            width: 100px;
        }

        #librayr input{
            position:absolute;
            right: 0;
            bottom: 0;
        }

        #librayr a:hover{
            border: 1px solid red;
        }

        .product-images img{
            width: 100px;
        }
        input {
            border: 1px solid #c1bdbd !important;
        }
        select {
            border: 1px solid #c1bdbd !important;
        }
        textarea {
            border: 1px solid #c1bdbd !important;
        }
        .sidebar .nav .nav-item.active > .nav-link i, .sidebar .nav .nav-item.active > .nav-link .menu-title, .sidebar .nav .nav-item.active > .nav-link .menu-arrow {
            color: #008444 !important;
            font-weight: 600;
        }
        .sidebar .nav:not(.sub-menu) > .nav-item:hover > .nav-link {
            background: transparent;
            color: #008444 !important;
        }
        .nav-tabs .nav-item .nav-link.active {
            border: 0;
            border-bottom: 3px solid #008444;
            color: #008444;
        }
        .btn-primary, .wizard > .actions a {
            color: #fff;
            background-color: #008444 !important;
            border-color: #008444 !important;
        }
        .text-primary{
            color: #008444 !important;
        }
        .pagination .page-item.active .page-link,
        .pagination .page-item:hover .page-link{
            background: #008444 !important;
            border-color: #008444 !important;
            color: #ffffff;
        }
        .navbar .navbar-brand-wrapper .navbar-brand-inner-wrapper .navbar-brand img {
            width: calc(257px - 200px) !important;
            height: auto !important;
        }



        /* Cards Style */

        .c-dashboardInfo {
        margin-bottom: 15px;
        }
        .c-dashboardInfo .wrap {
        background: #ffffff;
        box-shadow: 2px 10px 20px rgba(0, 0, 0, 0.1);
        border-radius: 7px;
        text-align: center;
        position: relative;
        overflow: hidden;
        padding: 40px 25px 20px;
        height: 100%;
        }
        .c-dashboardInfo__title,
        .c-dashboardInfo__subInfo {
        color: #6c6c6c;
        font-size: 1.0em;
        }
        .c-dashboardInfo span {
        display: block;
        }
        .c-dashboardInfo__count {
        font-weight: 600;
        font-size: 2.5em;
        line-height: 64px;
        color: #323c43;
        }
        .c-dashboardInfo .wrap:after {
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 10px;
        content: "";
        }

        .c-dashboardInfo:nth-child(1) .wrap:after {
        background: linear-gradient(82.59deg, #00c48c 0%, #00a173 100%);
        }
        .c-dashboardInfo:nth-child(2) .wrap:after {
        background: linear-gradient(81.67deg, #0084f4 0%, #1a4da2 100%);
        }
        .c-dashboardInfo:nth-child(3) .wrap:after {
        background: linear-gradient(69.83deg, #0084f4 0%, #00c48c 100%);
        }
        .c-dashboardInfo:nth-child(4) .wrap:after {
        background: linear-gradient(81.67deg, #ff647c 0%, #1f5dc5 100%);
        }
        .c-dashboardInfo:nth-child(5) .wrap:after {
            background: linear-gradient(81.67deg, #eeff64 0%, #1f5dc5 100%);
        }
        .c-dashboardInfo:nth-child(6) .wrap:after {
            background: linear-gradient(81.67deg, #3ad1e9 0%, #ff08d5 100%);
        }
        .c-dashboardInfo:nth-child(7) .wrap:after {
            background: linear-gradient(81.67deg, #555a5a 0%, #08ff5f 100%);
        }
        .c-dashboardInfo:nth-child(8) .wrap:after {
            background: linear-gradient(81.67deg, #38f9f9 0%, #bd3cff 100%);
        }
        .c-dashboardInfo__title svg {
        color: #d7d7d7;
        margin-left: 5px;
        }
        .MuiSvgIcon-root-19 {
        fill: currentColor;
        width: 1em;
        height: 1em;
        display: inline-block;
        font-size: 24px;
        transition: fill 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
        user-select: none;
        flex-shrink: 0;
        }
        /* End  */

    </style>
</head>
<body>     
<div class="container-scroller">   
    @yield('content')
</div>
  <!-- plugins:js -->
  <script src="{{ asset('public/admin_dashboard/vendors/js/vendor.bundle.base.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="{{ asset('public/admin_dashboard/vendors/chart.js/Chart.min.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/vendors/datatables.net/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('public/admin_dashboard/js/off-canvas.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/js/hoverable-collapse.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/js/template.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/js/settings.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/js/todolist.js') }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{ asset('public/admin_dashboard/js/dashboard.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/js/data-table.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/vendors/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/js/file-upload.js') }}"></script>
  <script src="{{ asset('public/admin_dashboard/vendors/tinymce/tinymce.min.js') }}"></script>
  <!-- <script src="{{ asset('public/admin_dashboard/vendors/quill/quill.min.js') }}"></script> -->
  <script src="{{ asset('public/admin_dashboard/js/editorDemo.js') }}"></script>
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('public/filemanager/fancybox/jquery.fancybox-1.3.4.css') }}" media="screen" />
  <script type="text/javascript" src="{{ asset('public/filemanager/fancybox/jquery.fancybox-1.3.4.pack.js') }}"></script> -->
  <!-- End custom js for this page-->
  <script>
        $('.file').on('change',function(){
        myFile = $(".file").val();
        console.log(myFile);
        var upld = myFile.split('.').pop();
        if(upld=='pdf' || upld=='PDF' || upld=='xls' || upld=='xlsx' || upld=='PNG' || upld=='png' || upld=='JPG' || upld=='jpg' || upld=='jpeg' || upld=='JPEG'){
        // alert("File uploaded is pdf")
        }else{
        alert("Only PDF, xlx, png, jpg, jpeg are allowed")
        $(".file").val("")
        }

        })
    </script>
</body>
</html>
