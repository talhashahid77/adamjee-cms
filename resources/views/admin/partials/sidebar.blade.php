<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
        <a class="nav-link" href="{{ url('/dashboard') }}">
            <i class="mdi mdi-home menu-icon"></i>
            <span class="menu-title">Dashboard</span>
        </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#ui-pages" aria-expanded="false" aria-controls="ui-pages">
            <i class="mdi mdi-account menu-icon"></i>
            <span class="menu-title">General Pages</span>
            <i class="menu-arrow"></i>
          </a>
          <div class="collapse" id="ui-pages">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="{{ url('/page') }}">Pages</a></li>
              <li class="nav-item"> <a class="nav-link" href="{{ url('/view') }}">Views</a></li>
              <li class="nav-item"> <a class="nav-link" href="{{ url('/menu') }}">Menu</a></li>
            </ul>
          </div>
        </li> 
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/user') }}">
            <i class="mdi mdi-account menu-icon"></i>
            <span class="menu-title">Users</span>
          </a>
        </li> 


        <li class="nav-item">
          <a class="nav-link" href="{{ url('/test') }}">
            <i class="mdi mdi-account menu-icon"></i>
            <span class="menu-title">Test</span>
          </a>
        </li> 
    </ul>
</nav>