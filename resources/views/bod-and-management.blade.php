{{-- @dd($innerPages); --}}
{{-- @dd($pagesdata); --}}

@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

<!-- Section Start -->
@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
	@php 
		$postImg =  getImageFile($device_type,$pagedata,"image","inner");
	@endphp
    <section class="HeaderInnerPage">
        <img src="{{ $postImg }}" />
        @include('partials.breadcrumb')
        <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
            <div class="uk-container containCustom">
                <h1>{{ $pagedata->title_en}}</h1>
                {!! $pagedata->short_desc_en !!}
            </div>
        </div>
    </section>
@endforeach
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="tabsSec">
			<!-- This is the nav containing the toggling elements -->
			<ul class="tabBtn" uk-switcher="connect: .TabBox">
				@foreach($innerPages as $innerPage)
			    	<li><a href="javascript:;">{{ $innerPage->title_en }}</a></li>
				@endforeach
			</ul>

			<!-- This is the container of the content items -->
			<ul class="uk-switcher TabBox">
				<!-- Tab li Start -->
				@foreach($innerPages as $innerPage)
			    <li>
			    	<ul uk-grid uk-height-match=".uk-card-body">
						@foreach( $innerPage->child as $child )
							@php 
								$postImg =  getImageFile($device_type,$child,"image","inner");
							@endphp
			    		<!-- Card Start -->
			    		<li class="uk-width-1-2@s uk-width-1-4@m">
		    				<a href="{{ url(session()->get('url').$child->path) }}" class="uk-card uk-card-default tabInner">
					            <div class="uk-card-media-top">
					                <img src="{{$postImg}}" alt="">
					            </div>
					            <div class="uk-card-body">
					                <h3>{{$child->title_en}}</h3>
					                {!! $child->sub_title_en !!}
					                {{-- <p>{{}}</p> --}}
					                <span>Read more <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></span>
					            </div>
			    			</a>
			    		</li>
			    		<!-- Card End -->
			    		@endforeach
			    	</ul>
			    </li>
			    @endforeach
			    <!-- Tab li End -->
			</ul>
		</div>
	</div>
</section>
<!-- Section End -->

<!-- footer start -->

@include('partials.footer')
@endif
<!-- footer end -->