<footer>
    <div class="uk-container containCustom">
        <div uk-grid>
            <div class="uk-width-1-5@m">
                <div class="ftrLinks">
                    <h6>About Us</h6>
                    <ul>
                        <li><a href="javascript:;">Company Introduction</a></li>
                        <li><a href="javascript:;">Investor Relations</a></li>
                        <li><a href="javascript:;">VMV</a></li>
                        <li><a href="javascript:;">Management and BODs</a></li>
                        <li><a href="javascript:;">Our Partners</a></li>
                        <li><a href="javascript:;">Our Culture and Ethos</a></li>
                        <li><a href="javascript:;">Awards and Achievements</a></li>
                        <li><a href="javascript:;">Training</a></li>
                        <li><a href="javascript:;">Social Impact</a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-width-1-5@m">
                <div class="ftrLinks">
                    <h6>Products/Plans</h6>
                    <ul>
                        <li><a href="javascript:;">Individual Life</a></li>
                        <li><a href="javascript:;">Bancassurance</a></li>
                        <li><a href="javascript:;">Online</a></li>
                        <li><a href="javascript:;">Corporate</a></li>
                        <li><a href="javascript:;">Calculators</a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-width-1-5@m">
                <div class="ftrLinks">
                    <h6>Existing Customers</h6>
                    <ul>
                        <li><a href="javascript:;">Policy Portal</a></li>
                        <li><a href="javascript:;">E services</a></li>
                        <li><a href="javascript:;">Policy Management</a></li>
                        <li><a href="javascript:;">Claims</a></li>
                        <li><a href="javascript:;">Orbis Rewards</a></li>
                        <li><a href="javascript:;">Complains and Queries</a></li>
                        <li><a href="javascript:;">Download App</a></li>
                        <li><a href="javascript:;">Testimonials</a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-width-1-5@m">
                <div class="ftrLinks">
                    <h6>About</h6>
                    <ul>
                        <li><a href="javascript:;">FAQs</a></li>
                        <li><a href="javascript:;">PSX</a></li>
                        <li><a href="javascript:;">News and eventd</a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-width-1-5@m">
                <div class="ftrLinks">
                    <h6>About</h6>
                    <div class="ftrAddress">
                        <p>Adamjee House, I.I. Chundrigar Road, Karachi - 74000</p>
                        <p><a href="tel:+9221111115433">111-11-LIFE(5433)</a></p>
                        <p><a href="mailto:info@adamjeelife.com">info@adamjeelife.com</a></p>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-1">
                <hr />
            </div>
            <div class="uk-width-1-4@m">
                <a href="javascript:;" class="ftrlogo">
                    <img src="{{asset('public/website/images/logo.svg')}}" alt="Adamjee Life" />
                </a>
            </div>
            <div class="uk-width-1-4@m">
                <a class="crfBtn" href="javascript:;">Complaint resolution forum</a>
            </div>
            <div class="uk-width-1-2@m">
                <div class="apiBtns">
                    <a href="javascript:;" target="_blank"><img src="{{asset('public/website/images/apstore.png')}}" /></a>
                    <a href="javascript:;" target="_blank"><img src="{{asset('public/website/images/gplay.png')}}" /></a>
                </div>
            </div>
            <div class="uk-width-1-4@m">
                <div class="copyRights">
                    <p>Copyright © AdamjeeLife. All rights reserved</p>
                </div>
            </div>
            <div class="uk-width-1-4@m">
                <a class="cocBtn" href="javascript:;">Company's own complain handling cell <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></a>
            </div>
            <div class="uk-width-1-2@m">
                <div class="SocialLinks">
                    <ul>
                        <li><a href="javascript:;"><span uk-icon="icon: facebook"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: instagram"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: twitter"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: youtube"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: linkedin"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-width-1-2@m"><p class="designDevelop">Designed &amp; Developed by <a target="_blank" href="https://www.convexinteractive.com/">Convex Interactive</a></p></div>
            <div class="uk-width-1-2@m">
                <ul class="ftrbtmLink">
                    <li><a href="javascript:;">Privacy policy</a></li>
                    <li><a href="javascript:;">Disclaimer</a></li>
                    <li>Last website update: 19/08/2022</li>
                </ul>
            </div>
        </div>
    </div>
</footer>
    </body>
</html>