<!DOCTYPE html>
<html>
<head>
    <title>Adamjee Life</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{asset('public/website/images/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('public/website/css/uikit.min.css')}}" />
    <link rel="stylesheet" href="{{asset('public/website/css/style.css')}}" />
    <link rel="stylesheet" href="{{asset('public/website/css/responsive-style.css')}}" />
    <script src="{{asset('public/website/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/website/js/uikit.min.js')}}"></script>
    <script src="{{asset('public/website/js/uikit-icons.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <meta charset="UTF-8">
    @if(isset($pagesdata))
    @foreach($pagesdata as $key => $pagedata)
    @if($pagedata->meta_title_en != "")
    <meta name="title" content="{{ $pagedata->meta_title_en }}">
    @endif
    @if($pagedata->meta_desc_en != "")
    <meta name="description" content="{{ $pagedata->meta_desc_en }}">
    @endif
    @if($pagedata->meta_keywords_en != "")
    <meta name="keywords" content="{{ $pagedata->meta_keywords_en }}">
    @endif
    @if($pagedata->author != "")
    <meta name="author" content="{{ $pagedata->author }}">
    @endif
    @endforeach
    @endif
</head>
<body>
<!-- Header Start -->
<header class="mainHeader">
     <!-- Top Bar Start -->
     <div class="topBar topBarsticky"  id="navbar" style="display:none">
        <div class="uk-container uk-container-expand">
             <a href="{{ url(session()->get('url').'/home') }}" class="logo">
                <img src="{{asset('public/website/images/whiteLogo.svg')}}" alt="Adamjee Life" />
            </a>
                <ul class="topRightMenu stickyTopBar">
                    <li>
                        <span>Your trusted <br/> insurance partner</span>
                    </li>
                    <li class="appointment">
                        <a href="javascript:;">Online appointment <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></a>
                    </li>
                    <li class="PhoneTopSticky">
                        <a href="tel:021-111-115-433 "><img src="{{asset('public/website/images/icons/phone.svg')}}" uk-svg /> 021-111-115-433 </a>
                    </li>
                </ul>
            <div class="topRight stickyTopBar">
                <ul class="topRightMenu">
                    <li class="PACRA">
                        <div>
                            <h6>A++</h6>
					        <span>PACRA Rating</span>  
                        </div>
                    </li>
                    <li class="WhitePACRA">
                        <div>
                            @if(isset($sliderInfo))
                                @php
                                    $sliderRespose = json_decode($sliderInfo->response);
                                @endphp
                                <h6>{{ $sliderRespose[0]->LIVES_PROTECTED }}</h6>
					            <span>Lives protected</span>   
                            @else 
                                <h6>0</h6>
                                <span>Lives protected</span> 
                            @endif  
                        </div>
                    </li>
                   
                </ul>
            </div>
        </div>
    </div>
    <!-- Top Bar End -->
    <!-- Top Bar Start -->
    <div class="topBar">
        <div class="uk-container uk-container-expand">
             <a href="{{ url(session()->get('url').'/home') }}" class="logo">
                <img src="{{asset('public/website/images/logo.svg')}}" alt="Adamjee Life" />
            </a>
            <div class="topRight">
                <ul class="topRightMenu">
                    <li>
                        <span>Talk to an expert </span><a href="tel:+9221111115433">021-111-115-433</a>
                    </li>
                    <li>
                        <a href="careers-content-critical.php">Careers</a>
                    </li>
                    <li>
                        <a href="pdf/Branches-contact-addresses.pdf" target="_blank">Branch locator <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></a>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="{{asset('public/website/images/world.svg')}}" uk-svg /> Eng <img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li class="uk-active"><a href="#">Eng</a></li>
                                <li><a href="#">اردو </a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="{{asset('public/website/images/login.svg')}}" uk-svg /> Login <img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                        <div uk-dropdown>
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="https://alpos.adamjeelife.com/Eservices" target="_blank">Conventional</a></li>
                                <li><a href="https://alpos.adamjeelife.com/tkfeservice" target="_blank">Takaful</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Top Bar End -->
    <!-- Menu Start -->
    <div class="MenuBar">
        <div class="uk-container uk-container-expand">
            <div class="mainMenu">
                <ul>
                    @if(isset($menus_parents))
                        @php
                            $menu_data = array();
                            $index = 0;
                        @endphp
                        @if(session()->get('url') == 'en')
                            @foreach($menus_parents as $menu_key => $menu_row)
                                @php 
                                    $menu_data[$menu_row->menu_parent_item_id][$index]['id'] = $menu_row->id;
                                    $menu_data[$menu_row->menu_parent_item_id][$index]['title_en'] = $menu_row->title_en;
                                    $menu_data[$menu_row->menu_parent_item_id][$index]['description_en'] = $menu_row->description_en;
                                    $menu_data[$menu_row->menu_parent_item_id][$index]['menu_parent_item_id'] = $menu_row->menu_parent_item_id;
                                    $menu_data[$menu_row->menu_parent_item_id][$index]['link_url_en'] = $menu_row->link_url_en;
                                    $menu_data[$menu_row->menu_parent_item_id][$index]['image_en'] = $menu_row->image_en;
                                    $menu_data[$menu_row->menu_parent_item_id][$index]['link_type_en'] = $menu_row->link_type_en;
                                    $index++; 
                                @endphp
                            @endforeach
                            @php
                                $index = 0;
                            @endphp
                            @foreach($menu_data[0] as $item_key => $item_rows)
                                @php 
                                if(session()->get('url') == "en"){
                                    $postImg = getMenuImages($device_type,$item_rows['image_en']);
                                } else { 
                                    $postImg = getMenuImages($device_type,$item_rows['image_ur']);
                                }
                                @endphp
                                <li class="dataGet">
                                    @if($item_rows['link_url_en'] == 'javascript:;')
                                        <a href="{{ $item_rows['link_url_en'] }}">{{ $item_rows['title_en'] }} @if(!empty(($menu_data[$item_rows['id']]))) <img src="{{asset('public/website/images/down.svg')}}" uk-svg/> @endif</a>
                                    @else
                                        <a href="{{ url(session()->get('url').$item_rows['link_url_en']) }}">{{ $item_rows['title_en'] }} @if(!empty(($menu_data[$item_rows['id']]))) <img src="{{asset('public/website/images/down.svg')}}" uk-svg/> @endif</a>
                                    @endif
                                    @if(!empty(($menu_data[$item_rows['id']])))
                                        <div class="wthFull" uk-dropdown="mode: hover; MenuBar: .topBar; boundary-align: true; offset: 0;delay-show: 400; delay-hide: 500;">
                                            <div class="uk-container containCustom">
                                                <div class="menuDropDownBox">
                                                    <div uk-grid>
                                                        <div class="uk-width-1-4@m">
                                                            <ul class="dropDownList">
                                                                @foreach($menu_data[$item_rows['id']] as $child_item_key => $child_item_rows)
                                                                    <?php $mCount = 0; ?>
                                                                    @if($child_item_rows['title_en'] != "")
                                                                        <li><a href="{{ url(session()->get('url').$child_item_rows['link_url_en']) }}">{{ $child_item_rows['title_en'] }}</a></li>
                                                                    @endif
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                        <div class="uk-width-3-4@m">
                                                            <div class="menuTbox">
                                                                <h3>{{ $item_rows['title_en'] }}</h3>
                                                                <img src="{{ $postImg }}" alt="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    
                                </li>
                                
                                @php 
                                    $index++;
                                @endphp
                            @endforeach
                        @endif
                    @endif
                    <!-- <li>
                        <a href="about-us.php">About us <img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                        <div class="wthFull" uk-dropdown="mode: hover; MenuBar: .topBar; boundary-align: true; offset: 0;delay-show: 400; delay-hide: 500;">
                            <div class="uk-container containCustom">
                                <div class="menuDropDownBox">
                                    <div uk-grid>
                                        <div class="uk-width-1-4@m">
                                            <ul class="dropDownList">
                                                <li><a href="bod-and-management.php">Management & BODs</a></li>
                                                <li><a href="investor-relations.php">Investor Relations</a></li>
                                                <li><a href="corporate-governance-content-critical.php">Corporate Governance</a></li>
                                                <li><a href="window-takaful-operations.php">Window Takaful Operations</a></li>
                                                <li><a href="awards-and-achievements.php">Awards & Achievements</a></li>
                                                <li><a href="csr.php">Sustainability</a></li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-3-4@m">
                                            <div class="menuTbox">
                                                <h3>About us</h3>
                                                <img src="{{asset('public/website/images/menu/6.png')}}" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="investor-relations.php">Investor Relations<img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                        <div class="wthFull" uk-dropdown="mode: hover; MenuBar: .topBar; boundary-align: true; offset: 0;delay-show: 400; delay-hide: 500;">
                            <div class="uk-container containCustom">
                                <div class="menuDropDownBox">
                                    <div uk-grid>
                                        <div class="uk-width-1-4@m">
                                            <ul class="dropDownList">
                                                <li><a href="notices-and-announcements.php">Notices & Announcements</a></li>
                                                <li><a href="financial-highlights.php">Financial Highlights</a></li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-3-4@m">
                                            <div class="menuTbox">
                                                <h3>Investor Relations</h3>
                                                <img src="{{asset('public/website/images/menu/1.png')}}" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;">Products<img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                        <div class="wthFull" uk-dropdown="mode: hover; MenuBar: .topBar; boundary-align: true; offset: 0;delay-show: 400; delay-hide: 500;">
                            <div class="uk-container containCustom">
                                <div class="menuDropDownBox">
                                    <div uk-grid>
                                        <div class="uk-width-1-4@m">
                                            <ul class="dropDownList">
                                                <li><a href="products-individual-life.php">Individual Life</a></li>
                                                <li><a href="products-bancassurance-conventional.php">Bancassurance</a></li>
                                                <li><a href="products-online-products.php">Online</a></li>
                                                <li><a href="product–corporate.php">Corporate</a></li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-3-4@m">
                                            <div class="menuTbox">
                                                <h3>Products</h3>
                                                <img src="{{asset('public/website/images/menu/2.png')}}" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;">Existing Customers<img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                        <div class="wthFull" uk-dropdown="mode: hover; MenuBar: .topBar; boundary-align: true; offset: 0;delay-show: 400; delay-hide: 500;">
                            <div class="uk-container containCustom">
                                <div class="menuDropDownBox">
                                    <div uk-grid>
                                        <div class="uk-width-1-4@m">
                                            <ul class="dropDownList">
                                                <li><a href="orbis.php">Orbis Rewards</a></li>
                                                <li><a href="#">Claims</a></li>
                                                <li><a href="#">Download App</a></li>
                                                <li><a href="#">Testimonials</a></li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-3-4@m">
                                            <div class="menuTbox">
                                                <h3>Existing Customers</h3>
                                                <img src="{{asset('public/website/images/menuimg.jpg')}}" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;">Advice and Guides<img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                        <div class="wthFull" uk-dropdown="mode: hover; MenuBar: .topBar; boundary-align: true; offset: 0;delay-show: 400; delay-hide: 500;">
                            <div class="uk-container containCustom">
                                <div class="menuDropDownBox">
                                    <div uk-grid>
                                        <div class="uk-width-1-4@m">
                                            <ul class="dropDownList">
                                                <li><a href="#">Process a claim</a></li>
                                                <li><a href="faqs.php">FAQs</a></li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-3-4@m">
                                            <div class="menuTbox">
                                                <h3>Advice and Guides</h3>
                                                <img src="{{asset('public/website/images/menu/3.png')}}" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="contact-us.php">Contact Us</a>
                    </li> -->
                </ul>
            </div>
            <div class="topRight">
                <a class="payOnline" href="pay-online.php">Pay online <img src="{{asset('public/website/images/right.svg')}}" uk-svg /></a>
                
                <a class="SearchBtn" href="#SearchModal" uk-search-icon uk-toggle></a>
            </div>
        </div>
    </div>
    <!-- Menu End -->
    <!-- Mobile Menu Sec Start -->
    <div class="MobileMenuBar">
        <div class="uk-container uk-container-expand">
            <a href="{{ url(session()->get('url').'/home') }}" class="logo">
                <img src="{{asset('public/website/images/logo.svg')}}" alt="Adamjee Life" />
            </a>
            <div class="grayPaca">
                @if(isset($sliderInfo))
                    @php
                        $sliderRespose = json_decode($sliderInfo->response);
                    @endphp
                    <p><strong>{{ $sliderRespose[0]->LIVES_PROTECTED }}</strong> lives protected</p>
                @else 
                    <p><strong>0</strong> lives protected</p>
                @endif
                <p><strong>A++</strong> PARCA Rating</p>
            </div>
            <div class="mobRght">
                <a href="javascript:;" class="recicon">
                    <span uk-icon="icon: receiver"></span>
                </a>
                <a href="#MobileMenu" uk-toggle>
                    <span uk-icon="icon: menu"></span>
                </a>
            </div>
            
        </div>
    </div>
    <!-- Mobile Menu Sec End -->
</header>
<!-- Mobile Menu Start -->
<div id="MobileMenu" class="MobileMenu" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar">
        <div class="headMobileMenu">
            <a href="{{ url(session()->get('url').'/home') }}" class="logo">
                <img src="{{asset('public/website/images/logo.svg')}}" alt="Adamjee Life" />
            </a>
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <a href="javascript:;" class="langBtn">اردو</a>
        </div>
        <div class="MobileMenuList">
            <ul class="uk-nav-default" uk-nav>
                <li class="uk-parent uk-active">
                    <a href="#">About Us <span uk-nav-parent-icon></span></a>
                    <ul class="uk-nav-sub">
                        <li class="uk-active"><a href="about-us.php">Overview</a></li>
                        <li><a href="bod-and-management.php">Management & BODs</a></li>
                        <li><a href="investor-relations.php">Investor Relations</a></li>
                        <li><a href="corporate-governance-content-critical.php">Corporate Governance</a></li>
                        <li><a href="window-takaful-operations.php">Window Takaful Operations</a></li>
                        <li><a href="awards-and-achievements.php">Awards & Achievements</a></li>
                        <li><a href="csr.php">Sustainability</a></li>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Investor Relations <span uk-nav-parent-icon></span></a>
                    <ul class="uk-nav-sub">
                        <li><a href="investor-relations.php">Overview</a></li>
                        <li><a href="notices-and-announcements.php">Notices & Announcements</a></li>
                        <li><a href="financial-highlights.php">Financial Highlights</a></li>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Products <span uk-nav-parent-icon></span></a>
                    <ul class="uk-nav-sub">
                        <li><a href="products-individual-life.php">Individual Life</a></li>
                        <li><a href="products-bancassurance-conventional.php">Bancassurance</a></li>
                        <li><a href="products-online-products.php">Online</a></li>
                        <li><a href="product–corporate.php">Corporate</a></li>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Existing Customers <span uk-nav-parent-icon></span></a>
                    <ul class="uk-nav-sub">
                        <li><a href="orbis.php">Orbis Rewards</a></li>
                        <li><a href="claims.php">Claims</a></li>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Advice and Guides <span uk-nav-parent-icon></span></a>
                    <ul class="uk-nav-sub">
                        <li><a href="free-advice-guides.php">Articles</a></li>
                        <li><a href="faqs.php">FAQs</a></li>
                    </ul>
                </li>
                <li><a href="contact-us.php">Contact Us</a></li>
                <li class=""><a href="careers-content-critical.php">Careers</a></li>
                <li class=""><a href="pdf/Branches-contact-addresses.pdf" target="_blank">Branch locator</a></li>
                <li class=""><a href="https://www.adamjeelife.com/appointment-booking/">Book an online appointment</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- Mobile Menu Start -->

<!-- Search Modal Start -->
<div id="SearchModal" class="uk-modal-full uk-modal" uk-modal>
    <div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle" uk-height-viewport>
        <button class="uk-modal-close-full" type="button" uk-close></button>
        <form class="uk-search uk-search-large">
            <input class="uk-search-input uk-text-center" type="search" placeholder="Search" autofocus>
        </form>
    </div>
</div>
<!-- Search Modal End -->

<script>

    window.onscroll = function() {
        myFunction()
    };

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;



    function myFunction() {
        if ($(this).scrollTop()>750) {
            navbar.classList.add("sticky")
            $('#navbar').show();
            // $('#navbar').css({'display':'block'});

        } else {
            navbar.classList.remove("sticky");
            $('#navbar').hide();
            
        }
    }    
</script>
