@if($errors->any())
    <div class="uk-alert-danger" uk-alert>
        <a class="uk-alert-close" uk-close></a>
            
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
            
        
    </div>
@endif


{{-- 
@if(Session::has('error'))
<div class="alert alert-info alert-dismissible fade show" role="alert">
    {{Session::get('error')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif --}}

{{-- 
@if ($notification = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $notification }}</strong>
</div>
@endif --}}


@if ($notification = Session::get('error'))
<div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    <p>{{ $notification }}</p>
</div>
@endif 



@if ($notification = Session::get('success'))
<div class="uk-alert-success" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    <p>{{ $notification }}</p>
</div>
@endif 


{{-- @if(Session::has('alert-success'))
    <div class="alert alert-pro alert-success">
        {{ Session::get('alert-success') }}
    </div>
@endif --}}

{{-- {{ $errors->first('name') }} --}}