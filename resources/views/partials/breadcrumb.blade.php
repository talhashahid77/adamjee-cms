<input type="hidden" value="{{ session()->get('url') }}" id="url_type"/>
<div class="breadcrumb">
    <ul class="uk-breadcrumb">
        @for($i = 2; $i <= count(Request::segments()); $i++)
            <!-- @if($i == 2)
                <li><a href="{{url('/')}}">@if(session()->get('url') == 'en') Home @else ہوم @endif</a></li>
            @endif -->
            <li>
                @if($i < count(Request::segments()))
                    <a href="{{ URL::to( implode( '/', array_slice(Request::segments(), 0 ,$i, true)))}}" id="lnk{{ $i }}">
                    <script>
                        var val = "{{ str_replace('en/','',implode( '/', array_slice(Request::segments(), 0 ,$i, true))) }}";
                        var explodevalue = val.split("/");
                        var linkarray = explodevalue.pop();
                        var url_type = $('#url_type').val();
                        $.ajax({
                            type: "POST",
                            url: "{{ URL::to('/api/listbread')}}",
                            data: {
                                    slug : linkarray,
                                },
                            success: function(response){
                                
                                if (response.status === true) {
                                    if(url_type == "en"){
                                        document.getElementById('lnk'+{{ $i }}).innerHTML = response.message.title_en;
                                    } else {
                                        document.getElementById('lnk'+{{ $i }}).innerHTML = response.message.title_ur;
                                    }
                                }
                            }
                        });
                    </script>
                    </a>
                @else
                    <span id="lnk{{ $i }}"></span>
                    <script>
                        var val = "{{ str_replace('en/','',implode( '/', array_slice(Request::segments(), 0 ,$i, true))) }}";
                        var explodevalue = val.split("/");
                        var linkarray = explodevalue.pop();
                        var url_type = $('#url_type').val();
                        $.ajax({
                            type: "POST",
                            url: "{{ URL::to('/api/listbread')}}",
                            data: {
                                    slug : linkarray,
                                },
                            success: function(response){
                                if (response.status === true) {
                                    if(url_type == "en"){
                                        document.getElementById('lnk'+{{ $i }}).innerHTML = response.message.title_en;
                                    } else {
                                        document.getElementById('lnk'+{{ $i }}).innerHTML = response.message.title_ur;
                                    }
                                }
                            }
                        });
                    </script>
                @endif
            </li>
        @endfor
    </ul>
</div> 