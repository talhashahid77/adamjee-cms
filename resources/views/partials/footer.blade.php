<footer>
    <link href="{{ asset('public/website/css/calendar.css')}}" rel="stylesheet" />
    <div class="uk-container containCustom">
        <div uk-grid>
                <div class="uk-width-1-5@m uk-float-left">
                    <ul class="topRightMenu">
                        <li>
                            <a href="javascript:;"><img src="{{asset('public/website/images/login.svg')}}" uk-svg /> Login <img src="{{asset('public/website/images/down.svg')}}" uk-svg /></a>
                            <div uk-dropdown>
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">Conventional</a></li>
                                    <li><a href="#">Takaful</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="javascript:;"> Careers </a>
                        </li>
                    </ul>    
                <!-- <ul class="ftrbtmLink1">
                    <li><a href="javascript:;">Login</a></li>
                    <li><a href="javascript:;">Careers</a></li>
                </ul> -->
                </div>
                <div class="uk-width-expand@s">
                    <a class="crfBtn" href="javascript:;">Complaint resolution forum</a>
                    <a  class="footBtn" href="contact-us.php"><u>Company's own complain handling cell</u> <img src="{{asset('public/website/images/right.svg')}}" uk-svg ></a>
                </div>
                <div class="uk-width-auto@s">
                    <div class="apiBtns">
                        <!-- <a href="javascript:;" target="_blank"><img src="{{asset('public/website/images/apstore.png')}}" /></a> -->
                        <a href="javascript:;" target="_blank"><img src="{{asset('public/website/images/gplay.png')}}" /></a>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <hr />
                </div>
                @if(isset($footer_menu))
                    @php
                        $menu_data = array();
                        $index = 0;
                    @endphp
                    @if(session()->get('url') == 'en')
                        @foreach($footer_menu as $menu_key => $menu_row)
                            @php 
                                $menu_data[$menu_row->menu_parent_item_id][$index]['id'] = $menu_row->id;
                                $menu_data[$menu_row->menu_parent_item_id][$index]['title_en'] = $menu_row->title_en;
                                $menu_data[$menu_row->menu_parent_item_id][$index]['description_en'] = $menu_row->description_en;
                                $menu_data[$menu_row->menu_parent_item_id][$index]['menu_parent_item_id'] = $menu_row->menu_parent_item_id;
                                $menu_data[$menu_row->menu_parent_item_id][$index]['link_url_en'] = $menu_row->link_url_en;
                                $menu_data[$menu_row->menu_parent_item_id][$index]['image_en'] = $menu_row->image_en;
                                $menu_data[$menu_row->menu_parent_item_id][$index]['link_type_en'] = $menu_row->link_type_en;
                                $index++; 
                            @endphp
                        @endforeach
                        @php
                            $index = 0;
                        @endphp
                        @foreach($menu_data[0] as $item_key => $item_rows)
                            @php 
                            if(session()->get('url') == "en"){
                                $postImg = getMenuImages($device_type,$item_rows['image_en']);
                            } else { 
                                $postImg = getMenuImages($device_type,$item_rows['image_ur']);
                            }
                            @endphp
                            <div class="uk-width-1-5@m uk-width-1-2">
                                <div class="ftrLinks">
                                    <h6>{{ $item_rows['title_en'] }}</h6>
                                    <ul>
                                        @if(!empty(($menu_data[$item_rows['id']])))
                                            @foreach($menu_data[$item_rows['id']] as $child_item_key => $child_item_rows)
                                                @if($child_item_rows['title_en'] != "")
                                                    @if($child_item_rows['link_url_en'] == 'javascript:;')
                                                        <li><a href="{{ $child_item_rows['link_url_en'] }}">{{ $child_item_rows['title_en'] }}</a></li>
                                                    @else
                                                        <li><a href="{{ url(session()->get('url').$child_item_rows['link_url_en']) }}">{{ $child_item_rows['title_en'] }}</a></li>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            @php 
                                $index++;
                            @endphp
                        @endforeach
                    @endif
                @endif
            <div class="uk-width-1-1">
                <hr />
            </div>
            <div class="uk-width-1-4@m uk-width-1-2@s">
                <a href="javascript:;" class="ftrlogo">
                    <img src="{{asset('public/website/images/logo.svg')}}" alt="Adamjee Life" />
                </a>
            </div>
            <div class="uk-width-1-4@m uk-visible@m">
                <!-- <a class="crfBtn" href="javascript:;">Complaint resolution forum</a> -->
            </div>
            <div class="uk-width-1-2@m uk-width-1-2@s">
                <div class="SocialLinks">
                    <ul>
                        <li><a href="javascript:;"><span uk-icon="icon: facebook"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: instagram"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: twitter"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: youtube"></span></a></li>
                        <li><a href="javascript:;"><span uk-icon="icon: linkedin"></span></a></li>
                    </ul>
                </div>
            </div>
            
            <div class="uk-width-1-2@m"><p class="designDevelop">Copyright© AdamjeeLife. All rights reserved | Powered by <a target="_blank" href="https://www.convexinteractive.com/">Convex Interactive</a></p></div>
            <div class="uk-width-1-2@m">
                <ul class="ftrbtmLink">
                    <li><a href="javascript:;">Privacy policy</a></li>
                    <li><a href="javascript:;">Disclaimer</a></li>
                    <li>Last website update: 19/08/2022</li>
                    <li><a class="scrollTop" href="#" uk-scroll><span uk-icon="icon: arrow-up"></span></a></li>
                </ul>
            </div>
        </div>

    </div>
</footer>

<div id="modal-book-appointment" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-body ">
            <div class="bookformMain NewsFilter">
                <ul class="uk-subnav uk-subnav-pill" uk-switcher="connect:.bookformTabs" hidden>
                    <li><a href="#">Item</a></li>
                    <li><a href="#">Item</a></li>
                    <li><a href="#">Item</a></li>
                    <li><a href="#">Item</a></li>
                </ul>
                <form class="bookingForm" action="javascript:void(0);" >
                    <ul class="uk-switcher uk-margin bookformTabs">
                        <li class="uk-grid uk-grid-medium" uk-grid>
                            <div class="uk-width-1-1@m">
                                <h2 class="">Please select a service and your preferred date</h2>
                            </div>
                            <div class="uk-width-1-1@s uk-width-1-1@m">
                                    <div class="NewsFilter1">
                                    <label class="uk-form-label">Select service</label>
                                        <select class="uk-select">
                                            <option>Virtual meeting</option>
                                            <option>Virtual meeting</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-width-1-1@s uk-width-1-1@m">
                                    <div class="NewsFilter1">
                                    <label class="uk-form-label">I'm available on or after</label>
                                        <select class="uk-select">
                                            
                                            <option>July 18, 2022</option>
                                            <option>July 18, 2022</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-width-1-2@s uk-width-1-1@m">
                                    <div class="days">
                                        <input id="Monday" type="checkbox" name="Monday" />
                                        <label for="Monday"> Mon </label>
                                        <input id="Tuesday" type="checkbox" name="Tuesday" />
                                        <label for="Tuesday"> Tue</label>
                                        <input id="Wednesday" type="checkbox" name="Wednesday" />
                                        <label for="Wednesday">Wed</label>
                                        <input id="Thrusday" type="checkbox" name="Thrusday" />
                                        <label for="Thrusday">Thru</label>
                                        <input id="Friday" type="checkbox" name="Friday" />
                                        <label for="Friday">Fri</label>
                                    </div>
                                </div>
                                
                                <div class="uk-width-1-1@s uk-width-1-2@m">
                                    <div class="NewsFilter1">
                                    <label class="uk-form-label">Start from</label>
                                        <select class="uk-select">
                                            
                                            <option>12:00 PM</option>
                                            <option>12:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-width-1-2@s uk-width-1-2@m">
                                    <div class="NewsFilter1">
                                    <label class="uk-form-label">Finish by</label>
                                        <select class="uk-select">
                                            
                                            <option>6:00 PM</option>
                                            <option>6:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-width-1-2@s uk-width-1-1@m">
                                    <a href="javascript:;" class="blueBtn" uk-switcher-item="next">Next <img src="images/right.svg" uk-svg="" hidden="true"></a>
                                </div>
                        </li>
                        <li class="uk-grid uk-grid-medium" uk-grid>
                            <div class="uk-width-1-1@m">
                                <h2 class="">Please select an available time slot</h2>
                            </div>
                            <div class="uk-width-1-1@s uk-width-3-5@m">
                                <div class="calendar-wrapper" id="calendar-wrapper"></div>
                                    <div id="editor"></div>
                            </div>
                            <div class="uk-width-1-1@s uk-width-1-3@m uk-margin-large-top">
                                <div class="Employee-Form_main">
                                    <h5><b>Thu, Jul 14</b></h5>
                                    <div class="uk-margin category" id="category">
                                        <!-- <h6>Select Job Category</h6> -->
                                        <label><input class="uk-radio" type="radio" name="radio2" checked> 12:00 pm</label>
                                        <br/>
                                        <label><input class="uk-radio" type="radio" name="radio2" disabled="disabled"> 12:00 pm</label>
                                        <br/>
                                        <label><input class="uk-radio" type="radio" name="radio2" disabled="disabled"> 12:00 pm</label>
                                    </div>    
                                </div>
                            </div>        
                            <div class="uk-width-1-1@s uk-width-1-1@m">
                                <a href="javascript:;" class="blueBtn " uk-switcher-item="previous"><img src="images/icons/chevron_left.svg" uk-svg="" hidden="true"> Back</a>
                                <a href="javascript:;" class="blueBtn " uk-switcher-item="next">Next <img src="images/right.svg" uk-svg="" hidden="true"></a>
                            </div>
                        
                        </li>

                        <li class="uk-grid uk-grid-medium JobForm " uk-grid>
                            <div class="uk-width-1-1@m">
                                <h2 class="">Please provide details</h2>
                            </div>
                            <div class="uk-width-1-1@s uk-width-1-1@m">
                                <div class="NewsFilter1">
                                    <label class="uk-form-label">Full Name</label>
                                    <input class="uk-input" type="text" placeholder="Enter your full name" required="">
                                </div>
                            </div>
                            <div class="uk-width-1-1@s uk-width-1-2@m">
                                <div class="NewsFilter1">
                                    <label class="uk-form-label">Phone</label>
                                    <input class="uk-input" type="text" placeholder="Enter phone number" required="">
                                </div>
                            </div>
                            <div class="uk-width-1-1@s uk-width-1-2@m">
                                <div class="NewsFilter1">
                                    <label class="uk-form-label">Email</label>
                                    <input class="uk-input" type="text" placeholder="Enter email" required="">
                                </div>
                            </div>

                            <div class="uk-width-1-1@s uk-width-1-2@m">
                                <div class="NewsFilter1">
                                    <label class="uk-form-label">Country</label>
                                        <select class="uk-select">
                                            
                                            <option>Pakistan</option>
                                            <option>12:00 PM</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="uk-width-1-1@s uk-width-1-2@m">
                                <div class="NewsFilter1">
                                    <label class="uk-form-label">City</label>
                                        <select class="uk-select">
                                            
                                            <option>Karachi</option>
                                            <option>12:00 PM</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="uk-width-1-1@s uk-width-1-2@m">
                            
                                    <a href="javascript:;" class="blueBtn " uk-switcher-item="previous"><img src="images/icons/chevron_left.svg" uk-svg="" hidden="true"> Back</a>
                                    <a href="javascript:;" type="submit" class="blueBtn Submit-reset-btn " uk-switcher-item="next" >Submit <img src="images/right.svg" uk-svg="" hidden="true"></a>            
                            
                            </div>    
                        </li>

                        <li class="uk-grid" uk-grid>
                            <div class="uk-width-1-1@m">
                                <h2 class="">Thank you! Your booking is complete. </h2>
                                    <p>An email with your booking details have been sent to you.</p>
                            </div>
                            
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        
    </div>
</div>
<script src="{{ asset('public/website/js/calendar.js')}}"></script>
<script>
    $(document).ready(function(){
    $(".Submit-reset-btn").click(function(){
    	$(".bookingForm").trigger("reset");
    }
    )}); 
</script>
    </body>
</html>
)