@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","inner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->title_en}}</h1>
                    {!! $pagedata->description_en !!}
                </div>
            </div>
        </section>
    @endforeach

    <section class="SecWrap SecTopSpace">
        <div class="uk-container containCustom">
            <div class="NewsFilter">
                <form>
                    <div class="uk-form-controls GuideSearchInput">
                        <input class="uk-input" id="email" type="text" placeholder="What's your email">
                    </div>
                    <div class="uk-form-controls">
                    <select class="uk-select">
                        @foreach($innerPages as $key => $innerPage)
                            <option value="{{ substr($innerPage->slug,1) }}">{{ $innerPage->title_en  }}</option>
                        @endforeach
                    </select>
                    </div>
                    <button class="blueBtn">Filter</button>
                </form>
            </div>
            <div class="NewsSec">
                <ul class="uk-grid-medium" uk-grid uk-height-match=".uk-card-body">
                    @foreach($innerPages as $key => $innerPage)
                        @foreach($innerPage->child as $innerChild)
                            @php 
                                $postImg =  getImageFile($device_type,$innerChild,"image","banner");
                            @endphp
                            <li class="uk-width-1-3@m uk-width-1-2@s">
                                <a href="{{ url(session()->get('url').$innerChild->path) }}" class="uk-card uk-card-default newsCard">
                                    <div class="uk-card-media-top">
                                        <img src="{{ $postImg }}" alt="">
                                    </div>
                                    <div class="uk-card-body">
                                        <div class="badgesBar">
                                                <div class="badgeBox">@if(session()->get('url') == 'en')  {{ $innerPage->title_en }} @else {{ $innerPage->title_ur }} @endif</div>	
                                            </div>
                                        <h3>@if(session()->get('url') == 'en')  {{ $innerChild->title_en }} @else {{ $innerChild->title_ur }} @endif</h3>
                                        @if(session()->get('url') == 'en') {!! $innerChild->short_desc_en !!} @elseif (session()->get('url') == 'ur') {!! $innerChild->short_desc_ur !!} @endif
                                        <span class="blueBtn">Read more <img src="{{asset('public/website/images/right.svg') }}" uk-svg /></span>
                                        
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    @include('partials.footer')
@endif  