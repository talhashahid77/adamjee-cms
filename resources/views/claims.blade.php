<style>
    label.error {
    color: red !important;
}
</style>
{{-- @dd($postsdata); --}}
{{-- @dd($pagesdata); --}}

{{-- @dd($innerPages); --}}


@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])


@foreach($pagesdata as $key => $pagedata)
    @php 
        $postImg =  getImageFile($device_type,$pagedata,"image","inner");
    @endphp
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="{{ $postImg }}" />
		@include('partials.breadcrumb')
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>{{ $pagedata->title_en}}</h1>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur</p> -->
		</div>
	</div>
</section>
@endforeach
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
        <div class="uk-grid uk-grid-small" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="contactRightContent JobForm NewsFilter claimForm">
                    <h3>Process your claim online</h3>
                    @include('partials.alart')	
                    
                    <form  id="myForm" class="uk-grid-small" uk-grid action="{{ route("contact-claim") }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <div class="NewsFilter1">
                            <label class="uk-form-label">Claim type</label>
                                <select name="claim_type" class="uk-select" required>
                                    <option value="">Select Claim Type</option>
                                    <option value="Banca">Banca</option>
                                    <option value="Banca">Banca</option>
                                </select>
                            </div>
                        </div>				    
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Your full name</label>
                            <input class="uk-input" type="text" placeholder="Enter your full name"  name="name" required>
                        </div>				    
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Policy Number</label>
                            <input class="uk-input"   type="number" placeholder="Your policy number"  name="policy_number" required >
                        </div>
                       
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">CNIC Number</label>
                            <input class="uk-input"  type="number" placeholder="Enter your CNIC Number" minlength="13" maxlength="13"  name="cnic" required>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Your Contact number</label>
                            <input class="uk-input"   type="number" placeholder="+92-xx-xxxxxxxx" minlength="13" maxlength="13"  name="contact_number"required>
                        </div>

                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Your email</label>
                            <input class="uk-input"    type="email" placeholder="yourname@example.com"  name="email"required >
                        </div>
                       
                        
                        <div class="uk-width-1-2@s uk-width-1-1@m">
                            <label class="uk-form-label">Description</label>
                            <textarea  rows="7"     class="uk-textarea"  name="summary" placeholder="Give a brief description" required ></textarea>
                        </div>

                        <div class="uk-width-1-2@s uk-width-1-1@m">
                            <div class="uk-upload-box">
                                <div id="error-alert" class="uk-alert-danger uk-margin-top uk-hidden" uk-alert>
                                    <p id="error-messages"></p>
                                </div>
                                <div class="drop__zone custom uk-placeholder uk-text-center">
                                    <span uk-icon="icon: cloud-upload"></span>
                                    <span class="uk-text-middle uk-margin-small-left">Drop file here or</span>
                                    <br/>
                                    <div class="selectForm" uk-form-custom c>
                                        <input id="resetFile" name="file[]" type="file" accept="image/png, image/jpeg, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple >
                                        <span class="uk-link">Select Files</span>
                                    </div>
                                    <ul id="preview" class="uk-list uk-grid-match uk-child-width-1-5@m uk-text-center uk-grid-small" uk-grid uk-scrollspy="cls: uk-animation-scale-up; target: .list-item; delay: 80"></ul>
                                </div>
                            </div>
                            <span>Supported file formats: PDF, Doc, Docx, Jpg</span>
                            <p style="color:red; display:none" id="fileError">Kindly select the file</p>
                        </div>    

                        <div class="uk-width-1-1">
                            <button id="submit" type="submit" class="blueBtn uk-margin-remove-top">Submit<img src="{{asset('public/website/images/right.svg')}}" uk-svg /></button>
                        </div>
                                            

                    </form>
                </div>
            </div>
            <div class="uk-width-1-2@m">
            <div class="detailsPage">


                
            @foreach($innerPages as $key => $innerPage)
                @php 
                    $postImg =  getImageFile($device_type,$innerPage,"image","banner");
                @endphp   
                @if($key == 0)
            <h1>{{ $innerPage->title_en }}</h1>
                @endif

			<div class="claimFaqs">
            <ul uk-accordion class="uk-accordion">
                @foreach($innerPage->article as $innerkey => $innerArticle)
                @if($key == 0)
                <li >    
                    <a class="uk-accordion-title" href="#"> {{ $innerArticle->title_en }}</a>
                    <div class="uk-accordion-content" aria-hidden="false">
                        {!! $innerArticle->description_en !!}
                    </div>
                </li>
                @endif
                @endforeach

            </ul>
            </div>
            @endforeach
			
		</div>
            </div>
            
        </div>
    </div>
</section>
<!-- Section End -->




@foreach($innerPages as $key => $innerPage)
@php 
    $postImg =  getImageFile($device_type,$innerPage,"image","inner");
@endphp
@if($key == 1)

<section class="SecWrap SecTopSpace uk-padding-remove-top">
	<div class="uk-container containCustom">
        <div class="uk-grid uk-grid-small" uk-grid>
            <div class="uk-width-1-1@m">
                


                <div class="contactRightContent JobForm NewsFilter claimForm">
                    <div class="cbox">
                        <h2><b>{{ $innerPage->title_en }}</b></h2>
                            @foreach($innerPage->article as $innerkey => $innerArticle)
                                @if($innerkey == 0)
                        <h4> <b>{{  $innerArticle->title_en }}</b></h4>
                        
                            @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                            @php 
                                $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                            @endphp  

                        <div class="addressicons addressiconsleft">
                            <img src="{{ $postImg }}" uk-svg /><a href="javascript:;">{!! $innerCustompost->description !!}</a>
                        </div>
                                @endforeach

                                @endif
                        @endforeach



                    @foreach($innerPage->article as $innerkey => $innerArticle)
                        @if($innerkey == 1)

                        <h4> <b>{{  $innerArticle->title_en }}</b></h4>

                        @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                            @php 
                                $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                            @endphp  

                        <div class="addressicons">
                            <span><a href="{{ $innerCustompost->url }}">{!! $innerCustompost->description !!}</a></span> <br/>
                            {{-- <span><a href="mailto:help_claims@adamjeelife.com">help_claims@adamjeelife.com</a></span> --}}
                        </div>
                            @endforeach
                        @endif
                    @endforeach
                    </div>



                    
                    @foreach($innerPage->article as $innerkey => $innerArticle)
                            @if($innerkey == 2)
                        <div class="cbox uk-margin-medium-top">
                            <h4> <b>{{  $innerArticle->title_en }}</b></h4>
                        
                        @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                            @php 
                                $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                            @endphp  

                        <div class="addressicons addressiconsleft">
                            <img src="{{ $postImg }}" uk-svg /><a href="{{ $innerCustompost->url }}">{!! $innerCustompost->description !!}</a>
                        </div>
                        @endforeach
                       @endif 
                    @endforeach



                        @foreach($innerPage->article as $innerkey => $innerArticle)
                            @if($innerkey == 3)

                        <h4> <b>{{  $innerArticle->title_en }}</b></h4>


                        @foreach($innerArticle->custompost as $innerkey => $innerCustompost)
                            @php 
                                $postImg =  getImageFile($device_type,$innerCustompost,"custom","banner");
                            @endphp 

                        <div class="addressicons">
                            <span>{!! $innerCustompost->description !!}</span>
                        
                        </div>
                        @endforeach

                        @endif
                        @endforeach
                    </div>
                </div>
            </div>

          
        </div>
    </div>
</section>
@endif
@endforeach

<!-- Section End -->





<!-- thank you  start -->
<div id="claim-process" class="uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

        <button class="uk-modal-close-default" type="button" uk-close></button>
		{{-- <span class="badge badge-success" id="modalMessage"></span> --}}
		<h4>Your claim has been processed</h4>
        <p>Note: The claim department have reserves the right to call for further requirements if deemed necessary.</p>
        <br/>
        <p>Your tracking number is: <a href="#"> HJSDGJHDSKJDH </a></p>
    </div>
</div>
<!-- thank you end -->


<script>
    const UPLOAD = {
    BASE_PATH: window.location.origin,
    ACCEPTED_DOC_MIMES: [
        "image/png",
        "image/jpeg",
        "application/pdf",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/msword"
    ],
    DOC_MIMES: [
        "application/pdf",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/msword"
    ],
    FileInputs: [...document.querySelectorAll("input[type='file']")],
    Init: () => {
        UPLOAD.AddEventListeners();
    },
    AddEventListeners: () => {
        UPLOAD.FileInputs.map((fileInput) => {
            fileInput.addEventListener("change", UPLOAD.HandleFileChange);
        });
        UPLOAD.FileInputs.map((fileInput) => {
            fileInput.addEventListener("blur", (e) => {
                // console.log(e.target);
            });
        });
        UPLOAD.FileInputs.map((fileInput) => {
            const box = fileInput.closest(".uk-placeholder");
            box.addEventListener(
                "dragover",
                (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    box.classList.add("uk-box-shadow-medium");
                },
                false
            );
        });
        UPLOAD.FileInputs.map((fileInput) => {
            const box = fileInput.closest(".uk-placeholder");
            box.addEventListener(
                "dragleave",
                (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    box.classList.remove("uk-box-shadow-medium");
                },
                false
            );
        });
        UPLOAD.FileInputs.map((fileInput) => {
            const box = fileInput.closest(".uk-placeholder");
            box.addEventListener(
                "drop",
                (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    box.classList.remove("uk-box-shadow-medium");
                    const dropZone = e.target.closest(".drop__zone");
                    const componentContainer = dropZone.closest(".uk-upload-box");
                    let preview, alertBox, alertMsg;
                    preview = dropZone.querySelector(`#preview`);
                    alertBox = componentContainer.querySelector(`#error-alert`);
                    alertMsg = componentContainer.querySelector(`#error-messages`);
                    let files;
                    let isMultiple = false;
                    if (fileInput.hasAttribute("multiple")) {
                        isMultiple = true;
                    }
                    if (!e.dataTransfer.files.length) return;
                    /** If items dropped are more than one file and the input doesn't accept multiple, do not accept the files. Clear preview field, show notification and add shake animatin to dropzone */
                    if (isMultiple === false && e.dataTransfer.files.length > 1) {
                        UPLOAD.RemoveChild(preview);
                        UPLOAD.AddShakeAnimation(dropZone);
                        UIkit.notification({
                            message: "Cannot accept multiple files when expecting a single file!",
                            status: "danger",
                            pos: "center",
                            timeout: 4000
                        });
                        return;
                    }
                    files = e.dataTransfer.files;
                    fileInput.files = files;
                    const options = {
                        isMultiple,
                        files,
                        preview,
                        alertBox,
                        alertMsg,
                        fileInput,
                        dropZone
                    };
                    if (isMultiple) {
                        UPLOAD.PreviewMultipleDocs(options);
                    } else {
                        UPLOAD.PreviewSingleDoc(options);
                    }
                },
                false
            );
        });
    },
    Reset: (e) => {
        if (e.target.files.length == 0) {
            const alertWrapper = e.target.closest("#error-alert");
            let alertMessage = alertWrapper.querySelector("#error-messages");
            alertWrapper.classList.add("uk-hidden");
            alertMessage.textContent = "";
            input.value = "";
            return;
        }
    },
    AddShakeAnimation: (parent) => {
        let timeout;
        clearTimeout(timeout);
        parent.classList.add("uk-animation", "uk-animation-shake");
        timeout = setTimeout(() => {
            parent.classList.remove("uk-animation", "uk-animation-shake");
        }, 1000);
    },
    HandleFileChange: (e) => {
        let files;
        let isMultiple = false;
        if (e.target.hasAttribute("multiple")) {
            isMultiple = true;
        }
        const dropZone = e.target.closest(".drop__zone");
        const componentContainer = dropZone.closest(".uk-upload-box");
        const documentCategory = dropZone.dataset.documentCategory;
        let preview, alertBox, alertMsg;
        preview = dropZone.querySelector(`#preview`);
        alertBox = componentContainer.querySelector(`#error-alert`);
        alertMsg = componentContainer.querySelector(`#error-messages`);
        /** Remove existing files and preview files if files lenght is 0 */
        if (!e.target.files.length) {
            UPLOAD.RemoveChild(preview);
            return;
        }
        files = e.target.files;
        const options = {
            files,
            fileInput: e.target,
            isMultiple,
            preview,
            alertBox,
            alertMsg,
            dropZone
        };
        if (isMultiple) {
            UPLOAD.PreviewMultipleDocs(options);
        } else {
            UPLOAD.PreviewSingleDoc(options);
        }
    },
    RemoveChild: (preview) => {
        while (preview.firstChild) {
            preview.removeChild(preview.firstChild);
        }
    },
    ImgPreviewLi: (readerResult, filename) => {
        const li = document.createElement("li");
        const div = document.createElement("div");
        const img = document.createElement("img");
        const span = document.createElement("span");
        li.className = "list-item uk-margin-medium-top";
        // div.className = "uk-cover-container";
        // img.setAttribute("id", "img-preview-responsive");
        // img.setAttribute("src", "images/icons/download.svg");
        // img.setAttribute("data-name", filename);
        // img.setAttribute("alt", "file-image-preview");
        span.className = "uk-text-meta uk-text-break file-upload-name";
        span.textContent = filename;
        // div.append(img);
        li.append(div, span);
        return li;
    },
    ValidateFileType: ({
        fileType,
        fileInput,
        alertBox,
        alertMsg,
        preview,
        dropZone
    }) => {
        if (!UPLOAD.ACCEPTED_DOC_MIMES.includes(fileType)) {
            alertMsg.textContent =
                "Sorry, one or more of your file type is not allowed.";
            alertMsg.classList.add("uk-text-danger");
            alertBox.classList.remove("uk-hidden");
            fileInput.value = "";
            UPLOAD.AddShakeAnimation(dropZone);
            UPLOAD.RemoveChild(preview);
            return false;
        }
        return true;
    },
    ValidateFileSize: ({
        fileInput,
        size,
        alertBox,
        alertMsg,
        preview,
        dropZone
    }) => {
        if (size > 2000000) {
            alertMsg.textContent =
                "Sorry, one or more of your files has exceeded the file size limit of 2MB.";
            alertMsg.classList.add("uk-text-danger");
            alertBox.classList.remove("uk-hidden");
            fileInput.value = "";
            UPLOAD.AddShakeAnimation(dropZone);
            UPLOAD.RemoveChild(preview);
            return false;
        }
        return true;
    },
    PreviewMultipleDocs: ({
        files,
        fileInput,
        preview,
        alertBox,
        alertMsg,
        dropZone
    }) => {
        const docFiles = [...files];
        docFiles.forEach((file) => {
            const size = file["size"];
            const fileType = file["type"];
            if (docFiles.length !== 0) {
                UPLOAD.RemoveChild(preview);
            }
            const fileOptions = {
                fileInput,
                preview,
                alertBox,
                alertMsg,
                fileType,
                size,
                dropZone
            };
            if (!UPLOAD.ValidateFileSize(fileOptions)) return;
            if (!UPLOAD.ValidateFileType(fileOptions)) return;
            alertMsg.textContent = "";
            alertBox.classList.add("uk-hidden");
            fileInput.files = files;
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                let filename = file["name"];
                let imgPreview = "";
                if (UPLOAD.DOC_MIMES.includes(fileType)) {
                    if (fileType === "application/pdf") {
                        imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
                        imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                    } else {
                        imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
                        imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                    }
                } else {
                    imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
                }
                preview.append(imgPreview);
            };
        });
    },
    PreviewSingleDoc: ({
        files,
        fileInput,
        preview,
        alertBox,
        alertMsg,
        dropZone
    }) => {
        const size = files[0]["size"];
        const fileType = files[0]["type"];
        let filename = files[0]["name"];
        if (files[0].length !== 0) {
            UPLOAD.RemoveChild(preview);
        }
        const fileOptions = {
            fileInput,
            preview,
            alertBox,
            alertMsg,
            fileType,
            size,
            dropZone
        };
        if (!UPLOAD.ValidateFileSize(fileOptions)) return;
        if (!UPLOAD.ValidateFileType(fileOptions)) return;
        alertMsg.textContent = "";
        alertBox.classList.add("uk-hidden");
        const reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload = () => {
            let imgPreview = "";
            let imagePath = "";
            if (UPLOAD.DOC_MIMES.includes(fileType)) {
                if (fileType === "application/pdf") {
                    imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
                    imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                } else {
                    imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
                    imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                }
            } else {
                imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
            }
            preview.append(imgPreview);
        };
    }
};
document.addEventListener("DOMContentLoaded", UPLOAD.Init());
</script>



<script>

$(document).ready(function(){
  
    $("#myForm").on('submit', (function(e) {
    e.preventDefault();
        $.ajax({
            url:  $(this).attr('action'),
            type: "POST",
            headers: {
            'X-CSRF-Token': '{{ csrf_token() }}',
            },
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
            //  console.log(response);

            if(response.status == false ){
                $('#fileError').css("display", "block");
                }else{
               
                    $("#myForm").trigger("reset"); // this line is to reset form input fields
                    $('#fileError').css("display", "none");
                    UIkit.modal("#claim-process").show();
                    // $('#modalMessage').innertHtml = response.message;
                    // alert('submitted'); 
                }
        
            },
                // error: function(e){
                // alert('Failed to submit');
                // }
            });
    
        }));   


});

</script>

<script>
    var form = $("#myForm");
  	form.validate({
    rules: {
        name: {
        required: true,
        },
        // claim_type: {
        //     required: true,
        // },
        // policy_number: {
        //     required: true,
        // },
        // cnic: {
        //     required: true,
        //     minlength: 13,
        //     maxlength:13.
        // },
        // contact_number: {
        //     required: true,
        //     minlength: 13,
        //     maxlength: 13,
        // },
        // email: {
        //     required: true,
        // }   
        // summary: {
        //     required: true,
        // }
    },
   
  });
</script>



@include('partials.footer')
@endif