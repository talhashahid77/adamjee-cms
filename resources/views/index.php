<?php include('header.php'); ?>

<!-- Home Slider Start -->
<section class="homeBanner">
	<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider>

    <ul class="uk-slider-items">
        <li>
            <div class="uk-panel">
                <img src="images/homebanner/slide1.png" width="2732" height="1100" alt="">
                <div class="homeBannerTxt">
                	<div class="mdl">
                		<div class="mdl_inner">
                			<div class="uk-container containCustom">
				                <div uk-grid>
				                	<div class="uk-width-1-2@m">
				                		<h2 uk-slider-parallax="x: 200,-200">Book an online appointment </h2>
				                    	<p uk-slider-parallax="x: 300,-300"> Contact an expert with Adamjee Life’s Digital Insurance Channel</p>
				                    	<div class="homeBanneBtn">
				                    		<a href="https://www.adamjeelife.com/appointment-booking/" target="_blank" uk-slider-parallax="x: 350,-350">Book an appointment <img src="images/right.svg" uk-svg /> </a>
				                    	</div>
				                	</div>
				                	<div class="uk-width-1-2@m">
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 200,-200">
					                			<h4>15000</h4>
					                			<p>LIVES PROTECTED</p>
					                		</div>
				                		</div>
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 300,-300">
					                			<h4>A++</h4>
					                			<p>PACRA RATING</p>
					                		</div>
				                		</div>
				                	</div>
				                </div>           
			             	</div>
                		</div>
                	</div>
	             </div>
            </div>
        </li>
		
		<li>
            <div class="uk-panel">
                <img src="images/homebanner/slide3.png" width="2732" height="1100" alt="">
                <div class="homeBannerTxt">
                	<div class="mdl">
                		<div class="mdl_inner">
                			<div class="uk-container containCustom">
				                <div uk-grid>
				                	<div class="uk-width-1-2@m">
				                		<h2 uk-slider-parallax="x: 200,-200">Orbis reward program</h2>
				                    	<p uk-slider-parallax="x: 300,-300">Avail amazing rewards, benefits, deals and discounts with your Adamjee Life Orbis card</p>
				                    	<div class="homeBanneBtn">
				                    		<a href="javascript:;" uk-slider-parallax="x: 350,-350">Order your card today<img src="images/right.svg" uk-svg /> </a>
											<a href="javascript:;" uk-slider-parallax="x: 350,-350">I have a card<img src="images/right.svg" uk-svg /> </a>
				                    	</div>
				                	</div>
				                	<!-- <div class="uk-width-1-2@m">
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 200,-200">
					                			<h4>15000</h4>
					                			<p>LIVES PROTECTED</p>
					                		</div>
				                		</div>
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 300,-300">
					                			<h4>AA+</h4>
					                			<p>PACRA RATING</p>
					                		</div>
				                		</div>
				                	</div> -->
				                </div>           
			             	</div>
                		</div>
                	</div>
	             </div>
            </div>
        </li>
		<li>
            <div class="uk-panel">
                <img src="images/homebanner/slide2.png" width="2732" height="1100" alt="">
                <div class="homeBannerTxt">
                	<div class="mdl">
                		<div class="mdl_inner">
                			<div class="uk-container containCustom">
				                <div uk-grid>
				                	<div class="uk-width-1-2@m">
				                		<h2 uk-slider-parallax="x: 200,-200">Take control of your policy</h2>
				                    	<p uk-slider-parallax="x: 300,-300">Manage all your policy details from the comfort of your home, 24/7</p>
				                    	<div class="homeBanneBtn">
				                    		<a href="https://alpos.adamjeelife.com/Eservices" target="_blank" uk-slider-parallax="x: 350,-350">Conventional<img src="images/right.svg" uk-svg /> </a>
											<a href="https://alpos.adamjeelife.com/tkfeservice" target="_blank" uk-slider-parallax="x: 350,-350">Takaful<img src="images/right.svg" uk-svg /> </a>
				                    	</div>
				                	</div>
				                	<div class="uk-width-1-2@m">
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 200,-200">
					                			<h4>15000</h4>
					                			<p>LIVES PROTECTED</p>
					                		</div>
				                		</div>
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 300,-300">
					                			<h4>AA+</h4>
					                			<p>PACRA RATING</p>
					                		</div>
				                		</div>
				                	</div>
				                </div>           
			             	</div>
                		</div>
                	</div>
	             </div>
            </div>
        </li>
        
        <li>
            <div class="uk-panel">
                <img src="images/homebanner/slide4.png" width="2732" height="1100" alt="">
                <div class="homeBannerTxt">
                	<div class="mdl">
                		<div class="mdl_inner">
                			<div class="uk-container containCustom">
				                <div uk-grid>
				                	<div class="uk-width-1-2@m">
				                		<h2 uk-slider-parallax="x: 200,-200">File your claim on the go</h2>
				                    	<p uk-slider-parallax="x: 300,-300">Process your claim anytime, anywhere</p>
				                    	<div class="homeBanneBtn">
				                    	<a href="javascript:;" uk-slider-parallax="x: 350,-350">Claim processing<img src="images/right.svg" uk-svg /> </a>
											<a href="javascript:;" uk-slider-parallax="x: 350,-350">Hotline<img src="images/right.svg" uk-svg /> </a>
				                    	</div>
				                	</div>
				                	<div class="uk-width-1-2@m">
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 200,-200">
					                			<h4>15000</h4>
					                			<p>LIVES PROTECTED</p>
					                		</div>
				                		</div>
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 300,-300">
					                			<h4>AA+</h4>
					                			<p>PACRA RATING</p>
					                		</div>
				                		</div>
				                	</div>
				                </div>           
			             	</div>
                		</div>
                	</div>
	             </div>
            </div>
        </li>
        <li>
            <div class="uk-panel">
                <img src="images/homebanner/slide6.jpg" width="2732" height="1100" alt="">
                <div class="homeBannerTxt">
                	<div class="mdl">
                		<div class="mdl_inner">
                			<div class="uk-container containCustom">
				                <div uk-grid>
				                	<div class="uk-width-1-2@m">
				                		<h2 uk-slider-parallax="x: 200,-200">Window Takaful Operations</h2>
				                    	<p uk-slider-parallax="x: 300,-300">Your shariah finance partner</p>
				                    	<div class="homeBanneBtn">
				                    		<a href="./product-takaful-individual-life.php" uk-slider-parallax="x: 350,-350">Explore more <img src="images/right.svg" uk-svg /> </a>
				                    	</div>
				                	</div>
				                	<div class="uk-width-1-2@m">
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 200,-200">
					                			<h4>15000</h4>
					                			<p>LIVES PROTECTED</p>
					                		</div>
				                		</div>
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 300,-300">
					                			<h4>AA+</h4>
					                			<p>PACRA RATING</p>
					                		</div>
				                		</div>
				                	</div>
				                </div>           
			             	</div>
                		</div>
                	</div>
	             </div>
            </div>
        </li>

		<li>
            <div class="uk-panel">
                <img src="images/homebanner/slide5.jpg" width="2732" height="1100" alt="">
                <div class="homeBannerTxt">
                	<div class="mdl">
                		<div class="mdl_inner">
                			<div class="uk-container containCustom">
				                <div uk-grid>
				                	<div class="uk-width-1-2@m">
				                		<h2 uk-slider-parallax="x: 200,-200">Plans for every need</h2>
				                    	<p uk-slider-parallax="x: 300,-300">Providing coverage for all your needs against financial uncertainty</p>
				                    	<div class="homeBanneBtn">
				                    		<a href="./products-individual-life.php" uk-slider-parallax="x: 350,-350">Explore more <img src="images/right.svg" uk-svg /> </a>
				                    	</div>
				                	</div>
				                	<div class="uk-width-1-2@m">
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 200,-200">
					                			<h4>15000</h4>
					                			<p>LIVES PROTECTED</p>
					                		</div>
				                		</div>
				                		<div class="homeBanneFull">
				                			<div class="homeBanneSqrBox" uk-slider-parallax="x: 300,-300">
					                			<h4>AA+</h4>
					                			<p>PACRA RATING</p>
					                		</div>
				                		</div>
				                	</div>
				                </div>           
			             	</div>
                		</div>
                	</div>
	             </div>
            </div>
        </li>

    </ul>

    <ul class="uk-slider-nav uk-dotnav homeBullets"></ul>

</div>
</section>
<!-- Home Slider End -->
<!-- Ticker Section Start -->
<section class="TickerSec">
	<div class="uk-container uk-container-expand">
		<div class="TickerBox">
		    <a class="viewAll" href="javascript:;">View all fund prices <img src="images/right.svg" uk-svg /></a>
	        <ul class="NewsTicker">
	            <li><p><strong>Investment Secure Fund(ISF-I)</strong></p><p><span>Bid price (Rs)</span><span>265.095900</span></p><p><span>Bid price (Rs)</span><span>265.095900</span></p></li>
	            <li><p><strong>Investment Secure Fund(ISF-I)</strong></p><p><span>Bid price (Rs)</span><span>265.095900</span></p><p><span>Bid price (Rs)</span><span>265.095900</span></p></li>
	            <li><p><strong>Investment Secure Fund(ISF-I)</strong></p><p><span>Bid price (Rs)</span><span>265.095900</span></p><p><span>Bid price (Rs)</span><span>265.095900</span></p></li>
	            <li><p><strong>Investment Secure Fund(ISF-I)</strong></p><p><span>Bid price (Rs)</span><span>265.095900</span></p><p><span>Bid price (Rs)</span><span>265.095900</span></p></li>
	            <li><p><strong>Investment Secure Fund(ISF-I)</strong></p><p><span>Bid price (Rs)</span><span>265.095900</span></p><p><span>Bid price (Rs)</span><span>265.095900</span></p></li>
	            <li><p><strong>Investment Secure Fund(ISF-I)</strong></p><p><span>Bid price (Rs)</span><span>265.095900</span></p><p><span>Bid price (Rs)</span><span>265.095900</span></p></li>
	            <li><p><strong>Investment Secure Fund(ISF-I)</strong></p><p><span>Bid price (Rs)</span><span>265.095900</span></p><p><span>Bid price (Rs)</span><span>265.095900</span></p></li>
	        </ul>
		</div>
	</div>
</section>
<!-- Ticker Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
		<div class="HeadingBox">
			<h3>Want to know which plan suits your needs?</h3>
			<p>Use our planner to find information relevant to your requirements</p>
			<!-- <a class="blueBtn" href="javascript:;">Book an appointment <img src="images/right.svg" uk-svg /> </a> -->
		</div>
		<div class="ScrollCarousal">
			<div uk-slider="finite: true;">
			    <ul class="uk-slider-items">
			        <li>
			        	<a href="#modal-education" uk-toggle class="ScrollCarousalBox">
			        		<img src="images/homebanner/Secondslider/3.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Education </h4>
								<!-- <p>Save for your child's education to to ensure they achieve their dreams</p> -->
							</div>
			        	</a>
			        </li>
			        <li>
			        	<a href="javascript:;" class="ScrollCarousalBox">
			        		<img src="images/homebanner/Secondslider/2.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Wedding </h4>
								<!-- <p>Save and invest to give your child the perfect wedding they deserve</p> -->
							</div>
			        	</a>
			        </li>
			        <li>
			        	<a href="javascript:;" class="ScrollCarousalBox">
			        		<img src="images/homebanner/Secondslider/1.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Comprehensive Coverage</h4>
								<!-- <p>Plan your retirement now so that you don't have to worry about your future</p> -->
							</div>
			        	</a>
			        </li>
			    </ul>
			</div>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap">
	<div class="uk-container containCustom2">
		<div class="SecBan">
			<img src="images/getcoverage.png" />
			<div class="SecBanTxt">
				<div class="mdl">
					<div class="mdl_inner">
						<ul class="uk-grid-small uk-flex uk-flex-right" uk-grid>
							<li class="uk-width-1-2@m">
								<div class="SecBanInner">
									<h3>How much life insurance do you need?</h3>
									<p>Just answer a few questions to get the estimated coverage you need</p>
									<a class="blueBtn" href="#modal-calculation" uk-toggle> Calculate<img src="images/right.svg" uk-svg /> </a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap">
	<div class="uk-container containCustom">
		<div class="HeadingBox">
			<h3>Make life rewarding with Adamjee Life Orbis</h3>
			<p>Avail exclusive discounts across Pakistan with our Orbis Reward Program<a class="LinkBtn" href="offers-discounts.php">Show more<img src="images/right.svg" uk-svg /> </a></p>
		</div>
		<div class="ScrollCarousal">
			<div uk-slider="finite: true;">
			    <ul class="uk-slider-items">
			        <li>
			        	<a href="educational-reimbursement.php" class="ScrollCarousalBox">
			        		<!-- <div class="badgeBox">UPTO 50% REIMBURSEMENT</div> -->
			        		<img src="images/homebanner/education.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Education</h4>
								<p>Doing our best to help your children achieve their best</p>
							</div>
			        	</a>
			        </li>
					<li>
			        	<a href="offers-discounts.php" class="ScrollCarousalBox">
			        		<!-- <div class="badgeBox">UPTO 50% REIMBURSEMENT</div> -->
			        		<img src="images/homebanner/Eateries.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Eateries </h4>
								<p>Enjoying Exciting flavors with Tantalizing discounts</p>
							</div>
			        	</a>
			        </li>
			        <li>
			        	<a href="offers-discounts.php" class="ScrollCarousalBox">
			        		<!-- <div class="badgeBox">UPTO 50% REIMBURSEMENT</div> -->
			        		<img src="images/homebanner/Lifestyle.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Lifestyle </h4>
								<p>Your Preferred lifestyle just became more accessible</p>
							</div>
			        	</a>
			        </li>
			        <li>
			        	<a href="offers-discounts.php" class="ScrollCarousalBox">
			        		<!-- <div class="badgeBox">UPTO 50% REIMBURSEMENT</div> -->
			        		<img src="images/homebanner/Health-wellness.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Health & Wellness </h4>
								<p>Caring for yourself and your loved ones just became easier</p>
							</div>
			        	</a>
			        </li>
			        <li>
			        	<a href="offers-discounts.php" class="ScrollCarousalBox">
			        		<!-- <div class="badgeBox">UPTO 50% REIMBURSEMENT</div> -->
			        		<img src="images/homebanner/Protection-Coverage.jpg" alt="">
			        		<div class="ScrollCarousalBoxHover">
								<h4>Insurance & Takaful</h4>
								<p>Bringing you and your family the Protection you deserve</p>
							</div>
			        	</a>
			        </li>
			    </ul>
			    <div class="ScrollBtl">
			    	<ul class="uk-slider-nav uk-dotnav"></ul>
			    </div>
			    
			</div>
		</div>
		
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap">
	<div class="uk-container containCustom2">
		<div class="SecBan textLight">
			<img src="images/controlpolicy.png" />
			<div class="SecBanTxt">
				<div class="mdl">
					<div class="mdl_inner">
						<ul class="uk-grid-small" uk-grid>
							<li class="uk-width-1-2@m">
								<div class="SecBanInner">
									<h3>Control your policy</h3>
									<p>Now you can take control of your insurance policies and access your policy details anywhere, anytime.</p>
									<a class="whiteBtn" href="https://alpos.adamjeelife.com/Eservices" target="_blank">Conventional <img src="images/right.svg" uk-svg /> </a>
									<a class="whiteBtn" href="https://alpos.adamjeelife.com/tkfeservice" target="_blank">Takaful <img src="images/right.svg" uk-svg /> </a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap">
	<div class="uk-container containCustom">
		<div class="HeadingBox">
			<h3>Knowledge Center</h3>
			<p>Here to help you in advice, claims and everything in between <a class="LinkBtn" href="./free-advice-guides.php">See all <img src="images/right.svg" uk-svg /> </a></p>
		</div>
		<div class="ScrollCarousal cardsCarousel">
			<div uk-slider="finite: true;">
			    <ul class="uk-slider-items" uk-height-match=".uk-card-body">
			        <li>
			        	<a href="./free-advice-guides-inner.php" class="uk-card uk-card-default">
				            <div class="uk-card-media-top">
				                <img src="images/cardimg.png" width="1800" height="1200" alt="">
				            </div>
				            <div class="uk-card-body">
				            	<div class="badgeBox">CLAIMS</div>
				                <h3>How To Fill An Insurance Form</h3>
				                <p>Life insurance can be confusing. Our five-minute guide can help you understand everything easily.</p>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./free-advice-guides-inner.php" class="uk-card uk-card-default">
				            <div class="uk-card-media-top">
				                <img src="images/cardimg.png" width="1800" height="1200" alt="">
				            </div>
				            <div class="uk-card-body">
				            	<div class="badgeBox">PRE SALES</div>
				                <h3>How to Get Insured</h3>
				                <p>Family life insurance protects you and your children, get the details in this guide!</p>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./free-advice-guides-inner.php" class="uk-card uk-card-default">
				            <div class="uk-card-media-top">
				                <img src="images/cardimg.png" width="1800" height="1200" alt="">
				            </div>
				            <div class="uk-card-body">
				            	<div class="badgeBox">PRE SALES</div>
				                <h3>Life Insurance And Its Types</h3>
				                <p>That’s right, our advice is free! So how do we make money? Find out in this guide.</p>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./free-advice-guides-inner.php" class="uk-card uk-card-default">
				            <div class="uk-card-media-top">
				                <img src="images/cardimg.png" width="1800" height="1200" alt="">
				            </div>
				            <div class="uk-card-body">
				            	<div class="badgeBox">CLAIMS</div>
				                <h3>How To Fill An Insurance Form</h3>
				                <p>Life insurance can be confusing. Our five-minute guide can help you understand everything easily.</p>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./free-advice-guides-inner.php" class="uk-card uk-card-default">
				            <div class="uk-card-media-top">
				                <img src="images/cardimg.png" width="1800" height="1200" alt="">
				            </div>
				            <div class="uk-card-body">
				            	<div class="badgeBox">CLAIMS</div>
				                <h3>How To Fill An Insurance Form</h3>
				                <p>Life insurance can be confusing. Our five-minute guide can help you understand everything easily.</p>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./free-advice-guides-inner.php" class="uk-card uk-card-default">
				            <div class="uk-card-media-top">
				                <img src="images/cardimg.png" width="1800" height="1200" alt="">
				            </div>
				            <div class="uk-card-body">
				            	<div class="badgeBox">CLAIMS</div>
				                <h3>How To Fill An Insurance Form</h3>
				                <p>Life insurance can be confusing. Our five-minute guide can help you understand everything easily.</p>
				            </div>
				        </a>
			        </li>
			    </ul>
			</div>
		</div>
		
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="">
	<div class="uk-container containCustom2">
		<div class="SecBan textLight">
			<img src="images/latestadamjee.png" />
			<div class="SecBanTxt">
				<div class="mdl">
					<div class="mdl_inner">
						<ul class="uk-grid-small" uk-grid>
							<li class="uk-width-1-1">
								<div class="SecBanInner fullWidth">
									<h3>Latest from Adamjee Life</h3>
									<p>Here to help you in advice, claims and everything in between <a class="LinkBtn" href="./latest-from-adamjee-life.php">See all <img src="images/right.svg" uk-svg /> </a></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="ScrollCarousal cardsCarousel Newsbox">
			<div uk-slider="finite: true;">
			    <ul class="uk-slider-items" uk-height-match=".uk-card-body">
			        <li>
			        	<a href="./new1.php" class="uk-card uk-card-default">
				            <div class="uk-card-body">
				            	
				                <h3>Nigehbaan Heatwave Campaign In collaboration with First Response Initiative of Pakistan (FRIP)</h3>
				                <p>Adamjee Life Nigehbaan partnered up with First Response Initiative of Pakistan (FRIP) for safety & response, airway & breathing, circulation & heatwave awareness session at Adamjee Life head office for its employees</p>
				                <div class="infoBox">
				                	<div class="badgeBox">CLAIMS</div>
				                	<div class="flrigt">
				                		<!-- <div class="autherName">Sofia Mayer</div> -->
				                		<div class="dateInfo">May 30, 2022</div>
				                	</div>	
				                </div>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./new2.php" class="uk-card uk-card-default">
				            <div class="uk-card-body">
				            	
				                <h3>Nigehbaan Heatwave Campaign In collaboration with Government of Sindh</h3>
				                <p>Following the Pakistan Meteorological Department’s (PMD) heatwave warning, Adamjee Life, in collaboration with the Sindh Govt., established a heat stroke prevention camp as a first response initiative in order to provide relief to affectees as part of their CSR platform, Adamjee Life Nigehbaan.</p>
				                <div class="infoBox">
				                	<div class="badgeBox">CLAIMS</div>
				                	<div class="flrigt">
				                		<!-- <div class="autherName">Sofia Mayer</div> -->
				                		<div class="dateInfo">May 27, 2022</div>
				                	</div>	
				                </div>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./new3.php" class="uk-card uk-card-default">
				            <div class="uk-card-body">
				            	
				                <h3>Nigehbaan Community Service Ramzan Campaign for Frontline Workers and Traffic Police Cops</h3>
				                <p>This Ramzan Adamjee Life has honored the hard work of our front line workers who tirelessly serve the community throughout Ramzan at all hours even during Iftar & Sehri.</p>
				                <div class="infoBox">
				                	<div class="badgeBox">CLAIMS</div>
				                	<div class="flrigt">
				                		<!-- <div class="autherName">Sofia Mayer</div> -->
				                		<div class="dateInfo">April 2, 2022</div>
				                	</div>	
				                </div>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./new4.php" class="uk-card uk-card-default">
				            <div class="uk-card-body">
				            	
				                <h3>Adamjee Life partners with Govt. of Sindh to Improve Healthcare Infrastructure</h3>
				                <p>Karachi – Nov 01st, 2021- Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province. This initiative is part of the Nigehbaan CSR platform of Adamjee Life that has led various education, environmental and health programs in the past.</p>
				                <div class="infoBox">
				                	<div class="badgeBox">CLAIMS</div>
				                	<div class="flrigt">
				                		<!-- <div class="autherName">Sofia Mayer</div> -->
				                		<div class="dateInfo">March 5, 2022</div>
				                	</div>	
				                </div>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./new5.php" class="uk-card uk-card-default">
				            <div class="uk-card-body">
				            	
				                <h3>Adamjee Life 14th NFEH’s CSR Award 2022</h3>
				                <p>National Forum for Environment & Health (commonly known as NFEH) is a purely Non-Governmental, Non-Profit and Voluntary Organization registered under the Voluntary Social Welfare Agencies Ordinance 1961, with an aim to facilitate, promote and help create environmental, healthcare and educational awareness among masses in general, among youths and children in particular.</p>
				                <div class="infoBox">
				                	<div class="badgeBox">CLAIMS</div>
				                	<div class="flrigt">
				                		<!-- <div class="autherName">Sofia Mayer</div> -->
				                		<div class="dateInfo">March 5, 2022</div>
				                	</div>	
				                </div>
				            </div>
				        </a>
			        </li>
			        <li>
			        	<a href="./new6.php" class="uk-card uk-card-default">
				            <div class="uk-card-body">
				            	
				                <h3>Adamjee Life wins 15th Consumer Choice Award 2021</h3>
				                <p>Adamjee Life, one of Pakistan’s most recognized life insurance companies, received the coveted “Icon Award” for “Excellence in Customer Service” at the Consumers Association of Pakistan’s 15th Consumers Choice Awards 2021 in Karachi.</p>
				                
								<div class="infoBox">
									
				                	<div class="badgeBox">CLAIMS</div>
				                	<div class="flrigt">
									
				                		<!-- <div class="autherName">Sofia Mayer</div> -->
				                		<div class="dateInfo">March 5, 2022</div>
				                	</div>	
				                </div>
				            </div>
				        </a>
			        </li>
			    </ul>
			</div>
		</div>
	</div>
</section>
<!-- Section End -->

<!-- plans modal start -->
<div id="modal-education" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
			<a href="index.php" class="logo">
                <img src="images/logo.svg" alt="Adamjee Life">
            </a>
        </div>
        <div class="uk-modal-body">
			<div class="uk-container containCustom">
				<div class="homeStep">
				<!-- multistep form -->
				<form class="wrappermultiform">
					<div class="header">
						<ul>
							<li class="active form_1_progessbar">
								
							</li>
							<li class="form_2_progessbar">
							
							</li>
							<li class="form_3_progessbar">
								
							</li>
							<li class="form_4_progessbar">
								
							</li>
						</ul>
					</div>
					<div class="form_wrap NewsFilter JobForm">
						<div class="form_1 data_info" >
							<h5>Question 1 of 4</h5>
							<h4><b>That’s so exciting. When do you need funds for <br/> your child’s admission?</b></h4>
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label for="full_name">Date</label>
										<input class="uk-input" type="date" placeholder="DD/MM/YYYY" >
									</div>
								</div>
						</div>
						<div class="form_2 data_info" style="display: none;">
							<h5>Question 2 of 4</h5>
							<h4><b>How much can you pay and when?</b></h4>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<label class="uk-form-label">Select a premium mode</label>
									<select class="uk-select">
										<option>Premium mode</option>
									</select>
								</div>

								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<label for="address">Enter an amount</label>
									<input class="uk-input" type="text" placeholder="PKR" >
								</div>
							</div> 
						</div>
						<div class="form_3 data_info" style="display: none;">
							<h5>Question 3 of 4</h5>
							<h4><b>How much can you pay and when?</b></h4>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
									<label for="Designation">Name</label>
									<input class="uk-input" type="text" placeholder="Your full name" >
								</div>

								<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
									<label for="full_name">Date of Birth</label>
									<input class="uk-input" type="date" placeholder="DD/MM/YYYY" >
								</div>

								<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
									<label class="uk-form-label">Gender</label>
									<select class="uk-select">
										<option>Male</option>
										<option>Female</option>
									</select>
								</div>

								<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
									<label for="full_name">Mobile number</label>
									<input class="uk-input" type="date" placeholder="+92 xx xxxxxxxx" >
								</div>

								<div class="uk-width-1-2@m  uk-grid-margin uk-first-column">
									<label class="uk-form-label">City</label>
									<select class="uk-select">
										<option>Karachi</option>
										<option>Karachi</option>
									</select>
								</div>
								
							</div>  
						</div>
						<div class="form_4 data_info" style="display: none;">
							<h5>Question 4 of 4</h5>
							<h4><b>Select your desired plan type</b></h4>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-2@m  uk-grid-margin uk-first-column" id="conventional">
									<label for="Conventional-coverage">
										<img src="images/homebanner/Conventional-coverage.jpg"/>
									</label>
									<input class="uk-radio" type="radio" name="Conventional-coverage" id="Conventional-coverage">Conventional coverage</label>
									<div class="CustomToolTip" uk-tooltip="Takaful coverage is commonly referred to as Islamic insurance. Takaful means a scheme based on brotherhood, solidarity and mutual assistance which provides for mutual financial aid and assistance to the participants in case of need."><img src="images/icons/tooltip.svg" /></div>
									<!-- <input class="uk-input" type="button" placeholder="Conventional coverage" value="Conventional coverage" > -->
									
								</div>

								<div class="uk-width-1-2@m  uk-grid-margin uk-first-column" id="takaful">
									<label for="takaful-coverage">
										<img src="images/homebanner/Takaful-coverage.jpg"/>
									</label>
									<input class="uk-radio" type="radio" name="takaful-coverage"  id="takaful-coverage">Takaful coverage</label>
            
									<!-- <input class="uk-input" type="button" placeholder="Conventional coverage" value="Conventional coverage" > -->
									<div class="CustomToolTip" uk-tooltip="Takaful coverage is commonly referred to as Islamic insurance. Takaful means a scheme based on brotherhood, solidarity and mutual assistance which provides for mutual financial aid and assistance to the participants in case of need."><img src="images/icons/tooltip.svg" /></div>
									<!-- <a id="js-modal-dialog" class="uk-button uk-button-default" ><img src="images/icons/tooltip.svg" /></a>  -->
								</div>
								

								<div id="Conventional-coverage-plans" style="display: none">
									<ul class="uk-grid-small"  uk-grid uk-height-match=".uk-card-body">
										<!-- Card Start -->
										<li class="uk-width-1-3@m" >
											<div class="uk-card uk-card-default newsCard">
												<div class="uk-card-media-top">
													<img src="images/products/1.jpg" alt="">
												</div>
												<div class="uk-card-body">
													<div class="badgesBar">
															<div class="badgeBox">TRENDING</div>	
														</div>
													<h3>Mehfooz Munafa</h3>
													<ul>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
													</ul>
													<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
													<a href="javascript:;" class=""><img src="images/icons/download.svg" uk-svg /> Brochure (PDF)</a><br/>
													<button type="submit" class="packageBtn" uk-toggle="#thank-you" >Contact an expert <img src="images/right.svg" uk-svg /></a>
													
												</div>
											</div>
										</li>
										<!-- Card Start -->
										<li class="uk-width-1-3@m" >
											<div class="uk-card uk-card-default newsCard">
												<div class="uk-card-media-top">
													<img src="images/products/1.jpg" alt="">
												</div>
												<div class="uk-card-body">
													<div class="badgesBar">
															<div class="badgeBox">TRENDING</div>	
														</div>
													<h3>Mehfooz Munafa</h3>
													<ul>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
													</ul>
													<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
													<a href="javascript:;" class=""><img src="images/icons/download.svg" uk-svg /> Brochure (PDF)</a><br/>
													<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="images/right.svg" uk-svg /></a>
													
												</div>
											</div>
										</li>
										<!-- Card Start -->
										<li class="uk-width-1-3@m" >
											<div class="uk-card uk-card-default newsCard">
												<div class="uk-card-media-top">
													<img src="images/products/1.jpg" alt="">
												</div>
												<div class="uk-card-body">
													<div class="badgesBar">
															<div class="badgeBox">TRENDING</div>	
														</div>
													<h3>Mehfooz Munafa</h3>
													<ul>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
													</ul>
													<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
													
													<a href="javascript:;" class=""><img src="images/icons/download.svg" uk-svg /> Brochure (PDF)</a><br/>
													<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="images/right.svg" uk-svg /></a>
												</div>
											</div>
										</li>
									</ul>
								</div>



								<div id="Takaful-coverage-plans" style="display: none">
									<ul class="uk-grid-small"  uk-grid uk-height-match=".uk-card-body">
										<!-- Card Start -->
										<li class="uk-width-1-3@m" >
											<div class="uk-card uk-card-default newsCard">
												<div class="uk-card-media-top">
													<img src="images/products/1.jpg" alt="">
												</div>
												<div class="uk-card-body">
													<div class="badgesBar">
															<div class="badgeBox">TRENDING</div>	
														</div>
													<h3>Mehfooz Munafa takaful-coverage</h3>
													<ul>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
													</ul>
													<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
													<a href="javascript:;" class=""><img src="images/icons/download.svg" uk-svg /> Brochure (PDF)</a><br/>
													<button type="submit" class="packageBtn" uk-toggle="#thank-you" >Contact an expert <img src="images/right.svg" uk-svg /></a>
													
												</div>
											</div>
										</li>
										<!-- Card Start -->
										<li class="uk-width-1-3@m" >
											<div class="uk-card uk-card-default newsCard">
												<div class="uk-card-media-top">
													<img src="images/products/1.jpg" alt="">
												</div>
												<div class="uk-card-body">
													<div class="badgesBar">
															<div class="badgeBox">TRENDING</div>	
														</div>
													<h3>Mehfooz Munafa</h3>
													<ul>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
													</ul>
													<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
													<a href="javascript:;" class=""><img src="images/icons/download.svg" uk-svg /> Brochure (PDF)</a><br/>
													<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="images/right.svg" uk-svg /></a>
													
												</div>
											</div>
										</li>
										<!-- Card Start -->
										<li class="uk-width-1-3@m" >
											<div class="uk-card uk-card-default newsCard">
												<div class="uk-card-media-top">
													<img src="images/products/1.jpg" alt="">
												</div>
												<div class="uk-card-body">
													<div class="badgesBar">
															<div class="badgeBox">TRENDING</div>	
														</div>
													<h3>Mehfooz Munafa</h3>
													<ul>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
														<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</li>
													</ul>
													<!-- <p>Adamjee Life and the Government of Sindh have collaborated to improve the infrastructure of state led health dispensaries in the province.</p> -->
													
													<a href="javascript:;" class=""><img src="images/icons/download.svg" uk-svg /> Brochure (PDF)</a><br/>
													<button type="submit" class="packageBtn" uk-toggle="#thank-you">Contact an expert <img src="images/right.svg" uk-svg /></a>
												</div>
											</div>
										</li>
									</ul>
								</div>
								
							</div>
						</div>
						
					</div>
					<div class="btns_wrap">
						<div class="common_btns form_1_btns">
							<button type="button" class="btn_next blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
						<div class="common_btns form_2_btns" style="display: none;">
							<button type="button" class="btn_back transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
						<div class="common_btns form_3_btns" style="display: none;">
							<button type="button" class="btn_back transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
						<div class="common_btns form_4_btns" style="display: none;">
							<button type="button" class="btn_back transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
							
						</div>
						<!-- <div class="common_btns form_5_btns" style="display: none;">
							<button type="button" class="btn_back transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_done">Done</button>
						</div> -->
					</div>
				</form>

				
				<div class="modal_wrapper" >
					<div class="shadow"></div>
					<div class="success_wrap">
						<span class="modal_icon"><ion-icon name="checkmark-sharp"></ion-icon></span>
						<p>You have successfully completed the process.</p>
					</div>
				</div>
			</div>    
		</div>
        </div>
    </div>
</div>
<!-- eductaion modal end -->



<!-- plans modal start -->
<div id="modal-calculation" class="modal-calculation" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
			<a href="index.php" class="logo">
                <img src="images/logo.svg" alt="Adamjee Life">
            </a>
        </div>
        <div class="uk-modal-body">
			<div class="uk-container containCustom">
				<div class="homeStep">
				<!-- multistep form -->
				<form class="wrappermultiform">
					<div class="header">
						<ul>
							<li class="active form_1_progessbar_calculation">
								
							</li>
							<li class="form_2_progessbar_calculation">
							
							</li>
							<li class="form_3_progessbar_calculation">
								
							</li>
							<li class="form_4_progessbar_calculation">
								
							</li>
							<li class="form_5_progessbar_calculation">
								
							</li>
							<li class="form_6_progessbar_calculation">
								
							</li>
							<li class="form_7_progessbar_calculation">
								
							</li>
						</ul>
					</div>
					<div class="form_wrap NewsFilter JobForm">
						<div class="form_1_calculation data_info_calculation" >
							<h5>Question 1 of 7</h5>
							<h4><b>Tell us a bit about yourself...</b></h4>
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label for="full_name">full</label> -->
										<input class="uk-input" type="text" placeholder="Full Name" >
									</div>
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label for="full_name">Date</label> -->
										<input class="uk-input" type="number" placeholder="Mobile Number" >
									</div>
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label class="uk-form-label">Select a premium mode</label> -->
										<select class="uk-select">
											<option>Gender</option>
											<option>Male</option>
											<option>Female</option>
										</select>
									</div>

									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label class="uk-form-label">Select a premium mode</label> -->
										<select class="uk-select">
											<option>City</option>
											<option>Karachi</option>
											<option>Lahore</option>
										</select>
									</div>
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<!-- <label class="uk-form-label">Select a premium mode</label> -->
										<input class="uk-input" type="date" placeholder="Date of Birth " >
									</div>
								</div>
						</div>
						<div class="form_2_calculation data_info_calculation" style="display: none;">
							<h5>Question 2 of 7</h5>
							<h4><b>How much annual income would you like to provide, if you were no longer here?</b></h4>
							<p>Think about how much money your family will need to cover daily living expenses. This is typically 60-80% of your individual post-tax income. Don’t include college savings or any debts that you would like to pay off immediately (such as your mortgage), since those are covered in other questions</p>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="text" placeholder="PKR" >
								</div>
							</div> 
						</div>
						<div class="form_3_calculation data_info_calculation" style="display: none;">
							<h5>Question 3 of 7</h5>
							<h4><b>How many years should income be provided after you’re gone?</b></h4>
							<p>Think about how long you’ll need the additional income to support you and your family</p>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="text" placeholder="PKR" >
								</div>	
							</div>  
						</div>

						<div class="form_3_calculation data_info_calculation" style="display: none;">
							<h5>Question 4 of 7</h5>
							<h4><b>How many years should income be provided after you’re gone?</b></h4>
							<p>Think about how long you’ll need the additional income to support you and your family</p>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="text" placeholder="PKR" >
								</div>	
							</div>  
						</div>

						<div class="form_4_calculation data_info_calculation" style="display: none;">
							<h5>Question 4 of 7</h5>
							<h4><b>How much debt would you like to pay off immediately?</b></h4>
							<p>Consider things like outstanding mortgage, credit card balances and car loans.</p>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="text" placeholder="PKR" >
								</div>	
							</div> 
						</div>

						<div class="form_5_calculation data_info_calculation" style="display: none;">
							<h5>Question 5 of 7</h5>
							<h4><b>How many children require college funding?</b></h4>
							<p>Consider things like outstanding mortgage, credit card balances and car loans.</p>
							<div class="childInfo">
								<h5><b>Child 1</b><a class="childremove">x</a></h5>
								
								<div class="uk-grid-medium" uk-grid >
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label">What is their current age?</label>
										<select class="uk-select">
											<option>City</option>
											<option>Karachi</option>
											<option>Lahore</option>
										</select>
									</div>
									
									<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
										<label class="uk-form-label">What type of school would you like to plan for?</label>
										<select class="uk-select">
											<option>School Type</option>
											<option>School Type</option>
											<option>School Type</option>
										</select>
									</div>
								</div>
							</div>
							<div class="childInfomore"></div>
							<a id="addChild">Add Child</a>
							 
						</div>

						<div class="form_6_calculation data_info_calculation" style="display: none;">
							<h5>Question 6 of 7</h5>
							<h4><b>How much would you like to set aside for an emergency fund?</b></h4>
							<p>If you don’t already have one, it’s a good idea to set aside at least three to six months’ worth of expenses in your emergency fund.</p>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="text" placeholder="PKR" >
								</div>	
							</div> 
						</div>

						<div class="form_7_calculation data_info_calculation" style="display: none;">
							<h5>Question 6 of 7</h5>
							<h4><b>How much personal life insurance do you already have?</b></h4>
							<p>Do not include any life insurance policies you may have through work, since these policies will likely be eliminated when your job changes.</p>
							<div class="uk-grid-medium" uk-grid >
								<div class="uk-width-1-3@m  uk-grid-margin uk-first-column">
									<!-- <label class="uk-form-label">Select a premium mode</label> -->
									<input class="uk-input" type="text" placeholder="PKR" >
								</div>	
							</div> 
						</div>
					</div>
					<div class="btns_wrap">
						<div class="common_btns_calculation form_1_btns_calculation">
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
						<div class="common_btns_calculation form_2_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
						<div class="common_btns form_3_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
						<div class="common_btns_calculation form_4_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
							
						</div>
						<div class="common_btns_calculation form_5_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn ">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
							<button type="button" class="btn_skip_calculation">Skip<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>

						<div class="common_btns_calculation form_6_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_next_calculation blueBtn">Next<img src="images/icons/chevron_right.svg" uk-svg /></button>
							<button type="button" class="btn_skip_calculation">Skip<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
						<div class="common_btns_calculation form_7_btns_calculation" style="display: none;">
							<button type="button" class="btn_back_calculation transparentbtn"><img src="images/icons/chevron_left.svg" uk-svg />Back </button>
							<button type="button" class="btn_done_calculation blueBtn" uk-toggle="#modal-calculation-result">Submit<img src="images/icons/chevron_right.svg" uk-svg /></button>
							<button type="button" class="btn_skip_calculation">Skip<img src="images/icons/chevron_right.svg" uk-svg /></button>
						</div>
					</div>
				</form>
			</div>    
		</div>
        </div>
    </div>
</div>
<!-- calculator modal end -->

<!-- calculate result start -->

<!-- plans modal start -->
<div id="modal-calculation-result" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
			<a href="index.php" class="logo">
                <img src="images/logo.svg" alt="Adamjee Life">
            </a>
        </div>
        <div class="uk-modal-body">
			<div class="uk-container uk-container-small mtauto containCustom">
				<h4><b>Your estimated life insurance need:</b></h4>
				<table class="uk-table uk-table-small Calculateresult" >
					<tbody>
						<tr>
							<td><b>Income replacement</b></td>
							<td><b>PKR. 34,543</b></td>
						</tr>
						<tr>
							<td>Debt to pay off</td>
							<td>Rs. 100</td>
						</tr>
						
						<tr>
							<td>College fund</td>
							<td>Rs. 100</td>
						</tr>

						<tr>
							<td>Emergency fund</td>
							<td>Rs. 100</td>
						</tr>

						<tr>
							<td>Current insurance</td>
							<td>(PKR. 100)</td>
						</tr>
						<tr>
							<td>Total need</td>
							<td>PKR. 24,567,354</td>
						</tr>
						
					</tbody>
				</table>	

				<div class="common_btns_calculation form_7_btns_calculation form_7_btns_calculation_result">
					<button type="button" class="btn_back_calculation transparentbtn" uk-toggle=".modal-calculation"><img src="images/icons/chevron_left.svg" uk-svg />Edit Response </button>
					<button class="uk-modal-close-default blueBtn" type="button" >Done<img src="images/icons/chevron_right.svg" uk-svg /></button>
					<!-- <button type="button" class="btn_done_calculation blueBtn calculationDone">Done<img src="images/icons/chevron_right.svg" uk-svg /></button> -->
				</div>
			</div>    
		</div>

		
        </div>
    </div>
</div>
<!-- calculator modal end -->


<!-- thank you  start -->
<div id="thank-you" class="uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

        <button class="uk-modal-close-default" type="button" uk-close></button>
		
		<h4>Thank you for choosing us</h4>
        <p>An expert will be in contact with you soon.</p>

    </div>
</div>
<!-- thank you end -->
<script type="text/javascript" src="js/acmeticker.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('.NewsTicker').AcmeTicker({
        type:'marquee',/*horizontal/horizontal/Marquee/type*/
        direction: 'left',/*up/down/left/right*/
        speed: 0.05,/*true/false/number*/ /*For vertical/horizontal 600*//*For marquee 0.05*//*For typewriter 50*/
    });
})
</script>
<!-- plans form -->
<script>
var form_1 = document.querySelector(".form_1");
var form_2 = document.querySelector(".form_2");
var form_3 = document.querySelector(".form_3");
var form_4 = document.querySelector(".form_4");
// var form_5 = document.querySelector(".form_5");


var form_1_btns = document.querySelector(".form_1_btns");
var form_2_btns = document.querySelector(".form_2_btns");
var form_3_btns = document.querySelector(".form_3_btns");
var form_4_btns = document.querySelector(".form_4_btns");
// var form_5_btns = document.querySelector(".form_5_btns");


var form_1_next_btn = document.querySelector(".form_1_btns .btn_next");
var form_2_back_btn = document.querySelector(".form_2_btns .btn_back");
var form_2_next_btn = document.querySelector(".form_2_btns .btn_next");
var form_3_back_btn = document.querySelector(".form_3_btns .btn_back");
var form_3_next_btn = document.querySelector(".form_3_btns .btn_next");
var form_4_back_btn = document.querySelector(".form_4_btns .btn_back");
var form_4_next_btn = document.querySelector(".form_4_btns .btn_next");
// var form_5_back_btn = document.querySelector(".form_5_btns .btn_back");

var form_2_progessbar = document.querySelector(".form_2_progessbar");
var form_3_progessbar = document.querySelector(".form_3_progessbar");
var form_4_progessbar = document.querySelector(".form_4_progessbar");
// var form_5_progessbar = document.querySelector(".form_5_progessbar");

var btn_done = document.querySelector(".btn_done");
var modal_wrapper = document.querySelector(".modal_wrapper");
var shadow = document.querySelector(".shadow");

form_1_next_btn.addEventListener("click", function(){
	form_1.style.display = "none";
	form_2.style.display = "block";

	form_1_btns.style.display = "none";
	form_2_btns.style.display = "flex";

	form_2_progessbar.classList.add("active");
});

form_2_back_btn.addEventListener("click", function(){
	form_1.style.display = "block";
	form_2.style.display = "none";

	form_1_btns.style.display = "flex";
	form_2_btns.style.display = "none";

	form_2_progessbar.classList.remove("active");
});

form_2_next_btn.addEventListener("click", function(){
	form_2.style.display = "none";
	form_3.style.display = "block";

	form_3_btns.style.display = "flex";
	form_2_btns.style.display = "none";

	form_3_progessbar.classList.add("active");
});

form_3_back_btn.addEventListener("click", function(){
	form_2.style.display = "block";
	form_3.style.display = "none";

	form_3_btns.style.display = "none";
	form_2_btns.style.display = "flex";

	form_3_progessbar.classList.remove("active");
});


form_3_next_btn.addEventListener("click", function(){
	form_3.style.display = "none";
	form_4.style.display = "block";

	form_4_btns.style.display = "flex";
	form_3_btns.style.display = "none";

	form_4_progessbar.classList.add("active");
});

	// Conventional-coverage
	 $(function() {
    	$("input[name='Conventional-coverage']").click(function() {
      if ($("#Conventional-coverage").is(":checked")) {
        $("#Conventional-coverage-plans").show();
		$("#conventional").hide();
		$("#takaful").hide();
      }
	  	else {
        $("#Conventional-coverage-plans").hide();
      }
    });
  });
//   takaful-coverage
	$(function() {
			$("input[name='takaful-coverage']").click(function() {
		if ($("#takaful-coverage").is(":checked")) {
			$("#Takaful-coverage-plans").show();
			$("#conventional").hide();
			$("#takaful").hide();
		}
			else {
			$("#Takaful-coverage-plans").hide();
		}
		});
	});
</script>
<!-- plans end -->

<!-- calculation modal start -->
<script>
var form_1_calculation = document.querySelector(".form_1_calculation");
var form_2_calculation = document.querySelector(".form_2_calculation");
var form_3_calculation = document.querySelector(".form_3_calculation");
var form_4_calculation = document.querySelector(".form_4_calculation");
var form_5_calculation = document.querySelector(".form_5_calculation");
var form_6_calculation = document.querySelector(".form_6_calculation");
var form_7_calculation = document.querySelector(".form_7_calculation");


var form_1_btns_calculation = document.querySelector(".form_1_btns_calculation");
var form_2_btns_calculation = document.querySelector(".form_2_btns_calculation");
var form_3_btns_calculation = document.querySelector(".form_3_btns_calculation");
var form_4_btns_calculation = document.querySelector(".form_4_btns_calculation");
var form_5_btns_calculation = document.querySelector(".form_5_btns_calculation");
var form_6_btns_calculation = document.querySelector(".form_6_btns_calculation")
var form_7_btns_calculation = document.querySelector(".form_7_btns_calculation")


var form_1_next_btn_calculation = document.querySelector(".form_1_btns_calculation .btn_next_calculation");
var form_2_back_btn_calculation = document.querySelector(".form_2_btns_calculation .btn_back_calculation");
var form_2_next_btn_calculation = document.querySelector(".form_2_btns_calculation .btn_next_calculation");
var form_3_back_btn_calculation = document.querySelector(".form_3_btns_calculation .btn_back_calculation");
var form_3_next_btn_calculation = document.querySelector(".form_3_btns_calculation .btn_next_calculation");
var form_4_back_btn_calculation = document.querySelector(".form_4_btns_calculation .btn_back_calculation");
var form_4_next_btn_calculation = document.querySelector(".form_4_btns_calculation .btn_next_calculation");
var form_5_back_btn_calculation = document.querySelector(".form_5_btns_calculation .btn_back_calculation");
var form_5_next_btn_calculation = document.querySelector(".form_5_btns_calculation .btn_next_calculation");
var form_5_skip_btn_calculation = document.querySelector(".form_5_btns_calculation .btn_skip_calculation");
var form_6_back_btn_calculation = document.querySelector(".form_6_btns_calculation .btn_back_calculation");
var form_6_next_btn_calculation = document.querySelector(".form_6_btns_calculation .btn_next_calculation");
var form_6_skip_btn_calculation = document.querySelector(".form_6_btns_calculation .btn_skip_calculation");
var form_7_back_btn_calculation = document.querySelector(".form_7_btns_calculation .btn_back_calculation");
var form_7_next_btn_calculation = document.querySelector(".form_7_btns_calculation .btn_next_calculation");
var form_7_skip_btn_calculation = document.querySelector(".form_7_btns_calculation .btn_skip_calculation");


var form_2_progessbar_calculation = document.querySelector(".form_2_progessbar_calculation");
var form_3_progessbar_calculation = document.querySelector(".form_3_progessbar_calculation");
var form_4_progessbar_calculation = document.querySelector(".form_4_progessbar_calculation");
var form_5_progessbar_calculation = document.querySelector(".form_5_progessbar_calculation");
var form_6_progessbar_calculation = document.querySelector(".form_6_progessbar_calculation");
var form_7_progessbar_calculation = document.querySelector(".form_7_progessbar_calculation");


var btn_done = document.querySelector(".btn_done");
var modal_wrapper = document.querySelector(".modal_wrapper");
var shadow = document.querySelector(".shadow");

form_1_next_btn_calculation.addEventListener("click", function(){
	form_1_calculation.style.display = "none";
	form_2_calculation.style.display = "block";

	form_1_btns_calculation.style.display = "none";
	form_2_btns_calculation.style.display = "flex";

	form_2_progessbar_calculation.classList.add("active");
});

form_2_back_btn_calculation.addEventListener("click", function(){
	form_1_calculation.style.display = "block";
	form_2_calculation.style.display = "none";

	form_1_btns_calculation.style.display = "flex";
	form_2_btns_calculation.style.display = "none";

	form_2_progessbar_calculation.classList.remove("active");
});

form_2_next_btn_calculation.addEventListener("click", function(){
	form_2_calculation.style.display = "none";
	form_3_calculation.style.display = "block";

	form_3_btns_calculation.style.display = "flex";
	form_2_btns_calculation.style.display = "none";

	form_3_progessbar_calculation.classList.add("active");
});

form_3_back_btn_calculation.addEventListener("click", function(){
	form_2_calculation.style.display = "block";
	form_3_calculation.style.display = "none";

	form_3_btns_calculation.style.display = "none";
	form_2_btns_calculation.style.display = "flex";

	form_3_progessbar_calculation.classList.remove("active");
});


form_3_next_btn_calculation.addEventListener("click", function(){
	form_3_calculation.style.display = "none";
	form_4_calculation.style.display = "block";

	form_4_btns_calculation.style.display = "flex";
	form_3_btns_calculation.style.display = "none";

	form_4_progessbar_calculation.classList.add("active");
});

form_4_back_btn_calculation.addEventListener("click", function(){
	form_3_calculation.style.display = "block";
	form_4_calculation.style.display = "none";

	form_4_btns_calculation.style.display = "none";
	form_3_btns_calculation.style.display = "flex";

	form_4_progessbar_calculation.classList.remove("active");
});


form_4_next_btn_calculation.addEventListener("click", function(){
	form_4_calculation.style.display = "none";
	form_5_calculation.style.display = "block";

	form_5_btns_calculation.style.display = "flex";
	form_4_btns_calculation.style.display = "none";

	form_5_progessbar_calculation.classList.add("active");
});

form_5_back_btn_calculation.addEventListener("click", function(){
	form_4_calculation.style.display = "block";
	form_5_calculation.style.display = "none";

	form_5_btns_calculation.style.display = "none";
	form_4_btns_calculation.style.display = "flex";

	form_5_progessbar_calculation.classList.remove("active");
});


form_5_next_btn_calculation.addEventListener("click", function(){
	form_5_calculation.style.display = "none";
	form_6_calculation.style.display = "block";

	form_6_btns_calculation.style.display = "flex";
	form_5_btns_calculation.style.display = "none";

	form_6_progessbar_calculation.classList.add("active");
});

form_5_skip_btn_calculation.addEventListener("click", function(){
	form_5_calculation.style.display = "none";
	form_6_calculation.style.display = "block";

	form_6_btns_calculation.style.display = "flex";
	form_5_btns_calculation.style.display = "none";

	form_6_progessbar_calculation.classList.add("active");
});

form_6_back_btn_calculation.addEventListener("click", function(){
	form_5_calculation.style.display = "block";
	form_6_calculation.style.display = "none";

	form_6_btns_calculation.style.display = "none";
	form_5_btns_calculation.style.display = "flex";

	form_6_progessbar_calculation.classList.remove("active");
});


form_6_next_btn_calculation.addEventListener("click", function(){
	form_6_calculation.style.display = "none";
	form_7_calculation.style.display = "block";

	form_7_btns_calculation.style.display = "flex";
	form_6_btns_calculation.style.display = "none";

	form_7_progessbar_calculation.classList.add("active");
});

form_6_skip_btn_calculation.addEventListener("click", function(){
	form_6_calculation.style.display = "none";
	form_7_calculation.style.display = "block";

	form_7_btns_calculation.style.display = "flex";
	form_6_btns_calculation.style.display = "none";

	form_7_progessbar_calculation.classList.add("active");
});


form_7_back_btn_calculation.addEventListener("click", function(){
	form_6_calculation.style.display = "block";
	form_7_calculation.style.display = "none";

	form_7_btns_calculation.style.display = "none";
	form_6_btns_calculation.style.display = "flex";

	form_7_progessbar_calculation.classList.remove("active");
});



$("#addChild").click(function(){
    $(".childInfomore").append(
		"<div class='childInfo'><h5><b>Child 1</b><a class='childremove'>x</a></h5><div class='uk-grid-medium' uk-grid ><div class='uk-width-1-3@m  uk-grid-margin uk-first-column'><label class='uk-form-label'>What is their current age?</label><select class='uk-select'><option>City</option><option>Karachi</option><option>Lahore</option></select></div><div class='uk-width-1-3@m  uk-grid-margin uk-first-column'><label class='uk-form-label'>What type of school would you like to plan for?</label><select class='uk-select'><option>School Type</option><option>School Type</option><option>School Type</option></select></div></div></div>");
  });

  
$('.childremove').click(function() {
    $('.childInfo').remove();
    	return false;
	
	});

$('.calculationDone').click(function() {
$('#modal-calculation-result').removeClass('uk-open');

});

// form_7_next_btn_calculation.addEventListener("click", function(){
// 	form_7_calculation.style.display = "none";
// 	form_8_calculation.style.display = "block";

// 	form_8_btns_calculation.style.display = "flex";
// 	form_7_btns_calculation.style.display = "none";

// 	form_8_progessbar_calculation.classList.add("active");
// });
</script>
<!-- calculation modal end -->

<?php include('footer.php'); ?>