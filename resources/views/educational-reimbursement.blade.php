@if(isset($pagesdata))
    @include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

    @foreach($pagesdata as $key => $pagedata)
        <!-- Banner Start -->
        @php 
            $postImg =  getImageFile($device_type,$pagedata,"image","inner");
        @endphp
        <section class="HeaderInnerPage">
            <img src="{{ $postImg }}" />
            @include('partials.breadcrumb')
            <div class="HeaderInnerTxt {{ $pagedata->grid_class_en}}">
                <div class="uk-container containCustom">
                    <h1>{{ $pagedata->title_en}}</h1>
                    {!! $pagedata->description_en !!}
                </div>
            </div>
        </section>
    @endforeach
    <section class="SecWrap SecTopSpace">
        <div class="uk-container containCustom">
            <div class="NewsFilter">
                <form>
                    <div class="uk-form-controls GuideSearchInput">
                        <input class="uk-input" id="email" type="text" placeholder="Enter keyword">
                    </div>
                    <div class="uk-form-controls">
                    <select class="uk-select">
                        <option value="">Select</option>
                    </select>
                    </div>
                    <div class="uk-form-controls">
                    <select class="uk-select">
                        <option value="">Select</option>
                    </select>
                    </div>
                    <div class="uk-form-controls">
                    <select class="uk-select">
                        @foreach($innerPages as $innerkey => $innerPage)
                            <option value="{{ substr($innerPage->slug,1) }}">{{ $innerPage->title_en }}</option>
                        @endforeach
                    </select>
                    </div>
                    <button class="blueBtn">Filter</button>
                </form>
            </div>
            <div class="education">
                <div class="innerPageContent2">
                    @foreach($postsdata as $key => $postdata)
                        @if($key == 0)
                            <h2 class="uk-margin-remove-bottom">{{ $postdata->title_en }}</h2>
                            {!! $postdata->description_en !!}
                        @endif
                    @endforeach
                    @foreach($innerPages as $innerPagekey => $innerPage)
                        @if($innerPagekey == 0)
                            @foreach($innerPage->child as $innerchildkey => $child)
                                @if($innerchildkey == 0)
                                    @foreach($child->article as $childkey => $innerChild)
                                        <p>{{ $innerChild->title_en }}</p>
                                        @foreach($innerChild->custompost as $customkey => $custompost)
                                            @if($customkey == 0)
                                                <ul class="uk-grid-medium" uk-grid uk-height-match=".uk-card-body">
                                            @endif
                                            <li class="uk-width-1-2@s uk-width-1-4@m">
                                                <div class="educationItem">
                                                    <h6>{{ $custompost->name }}</h6>
                                                    {!! $custompost->description !!}
                                                </div>
                                            </li>
                                        @endforeach
                                        @if(count($innerChild->custompost) > 0)
                                            </ul>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </div>
            </div>    
        </div>
    </section>

    <section class="SecWrap SecTopSpace WhiteBgSection">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>Payment Mechanism</h2>
                <p>The reimbursement amount will be provided to the customer as per the below mentioned breakup depending on fulfillment of the required terms & conditions by the client.</p>
                <ul>
                    <li>In case of new policy or policy active for year</li>
                    <li>In case of policy valid for 2 years or more</li>
                </ul>
                <h3>In case of new policy</h3>
                <div class="orbisContent">
                    <table class="uk-table uk-table-divider">
                        <thead>
                            <tr>
                                <th>Policy tenure</th>
                                <th>Installment</th>
                                <th>Reimbursement Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>New policy (Upfront payment)</td>
                                <td>1st installment</td>
                                <td>Up to 25% of admission fee</td>
                            </tr>
                            <tr>
                                <td>Upon completion of the first policy year*</td>
                                <td>2nd installment</td>
                                <td>Up to 25% of admission fee</td>
                            </tr>
                            <tr>
                                <td>Upon completion of the second policy year*</td>
                                <td>3rd installment</td>
                                <td>Up to 50% of admission fee</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p>* After next year’s premium receipt confirmation</p>

                <h3>In case of policy valid for 2 years or more</h3>
                <div class="orbisContent">
                    <table class="uk-table uk-table-divider">
                        <thead>
                            <tr>
                                <th>Policy tenure</th>
                                <th>Reimbursement percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>More than 2 years</td>
                                <td>Full payment</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p>* After next year’s premium receipt confirmation</p>
            </div>
        </div>
    </section>

    <section class="SecWrap SecTopSpace ">
        <div class="uk-container containCustom">
            <div class="innerPageContent2">
                <h2>Terms & Conditions</h2>
                <ul>
                    <li>This policy is applicable on policies with minimum annual regular premium of PKR 200,000 only. Ad-hoc premium and/or any other premium payment mode, other than annualized premium will not be considered for this policy.</li>
                    <li>Reimbursement shall be made against admission fee upto a maximum amount of PKR 50,000 per child in total.</li>
                    <li>Admission fee or any onetime fee paid at the time of admission only shall be reimbursed. Semester fee or tuition fee is not covered under this policy.</li>
                    <li>Educational institutes named in the above mentioned list will be covered under this policy. No payment shall be made in case of educational institutes that are not included in our list.</li>
                    <li>Eligibility criteria and list of institutes may change without prior notice to consumers.</li>
                    <li>Reimbursement will be paid after completion of free look up period.</li>
                    <li>Fees will be reimbursed in those admissions only that happen after the policy issuance date.</li>
                    <li>If the installment amount exceeds total reimbursement limit at any stage, Adamjee Life will not pay the amount exceeding PKR 50,000 in total.</li>
                    <li>Following documents need to be presented to Adamjee Life for Education policy refund claim:
                        <ul>
                            <li> Cnic of parent </li>
                            <li>  B-Form/ CNIC of student </li>
                            <li>  Fee payment invoice </li>
                            <li>  Admission Letter </li>
                            <li>  Orbis Card</li>
                        </ul>
                    </li>
                    <li>Documents can be sent through email i.e. scanned copies or through post at Adamjee Life Head office.</li>
                    <li>All such claims will be directed to customer service team and customer service team may call the customer for subsequent processes.</li>
                </ul>
            </div>
        </div>
    </section>

    @include('partials.footer')
@endif