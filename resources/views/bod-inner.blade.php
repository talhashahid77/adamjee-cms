{{-- @dd($pagesdata); --}}
{{-- @dd($postsdesc); --}}

@if(isset($pagesdata))
@include('partials.header', ['pagesdata' => $pagesdata,'menu_items' => $menu_items,'url_type' => $url_type])

<!-- Section Start -->
@foreach($pagesdata as $key => $pagedata)
    <!-- Banner Start -->
    @php 
        $postImg =  getImageFile($device_type,$pagedata,"image","banner");
    @endphp
	<section class="HeaderInnerPage">
		<img src="{{ $postImg }}" />
		@include('partials.breadcrumb')
		
	</section>
@endforeach

<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap ovverflowAuto">
	<div class="uk-container containCustom">
		<div class="detailsPage">

			@foreach($pagesdata as $pagesdataa)
			<h1>{{$pagesdataa->title_en}}</h1>
			<h6>{{$pagesdataa->sub_title_en}}</h6>	
			 {!! $pagesdataa->short_desc_en !!}
			{{-- <p>Mr. Mushtaq has held many leadership roles over the span of 19 years of his career. From 2011 to 2013, he served as the Managing Director/CEO of Adamjee Insurance Company. He then moved onto hold the position of Chief Executive Officer at Habib Metropolitan Financial Services Ltd in 2013 and served 8 years in the same company. Previously, he was also associated with Security General Insurance Company.</p> --}}
			@endforeach
			

			@if(!empty($postsdata))
			@foreach($postsdata as $postdata)
			<div class="uk-panel">
				 <!-- Banner Start -->
                @php 
                    $postImg =  getImageFile($device_type,$postdata,"image","banner");
                @endphp
			    <img class="uk-align-left uk-margin-remove-adjacent" src="{{$postImg}}" width="360" alt="Example image">
				{!! $postdata->description_en !!}
				{{-- <h3>Message from our CEO</h3>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.</p> --}}
			</div>
			@endforeach
			@endif
		</div>
	</div>
</section>
<!-- Section End -->

@include('partials.footer')
@endif