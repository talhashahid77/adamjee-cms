<?php include('header.php'); ?>
<!-- Section Start -->
<section class="HeaderInnerPage">
	<img src="images/claim/banner.jpg" />
	<div class="breadcrumb">
		<ul class="uk-breadcrumb">
		    <li><a href="javascript:;">Help</a></li>
		    <li><span>Claims</span></li>
		</ul>
	</div>
	<div class="HeaderInnerTxt">
		<div class="uk-container containCustom">
			<h1>Claims</h1>
			<!-- <p>Lorem ipsum dolor sit amet, consetetur</p> -->
		</div>
	</div>
</section>
<!-- Section End -->
<!-- Section Start -->
<section class="SecWrap SecTopSpace">
	<div class="uk-container containCustom">
        <div class="uk-grid uk-grid-small" uk-grid>
            <div class="uk-width-1-2@m">
                <div class="contactRightContent JobForm NewsFilter claimForm">
                    <h3>Process your claim online</h3>
                    <form class="uk-grid-small" uk-grid>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <div class="NewsFilter1">
                            <label class="uk-form-label">Claim type</label>
                                <select class="uk-select">
                                    <option>Banca</option>
                                    <option>Banca</option>
                                </select>
                            </div>
                        </div>				    
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Your full name</label>
                            <input class="uk-input" type="text" placeholder="Enter your full name" required="">
                        </div>				    
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Policy Number</label>
                            <input class="uk-input" type="number" placeholder="Your policy number" required="">
                        </div>
                       
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">CNIC Number</label>
                            <input class="uk-input" type="number" placeholder="Enter your CNIC Number" required="">
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Your Contact number</label>
                            <input class="uk-input" type="number" placeholder="+92-xx-xxxxxxxx">
                        </div>

                        <div class="uk-width-1-2@s uk-width-1-2@m">
                            <label class="uk-form-label">Your email</label>
                            <input class="uk-input" type="email" placeholder="yourname@example.com">
                        </div>
                       
                        
                        <div class="uk-width-1-2@s uk-width-1-1@m">
                            <label class="uk-form-label">Description</label>
                            <textarea  rows="7" class="uk-textarea">Give a brief description</textarea>
                        </div>

                        <div class="uk-width-1-2@s uk-width-1-1@m">
                            <div class="uk-upload-box">
                                <div id="error-alert" class="uk-alert-danger uk-margin-top uk-hidden" uk-alert>
                                    <p id="error-messages"></p>
                                </div>
                                <div class="drop__zone custom uk-placeholder uk-text-center">
                                    <span uk-icon="icon: cloud-upload"></span>
                                    <span class="uk-text-middle uk-margin-small-left">Drop file here or</span>
                                    <br/>
                                    <div class="selectForm" uk-form-custom c>
                                        <input name="file[]" type="file" accept="image/png, image/jpeg, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple>
                                        <span class="uk-link">Select Files</span>
                                    </div>
                                    <ul id="preview" class="uk-list uk-grid-match uk-child-width-1-5@m uk-text-center uk-grid-small" uk-grid uk-scrollspy="cls: uk-animation-scale-up; target: .list-item; delay: 80"></ul>
                                </div>
                            </div>
                            <span>Supported file formats: PDF, Doc, Docx, Jpg</span>
                        </div>    

                        <div class="uk-width-1-1">
                            <button class="blueBtn uk-margin-remove-top" uk-toggle="#claim-process">Submit<img src="images/right.svg" uk-svg /></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="uk-width-1-2@m">
            <div class="detailsPage">
			<h1>About AdamjeeLife</h1>
			
			<div class="claimFaqs">
            <ul uk-accordion class="uk-accordion">
                <li >
                    <a class="uk-accordion-title" href="#">Claim process</a>
                    <div class="uk-accordion-content" aria-hidden="false">
                        <p>We are here for you! Adamjee life offers its condolences and therefore offers an easy process to file a claim.</p>
                    </div>
                </li>
                <li class="uk-open">
                    <a class="uk-accordion-title" href="#">How to Claim</a>
                    <div class="uk-accordion-content" hidden="" aria-hidden="true">
                        <p>In the event of a claim, contact Adamjee Life Assurance Co. Ltd to obtain a claim form. The claim forms are also available on our official website. www.adamjeelife.com.</p>
                        <p> You will also be advised on all additional documentation that is required for the claim to be processed.</p>
                        <h6>Step 01</h6>
                        <p>Death intimation, on death of any covered employee, the claimant or company sends the Group Claim Intimation form to the claims department.</p>
                        <h6>Step 02</h6>
                        <p>On receiving the death intimation, the claims department would sent the claim forms 'Claimant Statement' & 'Last attending physician' from the claim department.</p>
                        <h6>Step 03</h6>
                        <p>The Company / Claimant is required to fill in the claim forms 'Claimant Statement' & 'Last attending physician', properly sign and stamp and send them back to the concerned department (Group Life), along with the following requirements.</p>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">For Death claims:</a>
                    <div class="uk-accordion-content" hidden="" aria-hidden="true">
                        <ul>
                            <li>Claim Forms (Death Claim Form - Physician Statement)</li>
                            <li>Copy Of CNIC</li>
                            <li>Death certificates (NADRA / Union Council Death Certificate)</li>
                            <li>Copy Of Salary Slip</li>
                            <li>Hospital Death Certificate</li>
                            <li>Police Report, Post Mortem Report, Rescue Report 1122 and Newspaper cutting (In case of accidental death).</li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">For disability claims:</a>
                    <div class="uk-accordion-content" hidden="" aria-hidden="true">
                        <p>Note: The claim department have reserves the right to call for further requirements if deemed necessary.</p>
                        <p>Normally when all the requirements have been submitted, it takes 5-7 working days for the decision of the claim. In case where verification is needed this time could be extended. However, in such cases the claimant is notified about it.</p> 
                        <p>The claim proceeds will be paid to the Claimant or the name of company.</p> 
                        <p>You can know the status of your claim or clarify any of your queries by contact us on 021-111-115-433 or email us at 
                            <br/>claims-dep@adamjeelife.com 
                            <br/>help_claims@adamjeelife.com
                        </p>
                    </div>
                </li>
            </ul>
            </div>
			
		</div>
                <!-- <div class="contactLeft">
                    <div class="contactLeftContent">
                        <div class="cbox">
                            <h4> <b>How to process a claim</b></h4>
                        </div>
                    </div>
                </div> -->
            </div>
            
        </div>
    </div>
</section>
<!-- Section End -->


<section class="SecWrap SecTopSpace uk-padding-remove-top">
	<div class="uk-container containCustom">
        <div class="uk-grid uk-grid-small" uk-grid>
            <div class="uk-width-1-1@m">
                
                <div class="contactRightContent JobForm NewsFilter claimForm">
                    <div class="cbox">
                        <h2><b>Contact us</b></h2>
                        <h4> <b>Telephone</b></h4>
                        <div class="addressicons">
                            <p><img src="images/icons/phone.svg" uk-svg /><a href="javascript:;">021-38677100 Ext 344 - 251</a></p>
                        </div>
                        <h4> <b>Email</b></h4>
                        <div class="addressicons">
                            <span><a href="mailto:claims-dep@adamjeelife.com">claims-dep@adamjeelife.com</a></span> <br/>
                            <span><a href="mailto:help_claims@adamjeelife.com">help_claims@adamjeelife.com</a></span>
                        </div>

                    </div>

                    <div class="cbox uk-margin-medium-top">
                        <h4> <b>Hotline Number</b></h4>
                        <div class="addressicons">
                            <p><img src="images/contact/whatsapp.svg" uk-svg /><a href="tel:+92-346-8209366">+92-346-8209366</a></p>
                        </div>
                        <h4> <b>Address</b></h4>
                        <div class="addressicons">
                            <span>Adamjee Life Assurance Co. Ltd. 3rd and 4th Floor, Adamjee House, I.I. Chundrigar Road, Karachi - 74000</span>
                        </div>
                    </div>
                </div>
            </div>

          
        </div>
    </div>
</section>
<!-- Section End -->

<!-- thank you  start -->
<div id="claim-process" class="uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

        <button class="uk-modal-close-default" type="button" uk-close></button>
		
		<h4>Your claim has been processed</h4>
        <p>Note: The claim department have reserves the right to call for further requirements if deemed necessary.</p>
        <br/>
        <p>Your tracking number is: <a href="#"> HJSDGJHDSKJDH </a></p>
    </div>
</div>
<!-- thank you end -->


<script>
    const UPLOAD = {
    BASE_PATH: window.location.origin,
    ACCEPTED_DOC_MIMES: [
        "image/png",
        "image/jpeg",
        "application/pdf",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/msword"
    ],
    DOC_MIMES: [
        "application/pdf",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/msword"
    ],
    FileInputs: [...document.querySelectorAll("input[type='file']")],
    Init: () => {
        UPLOAD.AddEventListeners();
    },
    AddEventListeners: () => {
        UPLOAD.FileInputs.map((fileInput) => {
            fileInput.addEventListener("change", UPLOAD.HandleFileChange);
        });
        UPLOAD.FileInputs.map((fileInput) => {
            fileInput.addEventListener("blur", (e) => {
                // console.log(e.target);
            });
        });
        UPLOAD.FileInputs.map((fileInput) => {
            const box = fileInput.closest(".uk-placeholder");
            box.addEventListener(
                "dragover",
                (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    box.classList.add("uk-box-shadow-medium");
                },
                false
            );
        });
        UPLOAD.FileInputs.map((fileInput) => {
            const box = fileInput.closest(".uk-placeholder");
            box.addEventListener(
                "dragleave",
                (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    box.classList.remove("uk-box-shadow-medium");
                },
                false
            );
        });
        UPLOAD.FileInputs.map((fileInput) => {
            const box = fileInput.closest(".uk-placeholder");
            box.addEventListener(
                "drop",
                (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    box.classList.remove("uk-box-shadow-medium");
                    const dropZone = e.target.closest(".drop__zone");
                    const componentContainer = dropZone.closest(".uk-upload-box");
                    let preview, alertBox, alertMsg;
                    preview = dropZone.querySelector(`#preview`);
                    alertBox = componentContainer.querySelector(`#error-alert`);
                    alertMsg = componentContainer.querySelector(`#error-messages`);
                    let files;
                    let isMultiple = false;
                    if (fileInput.hasAttribute("multiple")) {
                        isMultiple = true;
                    }
                    if (!e.dataTransfer.files.length) return;
                    /** If items dropped are more than one file and the input doesn't accept multiple, do not accept the files. Clear preview field, show notification and add shake animatin to dropzone */
                    if (isMultiple === false && e.dataTransfer.files.length > 1) {
                        UPLOAD.RemoveChild(preview);
                        UPLOAD.AddShakeAnimation(dropZone);
                        UIkit.notification({
                            message: "Cannot accept multiple files when expecting a single file!",
                            status: "danger",
                            pos: "center",
                            timeout: 4000
                        });
                        return;
                    }
                    files = e.dataTransfer.files;
                    fileInput.files = files;
                    const options = {
                        isMultiple,
                        files,
                        preview,
                        alertBox,
                        alertMsg,
                        fileInput,
                        dropZone
                    };
                    if (isMultiple) {
                        UPLOAD.PreviewMultipleDocs(options);
                    } else {
                        UPLOAD.PreviewSingleDoc(options);
                    }
                },
                false
            );
        });
    },
    Reset: (e) => {
        if (e.target.files.length == 0) {
            const alertWrapper = e.target.closest("#error-alert");
            let alertMessage = alertWrapper.querySelector("#error-messages");
            alertWrapper.classList.add("uk-hidden");
            alertMessage.textContent = "";
            input.value = "";
            return;
        }
    },
    AddShakeAnimation: (parent) => {
        let timeout;
        clearTimeout(timeout);
        parent.classList.add("uk-animation", "uk-animation-shake");
        timeout = setTimeout(() => {
            parent.classList.remove("uk-animation", "uk-animation-shake");
        }, 1000);
    },
    HandleFileChange: (e) => {
        let files;
        let isMultiple = false;
        if (e.target.hasAttribute("multiple")) {
            isMultiple = true;
        }
        const dropZone = e.target.closest(".drop__zone");
        const componentContainer = dropZone.closest(".uk-upload-box");
        const documentCategory = dropZone.dataset.documentCategory;
        let preview, alertBox, alertMsg;
        preview = dropZone.querySelector(`#preview`);
        alertBox = componentContainer.querySelector(`#error-alert`);
        alertMsg = componentContainer.querySelector(`#error-messages`);
        /** Remove existing files and preview files if files lenght is 0 */
        if (!e.target.files.length) {
            UPLOAD.RemoveChild(preview);
            return;
        }
        files = e.target.files;
        const options = {
            files,
            fileInput: e.target,
            isMultiple,
            preview,
            alertBox,
            alertMsg,
            dropZone
        };
        if (isMultiple) {
            UPLOAD.PreviewMultipleDocs(options);
        } else {
            UPLOAD.PreviewSingleDoc(options);
        }
    },
    RemoveChild: (preview) => {
        while (preview.firstChild) {
            preview.removeChild(preview.firstChild);
        }
    },
    ImgPreviewLi: (readerResult, filename) => {
        const li = document.createElement("li");
        const div = document.createElement("div");
        const img = document.createElement("img");
        const span = document.createElement("span");
        li.className = "list-item uk-margin-medium-top";
        // div.className = "uk-cover-container";
        // img.setAttribute("id", "img-preview-responsive");
        // img.setAttribute("src", "images/icons/download.svg");
        // img.setAttribute("data-name", filename);
        // img.setAttribute("alt", "file-image-preview");
        span.className = "uk-text-meta uk-text-break file-upload-name";
        span.textContent = filename;
        // div.append(img);
        li.append(div, span);
        return li;
    },
    ValidateFileType: ({
        fileType,
        fileInput,
        alertBox,
        alertMsg,
        preview,
        dropZone
    }) => {
        if (!UPLOAD.ACCEPTED_DOC_MIMES.includes(fileType)) {
            alertMsg.textContent =
                "Sorry, one or more of your file type is not allowed.";
            alertMsg.classList.add("uk-text-danger");
            alertBox.classList.remove("uk-hidden");
            fileInput.value = "";
            UPLOAD.AddShakeAnimation(dropZone);
            UPLOAD.RemoveChild(preview);
            return false;
        }
        return true;
    },
    ValidateFileSize: ({
        fileInput,
        size,
        alertBox,
        alertMsg,
        preview,
        dropZone
    }) => {
        if (size > 2000000) {
            alertMsg.textContent =
                "Sorry, one or more of your files has exceeded the file size limit of 2MB.";
            alertMsg.classList.add("uk-text-danger");
            alertBox.classList.remove("uk-hidden");
            fileInput.value = "";
            UPLOAD.AddShakeAnimation(dropZone);
            UPLOAD.RemoveChild(preview);
            return false;
        }
        return true;
    },
    PreviewMultipleDocs: ({
        files,
        fileInput,
        preview,
        alertBox,
        alertMsg,
        dropZone
    }) => {
        const docFiles = [...files];
        docFiles.forEach((file) => {
            const size = file["size"];
            const fileType = file["type"];
            if (docFiles.length !== 0) {
                UPLOAD.RemoveChild(preview);
            }
            const fileOptions = {
                fileInput,
                preview,
                alertBox,
                alertMsg,
                fileType,
                size,
                dropZone
            };
            if (!UPLOAD.ValidateFileSize(fileOptions)) return;
            if (!UPLOAD.ValidateFileType(fileOptions)) return;
            alertMsg.textContent = "";
            alertBox.classList.add("uk-hidden");
            fileInput.files = files;
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                let filename = file["name"];
                let imgPreview = "";
                if (UPLOAD.DOC_MIMES.includes(fileType)) {
                    if (fileType === "application/pdf") {
                        imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
                        imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                    } else {
                        imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
                        imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                    }
                } else {
                    imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
                }
                preview.append(imgPreview);
            };
        });
    },
    PreviewSingleDoc: ({
        files,
        fileInput,
        preview,
        alertBox,
        alertMsg,
        dropZone
    }) => {
        const size = files[0]["size"];
        const fileType = files[0]["type"];
        let filename = files[0]["name"];
        if (files[0].length !== 0) {
            UPLOAD.RemoveChild(preview);
        }
        const fileOptions = {
            fileInput,
            preview,
            alertBox,
            alertMsg,
            fileType,
            size,
            dropZone
        };
        if (!UPLOAD.ValidateFileSize(fileOptions)) return;
        if (!UPLOAD.ValidateFileType(fileOptions)) return;
        alertMsg.textContent = "";
        alertBox.classList.add("uk-hidden");
        const reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload = () => {
            let imgPreview = "";
            let imagePath = "";
            if (UPLOAD.DOC_MIMES.includes(fileType)) {
                if (fileType === "application/pdf") {
                    imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/pdf.svg`;
                    imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                } else {
                    imagePath = `${UPLOAD.BASE_PATH}/assets/images/doc/docx.svg`;
                    imgPreview = UPLOAD.ImgPreviewLi(imagePath, filename);
                }
            } else {
                imgPreview = UPLOAD.ImgPreviewLi(reader.result, filename);
            }
            preview.append(imgPreview);
        };
    }
};
document.addEventListener("DOMContentLoaded", UPLOAD.Init());
</script>
<?php include('footer.php'); ?>