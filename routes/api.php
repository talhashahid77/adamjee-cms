<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/listbread', [App\Http\Controllers\ApiController::class, 'listBreadcrump'])->name('listBreadcrump');
Route::post('/planFetch', [App\Http\Controllers\ApiController::class, 'listPlans'])->name('listPlans');
Route::get('/sliderInfo', [App\Http\Controllers\ApiController::class, 'sliderInfo'])->name('sliderInfo');
Route::post('/plan', [App\Http\Controllers\ApiController::class, 'plan'])->name('plan');
Route::post('/open_vacancy', [App\Http\Controllers\ApiController::class, 'openVacancy'])->name('openVacancy');
Route::get('/listAnnouncement', [App\Http\Controllers\ApiController::class, 'listAnnouncement'])->name('listAnnouncement');
Route::post('/fund_list', [App\Http\Controllers\ApiController::class, 'fundList'])->name('fundList');
