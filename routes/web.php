<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Admin Panel Links***********************************************************************

// General Pages
use App\Http\Controllers\Admin\General\PageController;
use App\Http\Controllers\Admin\General\PageDetailController;
use App\Http\Controllers\Admin\General\ViewController;
use App\Http\Controllers\Admin\General\UserController;
use App\Http\Controllers\Admin\General\ArticleController;
use App\Http\Controllers\Admin\General\PostController;
use App\Http\Controllers\Admin\General\MenuController;
use App\Http\Controllers\Admin\General\MenuItemController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\TestController;


// Dynamic Pages
use App\Http\Controllers\MultiController;
use App\Http\Controllers\WebController;

// Route::group(['middleware'=>['ipcheck']], function() {
    Auth::routes();
    //Admin Panel Routes
    Route::get('/dashboard', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('dashboard');
    
    // General Pages
    Route::post('/pageinc/{id}', [App\Http\Controllers\Admin\General\PageController::class, 'incCount']);
    Route::post('/pagedesc/{id}', [App\Http\Controllers\Admin\General\PageController::class, 'descCount']);
    Route::post('/articleinc/{id}', [App\Http\Controllers\Admin\General\ArticleController::class, 'incCount']);
    Route::post('/articledesc/{id}', [App\Http\Controllers\Admin\General\ArticleController::class, 'descCount']);
    Route::post('/menuIteminc/{id}', [App\Http\Controllers\Admin\General\MenuItemController::class, 'incCount']);
    Route::post('/menuItemdesc/{id}', [App\Http\Controllers\Admin\General\MenuItemController::class, 'descCount']);
    Route::get('/article-list/{id}', [App\Http\Controllers\Admin\General\ArticleController::class, 'index'])->name('article');
    Route::post('/add-custom-post', [App\Http\Controllers\Admin\General\CustomPostController::class, 'addCustomPost']);
    Route::post('/list-custom-post', [App\Http\Controllers\Admin\General\CustomPostController::class, 'listCustomPost']);
    Route::post('/delete-custom-post/{id}', [App\Http\Controllers\Admin\General\CustomPostController::class, 'deleteCustomPost']);
    Route::post('/get-custom-post/{id}', [App\Http\Controllers\Admin\General\CustomPostController::class, 'getCustomPost']); 
    Route::post('/update-custom-post/{id}', [App\Http\Controllers\Admin\General\CustomPostController::class, 'updateCustomPost']); 
    Route::resource('page', PageController::class);
    // Route::resource('pagedetail', PageDetailController::class);
    Route::resource('view', ViewController::class);
    Route::resource('user', UserController::class);
    Route::resource('menu', MenuController::class);
    Route::resource('menuitem', MenuItemController::class);
    Route::resource('article', ArticleController::class);

    Route::resource('test', TestController::class);
    
    // End Admin Panel Links ***********************************************************************    
// });

Route::get('/en/{slug}' , [MultiController::class , 'enShow'])->where('slug', '.*');
Route::get('/ur/{slug}' , [MultiController::class , 'urShow'])->where('slug', '.*');

Route::get('/', [WebController::class , 'index']);
Route::post('/contact', [ContactController::class , 'contact'])->name('contact');
Route::post('/contact-claim', [ContactController::class , 'contactClaim'])->name('contact-claim');

 // Website route start  *******************   
//  Route::get('/', [WebController::class, 'index'])->name('/');
//  Route::get('about-us', [WebController::class, 'aboutUs'])->name('about-us');
//  Route::get('bod-and-management', [WebController::class, 'bodandManagement'])->name('bod-and-management');

