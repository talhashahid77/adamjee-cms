<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('plan_type');
            $table->dateTime('plan_Date');
            $table->string('premium_mode');
            $table->integer('amount');
            $table->string('name');
            $table->dateTime('date_of_birth');
            $table->enum('gender',['male','female']);
            $table->string('mobile_no');
            $table->string('city');
            $table->string('plan_sub_type');
            $table->string('plan_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
