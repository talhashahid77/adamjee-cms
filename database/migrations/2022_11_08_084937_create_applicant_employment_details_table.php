<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantEmploymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_employment_details', function (Blueprint $table) {
            $table->id();
            $table->string('applicant_id');
            $table->string('current_employer_name');
            $table->string('designation');
            $table->string('functional_area');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->string('in_progress');
            $table->string('gross_salary');
            $table->string('benefits_details');
            $table->string('expected_salary');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_employment_details');
    }
}
